<?php
/**
 * Expected
 * @var Podcast[] $podcasts
 * @var array $options
 * @var array $class_modifiers
 * @var array $element_attributes
 */

use Backstage\Util;
use ChildTheme\Components\PodcastCard\PodcastCardView;
use ChildTheme\Podcast\Podcast;

if (empty($podcasts)) {
    return '';
}
?>

<div <?= Util::componentAttributes('podcast-list', $class_modifiers, $element_attributes); ?>>
    <div class="podcast-list__row content-row row">
        <?php foreach($podcasts as $Podcast): ?>
            <div class="podcast-list__column content-column col-md-3">
                <?= (new PodcastCardView($Podcast))->classModifiers($options); ?>
            </div>
        <?php endforeach; ?>
    </div>
</div>
