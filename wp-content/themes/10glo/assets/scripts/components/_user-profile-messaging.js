addAction(INIT, function () {
    var $messaging = $('.messaging');
    if (!$messaging.length) {
        return;
    }
    var $messagingSection = $messaging.parents('.messaging__section');
    var $closeButton = $messaging.find('.messaging__close');

    $closeButton.click(function () {
        $messagingSection.removeClass('messaging__section--show');
    });
});
