addAction(INIT, function () {
    var $checkoutButton = $('.video-watch__checkout-button');
    if (!$checkoutButton.length) {
        return;
    }
    var stripe = new Stripe(sit.publishable_key);
    $checkoutButton.on('click', function () {
        stripe.redirectToCheckout({
            sessionId: $(this).attr('data-session-id'),
        }).then(function (result) {
            console.log(result);
        });
    });
});
