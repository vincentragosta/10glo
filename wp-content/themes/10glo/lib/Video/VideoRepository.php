<?php

namespace ChildTheme\Video;

use Backstage\Producers\RelatedContent\RelatedContentRepository;
use ChildTheme\Playlist\Playlist;
use ChildTheme\Service\PremiumVideoService;
use ChildTheme\User\User;

/**
 * Class VideoRepository
 * @package ChildTheme\Video
 * @author Vincent Ragosta <vincentpasqualeragosta@gmail.com>
 * @version 1.0
 */
class VideoRepository extends RelatedContentRepository
{
    const DEFAULT_PAGE = 1;
    const META_CRITERIA = ['views', 'most-viewed', 'likes_count', 'top-rated'];

    protected $model_class = Video::class;
    protected $related_model_class = Video::class;

    public function findAllByAuthor($author_id)
    {
        if (is_object($Author = $author_id)) {
            $author_id = $Author->ID;
        }
        return $this->find([
            'post_status' => ['publish', 'private'],
            'author' => $author_id
        ]);
    }

    public function findPublishedByAuthor($author_id)
    {
        return parent::findAllByAuthor($author_id);
    }

    public function findRecent(array $atts)
    {
        return $this->find([
            'paged' => $atts['paged'] ?: self::DEFAULT_PAGE,
            'posts_per_page' => $atts['posts_per_page'] ?: get_option('posts_per_page'),
            'post__not_in' => strpos($atts['post__not_in'], ',') ? explode(',', $atts['post__not_in']) : $atts['post__not_in']
        ]);
    }

    public function findOneBySlug($name)
    {
        $posts = $this->find([
            'post_status' => ['publish', 'private'],
            'name' => $name,
            'posts_per_page' => 1
        ]);
        return $posts[0] ?? null;
    }

    public function findPremiumVideosFromUser(User $User)
    {
        if (empty($premium_video_ids = (new PremiumVideoService())->getUserPremiumVideos($User))) {
            return [];
        }
        return $this->find([
            'post_status' => ['publish', 'private'],
            'posts_per_page' => -1,
            'post__in' => $premium_video_ids,
            'orderby' => 'post__in'
        ]);
    }

    public function findCurated(array $atts)
    {
        $args = [
            'post_status' => ['publish'],
            'paged' => $atts['paged'] ?: self::DEFAULT_PAGE,
            'posts_per_page' => $atts['posts_per_page'] ?: get_option('posts_per_page'),
            'post__not_in' => strpos($atts['post__not_in'], ',') ? explode(',', $atts['post__not_in']) : $atts['post__not_in']
        ];
        if (!empty($category_id = $atts['cat_id'])) {
            $args['cat'] = $category_id;
        }
        if (!empty($term_ids = $atts['term_ids'])) {
            $args['tax_query'] = [
                [
                    'taxonomy' => Video::HASHTAG_TAXONOMY,
                    'terms' => $term_ids,
                    'field' => 'term_id',
                    'compare' => 'IN'
                ]
            ];
        }
        if (!empty($criteria = $atts['criteria']) && in_array($criteria, static::META_CRITERIA)) {
            if ($criteria == 'most-viewed') {
                $criteria = 'views';
            } else if ($criteria == 'top-rated') {
                $criteria = 'likes_count';
            }
            $args['meta_query'] = [
                [
                    'key' => $criteria,
                    'compare' => 'EXISTS'
                ]
            ];
            $args['orderby'] = 'meta_value_num';
        }
        if (!empty($date_after = $atts['date_after'])) {
            $args['date_query'] = [
                'after' => '-' . $date_after . ' days'
            ];
        }
        return $this->find($args) ?: [];
    }

    public function findFromPlaylist(Playlist $Playlist, array $atts = [])
    {
        if (empty($Playlist->video_ids)) {
            return [];
        }
        $args = [
            'paged' => $atts['paged'] ?: self::DEFAULT_PAGE,
            'posts_per_page' => !empty($atts['posts_per_page']) ? $atts['posts_per_page'] : get_option('posts_per_page'),
            'post__in' => $Playlist->video_ids
        ];
        if (!empty($criteria = $atts['criteria']) && in_array($atts['criteria'], static::META_CRITERIA)) {
            if ($criteria == 'most-viewed') {
                $criteria = 'views';
            } else if ($criteria == 'top-rated') {
                $criteria = 'likes_count';
            }
            $args = array_merge($args, [
                'meta_query' => [
                    [
                        'key' => $criteria,
                        'compare' => 'EXISTS'
                    ]
                ],
                'orderby' => 'meta_value_num'
            ]);
        }
        return $this->find($args) ?: [];
    }

    protected function getAssignedField()
    {
        return 'playlist_ids';
    }
}
