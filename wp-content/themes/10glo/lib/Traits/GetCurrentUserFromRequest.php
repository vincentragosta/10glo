<?php

namespace ChildTheme\Traits;

use ChildTheme\Exception\UserException;
use ChildTheme\User\User;
use \WP_REST_Request;

/**
 * Trait GetCurrentUserFromRequest
 * @package ChildTheme\Traits
 * @author Vincent Ragosta <vincentpasqualeragosta@gmail.com
 * @version 1.0
 */
trait GetCurrentUserFromRequest
{
    protected function getCurrentUserFromRequest(WP_REST_Request $request, string $message = '')
    {
        if (empty($current_user_id = $request->get_param('userId'))) {
            throw new UserException($message, 400);
        }
        return new User($current_user_id);
    }
}
