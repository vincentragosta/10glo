<?php

namespace ChildTheme\Components\PodcastList;

use Backstage\View\Component;
use Backstage\View\Element;
use Backstage\View\Link;
use ChildTheme\Podcast\Podcast;

/**
 * Class PodcastListView
 * @package ChildTheme\Components\PodcastList
 * @author Vincent Ragosta <vincentpasqualeragosta@gmail.com>
 * @version 1.0
 *
 * @property Podcast[] $podcasts
 * @property array $options
 */
class PodcastListView extends Component
{
    protected $name = 'podcast-list';
    protected static $default_properties = [
        'podcasts' => [],
        'options' => []
    ];

    public function __construct(array $podcasts, array $options = [])
    {
        parent::__construct(compact('podcasts', 'options'));
    }
}
