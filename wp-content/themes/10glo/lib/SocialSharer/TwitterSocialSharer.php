<?php

namespace ChildTheme\SocialSharer;

use Backstage\Models\PostBase;
use ChildTheme\Video\Video;

/**
 * Class TwitterSocialSharer
 * @package ChildTheme\SocialSharer
 * @author Vincent Ragosta <vragosta@situationinteractive.com>
 * @version 1.0
 */
class TwitterSocialSharer extends SocialSharer
{
    const LABEL = 'Twitter';
    const SHARER = 'https://twitter.com/intent/tweet?url=%s&text=%s&via=10gloTV';
    const ICON = 'twitter-circle';

    /**
     * @param PostBase|Video $Video
     * @return string
     */
    protected function generateSharerUrl($Video)
    {
        if (empty($permalink = $Video->permalink()) || empty($Video->title())) {
            return '';
        }
        return sprintf(static::SHARER, $permalink, $Video->title());
    }
}
