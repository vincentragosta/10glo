<?php

namespace ChildTheme\Controller;

use Backstage\SetDesign\Icon\IconView;

/**
 * Class SearchController
 * @package ChildTheme\Controller
 * @author Vincent Ragosta <vragosta@situationinteractive.com>
 * @version 1.0
 */
class SearchController
{
    const FORM_MARKUP = '
        <form role="search" method="get" class="search-form" action="%s">
            <label class="search-form__label">
                <span class="screen-reader-text">%s</span>
                %s
                <input type="search" class="search-form__field" placeholder="%s" value="%s" name="s" />
            </label>
            <input type="submit" class="button search-form__submit" value="Search" />
        </form>';

    public function __construct()
    {
        add_filter('get_search_form', [$this, 'formMarkup']);
        add_filter('pre_get_posts', [$this, 'updateSearchQuery']);
    }

    public function updateSearchQuery($query)
    {
        if ($query->is_search && !is_admin()) {
            $query->set('post_type', ['video']);
            $query->set('posts_per_page', -1);
        }
        return $query;
    }

    public function formMarkup($form)
    {
        return sprintf(
            static::FORM_MARKUP,
            esc_url(home_url('/')),
            _x('Search for:', 'label'),
            new IconView(['icon_name' => 'search']),
            esc_attr_x('Find a creator you love', 'placeholder'),
            get_search_query()
        );
    }
}
