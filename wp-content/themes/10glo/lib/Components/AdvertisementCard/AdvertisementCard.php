<?php

namespace ChildTheme\Components\AdvertisementCard;

use Backstage\VcLibrary\Support\Component;
use Backstage\View\Element;
use Backstage\View\Link;
use ChildTheme\Advertisement\AdvertisementRepository;
use ChildTheme\Advertisement\Advertisement;

/**
 * Class AdvertisementCard
 * @package ChildTheme\Components\AdvertisementCard
 * @author Vincent Ragosta <vincentpasqualeragosta@gmail.com>
 * @version 1.0
 */
class AdvertisementCard extends Component
{
    const NAME = 'Advertisement Card';
    const TAG = 'advertisement_card';

    protected $component_config = [
        'description' => 'Display an advertisement card.',
        'icon' => 'icon-wpb-toggle-small-expand',
        'wrapper_class' => 'clearfix',
        'is_container' => false,
        'category' => 'Content',
        'params' => [
            [
                'type' => 'textfield',
                'heading' => 'Advertisement ID',
                'param_name' => 'advertisement_id',
                'description' => 'Enter the Advertisement ID.',
                'admin_label' => true
            ]
        ]
    ];

    protected function createView(array $atts)
    {
        /** @var Advertisement $Advertisement */
        if (!($Advertisement = (new AdvertisementRepository())->findById($atts['advertisement_id'])) instanceof Advertisement) {
            return '';
        }
        if (empty($link = $Advertisement->link)) {
            return '';
        }
        return Element::create('div', new Link($link['url'], $Advertisement->featuredImage()->css_class('advertisement-card__image'), [
            'class' => 'advertisement-card__link',
        ]), ['class' => 'advertisement-card']);
    }
}
