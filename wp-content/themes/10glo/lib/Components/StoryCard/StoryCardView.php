<?php

namespace ChildTheme\Components\StoryCard;

use Backstage\View\Component;
use Backstage\View\Element;
use ChildTheme\News\NewsPost;

/**
 * Class StoryCardView
 * @package ChildTheme\Components\StoryCard
 * @author Vincent Ragosta <vincentpasqualeragosta@gmail.com>
 * @version 1.0
 *
 * @property \WP_Image|bool $image
 * @property string $title
 * @property string $content
 * @property string $url
 */
class StoryCardView extends Component
{
    const IMAGE_HEIGHT = 432;
    const IMAGE_WIDTH = 768;
    const EXCERPT_NUM_WORDS = 25;

    protected $name = 'story-card';
    protected static $default_properties = [
        'image' => false,
        'title' => '',
        'content' => '',
        'url' => ''
    ];

    public function __construct(NewsPost $Story, $num_words = self::EXCERPT_NUM_WORDS)
    {
        if (!empty($content = $Story->excerpt($num_words ?: static::EXCERPT_NUM_WORDS))) {
            $content .= '...';
        }
        parent::__construct([
            'image' => $Story->featuredImage(),
            'title' => $Story->title(),
            'content' => $content,
            'url' => $Story->permalink()
        ]);
        if ($this->image instanceof \WP_Image) {
            $this->image->width(static::IMAGE_WIDTH)->height(static::IMAGE_HEIGHT);
        }
        if ($this->content) {
            $this->content = Element::create('p', $this->content, ['class' => 'story-card__content']);
        }
    }

    public function overrideContent(string $content)
    {
        $this->content = $content;
        return $this;
    }
}
