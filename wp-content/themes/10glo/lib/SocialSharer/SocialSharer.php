<?php

namespace ChildTheme\SocialSharer;

use Backstage\Models\PostBase;
use Backstage\SetDesign\Icon\IconView;
use Backstage\View\Link;

/**
 * Class SocialSharer
 * @package ChildTheme\SocialSharer
 * @author Vincent Ragosta <vragosta@situationinteractive.com>
 * @version 1.0
 *
 * @property string $url
 */
abstract class SocialSharer
{
    const LABEL = null;
    const SHARER = null;
    const ICON = null;

    protected $url;

    public function __construct(PostBase $Post)
    {
        $this->url = $this->generateSharerUrl($Post);
    }

    public function getUrl()
    {
        return $this->url;
    }

    abstract protected function generateSharerUrl(PostBase $Post);
}
