<?php

namespace ChildTheme\Exception;

use Exception;

/**
 * Class UserLoginException
 * @package ChildTheme\Exception
 * @author Vincent Ragosta <vincentpasqualeragosta@gmail.com>
 * @version 1.0
 */
class UserLoginException extends Exception {}
