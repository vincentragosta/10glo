addAction(SetDesign.READY, function () {
    var $loginForm = $('#login-form');
    if (!$loginForm.length) {
        return;
    }
    $loginForm.submit(function (e) {
        e.preventDefault();

        var $messaging = $('.messaging');
        var $messagingSection = $messaging.parents('.messaging__section');
        var errors = ['<li><strong>There are errors with your submission:</strong></li>'];
        var username_or_email = $('#user_login').val();
        var user_password = $('#user_pass').val();
        var data = {
            user_login: username_or_email,
            user_password: user_password
        };

        if (!username_or_email) {
            errors.push('<li>Please enter a username or email.</li>');
        }

        if (!user_password) {
            errors.push('<li>Please enter a password.</li>');
        }

        if (errors.length > 1) {
            $messagingSection.addClass('messaging__section--show');
            $messaging.find('.messaging__content').html("<ul class='list--unstyled'>" + errors.join('') + "</ul>");
            return;
        }

        $.ajax({
            method: 'POST',
            url: sit.api_url + 'user/login',
            data: data,
            beforeSend: function (xhr) {
                xhr.setRequestHeader('X-WP-Nonce', sit.nonce);
                xhr.setRequestHeader('Authorization', 'Basic ' + sit.basic_auth);
            },
            success: function (response) {
                $messagingSection.addClass('messaging__section--show');
                $messaging.find('.messaging__content').html(response.message);
                setTimeout(function () {
                    location.href = sit.profile_url;
                }, 1000);
            },
            error: function (response) {
                var message = response.responseJSON.message ? response.responseJSON.message : response.message;
                $messagingSection.addClass('messaging__section--show');
                $messaging.find('.messaging__content').html(message);
            },
            fail: function (response) {
                var message = response.responseJSON.message ? response.responseJSON.message : response.message;
                $messagingSection.addClass('messaging__section--show');
                $messaging.find('.messaging__content').html(message);
            }
        });
    });
});
