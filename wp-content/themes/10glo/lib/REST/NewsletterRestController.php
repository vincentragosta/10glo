<?php

namespace ChildTheme\REST;

use Backstage\Controller\RestController;
use ChildTheme\Exception\MailchimpException;
use ChildTheme\Mailchimp\MailchimpList;
use ChildTheme\Mailchimp\MailchimpListMember;
use ChildTheme\Service\MailchimpService;
use \WP_REST_Request;

/**
 * Class NewsletterRestController
 * @package ChildTheme\Controller
 * @author Vincent Ragosta <vincentpasqualeragosta@gmail.com>
 * @version 1.0
 *
 * @property MailchimpService $MailchimpService
 */
class NewsletterRestController extends RestController
{
    protected $namespace = 'mailchimp';
    protected $MailchimpService;

     public function __construct()
     {
         parent::__construct();
         $this->MailchimpService = new MailchimpService();
     }

     public function registerRoutes()
     {
         $this->addCreateRoute('/add', [$this, 'addToList']);
     }

     public function addToList(WP_REST_Request $request)
     {
         $email = $this->getEmailFromRequest($request);
         $result = $this->MailchimpService->addToList(MailchimpList::createFromConfig(), new MailchimpListMember($email), 'Viewer');
         if (empty($result)) {
             throw new MailchimpException('You could not be added to the mailing list at this time.', 400);
         }
         return $result;
     }

    protected function getEmailFromRequest(WP_REST_Request $request)
    {
        if (empty($email = $request->get_param('emailAddress'))) {
            throw new MailchimpException('There was no email address sent in the request.', 400);
        }
        return $email;
    }
}
