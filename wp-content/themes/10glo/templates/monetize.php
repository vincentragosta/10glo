<?php

use ChildTheme\Components\Messaging\MessagingView;
use ChildTheme\Components\StripeConnect\StripeConnectView;
use ChildTheme\Options\GlobalOptions;
use ChildTheme\Service\UserMonetizationService;
use ChildTheme\User\User;

$CurrentUser = User::createFromCurrentUser();
$UserMonetizationService = new UserMonetizationService();

//$user = User::createFromUserLogin(get_query_var('related_user'));
$Page = GlobalOptions::monetizeYourVideosPage();
echo $Page->content(false);
?>

<?php if ($UserMonetizationService->isVerified($CurrentUser) && !empty($UserMonetizationService->getSupportMessage($CurrentUser))): ?>
    <section class="content-section anchor content-section--has-bg content-section--dark content-section--tpad-double content-section--bpad-double content-section--mb-none">
        <div class="container content-section__container">
            <div class="content-row row">
                <div class="content-column col-md-12 text--center content-column--last">
                    <div class="content-column__inner">
                        <div class="responsive-spacer responsive-spacer--2"></div>
                        <h2 class="heading heading--large heading--inverted">You have already completed the onboarding form!</h2>
                    </div>
                </div>
            </div>
            <div class="content-row row justify-content-center">
                <div class="content-column col-md-4 text--center content-column--last">
                    <div class="content-column__inner">
                        <div class="column-text">
                            <p><a id="monetize-payments-start" class="button button--secondary button--block" href="<?= $UserMonetizationService->getVendorPaymentSettingsLink($CurrentUser); ?>">View Payment Settings</a></p>
                        </div>
                        <div class="responsive-spacer responsive-spacer--2"></div>
                    </div>
                </div>
            </div>
        </div>
    </section>
<?php elseif (!empty(GlobalOptions::monetizationSteps())): ?>
    <section class="content-section anchor content-section--has-bg content-section--dark content-section--tpad-double content-section--bpad-double content-section--mb-none">
        <div class="container content-section__container">
            <div class="content-row row">
                <div class="content-column col-md-12 text--center content-column--last">
                    <div class="content-column__inner">
                        <div class="responsive-spacer responsive-spacer--2"></div>
                        <h2 class="heading heading--large heading--inverted">Sound Appealing? We will walk you through this five step process!</h2>
                    </div>
                </div>
            </div>
            <div class="content-row row justify-content-center">
                <div class="content-column col-md-4 text--center content-column--last">
                    <div class="content-column__inner">
                        <div class="column-text">
                            <p><a id="monetize-payments-start" class="button button--secondary button--block" href="#get-started">Get Started Now</a></p>
                        </div>
                        <div class="responsive-spacer responsive-spacer--2"></div>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <section class="content-section content-section--mb-double d-none" id="monetize-payments">
        <div class="content-section__container container">
            <div class="content-row row messaging__section">
                <div class="content-column col-12">
                    <div class="content-column__inner">
                        <?= new MessagingView(); ?>
                    </div>
                </div>
            </div>
            <div class="content-row row">
                <div class="content-column col-12">
                    <div class="content-column__inner">
                        <div class="monetize-payments">
                            <?php if (!empty($step_one = GlobalOptions::monetizationStepOne()) && !empty($step_one_text = $step_one['text'])): ?>
                                <div
                                    class="monetize-payments__step<?= ($is_material_ownership_confirmed = $UserMonetizationService->isMaterialOwnershipConfirmed($CurrentUser)) ? ' monetize-payments__step--complete' : ''; ?>"
                                    data-step-index="1">
                                    <h2 class="heading heading--large">Step
                                        1. <?= !empty($step_one_title = $step_one['title']) ? $step_one_title : ''; ?></h2>
                                    <?= $step_one_text; ?>
                                    <form id="confirm-material-ownership" action="" method="post"
                                          enctype="multipart/form-data">
                                        <input type="submit" class="button"
                                               value="I agree" <?= $is_material_ownership_confirmed ? 'disabled' : ''; ?>/>
                                    </form>
                                </div>
                            <?php endif; ?>
                            <?php if (!empty($step_two = GlobalOptions::monetizationStepTwo()) && !empty($step_two_text = $step_two['text'])): ?>
                                <div
                                    class="monetize-payments__step d-none<?= ($are_monetization_policies_confirmed = $UserMonetizationService->areMonetizationPoliciesConfirmed($CurrentUser)) ? ' monetize-payments__step--complete' : ''; ?>"
                                    data-step-index="2">
                                    <h2 class="heading heading--large">Step
                                        2. <?= !empty($step_two_title = $step_two['title']) ? $step_two_title : ''; ?></h2>
                                    <?= $step_two_text; ?>
                                    <form id="confirm-monetization-policies" action="" method="post"
                                          enctype="multipart/form-data">
                                        <input type="submit" class="button"
                                               value="I agree" <?= $are_monetization_policies_confirmed ? 'disabled' : ''; ?>/>
                                    </form>
                                </div>
                            <?php endif; ?>
                            <?php if (!empty($step_three = GlobalOptions::monetizationStepThree()) && !empty($step_three_text = $step_three['text'])): ?>
                                <div
                                    class="monetize-payments__step d-none<?= !empty($support_message = $UserMonetizationService->getSupportMessage($CurrentUser)) ? ' monetize-payments__step--complete' : ''; ?>"
                                    data-step-index="3">
                                    <h2 class="heading heading--large">Step
                                        3. <?= !empty($step_three_title = $step_three['title']) ? $step_three_title : ''; ?></h2>
                                    <?= $step_three_text; ?>
                                    <form id="add-support-message" action="" method="post"
                                          enctype="multipart/form-data">
                                        <?php wp_editor($support_message, 'user-support-message', ['textarea_name' => 'user-support-message', 'media_buttons' => false, 'quicktags' => false]); ?>
                                        <input type="submit" class="button"
                                               style="margin-top: var(--global__sizing__layout-spacing--default);"/>
                                    </form>
                                </div>
                            <?php endif; ?>
                            <?php if (!empty($step_four = GlobalOptions::monetizationStepFour()) && !empty($step_four_text = $step_four['text'])): ?>
                                <div
                                    class="monetize-payments__step d-none<?= !empty($country = $CurrentUser->getCountry()) ? ' monetize-payments__step--complete' : ''; ?>"
                                    data-step-index="4">
                                    <h2 class="heading heading--large">Step
                                        4. <?= !empty($step_four_title = $step_four['title']) ? $step_four_title : ''; ?></h2>
                                    <label for="user_country"><?= $step_four_text; ?></label>
                                    <select name="user_country"
                                            id="user_country" <?= !empty($country) ? 'disabled' : ''; ?>>
                                        <option value="">-- Select Country --</option>
                                        <option value="AF" <?= $country == 'AF' ? 'selected' : ''; ?>>Afghanistan
                                        </option>
                                        <option value="AX" <?= $country == 'AX' ? 'selected' : ''; ?>>Åland Islands
                                        </option>
                                        <option value="AL" <?= $country == 'AL' ? 'selected' : ''; ?>>Albania</option>
                                        <option value="DZ" <?= $country == 'DZ' ? 'selected' : ''; ?>>Algeria</option>
                                        <option value="AS" <?= $country == 'AS' ? 'selected' : ''; ?>>American Samoa
                                        </option>
                                        <option value="AD" <?= $country == 'AD' ? 'selected' : ''; ?>>Andorra</option>
                                        <option value="AO" <?= $country == 'AO' ? 'selected' : ''; ?>>Angola</option>
                                        <option value="AI" <?= $country == 'AI' ? 'selected' : ''; ?>>Anguilla</option>
                                        <option value="AQ" <?= $country == 'AQ' ? 'selected' : ''; ?>>Antarctica
                                        </option>
                                        <option value="AG" <?= $country == 'AG' ? 'selected' : ''; ?>>Antigua and
                                            Barbuda
                                        </option>
                                        <option value="AR" <?= $country == 'AR' ? 'selected' : ''; ?>>Argentina</option>
                                        <option value="AM" <?= $country == 'AM' ? 'selected' : ''; ?>>Armenia</option>
                                        <option value="AW" <?= $country == 'AW' ? 'selected' : ''; ?>>Aruba</option>
                                        <option value="AU" <?= $country == 'AU' ? 'selected' : ''; ?>>Australia</option>
                                        <option value="AT" <?= $country == 'AT' ? 'selected' : ''; ?>>Austria</option>
                                        <option value="AZ" <?= $country == 'AZ' ? 'selected' : ''; ?>>Azerbaijan
                                        </option>
                                        <option value="BS" <?= $country == 'BS' ? 'selected' : ''; ?>>Bahamas</option>
                                        <option value="BH" <?= $country == 'BH' ? 'selected' : ''; ?>>Bahrain</option>
                                        <option value="BD" <?= $country == 'BD' ? 'selected' : ''; ?>>Bangladesh
                                        </option>
                                        <option value="BB" <?= $country == 'BB' ? 'selected' : ''; ?>>Barbados</option>
                                        <option value="BY" <?= $country == 'BY' ? 'selected' : ''; ?>>Belarus</option>
                                        <option value="BE" <?= $country == 'BE' ? 'selected' : ''; ?>>Belgium</option>
                                        <option value="BZ" <?= $country == 'BZ' ? 'selected' : ''; ?>>Belize</option>
                                        <option value="BJ" <?= $country == 'BJ' ? 'selected' : ''; ?>>Benin</option>
                                        <option value="BM" <?= $country == 'BM' ? 'selected' : ''; ?>>Bermuda</option>
                                        <option value="BT" <?= $country == 'BT' ? 'selected' : ''; ?>>Bhutan</option>
                                        <option value="BO" <?= $country == 'BO' ? 'selected' : ''; ?>>Bolivia,
                                            Plurinational State of
                                        </option>
                                        <option value="BQ" <?= $country == 'BQ' ? 'selected' : ''; ?>>Bonaire, Sint
                                            Eustatius and Saba
                                        </option>
                                        <option value="BA" <?= $country == 'BA' ? 'selected' : ''; ?>>Bosnia and
                                            Herzegovina
                                        </option>
                                        <option value="BW" <?= $country == 'BW' ? 'selected' : ''; ?>>Botswana</option>
                                        <option value="BV" <?= $country == 'BV' ? 'selected' : ''; ?>>Bouvet Island
                                        </option>
                                        <option value="BR" <?= $country == 'BR' ? 'selected' : ''; ?>>Brazil</option>
                                        <option value="IO" <?= $country == 'IO' ? 'selected' : ''; ?>>British Indian
                                            Ocean Territory
                                        </option>
                                        <option value="BN" <?= $country == 'BN' ? 'selected' : ''; ?>>Brunei
                                            Darussalam
                                        </option>
                                        <option value="BG" <?= $country == 'BG' ? 'selected' : ''; ?>>Bulgaria</option>
                                        <option value="BF" <?= $country == 'BF' ? 'selected' : ''; ?>>Burkina Faso
                                        </option>
                                        <option value="BI" <?= $country == 'BI' ? 'selected' : ''; ?>>Burundi</option>
                                        <option value="KH" <?= $country == 'KH' ? 'selected' : ''; ?>>Cambodia</option>
                                        <option value="CM" <?= $country == 'CM' ? 'selected' : ''; ?>>Cameroon</option>
                                        <option value="CA" <?= $country == 'CA' ? 'selected' : ''; ?>>Canada</option>
                                        <option value="CV" <?= $country == 'CV' ? 'selected' : ''; ?>>Cape Verde
                                        </option>
                                        <option value="KY" <?= $country == 'KY' ? 'selected' : ''; ?>>Cayman Islands
                                        </option>
                                        <option value="CF" <?= $country == 'CF' ? 'selected' : ''; ?>>Central African
                                            Republic
                                        </option>
                                        <option value="TD" <?= $country == 'TD' ? 'selected' : ''; ?>>Chad</option>
                                        <option value="CL" <?= $country == 'CL' ? 'selected' : ''; ?>>Chile</option>
                                        <option value="CN" <?= $country == 'CN' ? 'selected' : ''; ?>>China</option>
                                        <option value="CX" <?= $country == 'CX' ? 'selected' : ''; ?>>Christmas Island
                                        </option>
                                        <option value="CC" <?= $country == 'CC' ? 'selected' : ''; ?>>Cocos (Keeling)
                                            Islands
                                        </option>
                                        <option value="CO" <?= $country == 'CO' ? 'selected' : ''; ?>>Colombia</option>
                                        <option value="KM" <?= $country == 'KM' ? 'selected' : ''; ?>>Comoros</option>
                                        <option value="CG" <?= $country == 'CG' ? 'selected' : ''; ?>>Congo</option>
                                        <option value="CD" <?= $country == 'CD' ? 'selected' : ''; ?>>Congo, the
                                            Democratic Republic of the
                                        </option>
                                        <option value="CK" <?= $country == 'CK' ? 'selected' : ''; ?>>Cook Islands
                                        </option>
                                        <option value="CR" <?= $country == 'CR' ? 'selected' : ''; ?>>Costa Rica
                                        </option>
                                        <option value="CI" <?= $country == 'CI' ? 'selected' : ''; ?>>Côte d'Ivoire
                                        </option>
                                        <option value="HR" <?= $country == 'HR' ? 'selected' : ''; ?>>Croatia</option>
                                        <option value="CU" <?= $country == 'CU' ? 'selected' : ''; ?>>Cuba</option>
                                        <option value="CW" <?= $country == 'CW' ? 'selected' : ''; ?>>Curaçao</option>
                                        <option value="CY" <?= $country == 'CY' ? 'selected' : ''; ?>>Cyprus</option>
                                        <option value="CZ" <?= $country == 'CZ' ? 'selected' : ''; ?>>Czech Republic
                                        </option>
                                        <option value="DK" <?= $country == 'DK' ? 'selected' : ''; ?>>Denmark</option>
                                        <option value="DJ" <?= $country == 'DJ' ? 'selected' : ''; ?>>Djibouti</option>
                                        <option value="DM" <?= $country == 'DM' ? 'selected' : ''; ?>>Dominica</option>
                                        <option value="DO" <?= $country == 'DO' ? 'selected' : ''; ?>>Dominican
                                            Republic
                                        </option>
                                        <option value="EC" <?= $country == 'EC' ? 'selected' : ''; ?>>Ecuador</option>
                                        <option value="EG" <?= $country == 'EG' ? 'selected' : ''; ?>>Egypt</option>
                                        <option value="SV" <?= $country == 'SV' ? 'selected' : ''; ?>>El Salvador
                                        </option>
                                        <option value="GQ" <?= $country == 'GQ' ? 'selected' : ''; ?>>Equatorial
                                            Guinea
                                        </option>
                                        <option value="ER" <?= $country == 'ER' ? 'selected' : ''; ?>>Eritrea</option>
                                        <option value="EE" <?= $country == 'EE' ? 'selected' : ''; ?>>Estonia</option>
                                        <option value="ET" <?= $country == 'ET' ? 'selected' : ''; ?>>Ethiopia</option>
                                        <option value="FK" <?= $country == 'FK' ? 'selected' : ''; ?>>Falkland Islands
                                            (Malvinas)
                                        </option>
                                        <option value="FO" <?= $country == 'FO' ? 'selected' : ''; ?>>Faroe Islands
                                        </option>
                                        <option value="FJ" <?= $country == 'FJ' ? 'selected' : ''; ?>>Fiji</option>
                                        <option value="FI" <?= $country == 'FI' ? 'selected' : ''; ?>>Finland</option>
                                        <option value="FR" <?= $country == 'FR' ? 'selected' : ''; ?>>France</option>
                                        <option value="GF" <?= $country == 'GF' ? 'selected' : ''; ?>>French Guiana
                                        </option>
                                        <option value="PF" <?= $country == 'PF' ? 'selected' : ''; ?>>French Polynesia
                                        </option>
                                        <option value="TF" <?= $country == 'TF' ? 'selected' : ''; ?>>French Southern
                                            Territories
                                        </option>
                                        <option value="GA" <?= $country == 'GA' ? 'selected' : ''; ?>>Gabon</option>
                                        <option value="GM" <?= $country == 'GM' ? 'selected' : ''; ?>>Gambia</option>
                                        <option value="GE" <?= $country == 'GE' ? 'selected' : ''; ?>>Georgia</option>
                                        <option value="DE" <?= $country == 'DE' ? 'selected' : ''; ?>>Germany</option>
                                        <option value="GH" <?= $country == 'GH' ? 'selected' : ''; ?>>Ghana</option>
                                        <option value="GI" <?= $country == 'GI' ? 'selected' : ''; ?>>Gibraltar</option>
                                        <option value="GR" <?= $country == 'GR' ? 'selected' : ''; ?>>Greece</option>
                                        <option value="GL" <?= $country == 'GL' ? 'selected' : ''; ?>>Greenland</option>
                                        <option value="GD" <?= $country == 'GD' ? 'selected' : ''; ?>>Grenada</option>
                                        <option value="GP" <?= $country == 'GP' ? 'selected' : ''; ?>>Guadeloupe
                                        </option>
                                        <option value="GU" <?= $country == 'GU' ? 'selected' : ''; ?>>Guam</option>
                                        <option value="GT" <?= $country == 'GT' ? 'selected' : ''; ?>>Guatemala</option>
                                        <option value="GG" <?= $country == 'GG' ? 'selected' : ''; ?>>Guernsey</option>
                                        <option value="GN" <?= $country == 'GN' ? 'selected' : ''; ?>>Guinea</option>
                                        <option value="GW" <?= $country == 'GW' ? 'selected' : ''; ?>>Guinea-Bissau
                                        </option>
                                        <option value="GY" <?= $country == 'GY' ? 'selected' : ''; ?>>Guyana</option>
                                        <option value="HT" <?= $country == 'HT' ? 'selected' : ''; ?>>Haiti</option>
                                        <option value="HM" <?= $country == 'HM' ? 'selected' : ''; ?>>Heard Island and
                                            McDonald Islands
                                        </option>
                                        <option value="VA" <?= $country == 'VA' ? 'selected' : ''; ?>>Holy See (Vatican
                                            City State)
                                        </option>
                                        <option value="HN" <?= $country == 'HN' ? 'selected' : ''; ?>>Honduras</option>
                                        <option value="HK" <?= $country == 'HK' ? 'selected' : ''; ?>>Hong Kong</option>
                                        <option value="HU" <?= $country == 'HU' ? 'selected' : ''; ?>>Hungary</option>
                                        <option value="IS" <?= $country == 'IS' ? 'selected' : ''; ?>>Iceland</option>
                                        <option value="IN" <?= $country == 'IN' ? 'selected' : ''; ?>>India</option>
                                        <option value="ID" <?= $country == 'ID' ? 'selected' : ''; ?>>Indonesia</option>
                                        <option value="IR" <?= $country == 'IR' ? 'selected' : ''; ?>>Iran, Islamic
                                            Republic of
                                        </option>
                                        <option value="IQ" <?= $country == 'IQ' ? 'selected' : ''; ?>>Iraq</option>
                                        <option value="IE" <?= $country == 'IE' ? 'selected' : ''; ?>>Ireland</option>
                                        <option value="IM" <?= $country == 'IM' ? 'selected' : ''; ?>>Isle of Man
                                        </option>
                                        <option value="IL" <?= $country == 'IL' ? 'selected' : ''; ?>>Israel</option>
                                        <option value="IT" <?= $country == 'IT' ? 'selected' : ''; ?>>Italy</option>
                                        <option value="JM" <?= $country == 'JM' ? 'selected' : ''; ?>>Jamaica</option>
                                        <option value="JP" <?= $country == 'JP' ? 'selected' : ''; ?>>Japan</option>
                                        <option value="JE" <?= $country == 'JE' ? 'selected' : ''; ?>>Jersey</option>
                                        <option value="JO" <?= $country == 'JO' ? 'selected' : ''; ?>>Jordan</option>
                                        <option value="KZ" <?= $country == 'KZ' ? 'selected' : ''; ?>>Kazakhstan
                                        </option>
                                        <option value="KE" <?= $country == 'KE' ? 'selected' : ''; ?>>Kenya</option>
                                        <option value="KI" <?= $country == 'KI' ? 'selected' : ''; ?>>Kiribati</option>
                                        <option value="KP" <?= $country == 'KP' ? 'selected' : ''; ?>>Korea, Democratic
                                            People's Republic of
                                        </option>
                                        <option value="KR" <?= $country == 'KR' ? 'selected' : ''; ?>>Korea, Republic
                                            of
                                        </option>
                                        <option value="KW" <?= $country == 'KW' ? 'selected' : ''; ?>>Kuwait</option>
                                        <option value="KG" <?= $country == 'KG' ? 'selected' : ''; ?>>Kyrgyzstan
                                        </option>
                                        <option value="LA" <?= $country == 'LA' ? 'selected' : ''; ?>>Lao People's
                                            Democratic Republic
                                        </option>
                                        <option value="LV" <?= $country == 'LV' ? 'selected' : ''; ?>>Latvia</option>
                                        <option value="LB" <?= $country == 'LB' ? 'selected' : ''; ?>>Lebanon</option>
                                        <option value="LS" <?= $country == 'LS' ? 'selected' : ''; ?>>Lesotho</option>
                                        <option value="LR" <?= $country == 'LR' ? 'selected' : ''; ?>>Liberia</option>
                                        <option value="LY" <?= $country == 'LY' ? 'selected' : ''; ?>>Libya</option>
                                        <option value="LI" <?= $country == 'LI' ? 'selected' : ''; ?>>Liechtenstein
                                        </option>
                                        <option value="LT" <?= $country == 'LT' ? 'selected' : ''; ?>>Lithuania</option>
                                        <option value="LU" <?= $country == 'LU' ? 'selected' : ''; ?>>Luxembourg
                                        </option>
                                        <option value="MO" <?= $country == 'MO' ? 'selected' : ''; ?>>Macao</option>
                                        <option value="MK" <?= $country == 'MK' ? 'selected' : ''; ?>>Macedonia, the
                                            former Yugoslav Republic of
                                        </option>
                                        <option value="MG" <?= $country == 'MG' ? 'selected' : ''; ?>>Madagascar
                                        </option>
                                        <option value="MW" <?= $country == 'MW' ? 'selected' : ''; ?>>Malawi</option>
                                        <option value="MY" <?= $country == 'MY' ? 'selected' : ''; ?>>Malaysia</option>
                                        <option value="MV" <?= $country == 'MV' ? 'selected' : ''; ?>>Maldives</option>
                                        <option value="ML" <?= $country == 'ML' ? 'selected' : ''; ?>>Mali</option>
                                        <option value="MT" <?= $country == 'MT' ? 'selected' : ''; ?>>Malta</option>
                                        <option value="MH" <?= $country == 'MH' ? 'selected' : ''; ?>>Marshall Islands
                                        </option>
                                        <option value="MQ" <?= $country == 'MQ' ? 'selected' : ''; ?>>Martinique
                                        </option>
                                        <option value="MR" <?= $country == 'MR' ? 'selected' : ''; ?>>Mauritania
                                        </option>
                                        <option value="MU" <?= $country == 'MU' ? 'selected' : ''; ?>>Mauritius</option>
                                        <option value="YT" <?= $country == 'YT' ? 'selected' : ''; ?>>Mayotte</option>
                                        <option value="MX" <?= $country == 'MX' ? 'selected' : ''; ?>>Mexico</option>
                                        <option value="FM" <?= $country == 'FM' ? 'selected' : ''; ?>>Micronesia,
                                            Federated States of
                                        </option>
                                        <option value="MD" <?= $country == 'MD' ? 'selected' : ''; ?>>Moldova, Republic
                                            of
                                        </option>
                                        <option value="MC" <?= $country == 'MC' ? 'selected' : ''; ?>>Monaco</option>
                                        <option value="MN" <?= $country == 'MN' ? 'selected' : ''; ?>>Mongolia</option>
                                        <option value="ME" <?= $country == 'ME' ? 'selected' : ''; ?>>Montenegro
                                        </option>
                                        <option value="MS" <?= $country == 'MS' ? 'selected' : ''; ?>>Montserrat
                                        </option>
                                        <option value="MA" <?= $country == 'MA' ? 'selected' : ''; ?>>Morocco</option>
                                        <option value="MZ" <?= $country == 'MZ' ? 'selected' : ''; ?>>Mozambique
                                        </option>
                                        <option value="MM" <?= $country == 'MM' ? 'selected' : ''; ?>>Myanmar</option>
                                        <option value="NA" <?= $country == 'NA' ? 'selected' : ''; ?>>Namibia</option>
                                        <option value="NR" <?= $country == 'NR' ? 'selected' : ''; ?>>Nauru</option>
                                        <option value="NP" <?= $country == 'NP' ? 'selected' : ''; ?>>Nepal</option>
                                        <option value="NL" <?= $country == 'NL' ? 'selected' : ''; ?>>Netherlands
                                        </option>
                                        <option value="NC" <?= $country == 'NC' ? 'selected' : ''; ?>>New Caledonia
                                        </option>
                                        <option value="NZ" <?= $country == 'NZ' ? 'selected' : ''; ?>>New Zealand
                                        </option>
                                        <option value="NI" <?= $country == 'NI' ? 'selected' : ''; ?>>Nicaragua</option>
                                        <option value="NE" <?= $country == 'NE' ? 'selected' : ''; ?>>Niger</option>
                                        <option value="NG" <?= $country == 'NG' ? 'selected' : ''; ?>>Nigeria</option>
                                        <option value="NU" <?= $country == 'NU' ? 'selected' : ''; ?>>Niue</option>
                                        <option value="NF" <?= $country == 'NF' ? 'selected' : ''; ?>>Norfolk Island
                                        </option>
                                        <option value="MP" <?= $country == 'MP' ? 'selected' : ''; ?>>Northern Mariana
                                            Islands
                                        </option>
                                        <option value="NO" <?= $country == 'NO' ? 'selected' : ''; ?>>Norway</option>
                                        <option value="OM" <?= $country == 'OM' ? 'selected' : ''; ?>>Oman</option>
                                        <option value="PK" <?= $country == 'PK' ? 'selected' : ''; ?>>Pakistan</option>
                                        <option value="PW" <?= $country == 'PW' ? 'selected' : ''; ?>>Palau</option>
                                        <option value="PS" <?= $country == 'PS' ? 'selected' : ''; ?>>Palestinian
                                            Territory, Occupied
                                        </option>
                                        <option value="PA" <?= $country == 'PA' ? 'selected' : ''; ?>>Panama</option>
                                        <option value="PG" <?= $country == 'PG' ? 'selected' : ''; ?>>Papua New Guinea
                                        </option>
                                        <option value="PY" <?= $country == 'PY' ? 'selected' : ''; ?>>Paraguay</option>
                                        <option value="PE" <?= $country == 'PE' ? 'selected' : ''; ?>>Peru</option>
                                        <option value="PH" <?= $country == 'PH' ? 'selected' : ''; ?>>Philippines
                                        </option>
                                        <option value="PN" <?= $country == 'PN' ? 'selected' : ''; ?>>Pitcairn</option>
                                        <option value="PL" <?= $country == 'PL' ? 'selected' : ''; ?>>Poland</option>
                                        <option value="PT" <?= $country == 'PT' ? 'selected' : ''; ?>>Portugal</option>
                                        <option value="PR" <?= $country == 'PR' ? 'selected' : ''; ?>>Puerto Rico
                                        </option>
                                        <option value="QA" <?= $country == 'QA' ? 'selected' : ''; ?>>Qatar</option>
                                        <option value="RE" <?= $country == 'RE' ? 'selected' : ''; ?>>Réunion</option>
                                        <option value="RO" <?= $country == 'RO' ? 'selected' : ''; ?>>Romania</option>
                                        <option value="RU" <?= $country == 'RU' ? 'selected' : ''; ?>>Russian
                                            Federation
                                        </option>
                                        <option value="RW" <?= $country == 'RW' ? 'selected' : ''; ?>>Rwanda</option>
                                        <option value="BL" <?= $country == 'BL' ? 'selected' : ''; ?>>Saint Barthélemy
                                        </option>
                                        <option value="SH" <?= $country == 'SH' ? 'selected' : ''; ?>>Saint Helena,
                                            Ascension and Tristan da Cunha
                                        </option>
                                        <option value="KN" <?= $country == 'KN' ? 'selected' : ''; ?>>Saint Kitts and
                                            Nevis
                                        </option>
                                        <option value="LC" <?= $country == 'LC' ? 'selected' : ''; ?>>Saint Lucia
                                        </option>
                                        <option value="MF" <?= $country == 'MF' ? 'selected' : ''; ?>>Saint Martin
                                            (French part)
                                        </option>
                                        <option value="PM" <?= $country == 'PM' ? 'selected' : ''; ?>>Saint Pierre and
                                            Miquelon
                                        </option>
                                        <option value="VC" <?= $country == 'VC' ? 'selected' : ''; ?>>Saint Vincent and
                                            the Grenadines
                                        </option>
                                        <option value="WS" <?= $country == 'WS' ? 'selected' : ''; ?>>Samoa</option>
                                        <option value="SM" <?= $country == 'SM' ? 'selected' : ''; ?>>San Marino
                                        </option>
                                        <option value="ST" <?= $country == 'ST' ? 'selected' : ''; ?>>Sao Tome and
                                            Principe
                                        </option>
                                        <option value="SA" <?= $country == 'SA' ? 'selected' : ''; ?>>Saudi Arabia
                                        </option>
                                        <option value="SN" <?= $country == 'SN' ? 'selected' : ''; ?>>Senegal</option>
                                        <option value="RS" <?= $country == 'RS' ? 'selected' : ''; ?>>Serbia</option>
                                        <option value="SC" <?= $country == 'SC' ? 'selected' : ''; ?>>Seychelles
                                        </option>
                                        <option value="SL" <?= $country == 'SL' ? 'selected' : ''; ?>>Sierra Leone
                                        </option>
                                        <option value="SG" <?= $country == 'SG' ? 'selected' : ''; ?>>Singapore</option>
                                        <option value="SX" <?= $country == 'SX' ? 'selected' : ''; ?>>Sint Maarten
                                            (Dutch part)
                                        </option>
                                        <option value="SK" <?= $country == 'SK' ? 'selected' : ''; ?>>Slovakia</option>
                                        <option value="SI" <?= $country == 'SI' ? 'selected' : ''; ?>>Slovenia</option>
                                        <option value="SB" <?= $country == 'SB' ? 'selected' : ''; ?>>Solomon Islands
                                        </option>
                                        <option value="SO" <?= $country == 'SO' ? 'selected' : ''; ?>>Somalia</option>
                                        <option value="ZA" <?= $country == 'ZA' ? 'selected' : ''; ?>>South Africa
                                        </option>
                                        <option value="GS" <?= $country == 'GS' ? 'selected' : ''; ?>>South Georgia and
                                            the South Sandwich Islands
                                        </option>
                                        <option value="SS" <?= $country == 'SS' ? 'selected' : ''; ?>>South Sudan
                                        </option>
                                        <option value="ES" <?= $country == 'ES' ? 'selected' : ''; ?>>Spain</option>
                                        <option value="LK" <?= $country == 'LK' ? 'selected' : ''; ?>>Sri Lanka</option>
                                        <option value="SD" <?= $country == 'SD' ? 'selected' : ''; ?>>Sudan</option>
                                        <option value="SR" <?= $country == 'SR' ? 'selected' : ''; ?>>Suriname</option>
                                        <option value="SJ" <?= $country == 'SJ' ? 'selected' : ''; ?>>Svalbard and Jan
                                            Mayen
                                        </option>
                                        <option value="SZ" <?= $country == 'SZ' ? 'selected' : ''; ?>>Swaziland</option>
                                        <option value="SE" <?= $country == 'SE' ? 'selected' : ''; ?>>Sweden</option>
                                        <option value="CH" <?= $country == 'CH' ? 'selected' : ''; ?>>Switzerland
                                        </option>
                                        <option value="SY" <?= $country == 'SY' ? 'selected' : ''; ?>>Syrian Arab
                                            Republic
                                        </option>
                                        <option value="TW" <?= $country == 'TW' ? 'selected' : ''; ?>>Taiwan, Province
                                            of China
                                        </option>
                                        <option value="TJ" <?= $country == 'TJ' ? 'selected' : ''; ?>>Tajikistan
                                        </option>
                                        <option value="TZ" <?= $country == 'TZ' ? 'selected' : ''; ?>>Tanzania, United
                                            Republic of
                                        </option>
                                        <option value="TH" <?= $country == 'TH' ? 'selected' : ''; ?>>Thailand</option>
                                        <option value="TL" <?= $country == 'TL' ? 'selected' : ''; ?>>Timor-Leste
                                        </option>
                                        <option value="TG" <?= $country == 'TG' ? 'selected' : ''; ?>>Togo</option>
                                        <option value="TK" <?= $country == 'TK' ? 'selected' : ''; ?>>Tokelau</option>
                                        <option value="TO" <?= $country == 'TO' ? 'selected' : ''; ?>>Tonga</option>
                                        <option value="TT" <?= $country == 'TT' ? 'selected' : ''; ?>>Trinidad and
                                            Tobago
                                        </option>
                                        <option value="TN" <?= $country == 'TN' ? 'selected' : ''; ?>>Tunisia</option>
                                        <option value="TR" <?= $country == 'TR' ? 'selected' : ''; ?>>Turkey</option>
                                        <option value="TM" <?= $country == 'TM' ? 'selected' : ''; ?>>Turkmenistan
                                        </option>
                                        <option value="TC" <?= $country == 'TC' ? 'selected' : ''; ?>>Turks and Caicos
                                            Islands
                                        </option>
                                        <option value="TV" <?= $country == 'TV' ? 'selected' : ''; ?>>Tuvalu</option>
                                        <option value="UG" <?= $country == 'UG' ? 'selected' : ''; ?>>Uganda</option>
                                        <option value="UA" <?= $country == 'UA' ? 'selected' : ''; ?>>Ukraine</option>
                                        <option value="AE" <?= $country == 'AE' ? 'selected' : ''; ?>>United Arab
                                            Emirates
                                        </option>
                                        <option value="GB" <?= $country == 'GB' ? 'selected' : ''; ?>>United Kingdom
                                        </option>
                                        <option value="US" <?= $country == 'US' ? 'selected' : ''; ?>>United States
                                        </option>
                                        <option value="UM" <?= $country == 'UM' ? 'selected' : ''; ?>>United States
                                            Minor Outlying Islands
                                        </option>
                                        <option value="UY" <?= $country == 'UY' ? 'selected' : ''; ?>>Uruguay</option>
                                        <option value="UZ" <?= $country == 'UZ' ? 'selected' : ''; ?>>Uzbekistan
                                        </option>
                                        <option value="VU" <?= $country == 'VU' ? 'selected' : ''; ?>>Vanuatu</option>
                                        <option value="VE" <?= $country == 'VE' ? 'selected' : ''; ?>>Venezuela,
                                            Bolivarian Republic of
                                        </option>
                                        <option value="VN" <?= $country == 'VN' ? 'selected' : ''; ?>>Viet Nam</option>
                                        <option value="VG" <?= $country == 'VG' ? 'selected' : ''; ?>>Virgin Islands,
                                            British
                                        </option>
                                        <option value="VI" <?= $country == 'VI' ? 'selected' : ''; ?>>Virgin Islands,
                                            U.S.
                                        </option>
                                        <option value="WF" <?= $country == 'WF' ? 'selected' : ''; ?>>Wallis and
                                            Futuna
                                        </option>
                                        <option value="EH" <?= $country == 'EH' ? 'selected' : ''; ?>>Western Sahara
                                        </option>
                                        <option value="YE" <?= $country == 'YE' ? 'selected' : ''; ?>>Yemen</option>
                                        <option value="ZM" <?= $country == 'ZM' ? 'selected' : ''; ?>>Zambia</option>
                                        <option value="ZW" <?= $country == 'ZW' ? 'selected' : ''; ?>>Zimbabwe</option>
                                    </select>
                                </div>
                            <?php endif; ?>
                            <?php if (!empty($step_five = GlobalOptions::monetizationStepFive()) && !empty($step_five_text = $step_five['text'])): ?>
                                <div class="monetize-payments__step d-none" data-step-index="5">
                                    <h2 class="heading heading--large">Step
                                        5. <?= !empty($step_five_title = $step_five['title']) ? $step_five_title : ''; ?></h2>
                                    <?= $step_five_text; ?>
                                    <?= new StripeConnectView($CurrentUser); ?>
                                </div>
                            <?php endif; ?>
                            <div class="monetize-payments__next-prev">
                                <button class="monetize-payments__prev button button--secondary d-none"
                                        data-step-index="0">Previous
                                </button>
                                <button
                                    class="monetize-payments__next button button--secondary<?= empty($is_material_ownership_confirmed) ? ' d-none' : ''; ?>"
                                    data-step-index="2">Next
                                </button>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
<?php endif; ?>
