<?php

namespace ChildTheme\Exception;

use Exception;

/**
 * Class VideoException
 * @package ChildTheme\Exception
 * @author Vincent Ragosta <vincentpasqualeragosta@gmail.com>
 * @version 1.0
 */
class VideoException extends Exception {}
