<?php

namespace ChildTheme\Controller;

use Backstage\Rewrite\QueryRewrite;
use Backstage\Vendor\AdvancedCustomFields\BidirectionalRelationship;
use ChildTheme\Playlist\Playlist;
use ChildTheme\Playlist\PlaylistRepository;
use ChildTheme\Video\Video;
use ChildTheme\Video\VideoRepository;

/**
 * Class VideoController
 * @package ChildTheme\Controller
 * @author Vincent Ragosta <vincentpasqualeragosta@gmail.com>
 * @version 1.0
 */
class VideoController
{
    const TAXONOMY_POSTS_PER_PAGE = 32;

    public function __construct()
    {
        new QueryRewrite('video/upload', ['video_upload' => true]);
        new QueryRewrite('premium-video/(.+)?$', ['premium_video' => '$1']);
        new QueryRewrite('watch/(.+)/edit?$', ['related_video' => '$1', 'video_edit' => true]);
        new QueryRewrite('embed/(.+)?$', ['video' => '$1', 'video_embed' => true]);
        new BidirectionalRelationship('playlist_ids');
        add_filter('body_class', [$this, 'addBodyClass']);
        add_filter('template_include', [$this, 'templateInclude'], 99);
        add_action('after_save_post_video', [$this, 'saveVideoToPlaylist']);
        add_action('after_save_post_video', [$this, 'fixLikesCount']);
    }

    public function addBodyClass($classes)
    {
        global $wp_query;
        if ($wp_query->get('video_upload')) {
            $classes = array_merge($classes, ['page-template-video-upload']);
        }
        if ($wp_query->get('video_edit') && $wp_query->get('related_video')) {
            $classes = array_merge($classes, ['page-template-video-edit']);
        }
        if ($wp_query->get('video_embed') && $wp_query->get('video')) {
            $classes = array_merge($classes, ['page-template-video-embed']);
        }
        return $classes;
    }

    public function templateInclude($template)
    {
        $wp_query = $GLOBALS['wp_query'];
        if ($wp_query->get('video_upload')) {
            $new_template = locate_template(['templates/video-upload.php']);
            if ('' != $new_template) {
                return $new_template;
            }
        }
        if ($wp_query->get('video_edit') && $wp_query->get('related_video')) {
            $new_template = locate_template(['templates/video-edit.php']);
            if ('' != $new_template) {
                return $new_template;
            }
        }
        if ($wp_query->get('video_embed') && $wp_query->get('video')) {
            $new_template = locate_template(['templates/video-embed.php']);
            if ('' != $new_template) {
                return $new_template;
            }
        }
        return $template;
    }

    public function saveVideoToPlaylist($object_id)
    {
        if (get_post_type($object_id) == Video::POST_TYPE) {
            $Video = new Video($object_id);
            if (!empty($playlist_ids = $Video->playlist_ids)) {
                foreach ($playlist_ids as $playlist_id) {
                    $Playlist = new Playlist($playlist_id);
                    $video_ids = $Playlist->video_ids;
                    if (!in_array($Video->ID, (array)$video_ids)) {
                        $video_ids[] = $Video->ID;
                        $Playlist->video_ids = $video_ids;
                        (new PlaylistRepository())->add($Playlist);
                    }
                }
            }
        }
    }

    public function fixLikesCount($object_id)
    {
        if (get_post_type($object_id) == Video::POST_TYPE) {
            $Video = new Video($object_id);
            if (empty($Video->likes_count)) {
                $Video->likes_count = count($Video->likes) ?: 0;
                (new VideoRepository())->add($Video);
            }
        }
    }
}
