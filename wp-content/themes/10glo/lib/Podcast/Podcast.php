<?php

namespace ChildTheme\Podcast;

use Backstage\Models\PostBase;

/**
 * Class Podcast
 * @package ChildTheme\Podcast
 * @author Vincent Ragosta <vincentpasqualeragosta@gmail.com
 * @version 1.0
 *
 * @property string $libsyn_link
 * @property string $libsyn_guid
 * @property string $libsyn_published_date
 * @property string $podcast_length
 * @property string $podcast_type
 * @property string $podcast_url
 *
 * @property string $post_status
 */
class Podcast extends PostBase
{
    const POST_TYPE = 'podcast';
}
