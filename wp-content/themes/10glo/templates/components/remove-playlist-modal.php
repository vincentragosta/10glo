<?php
/**
 * Expected
 * @var Playlist $Playlist
 */

use ChildTheme\Playlist\Playlist;
?>

<div class="remove-playlist-modal">
    <h2 class="remove-playlist-modal__heading heading heading--large">Warning</h2>
    <p>This process cannot be undone. Once you delete your playlist you will have to re-create it from scratch.</p>
    <button class="remove-playlist-modal__button button" data-playlist-id="<?= $Playlist->ID; ?>">Remove Playlist</button>
</div>
