<?php

namespace ChildTheme\SocialSharer;

use Backstage\Models\PostBase;
use ChildTheme\Video\Video;

/**
 * Class FacebookSocialSharer
 * @package ChildTheme\SocialSharer
 * @author Vincent Ragosta <vragosta@situationinteractive.com>
 * @version 1.0
 */
class FacebookSocialSharer extends SocialSharer
{
    const LABEL = 'Facebook';
    const SHARER = 'https://www.facebook.com/sharer/sharer.php?u=%s';
    const ICON = 'facebook-circle';

    /**
     * @param PostBase|Video $Video
     * @return string
     */
    protected function generateSharerUrl($Video)
    {
        if (empty($permalink = $Video->permalink())) {
            return '';
        }
        return sprintf(static::SHARER, $permalink);
    }
}
