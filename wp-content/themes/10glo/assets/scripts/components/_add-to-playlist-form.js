addAction(INIT, function () {
    var $addToPlaylistForm = $('#add-to-playlist-form');
    if (!$addToPlaylistForm.length) {
        return;
    }

    $addToPlaylistForm.submit(function (e) {
        e.preventDefault();

        var $messaging = $('.messaging');
        var $messagingSection = $messaging.parents('.messaging__section');
        var $playlistId = $('.add-to-playlist-form__playlists option:selected').val();
        var $modal = $('#add-to-playlist-modal');

        if (!$playlistId) {
            doAction('hideModal', $modal);
            $messagingSection.addClass('messaging__section--show');
            $messaging.find('.messaging__content').html("Please select a playlist.");
        }

        $.ajax({
            method: 'POST',
            url: sit.api_url + 'user/playlist/add',
            data: {
                playlistId: $playlistId,
                videoId: $('.add-to-playlist-form__video-id').val(),
                userId: sit.current_user_id
            },
            beforeSend: function (xhr) {
                xhr.setRequestHeader('X-WP-Nonce', sit.nonce);
                xhr.setRequestHeader('Authorization', 'Basic ' + sit.basic_auth);
            },
            success: function (response) {
                doAction('hideModal', $modal);
                $messagingSection.addClass('messaging__section--show');
                $messaging.find('.messaging__content').html("The video has been successfully been added.");

                setTimeout(function () {
                    location.href = sit.profile_url + '#playlists';
                }, 500);
            },
            error: function (response) {
                doAction('hideModal', $modal);
                var message = response.responseJSON.message ? response.responseJSON.message : response.message;
                $messagingSection.addClass('messaging__section--show');
                $messaging.find('.messaging__content').html(message);
            },
            fail: function (response) {
                doAction('hideModal', $modal);
                $messagingSection.addClass('messaging__section--show');
                $messaging.find('.messaging__content').html(response);
            }
        });
    });
});
