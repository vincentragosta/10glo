addAction(INIT, function() {
    var $sections = $('.external-links');
    if (!$sections.length) {
        return;
    }
    $sections.each(function (key, section) {
        var $links = $(section).find('a');
        $links.each(function (key, link) {
            var $link = $(link);
            if (!$link.attr('target')) {
                $link.attr('target', '_blank');
            }
        });
    });
});
