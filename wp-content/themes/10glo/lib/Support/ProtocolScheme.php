<?php

namespace ChildTheme\Support;

/**
 * Class ProtocolScheme
 * @package ChildTheme\Support
 * @author Vincent Ragosta <vincentpasqualeragosta@gmail.com>
 * @version 1.0
 */
class ProtocolScheme
{
    public static function addScheme($url, $scheme = 'http://')
    {
        return parse_url($url, PHP_URL_SCHEME) === null ? $scheme . $url : $url;
    }
}
