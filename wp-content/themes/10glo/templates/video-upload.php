<?php

use ChildTheme\Components\Messaging\MessagingView;
use ChildTheme\Options\GlobalOptions;
use ChildTheme\Playlist\PlaylistRepository;
use ChildTheme\Service\StripeService;
use ChildTheme\Service\UserMonetizationService;
use ChildTheme\User\User;

$CurrentUser = User::createFromCurrentUser();
?>

<section class="content-section content-section--width-narrow">
    <div class="content-section__container container">
        <div class="content-row row">
            <div class="content-column col-12">
                <div class="content-column__inner">
                    <?php if (
                        ($UserMonetizationService = new UserMonetizationService())->isEnabled($CurrentUser) && empty($stripe_account_id = $UserMonetizationService->getStripeIdFromVendor($CurrentUser)) ||
                        $UserMonetizationService->isEnabled($CurrentUser) && ($StripeService = new StripeService())->isAccountActive($UserMonetizationService->getAccountFromVendor($CurrentUser))
                    ): ?>
                    <div class="monetization-banner">
                        <h2 class="heading heading--medium">Howdy!</h2>
                        <p>If you’re interested in monetizing some or all of your content on 10glo, please first enroll in <a href="<?= $UserMonetizationService->getVendorMonetizeLink($CurrentUser); ?>">10glo’s Monetization Program</a>. Once you’re fully enrolled, you’ll see additional pricing settings on this page.</p>
                    </div>
                    <?php endif; ?>
                </div>
            </div>
        </div>
        <div class="content-row row messaging__section">
            <div class="content-column col-12">
                <div class="content-column__inner">
                    <?= new MessagingView(); ?>
                </div>
            </div>
        </div>
    </div>
</section>
<section class="content-section content-section--width-narrow">
    <div class="content-section__container container">
        <div class="content-row row">
            <div class="content-column col-12">
                <div class="content-column__inner">
                    <div class="video-upload">
                        <h2 class="video-upload__heading heading heading--large">Upload a Video</h2>
                        <p class="video-upload__text"><?= GlobalOptions::uploadVideoText(); ?></p>
                        <div class="upload-progress-bar__container">
                            <label for="upload-progress-bar">Upload progress:</label>
                            <progress id="upload-progress-bar" class="upload-progress-bar" value="0" max="100"></progress>
                        </div>
                        <form id="upload-video-form" class="video-upload__form upload-video-form" name="upload-video-form" action="" method="post" enctype="multipart/form-data">
                            <div class="upload-video-form__row">
                                <label for="video-title">Video Title</label>
                                <input type="text" class="upload-video-form__title" name="video-title" id="video-title"/>
                            </div>
                            <div class="upload-video-form__row">
                                <label for="upload-video-form__description">Video Description</label>
                                <?php wp_editor('', 'video-description', ['textarea_name' => 'upload-video-form__description', 'media_buttons' => false, 'quicktags' => false]); ?>
                                <small>Max length: 1500.</small>
                            </div>
                            <div class="upload-video-form__row upload-video-form__row--50">
                                <div class="upload-video-form__row-inner">
                                    <label for="upload-video-form__playlist">Playlists</label>
                                    <select name="upload-video-form__playlist" class="upload-video-form__playlist">
                                        <option value="">-- Select Playlist --</option>
                                        <?php foreach ((new PlaylistRepository())->findAllFromUser($CurrentUser) as $Playlist): ?>
                                            <option value="<?= $Playlist->ID; ?>"><?= $Playlist->post()->post_title; ?></option>
                                        <?php endforeach; ?>
                                    </select>
                                </div>
                                <div class="upload-video-form__row-inner">
                                    <label for="upload-video-form__category">Categories</label>
                                    <select name="upload-video-form__category" class="upload-video-form__category">
                                        <option value="">-- Select Category --</option>
                                        <?php foreach (get_categories(['hide_empty' => false, 'order' => 'ASC']) as $category): ?>
                                            <option value="<?= $category->term_id; ?>"><?= $category->name; ?></option>
                                        <?php endforeach; ?>
                                    </select>
                                </div>
                            </div>
                            <div class="upload-video-form__row">
                                <label for="upload-video-form__hashtags">Hashtags</label>
                                <input type="text" class="upload-video-form__hashtags" name="video-hashtags" id="video-hashtags"/>
                                <p class="upload-video-form__small"><small>Separate hashtags by using a comma. Please make sure to add the # before your tag. Spaces will be removed. <strong>Max 5 hashtags</strong>. If you’d like your video to appear in our BIPOC Voices playlist be sure to add the hashtag <strong>#bipocvoices</strong>.</small></p>
                            </div>
                            <div class="upload-video-form__row upload-video-form__row--50">
                                <button type="button" class="upload-video-form__video-button button button--secondary">Select Video File</button>
                                <input type="file" class="upload-video-form__video-file" name="video" id="video" />
                                <p class="upload-video-form__filename">No file uploaded</p>
                            </div>
                            <p class="upload-video-form__small"><small>10glo automatically generates a preview image for your video. If you'd like to upload a different image you can do so after upload by selecting Edit Video in the drop down menu below your video (hint: it looks like three vertical dots).</small></p>
                            <?php if ($UserMonetizationService->isVerified($CurrentUser)): ?>
                                <div class="upload-video-form__row" id="upload-video-form__monetization" style="background-color: rgba(var(--global__colors__secondary), .2); padding: var(--global__sizing__layout-spacing--half); border-radius: 15px;">
                                    <div class="upload-video-form__row--checkbox" style="margin-bottom: 0; display: flex; flex-flow: column nowrap;">
                                        <h3 class="heading heading--default">Monetization Options</h3>
                                        <p class="upload-video-form__small"><small>Your video will be free to watch by all users unless <strong>you select one, or both, options below</strong>.</small></p>
                                        <p class="upload-video-form__small"><small>If both boxes are selected, users who are not paying monthly members of your channel will be able to pay the one-time fee to watch this video.</small></p>
                                        <label class="upload-video-form__label"><input class="upload-video-form__one-time-fee" name="one-time-fee" type="checkbox">Charge users a one-time fee to watch this video</label>
                                        <div class="upload-video-form__one-time-fee-additional-options d-none" style="display: flex; flex-flow: column nowrap; width: 100%; padding: var(--global__sizing__layout-spacing--half) 0;">
                                            <label for="upload-video-form__video-price">Please enter the price you'd like to charge users to watch this video.</label>
                                            <div class="form-fields__prepend-icon">
                                                <span>$</span>
                                                <input type="text" class="upload-video-form__video-price" name="upload-video-form__video-price" id="upload-video-form__video-price" />
                                            </div>
                                        </div>
                                        <label class="upload-video-form__label"><input class="upload-video-form__monthly-members" name="monthly-members" type="checkbox"/>Make available to paying monthly members</label>
                                    </div>
                                </div>
                            <?php endif; ?>
                            <div class="upload-video-form__row upload-video-form__row--checkbox upload-video-form__row--bwayworldrecord">
                                <label class="upload-video-form__label"><input class="upload-video-form__bwayworldrecord" name="bwayworldrecord" type="checkbox">By uploading this video I agree that I have read and accept the <a href="<?= home_url('/bwayworldrecord-release-form/'); ?>" target="_blank">#BWAYWORLDRECORD release form.</a></label>
                            </div>
                            <input type="submit" class="upload-video-form__submit" value="Upload Video"/>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
