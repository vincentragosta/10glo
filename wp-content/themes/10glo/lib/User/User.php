<?php

namespace ChildTheme\User;

use Backstage\Models\PostBase;
use ChildTheme\PromoCode\PromoCode;

/**
 * Class User
 * @package ChildTheme\User
 * @author Vincent Ragosta <vincentpasqualeragosta@gmail.com>
 * @version 1.0
 */
class User extends \WP_User
{
    const USER_SCHOOL_META = 'user_school';
    const USER_LOCATION_META = 'user_location';
    const USER_PERSONAL_WEBSITE_URL_META = 'user_personal_website_url';
    const USER_FACEBOOK_URL_META = 'user_facebook_url';
    const USER_INSTAGRAM_URL_META = 'user_instagram_url';
    const USER_TWITTER_URL_META = 'user_twitter_url';
    const USER_BIO_META = 'user_bio';
    const USER_COVER_PHOTO = 'profile_cover_photo_url';
    const USER_COVER_PHOTO_COORDINATE = 'profile_cover_photo_coordinate';
    const USER_COVER_PHOTO_MOBILE_COORDINATE = 'profile_cover_photo_mobile_coordinate';
    const USER_PHOTO_META = 'user_photo_url';
//    const USER_FOLLOWERS_META = 'user_followers';
//    const USER_FOLLOWING_META = 'user_following';
    const USER_VERIFIED_META = 'user_verified';
    const USER_BROADWAY_PLUS_VIRTUAL_EXPERIENCE = 'user_broadway_plus_virtual_experience';
    const USER_PURCHASE_MY_SHEET_MUSICAL = 'user_purchase_my_sheet_musical';
    const USER_COUNTRY = 'user_country';
    const USER_PROMO_CODES = 'user_promo_codes';
    const ADMIN_ACCOUNT = '10gloadmin';

    public static function createFromCurrentUser()
    {
        $WP_User = wp_get_current_user();
        if ($WP_User->ID == 0) {
            return null;
        }
        return new static($WP_User->ID);
    }

    public static function createFromAuthor()
    {
        $Author = (get_query_var('author_name')) ? get_user_by('slug', get_query_var('author_name')) : get_userdata(get_query_var('author'));
        if ($Author->ID == 0 ) {
            return null;
        }
        return new static($Author->ID);
    }

    public static function createFromPost(PostBase $Post)
    {
        return $Post->post()->post_author ? new static($Post->post()->post_author) : null;
    }

    public static function createFromEmail(string $email)
    {
        if (!$User = get_user_by('email', $email)) {
            return false;
        }
        return new static($User);
    }

    public static function createFromUserLogin(string $user_login)
    {
        if (!$User = get_user_by('login', $user_login)) {
            return false;
        }
        return new static($User);
    }

    public static function createFromAdmin()
    {
        if (!$User = get_user_by('user_login', static::ADMIN_ACCOUNT)) {
            return false;
        }
        return new static($User);
    }

    public static function createFromUserId(string $user_id)
    {
        return new static($user_id);
    }

    public function isSameAs($User)
    {
        if (!$User instanceof User) {
            return false;
        }
        return $User->ID === $this->ID;
    }

    public function permalink()
    {
        return home_url(sprintf('/user/%s', $this->user_login));
    }

    public function isVerified()
    {
        return $this->getField(static::USER_VERIFIED_META, true);
    }

    // Remove

    public function removeProfileCoverPhotoCoordinate()
    {
        return $this->deleteField(static::USER_COVER_PHOTO_COORDINATE);
    }

    public function removeProfileCoverPhotoMobileCoordinate()
    {
        return $this->deleteField(static::USER_COVER_PHOTO_MOBILE_COORDINATE);
    }

    public function removeProfileCoverPhotoUrl()
    {
        return $this->deleteField(static::USER_COVER_PHOTO);
    }

    public function removeUserPhotoUrl()
    {
        return $this->deleteField(static::USER_PHOTO_META);
    }

    // Add

//    public function addToFollowingList(User $User)
//    {
//        $following = $this->getFollowing();
//        if (in_array($User->ID, $following)) {
//            return false;
//        }
//        if (empty($following)) {
//            $following = [$User->ID];
//        } else {
//            $following[] = $User->ID;
//        }
//        return $this->updateField(static::USER_FOLLOWING_META, json_encode($following));
//    }
//
//    public function follow(User $User)
//    {
//        $followers = $this->getFollowers();
//        if (!in_array($User->ID, $followers)) {
//            return false;
//        }
//        if (empty($followers)) {
//            $followers = [$User->ID];
//        } else {
//            $followers[] = $User->ID;
//        }
//        return $this->updateField(static::USER_FOLLOWERS_META, json_encode($followers));
//    }

    public function addPromoCode($promo_code)
    {
        if ($promo_code instanceof PromoCode) {
            $promo_code = $promo_code->ID;
        }
        if (empty($promo_code)) {
            return false;
        }
        $promo_codes = $this->getPromoCodes() ?: [];
        if (in_array($promo_code, $promo_codes) !== false) {
            return false;
        }
        $promo_codes[] = $promo_code;
        return $this->updatePromoCodes(json_encode($promo_codes));
    }

    // Update

    public function updateProfileCoverPhotoCoordinate($value)
    {
        if (empty($value)) {
            $value = 'center';
        }
        return $this->updateField(static::USER_COVER_PHOTO_COORDINATE, $value);
    }

    public function updateProfileCoverPhotoMobileCoordinate($value)
    {
        return $this->updateField(static::USER_COVER_PHOTO_MOBILE_COORDINATE, $value);
    }

    public function updateProfileCoverPhotoUrl($value)
    {
        return $this->updateField(static::USER_COVER_PHOTO, $value);
    }

    public function updateUserPhotoUrl($value)
    {
        return $this->updateField(static::USER_PHOTO_META, $value);
    }

    public function updateSchool($value)
    {
        return $this->updateField(static::USER_SCHOOL_META, $value);
    }

    public function updateLocation($value)
    {
        return $this->updateField(static::USER_LOCATION_META, $value);
    }

    public function updatePersonalWebsiteUrl($value)
    {
        return $this->updateField(static::USER_PERSONAL_WEBSITE_URL_META, $value);
    }

    public function updateFacebookUrl($value)
    {
        return $this->updateField(static::USER_FACEBOOK_URL_META, $value);
    }

    public function updateInstagramUrl($value)
    {
        return $this->updateField(static::USER_INSTAGRAM_URL_META, $value);
    }

    public function updateTwitterUrl($value)
    {
        return $this->updateField(static::USER_TWITTER_URL_META, $value);
    }

    public function updateBio($value)
    {
        return $this->updateField(static::USER_BIO_META, $value);
    }

    public function updatePromoCodes($value)
    {
        return $this->updateField(static::USER_PROMO_CODES, $value);
    }

    // Getters

    public function getDisplayName()
    {
        return $this->display_name ?: $this->user_login;
    }

    public function getProfileCoverPhotoCoordinate()
    {
        return $this->getField(static::USER_COVER_PHOTO_COORDINATE);
    }

    public function getProfileCoverPhotoMobileCoordinate()
    {
        return $this->getField(static::USER_COVER_PHOTO_MOBILE_COORDINATE);
    }

    public function getProfileCoverPhotoUrl()
    {
        return $this->getField(static::USER_COVER_PHOTO);
    }

    public function getUserPhotoUrl()
    {
        return $this->getField(static::USER_PHOTO_META);
    }

//    public function getFollowers()
//    {
//        return ($followers = $this->getField(static::USER_FOLLOWERS_META)) ? json_decode($followers) : [];
//    }
//
//    public function getFollowing()
//    {
//        return ($following = $this->getField(static::USER_FOLLOWING_META)) ? json_decode($following) : [];
//    }
//
//    public function getFollowersCount()
//    {
//        return count($this->getFollowers());
//    }
//
//    public function getFollowingCount()
//    {
//        return count($this->getFollowing());
//    }
//
//    public function getFollowingAsUsers()
//    {
//        if (empty($following_ids = $this->getFollowing())) {
//            return [];
//        }
//        return $this->getAsUsers($following_ids);
//    }
//
//    public function getFollowersAsUsers()
//    {
//        if (empty($follower_ids = $this->getFollowers())) {
//            return [];
//        }
//        return $this->getAsUsers($follower_ids);
//    }

    public function getSchool()
    {
        return $this->getField(static::USER_SCHOOL_META, true);
    }

    public function getLocation()
    {
        return $this->getField(static::USER_LOCATION_META, true);
    }

    public function getPersonalWebsiteUrl()
    {
        return $this->getField(static::USER_PERSONAL_WEBSITE_URL_META, true);
    }

    public function getFacebookUrl()
    {
        return $this->getField(static::USER_FACEBOOK_URL_META, true);
    }

    public function getInstagramUrl()
    {
        return $this->getField(static::USER_INSTAGRAM_URL_META, true);
    }

    public function getTwitterUrl()
    {
        return $this->getField(static::USER_TWITTER_URL_META, true);
    }

    public function getCountry()
    {
        return $this->getField(static::USER_COUNTRY, true);
    }

    public function getBio()
    {
        return $this->getField(static::USER_BIO_META, true);
    }

    public function getBroadwayPlusVirtualExperience()
    {
        return $this->getField(static::USER_BROADWAY_PLUS_VIRTUAL_EXPERIENCE, true);
    }

    public function getPurchaseMySheetMusical()
    {
        return $this->getField(static::USER_PURCHASE_MY_SHEET_MUSICAL, true);
    }

    public function getPromoCodes()
    {
        return ($promo_codes = $this->getField(static::USER_PROMO_CODES, true)) ? json_decode($promo_codes) : [];
    }

    // Has

    public function hasAboutAttributes()
    {
        return !empty($this->getBio()) || !empty($this->getLocation()) || !empty($this->getSchool()) || !empty($this->getPersonalWebsiteUrl()) || !empty($this->getFacebookUrl()) || !empty($this->getInstagramUrl()) || !empty($this->getTwitterUrl());
    }

    // Helpers

    public function deleteField(string $field)
    {
        return delete_user_meta($this->ID, $field);
    }

    public function updateField(string $meta, string $value)
    {
        return update_user_meta($this->ID, $meta, $value);
    }

    public function getField(string $field, bool $single_value = true)
    {
        return get_user_meta($this->ID, $field, $single_value);
    }

//    protected function getAsUsers(array $ids)
//    {
//        if (empty($ids)) {
//            return [];
//        }
//        return array_map(function($user_id) {
//            return new User($user_id);
//        }, $ids);
//    }
}
