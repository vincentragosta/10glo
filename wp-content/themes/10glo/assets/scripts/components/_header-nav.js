addAction(INIT, function() {
    var $headerNav = $('.header-nav');
    if (!$headerNav.length) {
        return;
    }
    var $search = $headerNav.find('.header-nav__list-item--search');
    if (!$search.length) {
        return;
    }
    var $input = $headerNav.find('.search-form__field');
    if (!$input.length) {
        return;
    }
    var SEARCH_TOGGLE_CLASS = 'header-nav--search-open';
    $search.on('click', function() {
        if (!$headerNav.hasClass(SEARCH_TOGGLE_CLASS)) {
            $headerNav.addClass(SEARCH_TOGGLE_CLASS);
        }
        $input[0].focus();
    });
    var $close = $headerNav.find('.search-form__close');
    if (!$close.length) {
        return;
    }
    $close.on('click', function() {
        if ($headerNav.hasClass(SEARCH_TOGGLE_CLASS)) {
            $headerNav.removeClass(SEARCH_TOGGLE_CLASS);
        }
    });
    var $hamburger = $headerNav.find('.header-nav__toggle');
    if (!$hamburger.length) {
        return;
    }
    var MOBILE_MENU_TOGGLE_CLASS = 'header-nav--mobile-menu-open';
    $hamburger.on('click', function() {
        $headerNav.toggleClass(MOBILE_MENU_TOGGLE_CLASS, !$headerNav.hasClass(MOBILE_MENU_TOGGLE_CLASS));
    });
    var $navItems = $headerNav.find('.header-nav__list-item-link');
    if (!$navItems.length) {
        return;
    }
    $navItems.each(function(index, value) {
        var href = $(value).attr('href');
        if (href && href.indexOf(window.location.pathname) !== -1 && window.location.pathname !== '/') {
            $(value).parent().addClass('header-nav__list-item--active');
        }
    });
    var $mobileNavItems = $headerNav.find('.header-nav__mobile-menu-list-item-link');
    if (!$mobileNavItems.length) {
        return;
    }
    $mobileNavItems.each(function(index, value) {
        var href = $(value).attr('href');
        if (href && href.indexOf(window.location.pathname) !== -1 && window.location.pathname !== '/') {
            $(value).parent().addClass('header-nav__mobile-menu-list-item--active');
        }
    });
});
