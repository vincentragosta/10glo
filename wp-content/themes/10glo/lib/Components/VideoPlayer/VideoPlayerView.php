<?php

namespace ChildTheme\Components\VideoPlayer;

use Backstage\View\Component;
use ChildTheme\User\User;
use ChildTheme\Video\Video;

/**
 * Class VideoPlayerView
 * @package ChildTheme\Components\VideoPlayer
 * @author Vincent Ragosta <vincentpasqualeragosta@gmail.com>
 * @version 1.0
 *
 * @property User $User
 * @property string $permalink
 * @property string $title
 * @property string $data_video_id
 * @property string $poster
 * @property string $url
 * @property string $type
 */
class VideoPlayerView extends Component
{
    const TITLE_MAX_CHAR_LENGTH = 75;
    const DEFAULT_TYPE = 'video/mp4';
    const PLAYER_SETTINGS = [
        'id' => 'video-player',
        'class' => 'vjs-custom vjs-big-play-centered',
        'controls' => '',
        'preload' => 'auto',
        'data-setup' => '{
            "fluid": true
        }'
    ];

    protected $name = 'video-player';
    protected static $default_properties = [
        'User' => null,
        'permalink' => '',
        'title' => '',
        'data-video-id' => '',
        'poster' => '',
        'url' => '',
        'type' => ''
    ];

    public function __construct(Video $Video, $autoplay = false)
    {
        parent::__construct([
            'User' => User::createFromPost($Video),
            'permalink' => $Video->permalink(),
            'title' => $Video->title(),
            'data_video_id' => $Video->ID,
            'poster' => $Video->poster_image_url,
            'url' => $Video->video_url,
            'type' => $Video->video_type ?: static::DEFAULT_TYPE
        ]);
        $player_settings = static::PLAYER_SETTINGS;
        if ($autoplay) {
            $player_settings['autoplay'] = true;
        }
        $this->elementAttributes(array_merge(
            $player_settings,
            ['data-video-id' => $this->data_video_id, 'poster' => $this->poster]
        ));
        if ($this->title && strlen($this->title) > 75) {
            $this->title = substr($this->title, 0, static::TITLE_MAX_CHAR_LENGTH);
            $this->title .= '...';
        }
    }
}
