addAction(INIT, function () {
    var $userPhotoForm = $('#user-photo-form');
    if (!$userPhotoForm.length) {
        return;
    }
    $userPhotoForm.submit(function (e) {
        e.preventDefault();

        var $messaging = $('.messaging');
        var $messagingSection = $messaging.parents('.messaging__section');
        var file = $('#user-photo')[0].files[0];
        var formData = new FormData();
        formData.append('file', file);
        formData.append('userId', sit.current_user_id);

        $.ajax({
            method: 'POST',
            url: sit.api_url + 'user/user-photo/create',
            data: formData,
            processData: false,
            contentType: false,
            beforeSend: function (xhr) {
                xhr.setRequestHeader('X-WP-Nonce', sit.nonce);
                xhr.setRequestHeader('Authorization', 'Basic ' + sit.basic_auth);
            },
            success: function (response) {
                $messagingSection.addClass('messaging__section--show');
                $messaging.find('.messaging__content').html(response.message);

                setTimeout(function () {
                    location.href = sit.profile_url;
                }, 500);
            },
            error: function (response) {
                var message = response.responseJSON.message ? response.responseJSON.message : response.message;
                $messagingSection.addClass('messaging__section--show');
                $messaging.find('.messaging__content').html(message);
            },
            fail: function (response) {
                $messagingSection.addClass('messaging__section--show');
                $messaging.find('.messaging__content').html(response);
            }
        });
    });
});
