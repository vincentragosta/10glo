<?php

namespace ChildTheme\Service;

use ChildTheme\Exception\StripeException;
use Stripe\Account;
use Stripe\Checkout\Session;
use Stripe\Customer;
use Stripe\PaymentIntent;
use Stripe\Price;
use Stripe\Product;
use Stripe\StripeClient;
use Stripe\Subscription;

/**
 * Class StripeService
 * @package ChildTheme\Service
 * @author Vincent Ragosta <vincentpasqualeragosta@gmail.com>
 * @version 1.0
 *
 * @property StripeClient $Stripe
 */
class StripeService
{
    // move to PremiumVideo?
    const PAID_STATUS = 'paid';
    const VALID_COUNTRIES = ['AU', 'AT', 'BE', 'BR', 'BG', 'CA', 'CY', 'CZ', 'DK', 'EE', 'FI', 'FR', 'DE', 'GR', 'HK', 'HU', 'IN', 'IE', 'IT', 'JP', 'LV', 'LT', 'LU', 'MT', 'MX', 'NL', 'NZ', 'NO', 'PL', 'PT', 'RO', 'SG', 'SK', 'SI', 'ES', 'SE', 'CH', 'GB', 'US'];

    protected $Stripe;

    public function __construct()
    {
        if (!defined('STRIPE_SECRET')) {
            throw new StripeException('The stripe secret is not set up correctly.', 400);
        }
        if (!defined('STRIPE_PUBLISHABLE_KEY')) {
            throw new StripeException('The stripe publishable key is not set up correctly.', 400);
        }
        $this->Stripe = new StripeClient(STRIPE_SECRET);
    }

    // Account

    public function isAccountValid($Account)
    {
        return $Account instanceof Account;
    }

    public function isAccountActive($Account)
    {
        return $this->areChargesAndPayoutsEnabled($Account) && $this->areDetailsSubmitted($Account);
    }

    public function areChargesAndPayoutsEnabled($Account)
    {
        if (!$this->isAccountValid($Account)) {
            return false;
        }
        /* @var Account $Account */
        return $Account->charges_enabled && $Account->payouts_enabled;
    }

    public function areDetailsSubmitted($Account)
    {
        if (!$this->isAccountValid($Account)) {
            return false;
        }
        /* @var Account $Account */
        return $Account->details_submitted;
    }

//    public function hasAccountFinishedOnboarding(Account $Account)
//    {
//        if (!$this->isAccountValid($Account)) {
//            return false;
//        }
//        return $Account->details_submitted;
//    }

    public function createAccount(array $config)
    {
        if (empty($config)) {
            return false;
        }
        return $this->Stripe->accounts->create($config);
    }

    public function getAccount(string $account_id)
    {
        if (empty($account_id)) {
            return false;
        }
        return $this->Stripe->accounts->retrieve($account_id);
    }

    public function getAccountLink(array $config)
    {
        if (empty($config)) {
            return false;
        }
        return $this->Stripe->accountLinks->create($config);
    }

    // Customer

    public function isCustomerValid($Customer)
    {
        return $Customer instanceof Customer;
    }

    public function createCustomer(array $config)
    {
        return $this->Stripe->customers->create($config);
    }

    // Product

    public function isProductValid($Product)
    {
        return $Product instanceof Product;
    }

    public function createProduct(string $name, array $config = [])
    {
        if (empty($name)) {
            return false;
        }
        return $this->Stripe->products->create(array_merge(compact('name'), $config));
    }

    public function getProduct(string $product_id)
    {
        if (empty($product_id)) {
            return false;
        }
        return $this->Stripe->products->retrieve($product_id);
    }

    public function updateProduct(Product $Product, array $config)
    {
        if (!$this->isProductValid($Product) || empty($config)) {
            return false;
        }
        return $this->Stripe->products->update($Product->id, $config);
    }

    // Price

    public function isPriceValid($Price)
    {
        return $Price instanceof Price;
    }

    public function createPrice(array $config)
    {
        if (empty($config)) {
            return false;
        }
        return $this->Stripe->prices->create($config);
    }

    public function getPrice(string $price_id)
    {
        if (empty($price_id)) {
            return false;
        }
        return $this->Stripe->prices->retrieve($price_id);
    }

    public function updatePrice(Price $Price, array $config)
    {
        if (!$this->isPriceValid($Price) || empty($config)) {
            return false;
        }
        return $this->Stripe->products->update($Price->id, $config);
    }

    // Session

    public function isSessionValid($Session)
    {
        return $Session instanceof Session;
    }

    // move to PremiumVideo?
    public function isCheckoutSessionStatusPaid(Session $Session)
    {
        return $Session->payment_status == static::PAID_STATUS;
    }

    public function createSession(array $config, array $options = [])
    {
        if (empty($config)) {
            return false;
        }
        return $this->Stripe->checkout->sessions->create($config, $options);
    }

    public function getSession(string $session_id)
    {
        if (empty($session_id)) {
            return false;
        }
        return $this->Stripe->checkout->sessions->retrieve($session_id);
    }

    // Payment Intent

    public function isPaymentIntentValid($PaymentIntent)
    {
        return $PaymentIntent instanceof PaymentIntent;
    }

    public function createPaymentIntent(array $config)
    {
        if (empty($config)) {
            return false;
        }
        return $this->Stripe->paymentIntents->create($config);
    }

    // Subscription

    public function isSubscriptionValid($Subscription)
    {
        return $Subscription instanceof Subscription;
    }

    public function createSubscription(array $config)
    {
        if (empty($config)) {
            return false;
        }
        return $this->Stripe->subscriptions->create($config);
    }

    public function updateSubscription(string $subscription_id, array $config = [])
    {
        if (empty($subscription_id)) {
            return false;
        }
        return $this->Stripe->subscriptions->update($subscription_id, $config);
    }
}
