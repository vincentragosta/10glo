<?php

use Backstage\Producers\RelatedContent\RelatedContentService;
use Backstage\SetDesign\Icon\IconView;
use Backstage\SetDesign\Modal\ModalView;
use Backstage\Util;
use ChildTheme\Advertisement\Advertisement;
use ChildTheme\Components\CommentCard\CommentCardView;
use ChildTheme\Components\Messaging\MessagingView;
use ChildTheme\Components\UserCard\UserCardView;
use ChildTheme\Components\VideoPosterCard\VideoPosterCardView;
use ChildTheme\Options\GlobalOptions;
use ChildTheme\Playlist\PlaylistRepository;
use ChildTheme\Service\PremiumVideoService;
use ChildTheme\Service\StripeService;
use ChildTheme\Service\UserMonetizationService;
use ChildTheme\Service\UserService;
use ChildTheme\Service\UserSubscribeService;
use ChildTheme\Support\NumberGenerator;
use ChildTheme\User\User;
use ChildTheme\Video\Video;
use ChildTheme\Video\VideoRepository;
use ChildTheme\Components\VideoPlayer\VideoPlayerView;

$StripeService = new StripeService();
$PremiumVideoService = new PremiumVideoService();

$Video = Video::createFromGlobal();
$CurrentUser = User::createFromCurrentUser();
$Author = User::createFromPost($Video);

// move this to VideoController
if (!$Author instanceof User) {
    echo 'The video author is not set for this video.';
    return;
}

$RelatedContentService = new RelatedContentService($Video, new VideoRepository(), get_option('posts_per_page'));
ModalView::load('share-video-modal', 'box', Util::getTemplateScoped('templates/components/share-video-modal.php', compact('Video')));
ModalView::load('video-support-modal', 'box', Util::getTemplateScoped('templates/components/video-support-modal', ['Vendor' => $Author]));

// move to webhook
if (!empty($session_id = $_GET['session_id'])) {
    $PremiumVideoService->setPaidVideoOnUserFromSession($StripeService->getSession($session_id), $Video);
}
?>

<div class="content-section">
    <div class="content-section__container container">
        <div class="content-row row">
            <div class="content-column col-12 col-sm-8">
                <div class="content-column__inner">
                    <div class="video-watch__messages messaging__section">
                        <?= new MessagingView(); ?>
                    </div>
                    <div class="video-watch__player-container">
                        <?php if (($PremiumVideoService->isPremiumVideo($Video) || $PremiumVideoService->isVideoMarkedForMonthlyMembers($Video)) && !$PremiumVideoService->canUserWatchVideo($Video, $CurrentUser)): ?>
                            <div class="video-watch__poster" data-video-id="<?= $Video->ID; ?>">
                                <img src="<?= Util::getAssetPath('images/logo.png'); ?>" class="video-watch__logo" />
                                <div class="video-watch__poster-image" style="background-image: url(<?= $Video->poster_image_url; ?>); filter: brightness(.25);"></div>
                                <?php if ($PremiumVideoService->isVideoMarkedForMonthlyMembers($Video) && !$PremiumVideoService->isPremiumVideo($Video)): ?>
                                    <div class="video-watch__disclaimer">
                                        <p>This video is available to members only.</p>
                                        <p>To become a member, click the link below.</p>
                                        <a href="#video-support-modal" class="button js-modal-trigger">Become a Member Now</a>
                                    </div>
                                <?php elseif ((new UserService())->isValidUser($CurrentUser) && $PremiumVideoService->isPremiumVideo($Video)): ?>
                                    <?php $CheckoutSession = (new PremiumVideoService())->createCheckoutSessionForPremiumVideo($Video); ?>
                                    <div class="video-watch__disclaimer">
                                        <p><?= $PremiumVideoService->getVideoPriceLabel($PremiumVideoService->getPriceFromVideo($Video)); ?></p>
                                        <p>This is a premium video on 10glo.com.</p>
                                        <p>Please click below to purchase and unlock this video for viewing.</p>
                                        <?php if ($StripeService->isSessionValid($CheckoutSession)): ?>
                                            <button class="video-watch__checkout-button button button--secondary" data-session-id="<?= $CheckoutSession->id; ?>" role="link" type="button">Pay To View</button>
                                        <?php endif; ?>
                                    </div>
                                <?php elseif (!(new UserService())->isValidUser($CurrentUser) && $PremiumVideoService->isPremiumVideo($Video)): ?>
                                    <div class="video-watch__disclaimer">
                                        <p><?= $PremiumVideoService->getVideoPriceLabel($PremiumVideoService->getPriceFromVideo($Video)); ?></p>
                                        <p>This is a premium video on 10glo.com.</p>
                                        <p>Please sign in to purchase this video.</p>
                                        <a href="/sign-in" class="button button--secondary">Sign In</a>
                                    </div>
                                <?php endif; ?>
                            </div>
                        <?php else: ?>
                            <?= new VideoPlayerView($Video, true); ?>
                        <?php endif; ?>
                        <div class="video-watch__activity-bar">
                            <div class="video-watch__views-date">
                                <h1 class="heading heading--medium"><?= $Video->title(); ?></h1>
                                <ul class="list--inline list--unstyled list--dotted">
                                    <li><?= NumberGenerator::generate($Video->views ?: 0); ?> views</li>
                                    <?php if (($Category = $Video->getPrimaryCategory()) instanceof WP_Term): ?>
                                        <li><a href="<?= get_term_link($Category); ?>"><?= $Category->name; ?></a></li>
                                    <?php endif; ?>
                                    <li><?= $Video->publishedDate('F j, Y'); ?></li>
                                </ul>
                            </div>
                            <div class="video-watch__interactions">
                                <ul class="video-watch__interactions-list list--inline">
                                    <li><button class="video-watch__rate button--unstyled button--with-icon" data-video-id="<?= $Video->ID; ?>"><?= new IconView(['icon_name' => 'thumbs-up']); ?><?= NumberGenerator::generate($Video->likes_count); ?></button></li>
                                    <li><a href="#share-video-modal" class="video-watch__share button--unstyled button--with-icon js-modal-trigger"><?= new IconView(['icon_name' => 'share']); ?>Share</a></li>
                                    <?php if ($Author->isSameAs($CurrentUser)): ?>
                                        <?php
                                        $playlists = array_filter((new PlaylistRepository())->findAllFromUser($CurrentUser), function($Playlist) use ($Video) {
                                            return !in_array($Video->ID, $Playlist->video_ids);
                                        });
                                        ?>
                                        <?php if (!empty($playlists)): ?>
                                            <?php ModalView::load('add-to-playlist-modal', 'box', Util::getTemplateScoped('templates/components/add-to-playlist-modal.php', ['playlists' => $playlists, 'video_id' => $Video->ID])); ?>
                                            <li><a href="#add-to-playlist-modal" class="video-watch__playlist button--unstyled button--with-icon js-modal-trigger"><?= new IconView(['icon_name' => 'create-playlist']); ?>Save</a></li>
                                        <?php endif; ?>
                                    <?php endif; ?>
                                    <li><button class="video-watch__ellipsis button--unstyled"><?= new IconView(['icon_name' => 'ellipsis']); ?></button></li>
                                </ul>
                                <ul class="video-watch__video-list dropdown-list" data-dropdown-activator=".video-watch__ellipsis">
                                    <?php if ($Author->isSameAs($CurrentUser)): ?>
                                        <?php ModalView::load('edit-video-modal', 'box', Util::getTemplateScoped('templates/components/edit-video-modal.php', compact('Video'))); ?>
                                        <?php ModalView::load('video-remove-modal', 'box', Util::getTemplateScoped('templates/components/video-remove-modal.php', compact('Video'))); ?>
                                        <li><a href="<?= sprintf('%sedit', $Video->permalink()); ?>"><?= new IconView(['icon_name' => 'cog']); ?> Edit Video</a></li>
                                        <li><a href="#video-remove-modal"><?= new IconView(['icon_name' => 'trash-can']); ?> Remove Video</a></li>
                                    <?php else: ?>
                                        <?php ModalView::load('report-video-modal', 'box', Util::getTemplateScoped('templates/components/report-video-modal.php')); ?>
                                        <li><a href="#report-video-modal"><?= new IconView(['icon_name' => 'flag']); ?> Report Video</a></li>
                                    <?php endif; ?>
                                </ul>
                            </div>
                        </div>
                    </div>
                    <hr class="video-watch__hr" />
                    <div class="video-watch__author-bio-container">
                        <div class="video-watch__author-bio">
                            <?= new UserCardView($Author->getUserPhotoUrl(), $Author->permalink()); ?>
                            <div class="video-watch__nameplate">
                                <strong><a href="<?= $Author->permalink(); ?>"><?= $Author->getDisplayName(); ?></a></strong>
                                <?php if ($Author->isVerified() && ($verified_artists_image = GlobalOptions::verifiedArtistsImage())): ?>
                                    <?= $verified_artists_image; ?>
                                <?php endif; ?>
                            </div>
                        </div>
                        <div class="video-watch__interactions">
                            <?php if (($UserMonetizationService = new UserMonetizationService())->isVerified($Author)): ?>
                                <a href="#video-support-modal" class="button js-modal-trigger">Support</a>
                            <?php endif; ?>
                            <?php if (!(new UserService())->isSameUser($CurrentUser, $Author) && !(new UserSubscribeService())->isUserFollowing($CurrentUser, $Author)): ?>
                                <button class="subscribe-to-user button button--secondary" data-user-id="<?= $Author->ID; ?>">Subscribe</button>
                            <?php endif; ?>
                        </div>
                    </div>
                    <?php if (!empty($hashtags = $Video->terms('hashtags'))): ?>
                        <div class="video-watch__hashtags">
                            <?php foreach($hashtags as $Hashtag): ?>
                                <a href="<?= get_term_link($Hashtag); ?>" class="button"><?= $Hashtag->name; ?></a>
                            <?php endforeach; ?>
                        </div>
                    <?php endif; ?>
                    <?php if (!empty($description = $Video->content(false))): ?>
                        <div class="video-watch__video-description external-links">
                            <?= $description; ?>
                        </div>
                    <?php endif; ?>
                    <div class="video-watch__comments-form">
                        <?php if ($CurrentUser instanceof User): ?>
                            <?php comment_form([
                                'logged_in_as' => '',
                                'title_reply' => 'Drop a comment'
                            ], $Video->ID); ?>
                        <?php else: ?>
                            <a href="<?= GlobalOptions::loginPage()->permalink(); ?>">Sign-In</a> or <a href="<?= GlobalOptions::registrationPage()->permalink(); ?>">Create an Account</a> to leave a comment.
                        <?php endif; ?>
                    </div>
                    <div class="video-watch__comments col-reset-exclude">
                        <?php if (!empty($comments = get_comments(['post_id' => $Video->ID]))): ?>
                            <?php foreach($comments as $Comment): ?>
                                <div class="video-watch__comments-row row">
                                    <div class="video-watch__comments-column col-12">
                                        <?= new CommentCardView($Comment); ?>
                                    </div>
                                </div>
                            <?php endforeach; ?>
                        <?php endif; ?>
                    </div>
                </div>
            </div>
            <div class="content-column col-12 col-sm-4">
                <div class="content-column__inner">
                    <?php if (!($RelatedContentCollection = $RelatedContentService->getRelatedContent())->isEmpty()): ?>
                        <h2 class="heading heading--medium">Up Next</h2>
                        <?php foreach($RelatedContentCollection as $key => $Video): ?>
                            <?= (new VideoPosterCardView($Video))->classModifiers('horizontal'); ?>
                            <?php if ($key == 0): ?>
                                <hr />
                            <?php elseif ($key == 2 && ($Advertisement = GlobalOptions::dvpUpcomingVideoAdvertisement()) instanceof Advertisement): ?>
                                <script async src="https://pagead2.googlesyndication.com/pagead/js/adsbygoogle.js"></script>
                                <!-- Dedicated Video Page - Up Next -->
                                <ins class="adsbygoogle"
                                     style="display:block"
                                     data-ad-client="ca-pub-4103352350078783"
                                     data-ad-slot="9938946846"
                                     data-ad-format="auto"
                                     data-full-width-responsive="true"></ins>
                                <script>
                                    (adsbygoogle = window.adsbygoogle || []).push({});
                                </script>
                            <?php endif; ?>
                        <?php endforeach; ?>
                    <?php endif; ?>
                </div>
            </div>
        </div>
    </div>
</div>
