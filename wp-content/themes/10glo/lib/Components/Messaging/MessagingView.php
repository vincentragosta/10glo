<?php

namespace ChildTheme\Components\Messaging;

use Backstage\View\Component;

/**
 * Class MessagingView
 * @package ChildTheme\Components\Messaging
 * @author Vincent Ragosta <vincentpasqualeragosta@gmail.com>
 * @version 1.0
 *
 * @var string $message
 */
class MessagingView extends Component
{
    protected $name = 'messaging';
    protected static $default_properties = [
        'message' => ''
    ];

    public function __construct(string $message = '')
    {
        parent::__construct(compact('message'));
    }
}
