<?php

namespace ChildTheme\Exception;

use Exception;

/**
 * Class PhotoException
 * @package ChildTheme\Exception
 * @author Vincent Ragosta <vincentpasqualeragosta@gmail.com>
 * @version 1.0
 */
class PhotoException extends Exception {}
