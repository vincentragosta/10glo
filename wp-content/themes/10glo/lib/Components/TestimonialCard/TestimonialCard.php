<?php

namespace ChildTheme\Components\TestimonialCard;

use Backstage\VcLibrary\Support\Component;
use ChildTheme\Testimonial\Testimonial;
use ChildTheme\Testimonial\TestimonialRepository;

/**
 * Class TestimonialCard
 * @package ChildTheme\Components\TestimonialCard
 * @author Vincent Ragosta <vincentpasqualeragosta@gmail.com>
 * @version 1.0
 */
class TestimonialCard extends Component
{
    const NAME = 'Testimonial Card';
    const TAG = 'testimonial_card';
    const VIEW = TestimonialCardView::class;

    protected $component_config = [
        'description' => 'Display a testimonial.',
        'icon' => 'icon-wpb-toggle-small-expand',
        'wrapper_class' => 'clearfix',
        'is_container' => false,
        'category' => 'Content',
        'params' => [
            [
                'type' => 'textfield',
                'heading' => 'Testimonial ID',
                'param_name' => 'testimonial_id',
                'description' => 'Enter the Testimonial ID.',
                'admin_label' => true
            ]
        ]
    ];

    protected function createView(array $atts)
    {
        if (!($Testimonial = (new TestimonialRepository())->findById($atts['testimonial_id'])) instanceof Testimonial) {
            return '';
        }
        $ViewClass = self::VIEW;
        return new $ViewClass($Testimonial);
    }
}
