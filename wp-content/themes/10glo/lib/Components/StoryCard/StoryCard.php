<?php

namespace ChildTheme\Components\StoryCard;

use Backstage\VcLibrary\Support\Component;
use ChildTheme\Story\NewsPostRepository;
use ChildTheme\Story\NewsPost;

/**
 * Class StoryCard
 * @package ChildTheme\Components\StoryCard
 * @author Vincent Ragosta <vincentpasqualeragosta@gmail.com>
 * @version 1.0
 */
class StoryCard extends Component
{
    const NAME = 'Story Card';
    const TAG = 'story_card';
    const VIEW = StoryCardView::class;

    protected $component_config = [
        'description' => 'Display a story.',
        'icon' => 'icon-wpb-toggle-small-expand',
        'wrapper_class' => 'clearfix',
        'is_container' => false,
        'category' => 'Content',
        'params' => [
            [
                'type' => 'textfield',
                'heading' => 'Story ID',
                'param_name' => 'story_id',
                'description' => 'Enter the Story ID.',
                'admin_label' => true
            ]
        ]
    ];

    protected function createView(array $atts)
    {
        if (!($Story = (new NewsPostRepository())->findById($atts['story_id'])) instanceof NewsPost) {
            return '';
        }
        $ViewClass = self::VIEW;
        return new $ViewClass($Story);
    }
}
