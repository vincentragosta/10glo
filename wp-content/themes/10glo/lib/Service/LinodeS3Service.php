<?php

namespace ChildTheme\Service;

use Aws\Credentials\Credentials;
use Aws\S3\S3Client;

/**
 * Class LinodeS3Service
 * @package ChildTheme\Service
 * @author Vincent Ragosta <vincentpasqualeragosta@gmail.com>
 * @version 1.0
 *
 * @property S3Client $S3
 */
class LinodeS3Service
{
    const PHOTO_FILE_ARCHITECTURE = 'photos/%s/%s';
    const VIDEO_FILE_ARCHITECTURE = 'videos/%s/%s/%s';
    const LINODE_BUCKET_HOSTNAME = 'https://10glo.us-east-1.linodeobjects.com/';
    const BWAYWORLDRECORD_VIDEO_FILE_ARCHITECTURE = 'bwayworldrecord/videos/%s/%s/%s';
    const BWAYWORLDRECORD_PHOTO_FILE_ARCHITECTURE = 'bwayworldrecord/photos/%s/%s/%s';

    protected $S3;

    public function __construct()
    {
        $this->S3 = new S3Client([
            'region' => LINODE_REGION,
            'version' => 'latest',
            'credentials' => new Credentials(LINODE_ACCESS_KEY, LINODE_ACCESS_SECRET),
            'endpoint' => LINODE_ENDPOINT
        ]);
    }

    public function deleteFile(string $filename)
    {
        if (static::hasHostInFileName($filename)) {
            $filename = static::removeHostFromFileName($filename);
        }
        return $this->S3->deleteObject([
            'Bucket' => LINODE_BUCKET,
            'Key' => $filename
        ]);
    }

    public function getObjectUrl(string $filename)
    {
        return $this->S3->getObjectUrl(LINODE_BUCKET, $filename);
    }

    public function putFile(string $filename, $source_file)
    {
        return $this->S3->putObject([
            'Bucket' => LINODE_BUCKET,
            'Key' => $filename,
            'SourceFile' => $source_file
        ]);
    }

    public static function getPhotoArchitecture(string $user_id, string $filename)
    {
        return sprintf(
            static::PHOTO_FILE_ARCHITECTURE,
            $user_id,
            str_replace(' ', '', $filename)
        );
    }

    public static function getVideoArchitecture(string $user_id, string $filename, string $override_basename = '')
    {
        return sprintf(
            static::VIDEO_FILE_ARCHITECTURE,
            $user_id,
            $override_basename ?
                pathinfo(str_replace(' ', '', $override_basename), PATHINFO_FILENAME):
                pathinfo(str_replace(' ', '', $filename), PATHINFO_FILENAME),
            $filename
        );
    }

    public static function getBWAYWORLDRECORDPhotoArchitecture(string $user_id, string $filename)
    {
        return sprintf(
            static::BWAYWORLDRECORD_PHOTO_FILE_ARCHITECTURE,
            $user_id,
            str_replace(' ', '', $filename)
        );
    }

    public static function getBWAYWORLDRECORDVideoArchitecture(string $user_id, string $filename, string $override_basename = '')
    {
        return sprintf(
            static::BWAYWORLDRECORD_VIDEO_FILE_ARCHITECTURE,
            $user_id,
            $override_basename ?
                pathinfo(str_replace(' ', '', $override_basename), PATHINFO_FILENAME):
                pathinfo(str_replace(' ', '', $filename), PATHINFO_FILENAME),
            $filename
        );
    }

    public static function hasHostInFileName(string $filename)
    {
        return strpos($filename, self::LINODE_BUCKET_HOSTNAME) !== -1;
    }

    public static function removeHostFromFilename(string $filename)
    {
        return str_replace(self::LINODE_BUCKET_HOSTNAME, '', $filename);
    }
}
