<?php

namespace ChildTheme\Traits;

use ChildTheme\Exception\VideoException;
use \WP_REST_Request;

/**
 * Trait GetVideoFileFromRequest
 * @package ChildTheme\Traits
 */
trait GetVideoFileFromRequest
{
    protected function getVideoFileFromRequest(WP_REST_Request $request, string $key = 'file')
    {
        if (empty($files = $request->get_file_params())) {
            throw new VideoException('No files were sent in the request.', 400);
        }
        if (empty($file = $files[$key])) {
            throw new VideoException('No files were sent in the request.', 400);
        }
        if ($file['error'] > 0) {
            throw new VideoException('There was an error with the submission.', 400);
        }
        if ($file['size'] > (static::MB * 1500) && empty($request->get_param('premiumVideo'))) {
            throw new VideoException('Right now 10glo is only accepting files up to 1.5GB. If you would like to upload a larger file please email help at <a href="mailto:hello@10glo.com">hello@10glo.com</a>.', 400);
        } elseif ($file['size'] > (static::MB * 2500) && !empty($request->get_param('premiumVideo'))) {
            throw new VideoException('Right now 10glo is only accepting files up to 2.5GB. If you would like to upload a larger file please email help at <a href="mailto:hello@10glo.com">hello@10glo.com</a>.', 400);
        }
        if (empty($file['tmp_name'])) {
            throw new VideoException('The file has no source location on the server', 400);
        }
        if (!strstr(mime_content_type($file['tmp_name']), 'video/') && !strstr(mime_content_type($file['tmp_name']), 'audio/mp4')) {
            throw new VideoException('The file type is not a video.', 400);
        }
        if (empty($file['name'])) {
            throw new VideoException('The file has no filename.', 400);
        }
        return $file;
    }
}
