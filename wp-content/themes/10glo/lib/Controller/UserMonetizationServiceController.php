<?php

namespace ChildTheme\Controller;

use Backstage\Models\Page;
use Backstage\Rewrite\QueryRewrite;
use ChildTheme\Options\GlobalOptions;
use ChildTheme\Service\UserMonetizationService;
use ChildTheme\User\User;

/**
 * Class UserMonetizationServiceController
 * @package ChildTheme\Controller
 * @author Vincent Ragosta <vincentpasqualeragosta@gmail.com>
 * @version 1.0
 */
class UserMonetizationServiceController
{
    public function __construct()
    {
        new QueryRewrite('user/(.+)/monetize/?$', ['related_user' => '$1', 'monetize' => true]);
        new QueryRewrite('user/(.+)/payment-settings/?$', ['related_user' => '$1', 'payment_settings' => true]);
        add_filter('body_class', [$this, 'addBodyClass']);
        add_filter('template_include', [$this, 'templateInclude'], 99);
        add_action('template_redirect', [$this, 'templateRedirect']);
        add_action('show_user_profile', [$this, 'customUserProfileFields']);
        add_action('edit_user_profile', [$this, 'customUserProfileFields']);
        add_action('personal_options_update', [$this, 'saveCustomUserProfileFields']);
        add_action('edit_user_profile_update', [$this, 'saveCustomUserProfileFields']);
    }

    public function addBodyClass($classes)
    {
        global $wp_query;
        if ($wp_query->get('related_user') && $wp_query->get('monetize')) {
            $classes = array_merge($classes, ['page-template-monetize']);
        }
        if ($wp_query->get('related_user') && $wp_query->get('payment_settings')) {
            $classes = array_merge($classes, ['page-template-payment-settings']);
        }
        return $classes;
    }

    public function templateInclude($template)
    {
        global $wp_query;
        if ($wp_query->get('related_user') && $wp_query->get('monetize')) {
            $new_template = locate_template(['templates/monetize.php']);
            if ('' != $new_template) {
                return $new_template;
            }
        }
        if ($wp_query->get('related_user') && $wp_query->get('payment_settings')) {
            $new_template = locate_template(['templates/payment-settings.php']);
            if ('' != $new_template) {
                return $new_template;
            }
        }
        return $template;
    }

    public function templateRedirect()
    {
        global $wp_query;
        if (
            $wp_query->get('related_user') && $wp_query->get('monetize') && (!GlobalOptions::monetizeYourVideosPage() instanceof Page || !(new UserMonetizationService())->isEnabled(User::createFromCurrentUser())) ||
            $wp_query->get('related_user') && $wp_query->get('payment_settings') && (!GlobalOptions::paymentSettingsPage() instanceof Page || !(new UserMonetizationService())->isEnabled(User::createFromCurrentUser()))
        ) {
            wp_safe_redirect(home_url());
            exit();
        }
    }

    public function customUserProfileFields($WPUser)
    {
        $User = new User($WPUser); ?>
        <table class="form-table">
        <tr>
            <th>
                <label for="user_full_monetization"><?php _e('Enable Full Monetization (Beta)'); ?></label>
            </th>
            <td>
                <input type="checkbox" name="user_full_monetization" id="user_full_monetization"
                       class="regular-text" <?= (new UserMonetizationService())->isEnabled($User) ? 'checked' : ''; ?> />
            </td>
        </tr>
        </table><?php
    }

    public function saveCustomUserProfileFields($user_id)
    {
        $User = User::createFromUserId($user_id);
        $User->updateField(UserMonetizationService::USER_MONETIZATION_META, sanitize_text_field($_POST[UserMonetizationService::USER_MONETIZATION_META]));
    }
}
