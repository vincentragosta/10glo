<?php

namespace ChildTheme\Mailchimp;

use ChildTheme\User\User;

/**
 * Class MailchimpListMember
 * @package ChildTheme\Mailchimp
 * @author Vincent Ragosta <vincentpasqualeragosta@gmail.com>
 * @version 1.0
 *
 * @property string $email_address
 * @property string $display_name
 * @property string $status
 * @property array $merge_fields
 */
class MailchimpListMember
{
    protected $email_address;
    protected $display_name;
    protected $status;
    protected $merge_fields;

    public function __construct(string $email_address, string $display_name = '', string $status = 'subscribed', array $merge_fields = [])
    {
        $this->email_address = $email_address;
        $this->display_name = $display_name;
        $this->status = $status;
        $this->merge_fields = $merge_fields;
    }

    public function getEmailAddress()
    {
        return $this->email_address;
    }

    public function getDisplayName()
    {
        return $this->display_name;
    }

    public function getStatus()
    {
        return $this->status;
    }

    public function getMergeFields()
    {
        return $this->merge_fields;
    }

    public function getSubscriberHash()
    {
        return md5($this->email_address);
    }

    public static function createFromUser(User $User)
    {
        return new static($User->user_email);
    }
}
