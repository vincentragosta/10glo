<?php

use Backstage\SetDesign\Icon\IconView;
use Backstage\SetDesign\Modal\ModalView;
use Backstage\Util;
use Backstage\View\Link;
use ChildTheme\Components\UserCard\UserCardView;
use ChildTheme\Options\GlobalOptions;
use ChildTheme\Service\UserMonetizationService;
use ChildTheme\User\User;

$CurrentUser = User::createFromCurrentUser();
?>

<header class="header-nav" data-gtm="Header">
    <div class="header-nav__container container">
        <ul class="header-nav__list">
            <li class="header-nav__list-item header-nav__list-item--hamburger">
                <button class="header-nav__toggle" aria-label="Toggle navigation">
                    <span class="header-nav__toggle-bar" aria-hidden="true"></span>
                </button>
            </li>
            <li class="header-nav__list-item header-nav__list-item--logo">
                <a class="header-nav__list-item-link" href="<?= home_url() ?>" title="10glo Home">
                    <?php if (($image = GlobalOptions::headerBrandImage()) instanceof WP_Image): ?>
                        <?= $image->css_class('header-nav__brand-image') ?>
                    <?php else: ?>
                        <strong><?php bloginfo('name'); ?></strong>
                    <?php endif; ?>
                </a>
            </li>
            <?php if (has_nav_menu('primary_navigation')): ?>
                <?php foreach(wp_get_nav_menu_items('Primary Navigation') as $NavItem): ?>
                    <?php $title = ($NavItem->post_title ? $NavItem->post_title : ($NavItem->title ? $NavItem->title : '')); ?>
                    <li class="header-nav__list-item">
                        <a class="header-nav__list-item-link" href="<?= $NavItem->url; ?>" <?= $NavItem->target ? 'target="_blank"' : ''; ?>><?= $title; ?></a>
                    </li>
                <?php endforeach; ?>
            <?php endif; ?>
            <li class="header-nav__list-item header-nav__list-item--search">
                <?= new IconView(['icon_name' => 'search']); ?>
            </li>
            <li class="header-nav__list-item header-nav__list-item--cta">
                <?php if ($CurrentUser instanceof User): ?>
                    <?php ModalView::load('upload-video-modal', 'box', Util::getTemplateScoped('templates/components/upload-video-modal.php', ['Author' => $CurrentUser])); ?>
                    <?php if ($is_user_monetization_service_enabled = ($UserMonetizationService = new UserMonetizationService())->isEnabled($CurrentUser)): ?>
                        <a href="<?= $UserMonetizationService->getVendorMonetizeLink($CurrentUser); ?>" class="monetize-my-videos">💳</a>
                    <?php endif; ?>
                    <a href="<?= home_url('video/upload/'); ?>" class="header-nav__create-video" title="Create">
                        <?= new IconView(['icon_name' => 'video-upload']); ?>
                    </a>
                    <div class="header-nav__user-profile">
                        <?= new UserCardView($CurrentUser->getUserPhotoUrl()); ?>
                        <ul class="header-nav__user-settings dropdown-list" data-dropdown-activator=".header-nav .user-card">
                            <li>
                                <ul class="header-nav__user-settings-sublist list--unstyled">
                                    <li><?= $CurrentUser->getDisplayName(); ?></li>
                                    <li><?= strlen($user_email = $CurrentUser->user_email) > 20 ? sprintf('%s...', substr($CurrentUser->user_email, 0, 20)) : $user_email; ?></li>
                                </ul>
                            </li>
                            <li><a href="<?= home_url('/profile/'); ?>">Profile</a></li>
                            <li><a href="<?= home_url('/edit-profile/'); ?>">Edit Profile</a></li>
                            <?php if ($is_user_monetization_service_enabled): ?>
                                <li><a href="<?= $UserMonetizationService->getVendorMonetizeLink($CurrentUser); ?>">Monetize My Videos</a></li>
                                <?php if ($UserMonetizationService->isVerified($CurrentUser)): ?>
                                    <li><a href="<?= $UserMonetizationService->getVendorPaymentSettingsLink($CurrentUser); ?>">Manage Monetization</a></li>
                                <?php endif; ?>
                            <?php endif; ?>
                            <li><a href="<?= home_url(sprintf('/user/%s/subscribers/', $CurrentUser->user_login)); ?>">My Subscriptions</a></li>
                            <li><a href="<?= home_url('updates-bugs'); ?>">Updates & Bugs</a></li>
                            <li><a href="<?= home_url('faqs'); ?>">FAQs</a></li>
                            <li><a href="<?= wp_logout_url(home_url()); ?>">Sign out</a></li>
                        </ul>
                    </div>
                <?php elseif ($cta = GlobalOptions::callToAction()):
                    $cta['title'] = new IconView(['icon_name' => 'user']) . $cta['title'];
                    $classes = ['header-nav__cta-link', 'button--with-icon', get_theme_mod('header__cta__link-style', 'button')];
                    echo Link::createFromField($cta)->class($classes);
                endif; ?>
            </li>
        </ul>
        <div class="header-nav__search-form">
            <form role="search" method="get" class="search-form" action="<?= esc_url(home_url('/')); ?>">
                <label class="search-form__label">
                    <span class="screen-reader-text"><?= _x('Search for:', 'label'); ?></span>
                    <?= new IconView(['icon_name' => 'search']); ?>
                    <input type="search" class="search-form__field" placeholder="<?= esc_attr_x('Find a creator you love', 'placeholder'); ?>" value="<?= get_search_query(); ?>" name="s" />
                    <button type="button" class="search-form__close"><?= new IconView(['icon_name' => 'close']); ?></button>
                </label>
            </form>
        </div>
    </div>
    <div class="header-nav__mobile-menu">
        <ul class="header-nav__mobile-menu-list">
            <li class="header-nav__mobile-menu-list-item">
                <div class="header-nav__mobile-search-form">
                    <form role="search" method="get" class="search-form" action="<?= esc_url(home_url('/')); ?>">
                        <label class="search-form__label">
                            <span class="screen-reader-text"><?= _x('Search for:', 'label'); ?></span>
                            <?= new IconView(['icon_name' => 'search']); ?>
                            <input type="search" class="search-form__field" placeholder="<?= esc_attr_x('Find a creator you love', 'placeholder'); ?>" value="<?= get_search_query(); ?>" name="s" />
                            <button type="button" class="search-form__close"><?= new IconView(['icon_name' => 'close']); ?></button>
                        </label>
                    </form>
                </div>
            </li>
            <?php if (has_nav_menu('primary_navigation')): ?>
                <?php foreach(wp_get_nav_menu_items('Primary Navigation') as $NavItem): ?>
                    <?php $title = ($NavItem->post_title ? $NavItem->post_title : ($NavItem->title ? $NavItem->title : '')); ?>
                    <li class="header-nav__mobile-menu-list-item">
                        <a class="header-nav__mobile-menu-list-item-link" href="<?= $NavItem->url; ?>" <?= $NavItem->target ? 'target="_blank"' : ''; ?>><?= $title; ?></a>
                    </li>
                <?php endforeach; ?>
            <?php endif; ?>
        </ul>
    </div>
</header>
