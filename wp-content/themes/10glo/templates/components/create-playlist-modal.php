<?php
/**
 * Expected:
 * @var User $User
 */

use ChildTheme\Options\GlobalOptions;
use ChildTheme\User\User;
use ChildTheme\Video\Video;
use ChildTheme\Video\VideoRepository;
?>

<div class="create-playlist-modal">
    <h2 class="create-playlist-modal__heading heading heading--large">Let's Create A Playlist</h2>
    <p class="create-playlist-modal__playlist-text"><?= GlobalOptions::createPlaylistText(); ?></p>
    <form class="create-playlist-modal__form create-playlist-form" id="create-playlist-form" name="create-user-form" method="post">
        <p class="create-playlist-form__playlist-name">
            <label class="create-playlist-form__label" for="playlist-name">Playlist Name</label>
            <input type="text" name="playlist-name" id="playlist-name" class="input" size="20"
                   required>
        </p>
        <?php if (!empty($videos = (new VideoRepository())->findAllByAuthor($User))): ?>
            <div class="create-playlist-form__videos-container">
                <h2 class="heading heading--default">Add videos:</h2>
                <div class="create-playlist-form__videos">
                    <?php foreach($videos as $Video): /** @var Video $Video */?>
                        <label class="create-playlist-form__label">
                            <input class="create-playlist-form__video-id" name="video-ids[]" type="checkbox" value="<?= $Video->ID; ?>"><?= $Video->title(); ?></label>
                    <?php endforeach; ?>
                </div>
            </div>
        <?php endif; ?>
        <div class="create-playlist-form__submit">
            <input type="submit" name="wp-submit" id="wp-submit" class="create-playlist-form__submit-button button button-primary" value="Create Playlist" />
        </div>
    </form>
</div>
