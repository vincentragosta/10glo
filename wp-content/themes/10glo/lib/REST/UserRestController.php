<?php

namespace ChildTheme\Rest;

use Backstage\Controller\RestController;
use ChildTheme\Exception\UserException;
use ChildTheme\Exception\UserLoginException;
use ChildTheme\Exception\UserRegistrationException;
use ChildTheme\Exception\UserUpdateException;
use ChildTheme\Mailchimp\MailchimpList;
use ChildTheme\Mailchimp\MailchimpListMember;
use ChildTheme\PromoCode\PromoCode;
use ChildTheme\PromoCode\PromoCodeRepository;
use ChildTheme\Service\EmailNotificationService;
use ChildTheme\Service\MailchimpService;
use ChildTheme\Service\UserSubscribeService;
use ChildTheme\Traits\GetCurrentUserFromRequest;
use ChildTheme\User\User;
use PHPMailer\PHPMailer\Exception;
use \WP_REST_Request;

/**
 * Class UserRestController
 * @package ChildTheme\Controller
 * @author Vincent Ragosta <vincentpasqualeragosta@gmail.com>
 * @version 1.0
 *
 * @property string $namespace
 */
class UserRestController extends RestController
{
    const MB = 1048576;

    protected $namespace = 'user';

    use GetCurrentUserFromRequest;

    public function registerRoutes()
    {
        $this->addCreateRoute('/register', [$this, 'registerUser']);
        $this->addCreateRoute('/update', [$this, 'updateUser']);
        $this->addCreateRoute('/login', [$this, 'loginUser']);
        $this->addCreateRoute('/subscribe', [$this, 'subscribe']);
    }

    public function registerUser(WP_REST_Request $request)
    {
        $fields = $this->validateRegistrationFields($request->get_params());
        $PromoCodeRepository = new PromoCodeRepository();

        $PromoCode = null;
        if (!empty($promo_code = $request->get_param('promo_code')) && !($PromoCode = $PromoCodeRepository->findByCode($promo_code)) instanceof PromoCode) {
            throw new UserException('The promo code you have entered does not match anything in our records. Please register with a different promo code.', 400);
        }

        $user_id = wp_insert_user($fields);
        if (is_wp_error($user_id)) {
            return $user_id;
        }
        $User = new User($user_id);

        if (!empty($promo_code) && $PromoCode instanceof PromoCode) {
            /* @var PromoCode $PromoCode */
            $assigned_user_ids = $PromoCode->assigned_user_ids ?: [];
            $assigned_user_ids[] = $User->ID;
            $PromoCode->assigned_user_ids = $assigned_user_ids;
            $PromoCodeRepository->add($PromoCode);
            $User->addPromoCode($PromoCode);
//            if (!$PromoCodeRepository->add($PromoCode)) {
//                throw new UserException('There was a problem assigning the user ID to the promo code.', 400);
//            }
//            if (!(new User)->addPromoCode($promo_code)) {
//                throw new UserException('There was a problem assigning the promo code to the user.', 400);
//            }
        }

        $this->loginFromUser($User);
        (new MailchimpService())->addToList(MailchimpList::createFromConfig(), new MailchimpListMember($fields['user_email'], $fields['user_login']));
        return ['message' => 'Welcome to 10glo! You have successfully created an account.', 'status' => 200];
    }

    public function updateUser(WP_REST_Request $request)
    {
        $fields = $this->validateUpdateFields($request->get_params());
        $user_id = wp_update_user($fields);
        if (is_wp_error($user_id)) {
            return $user_id;
        }
        $User = new User($user_id);
        if (!empty($location = $request->get_param('location'))) {
            $User->updateLocation($location);
        }
        if (!empty($school = $request->get_param('school'))) {
            $User->updateSchool($school);
        }
        if (!empty($personal_website_url = $request->get_param('personalWebsiteUrl'))) {
            $User->updatePersonalWebsiteUrl($personal_website_url);
        }
        if (!empty($facebook_url = $request->get_param('facebookUrl'))) {
            $User->updateFacebookUrl($facebook_url);
        }
        if (!empty($instagram_url = $request->get_param('instagramUrl'))) {
            $User->updateInstagramUrl($instagram_url);
        }
        if (!empty($twitter_url = $request->get_param('twitterUrl'))) {
            $User->updateTwitterUrl($twitter_url);
        }
        if (!empty($country = $request->get_param('country'))) {
            $User->updateField(User::USER_COUNTRY, $country);
        }
        if (!empty($bio = $request->get_param('bio'))) {
            $User->updateBio($bio);
        }
        return ['message' => 'You have successfully updated your account.', 'status' => 200];
    }

    public function loginUser(WP_REST_Request $request)
    {
        $fields = $this->validateLoginFields($request->get_params());
        $WP_User = wp_signon($fields);
        if (is_wp_error($WP_User)) {
            // filter login_errors did not work -- remove whenever you bake out lost password template
            if ($WP_User->get_error_code() == 'incorrect_password') {
                return new \WP_Error('incorrect_password', '<strong>Error:</strong> The password you entered is incorrect.');
            }
            return $WP_User;
        }
        $this->loginFromUser(new User($WP_User));
        return ['message' => 'The user has successfully logged in.', 'status' => 200];
    }

    public function subscribe(WP_REST_Request $request)
    {
        $CurrentUser = $this->getCurrentUserFromRequest($request, 'Please login or create an account to subscribe to this user.');
        $Author = $this->getAuthorFromRequest($request);
        if (!(new UserSubscribeService())->subscribe($CurrentUser, $Author)) {
            throw new UserException('You could not subscribe to the user at this time.', 400);
        }
        // add this try catch to the send method of EmailNotificationService
        try {
            (new EmailNotificationService($Author->user_email, 'A new user subscribed to your channel', 'Cue the (virtual) applause. You\'ve got a new subscriber on 10glo.<br />Keep up the good work!'))->send();
        } catch(Exception $e) {
            error_log($e->getMessage());
        }
        return ['message' => 'You have successfully subscribed to this user.', 'status' => 200];
    }

    protected function validateRegistrationFields(array $fields)
    {
        if (empty(trim($fields['user_login']))) {
            throw new UserRegistrationException('The username was not sent in the request.', 400);
        }
        if (empty(trim($fields['user_pass']))) {
            throw new UserRegistrationException('The password was not sent in the request.', 400);
        }
        if (empty(trim($fields['user_email']))) {
            throw new UserRegistrationException('The email was not sent in the request.', 400);
        }
        if (empty(trim($fields['display_name']))) {
            throw new UserRegistrationException('The display name was not sent in the request.', 400);
        }
        unset($fields['promo_code']);
        return $fields;
    }

    protected function validateUpdateFields($fields)
    {
        if (empty(trim($fields['ID']))) {
            throw new UserUpdateException('The user id was not sent in the request.', 400);
        }
        if (empty(trim($fields['user_login']))) {
            throw new UserUpdateException('The username was not sent in the request.', 400);
        }
        if (empty(trim($fields['user_email']))) {
            throw new UserUpdateException('The email was not sent in the request.', 400);
        }
        if (empty(trim($fields['display_name']))) {
            throw new UserUpdateException('The display name was not sent in the request.', 400);
        }
        return $fields;
    }

    protected function validateLoginFields($fields)
    {
        if (empty(trim($fields['user_login']))) {
            throw new UserLoginException('The username or email was not sent in the request.', 400);
        }
        if (empty(trim($fields['user_password']))) {
            throw new UserLoginException('The user password was not sent in the request.', 400);
        }
        return $fields;
    }

    protected function loginFromUser(User $User)
    {
        wp_set_current_user($User->ID, $User->user_login);
        wp_set_auth_cookie($User->ID);
        do_action('wp_login', $User->user_login);
    }

    protected function getAuthorFromRequest(WP_REST_Request $request)
    {
        if (empty($author_id = $request->get_param('authorId'))) {
            throw new UserException('You have to be logged in to send this request.', 400);
        }
        return new User($author_id);
    }
}
