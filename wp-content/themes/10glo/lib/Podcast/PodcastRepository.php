<?php

namespace ChildTheme\Podcast;

use Backstage\Repositories\PostRepository;

/**
 * Class PodcastRepository
 * @package ChildTheme\Podcast
 * @author Vincent Ragosta <vincentpasqualeragosta@gmail.com>
 * @version 1.0
 */
class PodcastRepository extends PostRepository
{
    const DEFAULT_PAGE = 1;
    const DEFAULT_POSTS_PER_PAGE = 4;
    protected $model_class = Podcast::class;

    public function findByTitle(string $title)
    {
        return $this->find(['title' => $title]);
    }

    public function findCurated(array $atts)
    {
        $args = [
            'post_status' => 'publish',
            'paged' => $atts['paged'] ?: self::DEFAULT_PAGE,
            'posts_per_page' => $atts['posts_per_page'] ?: static::DEFAULT_POSTS_PER_PAGE
        ];
        return $this->find($args) ?: [];
    }
}
