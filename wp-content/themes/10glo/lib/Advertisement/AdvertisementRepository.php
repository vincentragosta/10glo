<?php

namespace ChildTheme\Advertisement;

use Backstage\Repositories\PostRepository;

/**
 * Class AdvertisementRepository
 * @package ChildTheme\Advertisement
 * @author Vincent Ragosta <vincentpasqualeragosta@gmail.com>
 * @version 1.0
 */
class AdvertisementRepository extends PostRepository
{
    protected $model_class = Advertisement::class;
}
