<?php
use Backstage\SetDesign\Icon\IconView;
use Backstage\SetDesign\SocialIcons\SocialIcon;
use Backstage\Util;
use ChildTheme\Options\GlobalOptions;
?>

<div class="content-section content-section--width-narrow">
    <div class="content-section__container container">
        <div class="content-row row">
            <div class="content-column col-12 text--center">
                <div class="content-column__inner">
                    <a href="<?= home_url() ?>" title="10glo Home">
                        <img src="<?= Util::getAssetPath('images/logo-1080.png'); ?>" />
                    </a>
                    <?php if (!empty($newsletter_text = GlobalOptions::newsletterPageText())): ?>
                        <?= $newsletter_text; ?>
                    <?php endif; ?>
                    <div id="mc_embed_signup">
                        <form action="https://10glo.us18.list-manage.com/subscribe/post?u=de96fd6fd543851355f1008df&amp;id=7bc9b17910" method="post" id="mc-embedded-subscribe-form" name="mc-embedded-subscribe-form" class="validate" target="_blank" novalidate>
                            <input type="email" value="" name="EMAIL" class="required email" id="mce-EMAIL" placeholder="Email Address">
                            <div style="position: absolute; left: -5000px;" aria-hidden="true"><input type="text" name="b_de96fd6fd543851355f1008df_7bc9b17910" tabindex="-1" value=""></div>
                            <input type="submit" value="Subscribe" name="subscribe" id="mc-embedded-subscribe" class="button">
                        </form>
                    </div>
                    <script type='text/javascript' src='//s3.amazonaws.com/downloads.mailchimp.com/js/mc-validate.js'></script>
                    <script type='text/javascript'>(function($) {window.fnames = new Array(); window.ftypes = new Array();fnames[0]='EMAIL';ftypes[0]='email';fnames[1]='RH_CODE';ftypes[1]='text';fnames[2]='RH_REFLINK';ftypes[2]='text';fnames[3]='RH_SUBID';ftypes[3]='text';fnames[4]='RH_ISREF';ftypes[4]='text';fnames[5]='RH_TOTREF';ftypes[5]='number';fnames[6]='RH_LASTREF';ftypes[6]='date';fnames[7]='RH_ISSHARE';ftypes[7]='text';}(jQuery));var $mcj = jQuery.noConflict(true);</script>
<!--                    <form id="newsletter-form" class="footer-nav__form newsletter-form" name="newsletter-form" action="" method="post" enctype="multipart/form-data">-->
<!--                        <p class="newsletter-form__container">-->
<!--                            <input type="email" class="newsletter-form__email" name="newsletter-form__email" id="newsletter-form__email" placeholder="Email Address" />-->
<!--                            <input type="submit" class="newsletter-form__submit button" value="Subscribe" />-->
<!--                        </p>-->
<!--                    </form>-->
                    <ul class="list--inline">
                        <?php
                        if (!empty($social_icons = GlobalOptions::socialIcons())): ?>
                            <?php foreach($social_icons as $SocialIcon): /* @var SocialIcon $SocialIcon */ ?>
                                <li>
                                    <a href="<?= $SocialIcon->getUrl(); ?>" class="link-icon--<?= $SocialIcon->getName(); ?>" target="<?= $SocialIcon->getTarget(); ?>">
                                        <?= new IconView(['icon_name' => $SocialIcon->getName(), 'style' => 'dark']); ?>
                                    </a>
                                </li>
                            <?php endforeach; ?>
                        <?php endif; ?>
                    </ul>
                </div>
            </div>
        </div>
    </div>
</div>
