// addAction(INIT, function() {
//     $('#reposition-user-photo-form').submit(function(e) {
//         e.preventDefault();
//
//         var $messaging = $('.messaging');
//         var $messagingSection = $messaging.parents('.messaging__section');
//         var $modal = $('#reposition-user-photo-modal');
//
//         $.ajax({
//             method: 'POST',
//             url: sit.api_url + 'user/user-photo/reposition',
//             data: {
//                 userId: sit.current_user_id,
//                 direction: $('.reposition-user-photo-form__direction option:selected').val()
//             },
//             beforeSend: function (xhr) {
//                 xhr.setRequestHeader('X-WP-Nonce', sit.nonce);
//                 xhr.setRequestHeader('Authorization', 'Basic ' + sit.basic_auth);
//             },
//             success: function (response) {
//                 doAction('hideModal', $modal);
//                 $messagingSection.addClass('messaging__section--show');
//                 $messaging.find('.messaging__content').html(response.message);
//
//                 setTimeout(function() {
//                     location.href = sit.profile_url;
//                 }, 500);
//             },
//             error: function (response) {
//                 doAction('hideModal', $modal);
//                 $messagingSection.addClass('messaging__section--show');
//                 $messaging.find('.messaging__content').html(response.data);
//             },
//             fail: function (response) {
//                 doAction('hideModal', $modal);
//                 $messagingSection.addClass('messaging__section--show');
//                 $messaging.find('.messaging__content').html(response.data);
//             }
//         });
//     });
// });
