addAction(INIT, function() {
    var $monetizePaymentsStart = $('#monetize-payments-start');
    var $monetizePayments = $('#monetize-payments');
    var $messaging = $('.messaging');
    var $messagingSection = $messaging.parents('.messaging__section');
    if ($monetizePaymentsStart.length && $monetizePayments.length) {
        $monetizePaymentsStart.on('click', function (e) {
            e.preventDefault();
            var $sections = $('.content-section:not(#monetize-hero):not(#monetize-payments)');

            $sections.each(function (index, value) {
                var $value = $(value);
                if (!$value.hasClass('d-none')) {
                    $(value).addClass('d-none');
                }
            });

            if ($monetizePayments.hasClass('d-none')) {
                $monetizePayments.removeClass('d-none');
            }
        });
    }

    var $prevButton = $('.monetize-payments__prev');
    var $nextButton = $('.monetize-payments__next');
    if ($nextButton.length && $prevButton.length) {
        $prevButton.on('click', function (e) {
            e.preventDefault();
            var index = parseInt($(this).attr('data-step-index'));
            var nextIndex = parseInt($nextButton.attr('data-step-index'));
            $('.monetize-payments__step[data-step-index="' + (index+1) + '"]').addClass('d-none');
            $('.monetize-payments__step[data-step-index="' + index + '"]').removeClass('d-none');

            $(this).attr('data-step-index', (index - 1) < 0 ? 0 : index - 1);
            $nextButton.attr('data-step-index', (nextIndex - 1)).removeClass('d-none');

            if ((index - 1) <= 0 && !$(this).hasClass('d-none')) {
                $(this).addClass('d-none');
            }
        });
        $nextButton.on('click', function (e) {
            e.preventDefault();
            var index = parseInt($(this).attr('data-step-index'));
            var prevIndex = parseInt($prevButton.attr('data-step-index'));
            $('.monetize-payments__step[data-step-index="' + (index-1) + '"]').addClass('d-none');
            $('.monetize-payments__step[data-step-index="' + index + '"]').removeClass('d-none');

            // console.log(index);
            // console.log(!$('.monetize-payments__step[data-step-index="' + index + '"]').hasClass('monetize-payments__step--complete'));
            // console.log(!$(this).hasClass('d-none'));
            if (!$('.monetize-payments__step[data-step-index="' + index + '"]').hasClass('monetize-payments__step--complete') && !$(this).hasClass('d-none')) {
                // console.log('in here');
                $(this).addClass('d-none');
            }

            $(this).attr('data-step-index', index + 1);
            $prevButton.attr('data-step-index', (prevIndex + 1)).removeClass('d-none');

            if ((index + 1) > 5 && !$(this).hasClass('d-none')) {
                // console.log("what");
                $(this).addClass('d-none');
            }
        });
    }

    var $confirmMaterialOwnership = $('#confirm-material-ownership');
    if ($confirmMaterialOwnership.length) {
        $confirmMaterialOwnership.submit(function(e) {
            e.preventDefault();
            $('html, body').css('cursor', 'wait');

            var formData = new FormData();
            formData.append('userId', sit.current_user_id);

            $.ajax({
                method: 'POST',
                url: sit.api_url + 'user/monetize/confirm-material-ownership',
                data: formData,
                processData: false,
                contentType: false,
                beforeSend: function (xhr) {
                    xhr.setRequestHeader('X-WP-Nonce', sit.nonce);
                    xhr.setRequestHeader('Authorization', 'Basic ' + sit.basic_auth);
                },
                success: function (response) {
                    $('html, body').css('cursor', 'auto');
                    $confirmMaterialOwnership.find('input[type="submit"]').prop('disabled', 'disabled');
                    $confirmMaterialOwnership.parents('.monetize-payments__step').addClass('monetize-payments__step--complete');
                    if ($nextButton.hasClass('d-none')) {
                        $nextButton.removeClass('d-none');
                    }
                },
                error: function (response) {
                    $('html, body').css('cursor', 'auto');
                },
                fail: function (response) {
                    $('html, body').css('cursor', 'auto');
                }
            });
        });
    }

    var $confirmMonetizationPolicies = $('#confirm-monetization-policies');
    if ($confirmMonetizationPolicies.length) {
        $confirmMonetizationPolicies.submit(function(e) {
            e.preventDefault();
            $('html, body').css('cursor', 'wait');

            var formData = new FormData();
            formData.append('userId', sit.current_user_id);

            $.ajax({
                method: 'POST',
                url: sit.api_url + 'user/monetize/confirm-monetization-policies',
                data: formData,
                processData: false,
                contentType: false,
                beforeSend: function (xhr) {
                    xhr.setRequestHeader('X-WP-Nonce', sit.nonce);
                    xhr.setRequestHeader('Authorization', 'Basic ' + sit.basic_auth);
                },
                success: function (response) {
                    $('html, body').css('cursor', 'auto');
                    $confirmMonetizationPolicies.find('input[type="submit"]').prop('disabled', 'disabled');
                    $confirmMonetizationPolicies.parents('.monetize-payments__step').addClass('monetize-payments__step--complete');
                    if ($nextButton.hasClass('d-none')) {
                        $nextButton.removeClass('d-none');
                    }
                },
                error: function (response) {
                    $('html, body').css('cursor', 'auto');
                },
                fail: function (response) {
                    $('html, body').css('cursor', 'auto');
                }
            });
        });
    }

    var $supportMessageForm = $('#add-support-message');
    if ($supportMessageForm.length) {
        $supportMessageForm.submit(function(e) {
            e.preventDefault();

            var formData = new FormData();
            var content = tinymce.get('user-support-message').getContent().trim();
            if (!content.length) {
                $("html, body").animate({ scrollTop: 0 }, "slow");
                $messagingSection.addClass('messaging__section--show');
                $messaging.find('.messaging__content').html('You must enter a support message to get to the next step.');
                return;
            }
            formData.append('supportMessage', content);
            formData.append('userId', sit.current_user_id);

            $.ajax({
                method: 'POST',
                url: sit.api_url + 'user/monetize/support-message',
                data: formData,
                processData: false,
                contentType: false,
                beforeSend: function (xhr) {
                    xhr.setRequestHeader('X-WP-Nonce', sit.nonce);
                    xhr.setRequestHeader('Authorization', 'Basic ' + sit.basic_auth);
                },
                success: function (response) {
                    $('html, body').css('cursor', 'auto');
                    $supportMessageForm.find('input[type="submit"]').prop('disabled', 'disabled');
                    $supportMessageForm.parents('.monetize-payments__step').addClass('monetize-payments__step--complete');
                    if ($nextButton.hasClass('d-none')) {
                        $nextButton.removeClass('d-none');
                    }
                },
                error: function (response) {
                    $('html, body').css('cursor', 'auto');
                },
                fail: function (response) {
                    $('html, body').css('cursor', 'auto');
                }
            });
        });
    }

    var $userCountry = $('#user_country');
    if ($userCountry.length) {
        $userCountry.on('change', function() {
            $('html, body').css('cursor', 'wait');

            var formData = new FormData();
            formData.append('country', $('#user_country option:selected').val());
            formData.append('userId', sit.current_user_id);

            $.ajax({
                method: 'POST',
                url: sit.api_url + 'user/monetize/confirm-country',
                data: formData,
                processData: false,
                contentType: false,
                beforeSend: function (xhr) {
                    xhr.setRequestHeader('X-WP-Nonce', sit.nonce);
                    xhr.setRequestHeader('Authorization', 'Basic ' + sit.basic_auth);
                },
                success: function (response) {
                    $('html, body').css('cursor', 'auto');
                    $userCountry.prop('disabled', 'disabled');
                    $userCountry.parents('.monetize-payments__step').addClass('monetize-payments__step--complete');
                    if ($nextButton.hasClass('d-none')) {
                        $nextButton.removeClass('d-none');
                    }
                },
                error: function (response) {
                    $('html, body').css('cursor', 'auto');
                },
                fail: function (response) {
                    $('html, body').css('cursor', 'auto');
                }
            });
        });
    }
});
