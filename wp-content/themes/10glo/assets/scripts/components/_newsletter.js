addAction(INIT, function () {
    var $newsletterForm = $('#newsletter-form');
    if (!$newsletterForm.length) {
        return;
    }
    $newsletterForm.submit(function (e) {
        e.preventDefault();

        var $messaging = $('.messaging');
        var $messagingSection = $messaging.parents('.messaging__section');
        var $modal = $('#newsletter-modal');
        var email = $('#newsletter-form__email').val();

        if (!email) {
            $messagingSection.addClass('messaging__section--show');
            $messaging.find('.messaging__content').html('Please enter an email address for the newsletter sign-up.');
            return;
        }

        $.ajax({
            method: 'POST',
            url: sit.api_url + 'mailchimp/add',
            data: {
                emailAddress: email
            },
            beforeSend: function (xhr) {
                xhr.setRequestHeader('X-WP-Nonce', sit.nonce);
                xhr.setRequestHeader('Authorization', 'Basic ' + sit.basic_auth);
            },
            success: function (response) {
                doAction('hideModal', $modal);
                setTimeout(function () {
                    location.href = sit.home_url;
                }, 500);
            },
            error: function (response) {
                doAction('hideModal', $modal);
                var message = response.responseJSON.message ? response.responseJSON.message : response.message;
                $messagingSection.addClass('messaging__section--show');
                $messaging.find('.messaging__content').html(message);
            },
            fail: function (response) {
                doAction('hideModal', $modal);
                $messagingSection.addClass('messaging__section--show');
                $messaging.find('.messaging__content').html(response);
            }
        });
    });
});
