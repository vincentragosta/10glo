<?php

namespace ChildTheme\Components\CommentCard;

use Backstage\Support\DateTime;
use Backstage\View\Component;
use ChildTheme\Support\RelativeFormatConverter;
use ChildTheme\User\User;

/**
 * Class CommentCardView
 * @package ChildTheme\Components\CommentCard
 * @author Vincent Ragosta <vincentpasqualeragosta@gmail.com>
 * @version 1.0
 *
 * @property User $Author
 * @property string $date
 * @property string $content
 */
class CommentCardView extends Component
{
    protected $name = 'comment-card';
    protected static $default_properties = [
        'Author' => null,
        'date' => '',
        'content' => ''
    ];

    public function __construct(\WP_Comment $Comment)
    {
        parent::__construct([
            'Author' => ($Author = User::createFromEmail($Comment->comment_author_email)),
            'date' => (new RelativeFormatConverter(strtotime($Comment->comment_date)))->getRelativeTime(),
            'content' => $Comment->comment_content
        ]);
    }
}
