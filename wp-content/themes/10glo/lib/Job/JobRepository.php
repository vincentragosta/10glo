<?php

namespace ChildTheme\Job;

use Backstage\Repositories\PostRepository;

/**
 * Class JobRepository
 * @package ChildTheme\Job
 * @author Vincent Ragosta <vincentpasqualeragosta@gmail.com>
 * @version 1.0
 */
class JobRepository extends PostRepository
{
    protected $model_class = Job::class;

    public function findCurated(array $atts)
    {
        $args = [
            'posts_per_page' => $atts['posts_per_page'] ?: get_option('posts_per_page'),
          ];
        return $this->find($args) ?: [];
    }

  }
