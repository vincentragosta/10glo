<?php

namespace ChildTheme\Components\JobCard;

use Backstage\View\Component;
use ChildTheme\Job\Job;
use ChildTheme\Support\RelativeFormatConverter;
use ChildTheme\User\User;
use ChildTheme\SocialSharer\FacebookSocialSharer;
use ChildTheme\SocialSharer\TwitterSocialSharer;

/**
 * Class JobCardView
 * @package ChildTheme\Components\JobCard
 * @author Michael Miranda <m.miranda426@gmail.com>
 * @version 1.0
 *
 * @property string $title
 * @property User $Author
 * @property string $content
 * @property string $relative_published_time
 * @property string $url
 * @property string $facbook
 * @property string $twitter
 */
class JobCardView  extends Component
{
    protected $name = 'job-card';
    protected static $default_properties = [
        'title' => '',
        'Author' => '',
        'content' => '',
        'relative_published_time' => '',
        'url' => '',
        'facebook' => '',
        'twitter' => ''
    ];

    public function __construct(Job $JobsPost)
    {
        $args = [
            'title' => $JobsPost->title(),
            'Author' => User::createFromPost($JobsPost),
            'content' => $JobsPost->excerpt(),
            'url' => $JobsPost->permalink(),
            'facebook' => (new FacebookSocialSharer($JobsPost))->getUrl(),
            'twitter' => (new TwitterSocialSharer($JobsPost))->getUrl()
        ];
        if (($PublishedTime = new RelativeFormatConverter(strtotime($JobsPost->publishedDate())))->isLessThanAnHour()) {
            $args['relative_published_time'] = $PublishedTime->getRelativeTime();
        }
        parent::__construct($args);
    }
}
