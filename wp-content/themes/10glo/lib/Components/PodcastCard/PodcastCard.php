<?php

namespace ChildTheme\Components\PodcastCard;

use Backstage\VcLibrary\Support\Component;
use ChildTheme\Podcast\PodcastRepository;
use ChildTheme\Podcast\Podcast;

/**
 * Class PodcastCard
 * @package ChildTheme\Components\PodcastCard
 * @author Vincent Ragosta <vincentpasqualeragosta@gmail.com>
 * @version 1.0
 */
class PodcastCard extends Component
{
    const NAME = 'Podcast Card';
    const TAG = 'podcast_card';
    const VIEW = PodcastCardView::class;

    protected $component_config = [
        'description' => 'Display a podcast.',
        'icon' => 'icon-wpb-toggle-small-expand',
        'wrapper_class' => 'clearfix',
        'is_container' => false,
        'category' => 'Content',
        'params' => [
            [
                'type' => 'textfield',
                'heading' => 'Podcast ID',
                'param_name' => 'podcast_id',
                'description' => 'Enter the Podcast ID.',
                'admin_label' => true
            ],
            [
                'type' => 'checkbox',
                'heading' => 'Options',
                'param_name' => 'options',
                'value' => [
                    'Increase Heading Size' => 'increase-heading-size',
                    'Decrease Heading Size' => 'decrease-heading-size',
                    'Hide Content' => 'hide-content'
                ],
                'description' => 'Check the relevant options'
            ]
        ]
    ];

    protected function createView(array $atts)
    {
        if (!($Podcast = (new PodcastRepository())->findById($atts['podcast_id'])) instanceof Podcast) {
            return '';
        }
        $ViewClass = self::VIEW;
        return new $ViewClass($Podcast);
    }
}
