addAction(INIT, function () {
    if (navigator.share !== undefined && $(window).width() <= 575) {
        var $nativeShareButton = $('.social-sharer-card--more');

        $nativeShareButton.click(function (e) {
            e.preventDefault();

            navigator.share({
                title: document.title,
                //text: 'Check out this video on 10glo.com!',
                url: window.location.href
            });
        });
    }
});
