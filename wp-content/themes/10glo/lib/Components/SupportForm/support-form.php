<?php
/**
 * Expected:
// * @var string $heading_text
 * @var string $vendor_id
 * @var bool $is_submit_disabled
 * @var array $class_modifiers
 * @var array $element_attributes
 */

use Backstage\Util;

$element_attributes['id'] = 'support-form';
?>

<form <?= Util::componentAttributes('support-form', $class_modifiers, $element_attributes); ?>>
    <fieldset class="support-form__fieldset">
        <div class="support-form__radio-container">
            <label><input name="support-form__amount" type="radio" value="500" checked>$5</label>
            <label><input name="support-form__amount" type="radio" value="1000">$10</label>
            <label><input name="support-form__amount" type="radio" value="1500">$15</label>
        </div>
        <div class="form-fields__prepend-icon">
            <span>$</span>
            <label><input type="text" name="support-form__custom-amount" placeholder="20" /></label>
        </div>
    </fieldset>
    <fieldset class="support-form__fieldset">
        <div>
            <label for="support-form__name">Name</label>
            <input id="support-form__name" name="support-form__name" placeholder="Jane Doe" />
        </div>
        <div>
            <label for="support-form__email">Email</label>
            <input id="support-form__email" name="support-form__email" placeholder="janedoe@gmail.com" />
        </div>
        <div>
            <label for="support-form__message">Message</label>
            <textarea type="text" id="support-form__message" name="support-form__message">Say something nice!</textarea>
        </div>
    </fieldset>
    <button type="button" class="button button--block" id="support-form-toggle">Support $5</button>
    <fieldset class="support-form__fieldset d-none" id="support-form-submit-container">
        <div id="support-credit-card"></div>
        <input type="hidden" name="support-form__author-id" value="<?= $vendor_id; ?>" />
        <input type="submit" value="Support $5" <?= $is_submit_disabled ? 'disabled' : ''; ?>/>
    </fieldset>
</form>
