<?php

namespace ChildTheme\Components\UserCard;

use Backstage\View\Component;
use Backstage\View\Element;
use Backstage\View\Link;
use ChildTheme\User\User;

/**
 * Class UserCardView
 * @package ChildTheme\Components\UserCard
 * @author Vincent Ragosta <vincentpasqualeragosta@gmail.com>
 * @version 1.0
 *
 * @property string $image_url
 * @property string $url
 * @property string $heading
 * @property bool $is_verified
 */
class UserCardView extends Component
{
    protected $name = 'user-card';
    protected static $default_properties = [
        'image_url' => '',
        'url' => '',
        'heading' => '',
        'is_verified' => false
    ];

    public function __construct(string $image_url, string $url = '', string $heading = '')
    {
        parent::__construct(compact('image_url', 'url', 'heading'));
        if ($this->heading) {
            $this->heading = Element::create('h3', $this->url ? new Link($this->url, $this->heading) : $this->heading, ['class' => 'user-card__heading heading heading--default']);
        }
    }

    public static function createFromUser(User $User)
    {
        $View = new static($User->getUserPhotoUrl(), $User->permalink(), $User->getDisplayName());
        return $View->toggleVerifiedImage($User->isVerified());
    }

    public function toggleVerifiedImage($verified = false)
    {
        $this->is_verified = $verified;
        return $this;
    }
}
