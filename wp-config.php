<?php
//define('WP_DEBUG', true);
// ==============================
// Composer autoloader if present
// ==============================
if (file_exists(__DIR__ . '/wp-content/vendor/autoload.php')) {
    define('USE_COMPOSER_AUTOLOADER', true);
    require_once __DIR__ . '/wp-content/vendor/autoload.php';
}

// ===================================================
// Load database info and local development parameters
// ===================================================
if (file_exists(__DIR__ . '/local-config.php')) {
    include(__DIR__ . '/local-config.php');
}

// ===================================================
// Initialize Situation defaults
// ===================================================
if (class_exists('\Situation\WPConfig')) {
    new \Situation\WPConfig(__DIR__);
}

// ================================================
// You almost certainly do not want to change these
// ================================================
define('DB_CHARSET', 'utf8mb4');
define('DB_COLLATE', '');

// ==============================================================
// Salts, for security
// Grab these from: https://api.wordpress.org/secret-key/1.1/salt
// ==============================================================
define('AUTH_KEY',         'cG,PUfWDw@-PNu]f+U3FxS{qQsEEAY`Nf-s5~|8]jgb3u$YjZw-5toLc}|+Ft3XL');
define('SECURE_AUTH_KEY',  'TB]_/yE(/([|*5Tu=C<1?XQ{(U-8-T$}L!v?JvXAd:<eBQ(RDK%X$*Ci-U6ah193');
define('LOGGED_IN_KEY',    'S! 0n=fQqefbtZ[.uQaXIYP;F<^L4l ];BqRuWjUEA`~<XL+h+R!Kc.cYrj}P+,o');
define('NONCE_KEY',        'IK0Jn:3)=jey*e,>WWKZm?^@umTF8Y3`db;(;vkK/~|0=u~^d-/-+U;{nu0SMYLO');
define('AUTH_SALT',        'xo]F:!gT2xWi/p:;Hy?[)=46Y J<!*s_@q<=-7RMah6*=-;hx/Xe19r9w?Nd.k-m');
define('SECURE_AUTH_SALT', 'bkz34>kn,KM!FENe|P:aIw$V+<C3t;`Q2hL1~i++.?4C8(CaQ3i9}t9A]=>lk2Xi');
define('LOGGED_IN_SALT',   'd2dHuR&7l5D2^XA([XfL0=dDvkk5-bn]Y+P|T1f<18))K<ISd<=oS2yyd!- zb!8');
define('NONCE_SALT',       '%7(*!{,-q(!g@56sV.D=c^O]ATaDQ3dX.7:!|$VE-3osM[8FThOC(r7aKmsH|lXg');


// ==============================================================
// Table prefix
// Change this if you have multiple installs in the same database
// Changing default to 'sit_' to enhance security
// ==============================================================
$table_prefix = 'sit_';

// ================================
// Language
// Leave blank for American English
// ================================
define('WPLANG', '');

// ===================
// Bootstrap WordPress
// ===================
if (! defined('ABSPATH')) {
    define('ABSPATH', dirname(__FILE__) . '/wp/');
}
require_once(ABSPATH . 'wp-settings.php');
