<?php

namespace ChildTheme\Advertisement;

use Backstage\Models\PostBase;

/**
 * Class Advertisement
 * @package ChildTheme\Advertisement
 * @author Vincent Ragosta <vincentpasqualeragosta@gmail.com>
 * @version 1.0
 *
 * @property string $fathom_code
 * @property array $link
 */
class Advertisement extends PostBase
{
    const POST_TYPE = 'advertisement';
}
