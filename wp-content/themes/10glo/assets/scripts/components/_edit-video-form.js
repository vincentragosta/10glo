addAction(INIT, function () {
    var $editVideoForm = $('#edit-video-form');
    if (!$editVideoForm.length) {
        return;
    }

    var $previewImageButton = $('.edit-video-form__preview-image-button');
    var $previewImage = $('.edit-video-form__preview-image');

    $previewImageButton.click(function () {
        $previewImage.trigger('click');
    });

    $previewImage.on('change', function () {
        var text = 'No file uploaded';
        var $file = $(this)[0].files[0];
        if ($file.name.length > 20) {
            text = $file.name.slice(0, 20);
            text += '...';
        } else if ($file.name.length <= 20) {
            text = $file.name;
        }
        $('.edit-video-form__filename').text(text);
    });

    $('.edit-video-form__one-time-fee').on('click', function() {
        var $sibling = $(this).parents().siblings('.edit-video-form__one-time-fee-additional-options');
        if ($sibling.hasClass('d-none')) {
            $sibling.removeClass('d-none');
        } else {
            $sibling.addClass('d-none');
        }
    });

    var $videoPrice = $('.edit-video-form__video-price');
    if ($videoPrice.length) {
        $videoPrice.change(function () {
            var $this = $(this);
            $this.val(Math.ceil($this.val()) - 0.01);
        });
    }

    $editVideoForm.submit(function (e) {
        e.preventDefault();

        var $messaging = $('.messaging');
        var $messagingSection = $messaging.parents('.messaging__section');
        var previewImage = $('#preview-image')[0].files[0];
        var videoTitle = $('.edit-video-form__title').val();
        var categoryValue = $('.edit-video-form__category option:selected').val();
        var hashtags = $('.edit-video-form__hashtags').val();
        var $monetization = $('#edit-video-form__monetization');

        if (!videoTitle) {
            $messagingSection.addClass('messaging__section--show');
            $messaging.find('.messaging__content').html('Make sure your video doesn\'t get lost – please add a title before uploading.');
            return;
        }

        if (!categoryValue) {
            $messagingSection.addClass('messaging__section--show');
            $messaging.find('.messaging__content').html('Make sure your video doesn\'t get lost – please select a category before uploading.');
            return;
        }

        if (previewImage && previewImage.size >= 5000000) {
            $messagingSection.addClass('messaging__section--show');
            $messaging.find('.messaging__content').html('The preview image size was larger than 5 MB. Please compress the image and try again.');
            return;
        }

        if (hashtags) {
            var computed_hashtags = hashtags.split(',');
            for (var i = 0; i < computed_hashtags.length; i++) {
                var totalHashtags = computed_hashtags[i].match(/#/g);
                if (totalHashtags && totalHashtags.length > 1) {
                    $("html, body").animate({ scrollTop: 0 }, "slow");
                    $messagingSection.addClass('messaging__section--show');
                    $messaging.find('.messaging__content').html('Oops! Please use a comma to separate hashtags.');
                    return;
                }
            }
        }

        var videoPrice = '';
        if ($monetization.length) {
            videoPrice = $('.edit-video-form__video-price').val();
            if ($('.edit-video-form__one-time-fee').prop('checked') && !videoPrice) {
                $("html, body").animate({ scrollTop: 0 }, "slow");
                $messagingSection.addClass('messaging__section--show');
                $messaging.find('.messaging__content').html('You have elected to sell this video on 10glo, please enter a price to proceed.');
                return;
            }
            if (videoPrice && videoPrice.replace(/\./g, "") >= 29999) {
                $("html, body").animate({ scrollTop: 0 }, "slow");
                $messagingSection.addClass('messaging__section--show');
                $messaging.find('.messaging__content').html('You cannot enter a price greater than $299.99. Please reach out to <a href="mailto:help@10glo.com">help@10glo.com</a> for more help.');
                return;
            }
        }

        var formData = new FormData();
        formData.append('previewImage', previewImage);
        formData.append('videoId', $('.edit-video-form__video-id').val());
        formData.append('videoTitle', videoTitle);
        formData.append('categoryId', categoryValue);
        formData.append('hashtags', hashtags);
        formData.append('description', tinymce.activeEditor.getContent());
        if ($monetization.length) {
            videoPrice = $('.edit-video-form__video-price').val();
            formData.append('oneTimeFee', $('.edit-video-form__one-time-fee').prop('checked'));
            formData.append('monthlyMembers', $('.edit-video-form__monthly-members').prop('checked'));
            if (videoPrice) {
                formData.append('videoPrice', videoPrice.replace(/\./g, ""));
            }
        }
        formData.append('userId', sit.current_user_id);

        $('html, body').css('cursor', 'wait');

        $.ajax({
            method: 'POST',
            url: sit.api_url + 'video/update',
            data: formData,
            processData: false,
            contentType: false,
            beforeSend: function (xhr) {
                xhr.setRequestHeader('X-WP-Nonce', sit.nonce);
                xhr.setRequestHeader('Authorization', 'Basic ' + sit.basic_auth);
            },
            success: function (response) {
                $('html, body').css('cursor', 'auto');
                $messagingSection.addClass('messaging__section--show');
                $messaging.find('.messaging__content').html(response.message);

                // setTimeout(function () {
                //     window.location = document.referrer;
                // }, 500);
            },
            error: function (response) {
                $('html, body').css('cursor', 'auto');
                var message = response.responseJSON.message ? response.responseJSON.message : response.message;
                $messagingSection.addClass('messaging__section--show');
                $messaging.find('.messaging__content').html(message);
            },
            fail: function (response) {
                $('html, body').css('cursor', 'auto');
                var message = response.responseJSON.message ? response.responseJSON.message : response.message;
                $messagingSection.addClass('messaging__section--show');
                $messaging.find('.messaging__content').html(message);
            }
        });
    });
});
