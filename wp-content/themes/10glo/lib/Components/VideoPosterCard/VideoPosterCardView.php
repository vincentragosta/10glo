<?php

namespace ChildTheme\Components\VideoPosterCard;

use Backstage\View\Component;
use Backstage\View\Element;
use ChildTheme\Service\PremiumVideoService;
use ChildTheme\User\User;
use ChildTheme\Video\Video;

/**
 * Class VideoPosterCardView
 * @package ChildTheme\Components\VideoPosterCard
 * @author Vincent Ragosta <vincentpasqualeragosta@gmail.com>
 * @version 1.0
 *
 * @property string $permalink
 * @property string $poster_image_url
 * @property string $title
 * @property User $Author
 * @property string $views
 * @property string $date
 * @property Element $PremiumBanner
 */
class VideoPosterCardView extends Component
{
    protected $name = 'video-poster-card';
    protected static $default_properties = [
        'permalink' => '',
        'poster_image_url' => '',
        'title' => '',
        'Author' => null,
        'views' => [],
        'date' => '',
        'PremiumBanner' => null
    ];

    public function __construct(Video $Video)
    {
        $args = [
            'permalink' => $Video->permalink(),
            'poster_image_url' => $Video->poster_image_url,
            'title' => $Video->post()->post_title,
            'Author' => $Author = User::createFromPost($Video),
            'views' => $Video->views,
            'date' => $Video->getRelativePublishedDate(),
        ];

        if (($PremiumBanner = $this->getPremiumBannerFromVideo($Video)) instanceof Element) {
            $args['PremiumBanner'] = $PremiumBanner;
        }

        parent::__construct($args);
        if ($this->PremiumBanner) {
            $this->classModifiers('premium-video');
        }
    }

    public function overridePermalink(string $permalink)
    {
        $this->permalink = $permalink;
        return $this;
    }

    public function getPremiumBannerFromVideo(Video $Video)
    {
        $label = '';
        $classes = [
            'button',
            'button--secondary',
            'video-poster-card__premium-banner'
        ];
        $is_premium_video = ($PremiumVideoService = new PremiumVideoService())->isPremiumVideo($Video);
        $is_members_only = $PremiumVideoService->isVideoMarkedForMonthlyMembers($Video);
        if ($is_premium_video && !$is_members_only) {
            $label = 'Premium';
        } elseif ($is_premium_video && $is_members_only) {
            $label = 'Premium<span> or<br />&nbsp;Members Only</span>';
            $classes[] = 'video-poster-card__premium-banner--extended';
        } else if (!$is_premium_video && $is_members_only) {
            $label = 'Members <span>Only</span>';
            $classes[] = 'video-poster-card__premium-banner--members';
        }
        if (empty($label)) {
            return false;
        }
        return Element::create('span', $label, ['class' => implode(' ', $classes)]);
    }
}
