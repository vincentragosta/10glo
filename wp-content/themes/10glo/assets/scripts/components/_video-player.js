addAction(INIT, function () {
    var $videoPlayer = $('#video-player');
    if (!$videoPlayer.length) {
        return;
    }

    if ($('body').hasClass('page-template-video-embed')) {
        var controlBar = videojs('video-player').getChild('ControlBar');
        var Button = videojs.getComponent('Button');
        var button = new Button(videojs('video-player'), {
            clickHandler: function (event) {
                var $drawer = $('.video-js__drawer');
                if (!$drawer.length) {
                    return;
                }
                var DRAWER_ACTIVE_CLASS = 'video-js__drawer--active';
                $.each($drawer, function (index, value) {
                    $(value).toggleClass(DRAWER_ACTIVE_CLASS, !$(value).hasClass(DRAWER_ACTIVE_CLASS));
                });
            }
        });
        button.el().innerHTML = '<img src="' + window.location.protocol + '//' + window.location.hostname + '/wp-content/themes/10glo/dist/images/video-embed-logo.png" />';
        button.addClass('video-js__custom-button');
        controlBar.addChild(button);
    }

    var viewRequestSentPerPageLoad = false;
    videojs('video-player').on('play', function () {
        if (!viewRequestSentPerPageLoad) {
            videojs('video-player').setTimeout(function () {
                viewRequestSentPerPageLoad = true;
                $.ajax({
                    method: 'POST',
                    url: sit.api_url + 'video/views',
                    data: {
                        videoId: $videoPlayer.data('video-id')
                    },
                    beforeSend: function (xhr) {
                        xhr.setRequestHeader('X-WP-Nonce', sit.nonce);
                        xhr.setRequestHeader('Authorization', 'Basic ' + sit.basic_auth);
                    },
                    success: function (response) {
                    },
                    error: function (response) {
                    },
                    fail: function (response) {
                    }
                });
            }, ((this.duration() / 60) * 0.25) >= 25 ? ((this.duration() / 60) * 0.25) * 1000 : 25000);
        }
    });
});
