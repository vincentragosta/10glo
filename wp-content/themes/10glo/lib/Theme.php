<?php

namespace ChildTheme;

use Backstage\Util\AfterSavePostHandler;
use ChildTheme\Components;
use ChildTheme\Controller;
use ChildTheme\REST;
use ChildTheme\Options\GlobalOptions;
use ChildTheme\User\User;
use Orchestrator\Theme as ThemeBase;

/**
 * Class Theme
 *
 * Configure settings by overriding parent class constants
 *
 * @package Theme
 */
class Theme extends ThemeBase
{
    const REMOVE_DEFAULT_POST_TYPE = true;
    const REMOVE_COMMENTS = false;

    const NAV_MENUS = [
        'primary_navigation' => 'Primary/Header Navigation',
        'footer_navigation_1' => 'Footer Navigation 1',
        'footer_navigation_2' => 'Footer Navigation 2',
        'footer_navigation_3' => 'Footer Navigation 3',
        'footer_navigation_4' => 'Footer Navigation 4',
        'stories_navigation' => 'Stories Navigation',
        'user_settings_navigation' => 'User Settings Navigation'
    ];

    const PLATFORM_THEME_SUPPORT = [
        'set-design/nav-menu',
        'design-producer',
        'preview-producer'
    ];

    /**
     * Add theme-specific hooks
     */
    public function __construct()
    {
        parent::__construct();
        add_filter('global_js_vars', [$this, 'updateJsVars'], 10, 1);
        register_nav_menus(self::NAV_MENUS);
//        add_filter('add-primary-category-to-post/post-types', function($post_types) {
//            return array_merge($post_types, ['video']);
//        }, 1, 1);
//        add_filter('add-primary-category-to-post/default-query-args', function($args) {
//            return array_merge($args, ['post_status' => 'publish']);
//        }, 1, 1);
        add_action('wp_head', function() { ?>
            <script data-ad-client="ca-pub-4103352350078783" async src="https://pagead2.googlesyndication.com/pagead/js/adsbygoogle.js"></script><?php
        });
    }

    /**
     * Add theme-specific style and script enqueues
     */
    public function assets()
    {
        parent::assets();
        wp_enqueue_script('comment-reply');
        wp_enqueue_style('google-fonts', 'https://fonts.googleapis.com/css2?family=Roboto&family=Roboto+Condensed&display=swap', '', null);
        wp_enqueue_style('videojs-css', 'https://vjs.zencdn.net/7.8.3/video-js.css', '', null);
        wp_enqueue_script('videojs', 'https://vjs.zencdn.net/7.8.3/video.js', '', null, true);
        wp_enqueue_script('interactjs', 'https://cdn.jsdelivr.net/npm/interactjs/dist/interact.min.js', '', null, true);
        wp_enqueue_script('stripejs', 'https://js.stripe.com/v3', '', null, true);
    }

    public function updateJSVars($js_vars)
    {
        $args = [
            'nonce' => wp_create_nonce('wp_rest'),
            'profile_url' => '/profile/',
            'home_url' => home_url(),
            'posts_per_page' => get_option('posts_per_page'),
        ];
        if ($LoginPage = GlobalOptions::loginPage()) {
            $args['login_url'] = $LoginPage->permalink();
        }
        if ($BWAYWORLDRECORD_category = GlobalOptions::bwayworldrecordCategory()) {
            $args['bwayworldrecord_category'] = $BWAYWORLDRECORD_category->term_id;
        }
        if (defined('BASIC_AUTH_CREDENTIALS')) {
            $args['basic_auth'] = BASIC_AUTH_CREDENTIALS;
        }
        if (defined('STRIPE_PUBLISHABLE_KEY')) {
            $args['publishable_key'] = STRIPE_PUBLISHABLE_KEY;
        }
        if (($User = User::createFromCurrentUser()) instanceof User) {
            $args['current_user_id'] = $User->ID;
        }
        return array_merge($js_vars, $args);
    }

    public function removeDefaultPostType()
    {
        remove_menu_page('edit.php');
        if (self::REMOVE_COMMENTS) {
            remove_menu_page('edit-comments.php');
        }
    }

    const EXTENSIONS = [
        AfterSavePostHandler::class,
        Controller\VcLibraryController::class,
        Controller\SearchController::class,
        Controller\UserAccessController::class,
        Controller\VideoController::class,
        Controller\PlaylistController::class,
        Controller\CommentsController::class,
        Controller\EmailNotificationsController::class,
        Controller\OpenGraphController::class,
        Controller\UserController::class,
        Controller\FathomAnalyticsController::class,
        Controller\BetaRibbonController::class,
        Controller\WPEditorController::class,
        Controller\NewsletterController::class,
        Controller\SetDesignController::class,
        Controller\NewsPostController::class,
        Controller\GlobalOptionsController::class,
        Controller\PodcastController::class,
        Controller\PremiumVideoServiceController::class,
        Controller\UserMonetizationServiceController::class,
        Controller\UserSubscribeServiceController::class,

        REST\VideoRestController::class,
        REST\UserRestController::class,
        REST\ProfileCoverPhotoRestController::class,
        REST\UserPhotoRestController::class,
        REST\PlaylistRestController::class,
        REST\NewsletterRestController::class,
        REST\UserMonetizationRestController::class,
        REST\StripeRestController::class,

        Components\VideoPosterCard\VideoPosterCard::class,
        Components\Playlist\Playlist::class,
        Components\Playlist\SmartList::class,
        Components\AdvertisementCard\AdvertisementCard::class,
        Components\TestimonialCard\TestimonialCard::class,
        Components\PodcastCard\PodcastCard::class,
        Components\PodcastList\PodcastList::class,
        Components\NewsPostCard\NewsPostCard::class,
        Components\SmartNewsList\SmartNewsList::class,
        Components\SmartJobsList\SmartJobsList::class,
        Components\JobCard\JobCard::class,
        Components\SearchBar\SearchBar::class
    ];
}
