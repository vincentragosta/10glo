addAction(INIT, function () {
    var $videoRating = $('.video-watch__rate');
    if (!$videoRating.length) {
        return;
    }
    $videoRating.on('click tap touchstart', function () {
        var $messaging = $('.messaging');
        var $messagingSection = $messaging.parents('.messaging__section');

        $.ajax({
            method: 'POST',
            url: sit.api_url + 'video/rate',
            data: {
                videoId: $(this).data('video-id'),
                userId: sit.current_user_id
            },
            beforeSend: function (xhr) {
                xhr.setRequestHeader('X-WP-Nonce', sit.nonce);
                xhr.setRequestHeader('Authorization', 'Basic ' + sit.basic_auth);
            },
            success: function (response) {
                $messagingSection.addClass('messaging__section--show');
                $messaging.find('.messaging__content').html(response.message);
            },
            error: function (response) {
                var message = response.responseJSON.message ? response.responseJSON.message : response.message;
                $messagingSection.addClass('messaging__section--show');
                $messaging.find('.messaging__content').html(message);
            },
            fail: function (response) {
                $messagingSection.addClass('messaging__section--show');
                $messaging.find('.messaging__content').html(response);
            }
        });
    });
});
