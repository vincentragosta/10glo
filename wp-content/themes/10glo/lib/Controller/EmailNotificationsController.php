<?php

namespace ChildTheme\Controller;

/**
 * Class EmailNotificationsController
 * @package ChildTheme\Controller
 * @author Vincent Ragosta <vincentpasqualeragosta@gmail.com>
 * @version 1.0
 */
class EmailNotificationsController
{
    public function __construct()
    {
        add_filter('wp_mail_content_type', function () {
            return 'text/html';
        });
    }
}
