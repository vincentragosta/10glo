<?php
/**
 * Expected:
 * @var WP_Image|bool $Image
 * @var string $title
 * @var User $User
 * @var string $content
 * @var string $relative_published_time
 * @var string $url
 * @var array $deep_links
 * @var array $class_modifiers
 * @var array $element_attributes
 */

use Backstage\SetDesign\Icon\IconView;
use Backstage\Util;
use Backstage\View\Link;
use ChildTheme\User\User;

if (empty($title) || empty($url)) {
    return;
}
$element_attributes['class'] .= 'col-reset-exclude';
?>

<div <?= Util::componentAttributes('news-post-card', $class_modifiers, $element_attributes); ?>>
    <div class="news-post-card__container">
        <?php if ($Image instanceof WP_Image): ?>
            <div class="news-post-card__image-container">
                <a href="<?= $url; ?>" class="news-post-card__image-link">
                    <figure class="news-post-card__figure">
                        <?= $Image->css_class('news-post-card__image'); ?>
                        <?php if ($caption = $image->caption): ?>
                            <figcaption class="news-post-card__image-caption"><?= $caption; ?></figcaption>
                        <?php endif; ?>
                    </figure>
                </a>
            </div>
        <?php endif; ?>
        <div class="news-post-card__content-container">
            <h2 class="news-post-card__heading heading heading--default heading--dark">
                <a href="<?= $url; ?>" class="news-post-card__heading-link">
                    <?= $title; ?>
                </a>
            </h2>
            <?php if (!empty($content)): ?>
                <p class="news-post-card__content">
                    <?= $content; ?>
                </p>
            <?php endif; ?>
            <?php if ($Author instanceof User || $relative_published_time): ?>
                <div class="news-post-card__author-time-container">
                    <?php if ($Author instanceof User): ?>
                        <p class="news-post-card__author">By <a href="<?= $Author->permalink(); ?>"><?= $Author->getDisplayName(); ?></a></p>
                    <?php endif; ?>
                    <?php if ($relative_published_time): ?>
                        <p class="news-post-card__relative-published-time"><?= $relative_published_time; ?></p>
                    <?php endif; ?>
                </div>
            <?php endif; ?>
            <?php if (!empty($deep_links)): ?>
                <ul class="news-post-card__deep-links list list--unstyled">
                    <?php foreach($deep_links as $link): ?>
                        <li class="news-post-card__deep-link-item"><?= new IconView(['icon_name' => 'simple-diamond']); ?><?= Link::createFromField(array_shift($link)); ?></li>
                    <?php endforeach; ?>
                </ul>
            <?php endif; ?>
        </div>
    </div>
</div>
