<?php

namespace ChildTheme\Components\SocialSharerCard;

use Backstage\SetDesign\Icon\IconView;
use Backstage\View\Component;
use Backstage\View\Element;
use Backstage\View\Link;
use ChildTheme\SocialSharer\EmbedSocialSharer;
use ChildTheme\SocialSharer\SocialSharer;

/**
 * Class SocialSharerCardView
 * @package ChildTheme\Components\SocialSharerCard
 * @author Vincent Ragosta <vincentpasqualeragosta@gmail.com>
 * @version 1.0
 *
 * @property string $icon
 * @property string $label
 */
class SocialSharerCardView extends Component
{
    protected $name = 'social-sharer-card';
    protected static $default_properties = [
        'icon' => '',
        'label' => ''
    ];
    const BUTTON_CLASSES = [
        EmbedSocialSharer::class
    ];

    public function __construct(SocialSharer $SocialSharer)
    {
        $icon = $this->getIconView($SocialSharer);
        parent::__construct([
            'icon' => $icon->addClass('social-sharer-card__icon'),
            'label' => $SocialSharer::LABEL
        ]);
        $this->classModifiers(strtolower(str_replace(' ', '', $this->label)));
    }

    protected function getIconView(SocialSharer $SocialSharer)
    {
        return in_array(get_class($SocialSharer), static::BUTTON_CLASSES, true) ?
            Element::create('button', new IconView(['icon_name' => $SocialSharer::ICON])) :
            Link::createFromField([
                    'title' => new IconView(['icon_name' => $SocialSharer::ICON]),
                    'url' => $SocialSharer->getUrl(),
                    'target' => '_blank'
                ]
            );
    }
}
