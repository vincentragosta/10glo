<?php

use ChildTheme\Components\StripeConnect\StripeConnectView;
use ChildTheme\Options\GlobalOptions;
use ChildTheme\Service\UserMonetizationService;
use ChildTheme\User\User;

$CurrentUser = User::createFromCurrentUser();
$Page = GlobalOptions::paymentSettingsPage();
$Vendor = User::createFromUserLogin(get_query_var('related_user'));
echo $Page->content(false);
?>

<div class="content-section">
    <div class="content-section__container container">
        <div class="content-row row">
            <div class="content-column col-12">
                <div class="content-column__inner">
                    <?= new StripeConnectView($Vendor); ?>
                    <p>First, go ahead and add a message to your fans below about why you could use their financial support. </p>
                    <form class="payment-settings-form" name="payment-settings-form" id="payment-settings-form" method="POST">
                        <div class="payment-settings-form__support-message">
                            <label class="payment-settings-form__label" for="user-support-message">Support Message</label>
                            <?php wp_editor((new UserMonetizationService())->getSupportMessage($Vendor), 'user-support-message', ['textarea_name' => 'user-support-message', 'media_buttons' => false, 'quicktags' => false]); ?>
                            <input type="submit" class="button" style="margin-top: var(--global__sizing__layout-spacing--default);"/>
                        </div>
                    </form>
                    <p>Then, you're ready to upload your first video. Click the upload button at the top of this screen and you'll see new options available to you to set payment terms for this video.</p>
                    <p>You can always come to this page from your profile menu to view your Stripe Dashboard or edit this message to your fans.</p>
                </div>
            </div>
        </div>
    </div>
</div>
