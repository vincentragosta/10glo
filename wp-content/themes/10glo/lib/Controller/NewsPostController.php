<?php

namespace ChildTheme\Controller;

use Backstage\Rewrite\QueryRewrite;

/**
 * Class NewsPostController
 * @package ChildTheme\Controller
 * @author Vincent Ragosta <vincentpasqualeragosta@gmail.com>
 * @version 1.0
 */
class NewsPostController
{
    const STORY_QUERY_VAR = 'story_category';

    public function __construct()
    {
        new QueryRewrite('news/category/(.+)?$', ['story_category' => '$1']);
//        new QueryRewrite('news/recent', ['news_recent' => true]);
        add_filter('body_class', [$this, 'addBodyClass']);
        add_filter('template_include', [$this, 'templateInclude'], 99);
    }

    public function addBodyClass($classes)
    {
        global $post, $wp_query;
        if ($post->post_name === 'news') {
            $classes = array_merge($classes, ['page-template-news']);
        }
        if ($wp_query->get(static::STORY_QUERY_VAR)) {
            $classes = array_merge($classes, ['page-template-news-category']);
        }
        return $classes;
    }

    public function templateInclude($template)
    {
        $wp_query = $GLOBALS['wp_query'];
        if ($wp_query->get(static::STORY_QUERY_VAR)) {
            $new_template = locate_template(['taxonomy-story-category.php']);
            if ('' != $new_template) {
                return $new_template;
            }
        }
        return $template;
    }
}
