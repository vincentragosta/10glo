<?php

namespace ChildTheme\Components\SmartNewsList;

use Backstage\VcLibrary\Support\Component;
use ChildTheme\News\NewsPost;
use ChildTheme\News\NewsPostRepository;

/**
 * Class SmartNewsList
 * @package ChildTheme\Components\SmartNewsList
 * @author Vincent Ragosta <vincentpasqualeragosta@gmail.com>
 * @version 1.0
 */
class SmartNewsList extends Component
{
    const NAME = 'Smart News List';
    const TAG = 'smart_news_list';
    const VIEW = SmartNewsListView::class;

    protected $component_config = [
        'description' => 'Display a list of news posts',
        'icon' => 'icon-wpb-toggle-small-expand',
        'wrapper_class' => 'clearfix',
        'is_container' => false,
        'category' => 'Content',
        'params' => [
            'story-categories' => [
                'type' => 'dropdown',
                'heading' => 'Category',
                'param_name' => 'story_category_id',
                'value' => [],
                'description' => 'Select a category',
                'admin_label' => true,
                'group' => 'Query'
            ],
            'posts_per_page' => [
                'type' => 'textfield',
                'heading' => 'Posts per page',
                'param_name' => 'posts_per_page',
                'description' => 'Enter how many posts to display. Default: 8.',
                'admin_label' => true,
                'group' => 'Query'
            ],
            'excluded_category' => [
                'type' => 'textfield',
                'heading' => 'Categories to Exclude',
                'param_name' => 'excluded_story_category_ids',
                'description' => 'Enter the negative category ID to exclude them from the smart list query. Separate by comma to remove multiple.',
                'group' => 'Query'
            ],
            'excluded_news_post_ids' => [
                'type' => 'textfield',
                'heading' => 'Exclude News Post IDs',
                'param_name' => 'post__not_in',
                'description' => 'Enter any number of news post IDs separated by comma to exclude them from the smart list query. Please use the WP ID and not the custom generated ID.',
                'group' => 'Query'
            ],
            'layout' => [
                'type' => 'dropdown',
                'heading' => 'Pre Formatted Layout',
                'param_name' => 'pre_formatted_layout',
                'value' => [
                    '-- Select Layout --' => '',
                    'Numbered List' => 'numbered-list'
                ],
                'description' => 'Any News Card Options will be obsolete if a layout is selected.',
                'admin_label' => true,
                'group' => 'Layout / Options'
            ],
            'options' => [
                'type' => 'checkbox',
                'heading' => 'News Card Options',
                'param_name' => 'options',
                'value' => [
                    'Display Image (Top)' => 'display-image-top',
                    'Display Image (Left)' => 'display-image-left',
                    'Display Image (Right)' => 'display-image-right',
                    'Increase Heading Size' => 'increase-heading-size',
                    'Decrease Heading Size' => 'decrease-heading-size',
                    'Hide Content' => 'hide-content'
                ],
                'description' => 'Add custom options to the list.',
                'group' => 'Layout / Options'
            ]
        ]
    ];

    public function setupConfig()
    {
        parent::setupConfig();
        $this->setCategories();
    }

    protected function setCategories()
    {
        $options['-- Select Category --'] = '';
        foreach(get_terms(['taxonomy' => NewsPost::TAXONOMY, 'hide_empty' => false, 'order' => 'DESC']) as $category) {
            $options[$category->name] = $category->term_id;
        }
        $this->component_config['params']['story-categories']['value'] = $options;
    }

    protected function createView(array $atts)
    {
        $NewsPostRepository = new NewsPostRepository();
        // consider doing this check in findCurated()
        if (!empty($excluded_categories = $atts['excluded_story_category_ids'])) {
            // perhaps perform a check here to ensure they all are negative values
            $atts['story_category_id'] .= $excluded_categories;
        }

        if (empty($news = $NewsPostRepository->findCurated($atts))) {
            return 'The have been no news posted with this criteria.';
        }

        /* @var SmartNewsListView $ViewClass */
        $ViewClass = self::VIEW;
        if (!empty($layouts = array_filter(explode(',', $atts['pre_formatted_layout'])))) {
            return (new $ViewClass($news))->classModifiers($layouts);
        }
        return new $ViewClass($news, $atts['options'] ? explode(',', $atts['options']) : []);
    }
}
