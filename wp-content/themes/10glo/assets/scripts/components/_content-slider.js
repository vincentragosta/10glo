addAction(INIT, function() {
    SetDesign.addFilter('content-slider/owl-settings', function(settings) {
        settings.nav = true;
        settings.dots = false;
        settings.items = 4;
        settings.margin = 20;
        settings.navText = ['<span class="icon icon-direction-up"><svg><use class="icon-use" xlink:href="#icon-long-arrow"></use></svg></span>', '<span class="icon"><svg><use class="icon-use" xlink:href="#icon-long-arrow"></use></svg></span>'];
        settings.responsive = {
            0: {
                items: 1
            },
            480: {
                items: 2
            },
            768: {
                items: 3
            }
        };
        return settings;
    });
});
