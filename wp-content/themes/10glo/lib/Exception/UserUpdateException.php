<?php

namespace ChildTheme\Exception;

use Exception;

/**
 * Class UserUpdateException
 * @package ChildTheme\Exception
 * @author Vincent Ragosta <vincentpasqualeragosta@gmail.com>
 * @version 1.0
 */
class UserUpdateException extends Exception {}
