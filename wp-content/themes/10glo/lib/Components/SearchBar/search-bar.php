<?php
/**
 * Expected:
 * @var array $class_modifiers
 * @var array $element_attributes
 */

use Backstage\Util;

?>

<div <?= Util::componentAttributes('search-bar', $class_modifiers, $element_attributes); ?>>
    <?= get_search_form(); ?>
</div>
