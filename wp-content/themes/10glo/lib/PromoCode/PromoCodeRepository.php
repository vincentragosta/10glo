<?php

namespace ChildTheme\PromoCode;

use Backstage\Repositories\PostRepository;

/**
 * Class PromoCodeRepository
 * @package ChildTheme\PromoCode
 * @author Vincent Ragosta <vincentpasqualeragosta@gmail.com>
 * @version 1.0
 */
class PromoCodeRepository extends PostRepository
{
    protected $model_class = PromoCode::class;

    public function findByCode(string $code)
    {
        if (empty($code)) {
            return [];
        }
        return $this->findOne([
            'meta_query' => [
                [
                    'key' => 'code',
                    'value' => $code,
                    'compare' => '='
                ]
            ]
        ]);
    }
}
