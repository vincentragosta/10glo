<?php

namespace ChildTheme\Controller;

use Backstage\Rewrite\QueryRewrite;

/**
 * Class NewsletterController
 * @package ChildTheme\Controller
 * @author Vincent Ragosta <vincentpasqualeragosta@gmail.com>
 * @version 1.0
 */
class NewsletterController
{
    public function __construct()
    {
        new QueryRewrite('newsletter', ['newsletter' => true]);
        add_filter('template_include', [$this, 'templateInclude'], 99);
        add_filter('body_class', [$this, 'addBodyClass']);
        add_action('init', function() {
            add_action('wp_head', [$this, 'addHeadSnippet'], 1);
        });
    }

    public function templateInclude($template)
    {
        $wp_query = $GLOBALS['wp_query'];
        if ($wp_query->get('newsletter')) {
            add_filter('show_site_header', '__return_false');
            add_filter('show_site_footer', '__return_false');
            $new_template = locate_template(['templates/newsletter.php']);
            if ('' != $new_template) {
                return $new_template;
            }
        }
        return $template;
    }

    public function addBodyClass($classes)
    {
        global $wp_query;
        if ($wp_query->get('newsletter')) {
            $classes = array_merge($classes, ['page-template-newsletter']);
        }
        return $classes;
    }

    public function addHeadSnippet()
    {
        global $wp_query;
        if ($wp_query->get('newsletter')): ?>
            <!-- Spark Loop -->
            <script async src='https://dash.sparkloop.app/widget/MF3ab5b58f3d/embed.js' data-sparkloop></script><?php
        endif;
    }
}
