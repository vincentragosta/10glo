<?php
/**
 * Expected:
 * @var bool $is_private
 * @var string $permalink
 * @var string $poster_image_url
 * @var string $title
 * @var User $Author
 * @var string $views
 * @var string $date
 * @var Element $PremiumBanner
 * @var array $class_modifiers
 * @var array $element_attributes
 */

use Backstage\Util;
use Backstage\View\Element;
use ChildTheme\Components\UserCard\UserCardView;
use ChildTheme\Options\GlobalOptions;
use ChildTheme\User\User;

if (empty($poster_image_url) || empty($permalink) || empty($title) || !$Author instanceof User) {
    return '';
}
?>

<div <?= Util::componentAttributes('video-poster-card', $class_modifiers, $element_attributes); ?>>
    <a href="<?= $permalink; ?>" class="video-poster-card__permalink">
        <img src="<?= Util::getAssetPath('images/logo.png'); ?>" class="video-poster-card__logo" />
        <span class="video-poster-card__poster-image" style="background-image: url(<?= $poster_image_url; ?>);"></span>
        <?= $PremiumBanner; ?>
    </a>
    <div class="video-poster-card__content-container">
        <?= (new UserCardView($Author->getUserPhotoUrl(), $Author->permalink()))->elementAttributes(['class' => 'video-poster-card__user-card']); ?>
        <div class="video-poster-card__content-container-inner">
            <h2 class="video-poster-card__heading heading"><a href="<?= $permalink; ?>"><?= $title; ?></a></h2>
            <?php if (!empty($display_name = $Author->getDisplayName())): ?>
                <div class="video-poster-card__header-container">
                    <a href="<?= $Author->permalink(); ?>" class="video-poster-card__author-username"><?= $display_name; ?></a>
                    <?php if ($Author->isVerified() && ($verified_artists_image = GlobalOptions::verifiedArtistsImage())): ?>
                        <?= $verified_artists_image; ?>
                    <?php endif; ?>
                </div>
            <?php endif; ?>
            <ul class="video-poster-card__list list--inline list--dotted">
                <?php if (!empty($views)): ?>
                    <li><?= $views; ?> views</li>
                <?php endif; ?>
                <?php if (!empty($date)): ?>
                    <li><?= $date; ?></li>
                <?php endif; ?>
            </ul>
        </div>
    </div>
</div>
