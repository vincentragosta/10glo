<?php
/**
 * Expected:
 * @var string $title
 * @var string $content
 * @var string $url
 * @var array $class_modifiers
 * @var array $element_attributes
 */

use Backstage\Util;

if (empty($title) || empty($url)) {
    return;
}
?>

<div <?= Util::componentAttributes('podcast-card', $class_modifiers, $element_attributes); ?>>
<!--    <div class="podcast-card__content-container">-->
        <h2 class="podcast-card__heading heading heading--default">
            <a href="<?= $url; ?>" class="podcast-card__heading-link"><?= $title; ?></a>
        </h2>
        <?php if (!empty($content)): ?>
            <p class="news-post-card__content">
                <?= $content; ?>
            </p>
        <?php endif; ?>
<!--    </div>-->
</div>
