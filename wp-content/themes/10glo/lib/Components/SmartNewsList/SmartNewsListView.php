<?php

namespace ChildTheme\Components\SmartNewsList;

use Backstage\View\Component;
use ChildTheme\News\NewsPost;

/**
 * Class SmartNewsListView
 * @package ChildTheme\Components\SmartNewsList
 * @author Vincent Ragosta <vincentpasqualeragosta@gmail.com>
 * @version 1.0
 *
 * @property NewsPost[] $news
 * @property array $options
 */
class SmartNewsListView extends Component
{
    protected $name = 'smart-news-list';
    protected static $default_properties = [
        'news' => [],
        'options' => []
    ];

    public function __construct(array $news, array $options = [])
    {
        parent::__construct(compact('news', 'options'));
    }
}
