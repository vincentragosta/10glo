<?php

namespace ChildTheme\Video;

use ChildTheme\Options\GlobalOptions;
use ChildTheme\Playlist\PlaylistRepository;
use ChildTheme\RelatedContent\RelatedContentPost;
use ChildTheme\SocialSharer\EmbedSocialSharer;
use ChildTheme\SocialSharer\FacebookSocialSharer;
use ChildTheme\SocialSharer\NativeShareSocialSharer;
use ChildTheme\SocialSharer\SocialSharer;
use ChildTheme\SocialSharer\TwitterSocialSharer;
use ChildTheme\Support\RelativeFormatConverter;
use Stripe\Product;

/**
 * Class Video
 * @package ChildTheme\Video
 * @author Vincent Ragosta <vincentpasqualeragosta@gmail.com>
 * @version 1.0
 *
 * @property string $video_filename
 * @property string $poster_filename
 * @property string $poster_image_url
 * @property string $sharer_filename
 * @property string $sharer_image_url
 * @property string $video_url
 * @property string $post_status
 * @property string $post_name
 * @property string $category
 * @property array $hashtags
 * @property array $tags
 * @property array|string $playlist_ids
 * @property array $likes
 * @property string $likes_count
 * @property array $views
 * @property string $comment_status
 * @property string $video_type
 * @property array $premium_video_service_user_ids
 * @property string $stripe_product_id
 * @property string $stripe_price_id
 */
class Video extends RelatedContentPost
{
    const POST_TYPE = 'video';
    const RELATED_TAXONOMY = 'category';
    const HASHTAG_TAXONOMY = 'hashtags';
    const SOCIAL_SHARER_CLASSES = [
        EmbedSocialSharer::class,
        FacebookSocialSharer::class,
        TwitterSocialSharer::class,
        NativeShareSocialSharer::class
    ];

    protected static function getQuery($args = [])
    {
        $defaults = [
            'post_type' => static::POST_TYPE,
            'post_status' => 'publish',
            'posts_per_page' => -1
        ];
        if (current_user_can('read_private_posts')) {
            $defaults['post_status'] = ['publish', 'private'];
        }
        $defaults = wp_parse_args(static::getDefaultQuery(), $defaults);
        return wp_parse_args($args, $defaults);
    }

    /**
     * @param array $args
     *
     * @return static[]
     */
    public static function getPosts($args = [])
    {
        $args = static::getQuery($args);
        $query = new \WP_Query();
        $posts = $query->query($args);
        $ret = [];
        foreach ($posts as $post_obj) {
            $ret[] = static::create($post_obj);
        }
        return $ret;
    }

    public function getSocialSharers(): array
    {
        if (empty($social_sharers = self::SOCIAL_SHARER_CLASSES)) {
            return [];
        }
        return array_filter(array_map(function ($SocialSharer) {
            if ($SocialSharer == NativeShareSocialSharer::class && !wp_is_mobile()) {
                return false;
            }
            /** @var SocialSharer $SocialSharer */
            return new $SocialSharer($this);
        }, $social_sharers));
    }

    protected function getLikes()
    {
        return json_decode($this->field('likes'));
    }

    public function getPrimaryCategory()
    {
        if (empty($categories = $this->category)) {
            return false;
        }
        return $categories[0];
    }

    public function hasBIPOCTag()
    {
        $terms = $this->terms('post_tag');;
        if (empty($terms) && !GlobalOptions::bipocTag() instanceof \WP_Term) {
            return false;
        }
        foreach ($terms as $WP_Term) {
            if ($WP_Term->slug == GlobalOptions::bipocTag()->slug) {
                return true;
            }
        }
        return false;
    }

    public function getRelativePublishedDate()
    {
        $time = strtotime($this->post()->post_date);
        return (new RelativeFormatConverter($time))->getRelativeTime();
    }

    /**
     * Note: Override getAssignedIds to pull in playlist videos.
     *
     * @param string $context
     * @return array
     */
    public function getAssignedIds(string $context = 'playlist_ids'): array
    {
        if (empty($playlists = (new PlaylistRepository())->findWithIds($this->field($context) ?: []))) {
            return [];
        }
        $video_ids = [];
        foreach ($playlists as $Playlist) {
            if (!empty($playlist_video_ids = $Playlist->video_ids)) {
                $video_ids = array_merge($video_ids, $playlist_video_ids);
                if (count($video_ids) >= 8) {
                    break;
                }
            }
        }
        return array_filter(array_map(function ($id) {
            return $id == $this->ID ? false : $id;
        }, $video_ids));
    }

//    protected function getStripeProduct()
//    {
//        return unserialize(get_post_meta($this->ID, 'StripeProduct', true));
//    }
}
