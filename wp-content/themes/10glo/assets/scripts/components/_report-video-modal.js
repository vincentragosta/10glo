addAction(INIT, function () {
    var $modal = $('#report-video-modal');
    if (!$modal.length) {
        return;
    }
    var $reportForm = $modal.find('#report-video-modal__form');
    var $reportDescription = $modal.find('.report-video-modal__description');

    $reportForm.submit(function (e) {
        e.preventDefault();
        $.ajax({
            method: 'POST',
            url: sit.api_url + 'video/report',
            data: {description: $reportDescription.val()},
            success: function (response) {
                doAction('hideModal', $modal);
            },
            error: function (response) {
                doAction('hideModal', $modal);
            },
            fail: function (response) {
                doAction('hideModal', $modal);
            },
        });
    });
});
