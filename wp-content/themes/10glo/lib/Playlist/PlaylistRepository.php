<?php

namespace ChildTheme\Playlist;

use Backstage\Repositories\PostRepository;
use ChildTheme\User\User;
use ChildTheme\Video\Video;

/**
 * Class PlaylistRepository
 * @package ChildTheme\Playlist
 * @author Vincent Ragosta <vincentpasqualeragosta@gmail.com>
 * @version 1.0
 */
class PlaylistRepository extends PostRepository
{
    protected $model_class = Playlist::class;

    public function findByPlaylistCode(string $playlist_code)
    {
        $results = $this->find([
            'meta_query' => [
                [
                    'key' => 'playlist_code',
                    'compare' => '=',
                    'value' => $playlist_code
                ]
            ]
        ]);
        return count($results) > 0 ? $results[0] : false;
    }

    public function findAllFromUser(User $User)
    {
        return $this->find([
            'author' => $User->ID
        ]) ?: [];
    }

    public function findFromVideo(Video $Video)
    {
        return $this->findWithIds($Video->playlist_ids ?: []);
    }
}
