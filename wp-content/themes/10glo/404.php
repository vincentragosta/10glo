<div class="content-section content-section--height-full">
  <div class="content-section__container container">
    <div class="content-row">
      <div class="content-column col-12 text--center">
        <div class="content-column__inner">
          <div class="four-oh-four">
          <h1 class="heading heading--xlarge four-oh-four__heading">Hold, Please!</h1>
          <p class="four-oh-four__description">It looks like this page was cut.</p>
          <a href="https://10glo.com" class="button">10glo Home</a>
        </div>
        </div>
      </div>
    </div>
  </div>
</div>
