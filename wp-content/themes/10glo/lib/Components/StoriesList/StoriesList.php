<?php

namespace ChildTheme\Components\StoriesList;

use Backstage\Util;
use Backstage\VcLibrary\Support\Component;
use Backstage\View\Element;
use Backstage\View\Link;
use ChildTheme\News\NewsPost;
use ChildTheme\News\NewsPostRepository;

/**
 * Class StoriesList
 * @package ChildTheme\Components\StoriesList
 * @author Vincent Ragosta <vincentpasqualeragosta@gmail.com>
 * @version 1.0
 */
class StoriesList extends Component
{
    const NAME = 'Stories List';
    const TAG = 'stories_list';
    const VIEW = StoriesListView::class;

    protected $component_config = [
        'description' => 'Display a list of stories.',
        'icon' => 'icon-wpb-toggle-small-expand',
        'wrapper_class' => 'clearfix',
        'is_container' => false,
        'category' => 'Content',
        'params' => [
            'heading' => [
                'type' => 'textfield',
                'heading' => 'Heading Text',
                'param_name' => 'heading_text',
                'description' => 'Set optional heading text.',
                'admin_label' => true,
                'group' => 'Heading'
            ],
            [
                'type' => 'dropdown',
                'heading' => 'Tag',
                'param_name' => 'heading_tag',
                'value' => [
                    'H2' => 'h2',
                    'H3' => 'h3',
                    'H4' => 'h4',
                    'H1' => 'h1'
                ],
                'description' => 'Set the heading tag [H1 -> H4]. This is for accessibility purposes only, this will not affect appearance. H1 should only be used once at the top of the page (if page title is not automatically added).',
                'group' => 'Heading',
                'admin_label' => true
            ],
            [
                'type' => 'dropdown',
                'heading' => 'Size',
                'param_name' => 'heading_size',
                'description' => 'Set the heading size.',
                'value' => [
                    'Default' => 'default',
                    'Medium' => 'medium',
                    'Large' => 'large',
                    'Extra Large' => 'xlarge'
                ],
                'group' => 'Heading',
                'admin_label' => true
            ],
            'categories' => [
                'type' => 'dropdown',
                'heading' => 'Story Category',
                'param_name' => 'story_category',
                'value' => [],
                'description' => 'Select a story category',
                'admin_label' => true,
                'group' => 'Query'
            ],
            'posts_per_page' => [
                'type' => 'textfield',
                'heading' => 'Posts per page',
                'param_name' => 'posts_per_page',
                'description' => 'Enter how many posts to display. Default: 8.',
                'admin_label' => true,
                'group' => 'Query'
            ],
            [
                'type' => 'checkbox',
                'heading' => 'Detailed List',
                'param_name' => 'is_detailed',
                'description' => 'Display stories in detailed format.',
                'value' => [
                    'Yes' => 'yes',
                ],
                'group' => 'Options',
                'admin_label' => true
            ],
        ]
    ];

    public function setupConfig()
    {
        parent::setupConfig();
        $this->setStoryCategories();
    }

    protected function setStoryCategories()
    {
        $options['-- Select Story Category --'] = '';
        foreach(get_terms(['taxonomy' => NewsPost::TAXONOMY, 'hide_empty' => false, 'order' => 'DESC']) as $category) {
            $options[$category->name] = $category->term_id;
        }
        $this->component_config['params']['categories']['value'] = $options;
    }

    protected function createView(array $atts)
    {
        $StoryRepository = new NewsPostRepository();
        $ViewClass = self::VIEW;
        /** @var StoriesListView $View */
        $Heading = $atts['heading_text'] ? Element::create($atts['heading_tag'], $atts['heading_text'])->class(Util::componentClasses('heading', [$atts['heading_size']])) : null;
        $View = new $ViewClass($stories = $StoryRepository->findCurated($atts), $Heading);
        if (count($stories) == ($atts['posts_per_page'] ?: get_option('posts_per_page'))) {
            if (!empty($story_category_id = $atts['story_category'])) {
                return $View
                    ->toggleReadMore(new Link(get_term_link(get_term($story_category_id), NewsPost::TAXONOMY), 'Read More', ['class' => 'button button--secondary button--full-width']))
                    ->toggleCompact(!$atts['is_detailed']);
            } else {
                return $View
                    ->toggleReadMore(new Link(home_url('/news/recent'), 'Read More', ['class' => 'button button--secondary button--full-width']))
                    ->toggleCompact(!$atts['is_detailed']);
            }
        }
        return $View->toggleCompact(!$atts['is_detailed']);
    }
}
