<?php

namespace ChildTheme\REST;

use Backstage\Controller\RestController;
use ChildTheme\Exception\UserMonetizationException;
use ChildTheme\Service\StripeService;
use ChildTheme\Service\UserMonetizationService;
use ChildTheme\User\User;

/**
 * Class UserMonetizationRestController
 * @package ChildTheme\Controller
 * @author Vincent Ragosta <vincentpasqualeragosta@gmail.com>
 * @version 1.0
 */
class UserMonetizationRestController extends RestController
{
    protected $namespace = 'user/monetize';

    public function registerRoutes()
    {
        $this->addCreateRoute('/confirm-material-ownership', [$this, 'confirmMaterialOwnership']);
        $this->addCreateRoute('/confirm-monetization-policies', [$this, 'confirmMonetizationPolicies']);
        $this->addCreateRoute('/confirm-country', [$this, 'confirmCountry']);
        $this->addCreateRoute('/support-message', [$this, 'supportMessage']);
        // deprecate this in favor of new support message endpoint
        $this->addCreateRoute('/payment-settings', [$this, 'paymentSettings']);
        $this->addCreateRoute('/tip', [$this, 'tipVendor']);
        $this->addCreateRoute('/subscribe', [$this, 'subscribeToVendor']);
    }

    public function confirmMaterialOwnership(\WP_REST_Request $request)
    {
        if (!($User = User::createFromUserId($request->get_param('userId'))) instanceof User) {
            throw new UserMonetizationException('The user id was not sent in the request.', 400);
        }
        if (!(new UserMonetizationService())->confirmUserMaterialOwnership($User)) {
            throw new UserMonetizationException('Something went wrong when assigning the data to the user.', 400);
        }
        return ['message' => 'Material ownership confirmed.', 'status' => 200];
    }

    public function confirmMonetizationPolicies(\WP_REST_Request $request)
    {
        if (!($User = User::createFromUserId($request->get_param('userId'))) instanceof User) {
            throw new UserMonetizationException('The user id was not sent in the request.', 400);
        }
        if (!(new UserMonetizationService())->confirmMonetizationPolicies($User)) {
            throw new UserMonetizationException('Something went wrong when assigning the data to the user.', 400);
        }
        return ['message' => 'Monetization policies confirmed', 'status' => 200];
    }

    public function confirmCountry(\WP_REST_Request $request)
    {
        if (!($User = User::createFromUserId($request->get_param('userId'))) instanceof User) {
            throw new UserMonetizationException('The user id was not sent in the request.', 400);
        }
        if (empty($country = $request->get_param('country'))) {
            throw new UserMonetizationException('The country was not sent in the request.', 400);
        }
        if (!$User->updateField(User::USER_COUNTRY, $country)) {
            throw new UserMonetizationException('Something went wrong when assigning the data to the user.', 400);
        }
        return ['message' => 'Country confirmed.', 'status' => 200];
    }

    public function supportMessage(\WP_REST_Request $request)
    {
        if (!($User = User::createFromUserId($request->get_param('userId'))) instanceof User) {
            throw new UserMonetizationException('The user id was not sent in the request.', 400);
        }
        if (!(new UserMonetizationService())->updateSupportMessage($request->get_param('supportMessage'), $User)) {
            throw new UserMonetizationException('Something went wrong when assigning the data to the user.', 400);
        }
        return ['message' => 'Success', 'status' => 200];
    }

    public function paymentSettings(\WP_REST_Request $request)
    {
        if (!($User = User::createFromUserId($request->get_param('userId'))) instanceof User) {
            throw new UserMonetizationException('The user id was not sent in the request.', 400);
        }
        if (!(new UserMonetizationService())->updateSupportMessage($request->get_param('supportMessage'), $User)) {
            throw new UserMonetizationException('Something went wrong when assigning the data to the user.', 400);
        }
        return ['message' => 'Success', 'status' => 200];
    }

    public function tipVendor(\WP_REST_Request $request)
    {
        if (!($User = User::createFromUserId($request->get_param('userId'))) instanceof User) {
            throw new UserMonetizationException('The user id was not sent in the request.', 400);
        }
        if (empty($amount = $request->get_param('amount'))) {
            throw new UserMonetizationException('The amount was not sent in the request.', 400);
        }
        if (empty($token = $request->get_param('customerToken'))) {
            throw new UserMonetizationException('There was an issue with the customer token.', 400);
        }
        $Customer = ($UserMonetizationService = new UserMonetizationService())->createCustomer($token, $request->get_param('customerName'), $request->get_param('customerEmail'));
        $PaymentIntent = $UserMonetizationService->tipVendor($Customer, $User, $amount);
        if (!(new StripeService())->isPaymentIntentValid($PaymentIntent)) {
            throw new UserMonetizationException('There was an issue creating the payment intent.', 400);
        }
        return ['message' => 'Success', 'data' => ['payment_intent_id' => $PaymentIntent->id], 'status' => '200'];
    }

    public function subscribeToVendor(\WP_REST_Request $request)
    {
        if (!($CurrentUser = User::createFromUserId($request->get_param('currentUserId'))) instanceof User) {
            throw new UserMonetizationException('The user must be logged in to make this request.', 400);
        }
        if (!($Vendor = User::createFromUserId($request->get_param('userId'))) instanceof User) {
            throw new UserMonetizationException('The user id was not sent in the request.', 400);
        }
        if (empty($amount = $request->get_param('amount'))) {
            throw new UserMonetizationException('The amount was not sent in the request.', 400);
        }
        if (empty($token = $request->get_param('customerToken'))) {
            throw new UserMonetizationException('There was an issue with the customer token.', 400);
        }

        if (empty($Product = ($UserMonetizationService = new UserMonetizationService())->getProductFromVendor($Vendor))) {
            $Product = $UserMonetizationService->createVendorAsProduct($Vendor);
        }
        if (!($StripeService = new StripeService())->isProductValid($Product)) {
            throw new UserMonetizationException('Their was an issue retrieving the product information from the user.', 400);
        }
        $Customer = $UserMonetizationService->createCustomer($token, $request->get_param('customerName'), $request->get_param('customerEmail'));
        if (!$StripeService->isCustomerValid($Customer)) {
            throw new UserMonetizationException('There was an issue creating the customer record in Stripe.', 400);
        }
        $Subscription = $UserMonetizationService->subscribeToVendor($Customer, $Vendor, $amount);
        if (!$StripeService->isSubscriptionValid($Subscription)) {
            throw new UserMonetizationException('There was an issue creating the subscription.', 400);
        }
        $UserMonetizationService->addCustomerToVendorMemberships($CurrentUser, $Vendor);
        return ['message' => 'Success', 'data' => ['subscription_id' => $Subscription->id], 'status' => '200'];
    }
}
