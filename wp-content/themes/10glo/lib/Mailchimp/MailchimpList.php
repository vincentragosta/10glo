<?php

namespace ChildTheme\Mailchimp;

/**
 * Class MailchimpList
 * @package ChildTheme\MailchimpList
 * @author Vincent Ragosta <vincentpasqualeragosta@gmail.com>
 * @version 1.0
 *
 * @property string $list_id
 */
class MailchimpList
{
    protected $list_id;

    public function __construct(string $list_id)
    {
        $this->list_id = $list_id;
    }

    public function getListId()
    {
        return $this->list_id;
    }

    public static function createFromConfig()
    {
        return defined('MAILCHIMP_LIST_ID') ?
            new static(MAILCHIMP_LIST_ID) : false;
    }
}
