<?php

use ChildTheme\Components\VideoPlayer\VideoPlayerView;
use ChildTheme\Video\Video;

if ($video_id = get_query_var('video')) {
    $query = (new \WP_Query([
        'post_type' => Video::POST_TYPE,
        'post_per_page' => 1,
        'name' => $video_id
    ]));
    if (!empty($posts = $query->posts)) {
        echo new VideoPlayerView(new Video($posts[0]));
    }
}
