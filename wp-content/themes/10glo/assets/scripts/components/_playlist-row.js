addAction(INIT, function() {
    var $sortContainer = $('.playlist-row__sort');
    if ($sortContainer.length) {
        $sortContainer.each(function(key, sort) {
            var $sort = $(sort);
            $sort.find('li').on('click', function() {
                var value = $(this).attr('data-value');
                var url = location.protocol + '//' + location.hostname + location.pathname;
                if (value === 'most-viewed') {
                    return location.replace(url + location.hash);
                }
                location.replace(url + '?sort=' + value + location.hash);
            });
        });
    }

    var $playlistLoadMoreButtons = $('.playlist-row__load-more');
    if (!$playlistLoadMoreButtons.length) {
        return;
    }
    $playlistLoadMoreButtons.each(function(key, button) {
        var $button = $(button);
        $button.on('click', function() {
            var $_this = $(this);
            var $playlist = $button.parents('.playlist-row');
            var $lastVideo = $playlist.find('.playlist-row__row:first-child > *:last-child');
            var data = $playlist.data();
            var nextPage = parseInt($playlist.attr('data-page')) + 1;
            data.page = nextPage;
            $.ajax({
                method: 'POST',
                url: sit.api_url + 'user/playlist/load',
                data: data,
                beforeSend: function (xhr) {
                    xhr.setRequestHeader('X-WP-Nonce', sit.nonce);
                    xhr.setRequestHeader('Authorization', 'Basic ' + sit.basic_auth);
                },
                success: function (response) {
                    if (!response.data.length) {
                        return;
                    }
                    var videoString = '';
                    var posts_per_page = $playlist.attr('data-posts-per-page') ? $playlist.attr('data-posts-per-page') : sit.posts_per_page;
                    $playlist.attr('data-page', nextPage);
                    response.data.forEach(function(item, index) {
                        videoString +=
                            '<div class="playlist-row__column col-12 col-sm-6 col-md-4 col-lg-3">' +
                                '<div data-gtm="video-poster-card" class="video-poster-card">' +
                                    '<a href="' + sit.home_url + '/watch/' + item.postName + '/" class="video-poster-card__permalink">' +
                                        '<img src="' + sit.home_url + '/wp-content/themes/10glo/dist/images/logo.png" class="video-poster-card__logo">' +
                                        '<div class="video-poster-card__poster-image" style="background-image: url(' + item.posterImageUrl + ');"></div>' +
                                    '</a>' +
                                    '<div class="video-poster-card__content-container">' +
                                        '<div data-gtm="user-card" class="user-card video-poster-card__user-card">' +
                                            '<div class="user-card__container">' +
                                                '<a href="' + sit.home_url + '/user/' + item.postAuthor.userNicename + '" class="user-card__permalink">' +
                                                    '<span class="user-card__image" style="background-image: url(' + item.postAuthor.userPhotoUrl + ')" title="User Profile Photo"></span>' +
                                                '</a>' +
                                            '</div>' +
                                        '</div>' +
                                        '<div class="video-poster-card__content-container-inner">' +
                                            '<h2 class="video-poster-card__heading heading"><a href="' + sit.home_url + '/watch/' + item.postName + '/">' + item.postTitle + '</a></h2>' +
                                            '<div class="video-poster-card__header-container">' +
                                                '<a href="' + sit.home_url + '/user/' + item.postAuthor.userNicename + '" class="video-poster-card__author-username">' + item.postAuthor.displayName + '</a>' +
                                                (item.postAuthor.isVerified ? '<img src="/wp-content/uploads/2020/09/check-mark.png" width="1000" height="1000" alt="check-mark">' : '') +
                                            '</div>' +
                                            '<ul class="video-poster-card__list list--inline list--dotted">' +
                                                '<li>' + item.views + ' views</li>' +
                                                '<li>' + item.postDate + '</li>' +
                                            '</ul>' +
                                        '</div>' +
                                    '</div>' +
                                '</div>' +
                            '</div>';
                    });
                    if ($(window).width <= 768) {
                        $([document.documentElement, document.body]).animate({
                            scrollTop: $lastVideo.offset().top - 80
                        }, 1000);
                    }
                    if (videoString.length) {
                        $playlist.find('.playlist-row__row--videos').append(videoString);
                    }
                    if (response.data.length < posts_per_page) {
                        $_this.parents('.playlist-row__row').remove();
                    }
                },
                error: function (response) {},
                fail: function (response) {}
            });
        });
    });
});
