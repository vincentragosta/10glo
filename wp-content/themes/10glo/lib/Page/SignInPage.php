<?php

namespace ChildTheme\Page;

use Backstage\Models\Page;

/**
 * Class SignInPage
 * @package ChildTheme\Page
 * @author Vincent Ragosta <vincentpasqualeragosta@gmail.com>
 * @version 1.0
 */
class SignInPage extends Page
{
    public function secondaryImage()
    {
        $image_id = $this->field('secondary_image_id');
        return \WP_Image::get_by_attachment_id($image_id);
    }
}
