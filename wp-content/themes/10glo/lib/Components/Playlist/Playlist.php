<?php

namespace ChildTheme\Components\Playlist;

use Backstage\Util;
use Backstage\VcLibrary\Support\Component;
use Backstage\View\Element;
use ChildTheme\Playlist\PlaylistRepository;
use ChildTheme\Video\VideoRepository;

/**
 * Class PlaylistRowView
 * @package ChildTheme\Components\PlaylistRow
 * @author Vincent Ragosta <vincentpasqualeragosta@gmail.com>
 * @version 1.0
 */
class Playlist extends Component
{
    const NAME = 'Playlist';
    const TAG = 'playlist_row';
    const VIEW = PlaylistView::class;

    protected $component_config = [
        'description' => 'Display a playlist from a playlist code.',
        'icon' => 'icon-wpb-toggle-small-expand',
        'wrapper_class' => 'clearfix',
        'is_container' => false,
        'category' => 'Content',
        'params' => [
            'heading' => [
                'type' => 'textfield',
                'heading' => 'Heading Text',
                'param_name' => 'heading_text',
                'description' => 'Set optional heading text.',
                'admin_label' => true,
                'group' => 'Heading'
            ],
            [
                'type' => 'dropdown',
                'heading' => 'Tag',
                'param_name' => 'heading_tag',
                'value' => [
                    'H2' => 'h2',
                    'H3' => 'h3',
                    'H4' => 'h4',
                    'H1' => 'h1'
                ],
                'description' => 'Set the heading tag [H1 -> H4]. This is for accessibility purposes only, this will not affect appearance. H1 should only be used once at the top of the page (if page title is not automatically added).',
                'group' => 'Heading',
                'admin_label' => true
            ],
            [
                'type' => 'dropdown',
                'heading' => 'Size',
                'param_name' => 'heading_size',
                'description' => 'Set the heading size.',
                'value' => [
                    'Default' => 'default',
                    'Medium' => 'medium',
                    'Large' => 'large',
                    'Extra Large' => 'xlarge'
                ],
                'group' => 'Heading',
                'admin_label' => true
            ],
            [
                'type' => 'textfield',
                'heading' => 'Playlist ID',
                'param_name' => 'playlist_id',
                'description' => 'Enter the playlist ID.',
                'admin_label' => true
            ],
            [
                'type' => 'textfield',
                'heading' => 'Number of videos',
                'param_name' => 'posts_per_page',
                'description' => 'Enter the max number of videos to display. Enter -1 for all videos in the playlist.',
                'admin_label' => true
            ],
            [
                'type' => 'checkbox',
                'heading' => 'Show Sort?',
                'param_name' => 'show_sort',
                'description' => 'Display Most Recent, Most Viewed and Top Rated dropdown.',
                'group' => 'Settings'
            ],
        ]
    ];

    protected function createView(array $atts)
    {
        if (!($Playlist = (new PlaylistRepository())->findById($atts['playlist_id'])) instanceof \ChildTheme\Playlist\Playlist) {
            return '';
        }
        /**
         * @var \ChildTheme\Playlist\Playlist $Playlist
         * @var PlaylistView $View
         */
        $ViewClass = self::VIEW;
        $posts_per_page = $atts['posts_per_page'] ?: get_option('posts_per_page');
        $args = [
            'posts_per_page' => $posts_per_page,
            'criteria' => isset($_GET['sort']) ? $_GET['sort'] : 'views'
        ];
        $videos = (new VideoRepository())->findFromPlaylist($Playlist, $args);
        $Heading = $atts['heading_text'] ? Element::create($atts['heading_tag'], $atts['heading_text'])->class(Util::componentClasses('heading', [$atts['heading_size']])) : null;
        $View = new $ViewClass($videos, $Heading);
        return $View
            ->toggleLoadMore(count($videos) == $posts_per_page)
            ->showSort($atts['show_sort'] ?: false)
            ->elementAttributes($this->getDataFromComponentAttributes($atts));
    }

    protected function getDataFromComponentAttributes($attributes)
    {
        if (empty($attributes)) {
            return [];
        }
        $dataAttributes = [];
        foreach(array_filter($attributes) as $key => $value) {
            $key = str_replace('_', '-', $key);
            $dataAttributes["data-{$key}"] = $value;
        }
        $dataAttributes['data-page'] = 1;
        $dataAttributes['data-criteria'] = isset($_GET['sort']) ? $_GET['sort'] : 'views';
        return $dataAttributes;
    }
}
