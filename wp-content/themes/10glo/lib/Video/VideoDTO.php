<?php

namespace ChildTheme\Video;

use Backstage\Models\DTO;
use ChildTheme\User\User;
use ChildTheme\User\UserDTO;

/**
 * Class VideoDTO
 * @package ChildTheme\Video
 * @author Vincent Ragosta <vragosta@situationinteractive.com>
 * @version 1.0
 *
 * @property string $id
 * @property string $post_title
 * @property string $post_name
 * @property string $post_status
 * @property string $post_author
 * @property string $post_date
 * @property string $poster_image_url
 * @property string $sharer_image_url
 * @property string $video_url
 * @property string $views
 * @property bool $is_author_verified
 */
class VideoDTO extends DTO
{
    protected $id;
    protected $post_title;
    protected $post_name;
    protected $post_status;
    protected $post_author;
    protected $post_date;
    protected $poster_image_url;
    protected $sharer_image_url;
    protected $video_url;
    protected $views;

    public function __construct(Video $Video)
    {
        $this->id = $Video->ID;
        $this->post_title = html_entity_decode($Video->title());
        $this->post_name = $Video->post_name;
        $this->post_status = $Video->post_status;
        $this->post_author = new UserDTO(User::createFromPost($Video));
        $this->post_date = $Video->getRelativePublishedDate();
        $this->poster_image_url = $Video->poster_image_url;
        $this->sharer_image_url = $Video->sharer_image_url;
        $this->video_url = $Video->video_url;
        $this->views = $Video->views;
    }
}
