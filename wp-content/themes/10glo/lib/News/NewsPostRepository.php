<?php

namespace ChildTheme\News;

use Backstage\Repositories\PostRepository;
use ChildTheme\Options\GlobalOptions;

/**
 * Class NewsRepository
 * @package ChildTheme\News
 * @author Vincent Ragosta <vincentpasqualeragosta@gmail.com>
 * @version 1.0
 */
class NewsPostRepository extends PostRepository
{
    const FEATURED_STORIES_COUNT = 3;
    const DEFAULT_PAGE = 1;
    protected $model_class = NewsPost::class;

    public function findHeadlines()
    {
        if (!($HeadlinesCategory = GlobalOptions::headlinesCategory()) instanceof \WP_Term) {
            return [];
        }
        return $this->findWithTermIds([$HeadlinesCategory->term_id], NewsPost::TAXONOMY, static::FEATURED_STORIES_COUNT);
    }

    public function findPinnedByNewsCategoryId(string $category_id)
    {
        if (empty($category_id)) {
            return [];
        }
        return $this->find([
            'posts_per_page' => '3',
            'tax_query' => [
                [
                    'taxonomy' => NewsPost::TAXONOMY,
                    'terms' => $category_id
                ]
            ],
            'meta_query' => [
                [
                    'key' => 'pinned',
                    'value' => true,
                    'compare' => '='
                ]
            ]
        ]);
    }

    public function findCurated(array $atts)
    {
        $args = [
            'paged' => $atts['paged'] ?: self::DEFAULT_PAGE,
            'posts_per_page' => $atts['posts_per_page'] ?: get_option('posts_per_page'),
            'post__not_in' => strpos($atts['post__not_in'], ',') ? explode(',', $atts['post__not_in']) : $atts['post__not_in']
        ];
        if (!empty($category_id = $atts['story_category_id'])) {
            $args['tax_query'] = [
                [
                    'taxonomy' => NewsPost::TAXONOMY,
                    'terms' => $category_id
                ]
            ];
        }
        return $this->find($args) ?: [];
    }

    public function findRecent(array $atts = [])
    {
        $args = [
            'post_status' => 'publish',
            'posts_per_page' => $atts['posts_per_page'] ?: get_option('posts_per_page')
        ];
        if (isset($atts['post__not_in'])) {
            $args['post__not_in'] = strpos($atts['post__not_in'], ',') ? explode(',', $atts['post__not_in']) : $atts['post__not_in'];
        }
        return $this->find($args);
    }

    public function findByNewsCategoryId(string $story_category_id, array $atts = [])
    {
        if (empty($story_category_id)) {
            return [];
        }
        $args = [
            'post_status' => 'publish',
            'posts_per_page' => $atts['posts_per_page'] ?: get_option('posts_per_page'),
            'tax_query' => [
                [
                    'taxonomy' => NewsPost::TAXONOMY,
                    'terms' => $story_category_id
                ]
            ]
        ];
        if (isset($atts['post__not_in'])) {
            $args['post__not_in'] = strpos($atts['post__not_in'], ',') ? explode(',', $atts['post__not_in']) : $atts['post__not_in'];
        }
        return $this->find($args);
    }
}
