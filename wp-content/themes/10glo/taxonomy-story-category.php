<?php

use Backstage\SetDesign\SubNav\SubNavView;
use ChildTheme\Components\NewsPostCard\NewsPostCardView;
use ChildTheme\Components\StoryCard\StoryCardView;
use ChildTheme\Controller\NewsPostController;
use ChildTheme\News\NewsPost;
use ChildTheme\News\NewsPostRepository;

$query_var = get_query_var(NewsPostController::STORY_QUERY_VAR);
if (empty($query_var)) {
    wp_redirect(home_url());
    exit();
}
$StoryCategory = get_term_by('slug', $query_var, NewsPost::TAXONOMY);
?>
<section class="content-section anchor content-section--has-bg content-section--dark content-section--tpad-double content-section--bpad-double">
    <span class="content-background-image">
        <span class="content-background-image__images">
            <span class="content-background-image__img content-background-image__img--desktop" style="background-image: url(&quot;https://10glo.vincentragosta.dev/wp-content/uploads/2020/09/10glo-Stories-Hero-V3.png&quot;);"></span>
        </span>
    </span>
    <div class="container content-section__container">
        <div class="content-row row">
            <div class="content-column stories-hero-row col-md-6 content-column--last">
                <div class="content-column__inner">
                    <div class="vc-single-image vc-single-image--center">
                        <figure class="vc-single-image__figure image-loaded">
                            <img class="lazy-image-loaded vc-single-image__image in-view" src="/wp-content/uploads/2020/09/logo-1080-edges.png" alt="logo-1080-edges" width="783" height="714">
                        </figure>
                    </div>
                    <h1 class="heading heading--xlarge">News</h1>
                </div>
            </div>
        </div>
    </div>
</section>
<div class="content-section content-section--extended">
    <div class="content-section__container container">
        <div class="content-row row">
            <div class="content-column col-md-12 content-column--last border-exclude">
                <div class="content-column__inner">
                    <h2 class="heading heading--medium" style="margin-bottom: 0;"><?= $StoryCategory->name; ?></h2>
                    <a href="<?= home_url('/news/'); ?>" style="font-size: 12px; color: #666;">Back to News</a>
                </div>
            </div>
        </div>
    </div>
</div>
<?php if (!empty($news = (new NewsPostRepository())->findWithTermIds([$StoryCategory->term_id], NewsPost::TAXONOMY))): ?>
    <div class="content-section content-section--extended">
        <div class="content-section__container container">
            <div class="content-row row">
                <?php foreach($news as $NewsPost): ?>
                    <div class="content-column col-12 col-sm-6 col-md-3">
                        <div class="content-column__inner">
                            <?= new NewsPostCardView($NewsPost); ?>
                        </div>
                    </div>
                <?php endforeach; ?>
            </div>
        </div>
    </div>
<?php else: ?>
    <div class="content-section content-section--extended">
        <div class="content-section__container container">
            <div class="content-row row">
                <div class="content-column col-12 border-exclude">
                    <div class="content-column__inner">
                        <p>There are no news tagged with that category.</p>
                    </div>
                </div>
            </div>
        </div>
    </div>
<?php endif; ?>
