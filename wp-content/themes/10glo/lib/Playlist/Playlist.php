<?php

namespace ChildTheme\Playlist;

use Backstage\Models\PostBase;

/**
 * Class Playlist
 * @package ChildTheme\Playlist
 * @author Vincent Ragosta <vincentpasqualeragosta@gmail.com>
 * @version 1.0
 *
 * @property string $post_status
 * @property string $post_author
 * @property array|string $video_ids
 * @property string $post_name
 */
class Playlist extends PostBase
{
    const POST_TYPE = 'playlist';
}
