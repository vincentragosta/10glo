<?php

namespace ChildTheme\Exception;

use Exception;

/**
 * Class UserRegistrationException
 * @package ChildTheme\Exception
 * @author Vincent Ragosta <vincentpasqualeragosta@gmail.com>
 * @version 1.0
 */
class UserRegistrationException extends Exception {}
