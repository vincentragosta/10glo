<?php
/**
 * Expected:
 * @var string $url
 * @var string $label
 * @var string $icon_text
 * @var array $class_modifiers
 * @var array $element_attributes
 */

use Backstage\SetDesign\Icon\IconView;
use Backstage\Util;

?>

<div <?= Util::componentAttributes('stripe-connect', $class_modifiers, $element_attributes); ?>>
    <div class="stripe-connect__icon-container">
        <?= new IconView(['icon_name' => 'stripe']); ?>
    </div>
    <div class="stripe-connect__link-container">
        <a href="<?= $url; ?>" class="stripe-connect__link" target="_blank">
            <span class="stripe-connect__heading heading heading--default"><?= $label; ?></span>
            <span class="stripe-connect__icon"><?= $icon_text; ?> <?= new IconView(['icon_name' => 'share']); ?></span>
        </a>
    </div>
</div>
