<?php

namespace ChildTheme\Exception;

use Exception;

/**
 * Class MailchimpException
 * @package ChildTheme\Exception
 * @author Vincent Ragosta <vincentpasqualeragosta@gmail.com>
 * @version 1.0
 */
class MailchimpException extends Exception {}
