<?php

namespace ChildTheme\Controller;

use ChildTheme\Podcast\Podcast;
use ChildTheme\Podcast\PodcastRepository;

/**
 * Class GlobalOptionsController
 * @package ChildTheme\Controller
 * @author Vincent Ragosta <vincentpasqualeragosta@gmail.com>
 * @version 1.0
 */
class GlobalOptionsController
{
    public function __construct()
    {
//        add_action('init', function() {
            add_action('acf/save_post', [$this, 'libsynSync'], 5);
//        });
    }

    public function libsynSync($post_id)
    {
        if (get_current_screen()->id === 'toplevel_page_acf-global' && $libsyn_sync = get_field('libsyn_sync', 'option')) {
            $cURLConnection = curl_init();
            curl_setopt($cURLConnection, CURLOPT_URL, 'https://the10gloshow.libsyn.com/rss');
            curl_setopt($cURLConnection, CURLOPT_RETURNTRANSFER, true);
            $xml = simplexml_load_string(curl_exec($cURLConnection), 'SimpleXMLElement', LIBXML_NOCDATA);
            curl_close($cURLConnection);
            $json = json_encode($xml);
            $array = json_decode($json, TRUE);

            if (!empty($items = array_shift(array_column($array, 'item')))) {
                $PodcastRepository = new PodcastRepository();
                foreach ($items as $item) {
                    $Podcast = new Podcast();
                    if (empty($title = $item['title'])) {
                        continue;
                    }
                    $Podcast->post_title = $title;
                    if (!empty($content = $item['description'])) {
                        $Podcast->post_content = $content;
                    }
                    if (!empty($libsyn_link = $item['link'])) {
                        $Podcast->libsyn_link = $libsyn_link;
                    }
                    if (!empty($guid = $item['guid'])) {
                        $Podcast->libsyn_guid = $guid;
                    }
                    if (!empty($pub_date = $item['pubDate'])) {
                        $Podcast->libsyn_published_date = $pub_date;
                    }
                    if (!empty($podcast_length = $item['enclosure']['@attributes']['length'])) {
                        $Podcast->podcast_length = $podcast_length;
                    }
                    if (!empty($podcast_type = $item['enclosure']['@attributes']['type'])) {
                        $Podcast->podcast_type = $podcast_type;
                    }
                    if (!empty($podcast_url = $item['enclosure']['@attributes']['url'])) {
                        $Podcast->podcast_url = $podcast_url;
                    }
                    $Podcast->post_status = 'publish';

                    if ($PodcastRepository->findByTitle($title)) {
                        continue;
                    }
                    $PodcastRepository->add($Podcast);
                }
            }
        }
    }
}
