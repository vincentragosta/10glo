<?php
/**
 * Template Name: Sign In
 */

use Backstage\Models\Page;
use ChildTheme\Components\Messaging\MessagingView;
use ChildTheme\Options\GlobalOptions;
use ChildTheme\Page\SignInPage;

$Page = SignInPage::createFromGlobal();
?>

<section class="content-section content-section--width-narrow">
    <div class="content-section__container container">
        <div class="content-row row messaging__section">
            <div class="content-column col-12">
                <div class="content-column__inner">
                    <?= new MessagingView(); ?>
                </div>
            </div>
        </div>
        <div class="content-row row content-row--mb-double">
            <div class="content-column col-12 text--center">
                <div class="content-column__inner">
                    <h2 class="heading heading--xlarge">Welcome back.<br />Please sign in.</h2>
                </div>
            </div>
        </div>
        <div class="content-row row">
            <div class="content-column col-12 text--center">
                <div class="content-column__inner">
                    <form name="login-form" id="login-form" action="" method="post">
                        <p class="login-username">
                            <label for="user_login">Username or Email Address</label>
                            <input type="text" name="log" id="user_login" class="input" value="" size="20" placeholder="Email Address" required>
                        </p>
                        <p class="login-password">
                            <label for="user_pass">Password</label>
                            <input type="password" name="pwd" id="user_pass" class="input" value="" size="20" placeholder="Password" required>
                        </p>
                        <p class="login-submit">
                            <input type="submit" name="wp-submit" id="wp-submit" class="button button-primary" value="Sign In">
                            <input type="hidden" name="redirect_to" value="<?= home_url('/profile/'); ?>">
                        </p>
                    </form>
                </div>
            </div>
        </div>
        <div class="content-row row">
            <div class="content-column col-12 text--center">
                <div class="content-column__inner">
                    <?php if (($RegistrationPage = GlobalOptions::registrationPage()) instanceof Page): ?>
                        <p>Dont have an account? <a href="<?= $RegistrationPage->permalink(); ?>">Join for free here.</a></p>
                    <?php endif; ?>
                    Forgot Your Password? Click <a href="mailto:hello@10glo.com">here</a>.
                </div>
            </div>
        </div>
    </div>
</section>
