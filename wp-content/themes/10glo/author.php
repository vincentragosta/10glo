<?php

use Backstage\Util;
use Backstage\View\Element;
use ChildTheme\Components\MembershipForm\MembershipFormView;
use ChildTheme\Components\Messaging\MessagingView;
use ChildTheme\Components\Playlist\PlaylistView;
use ChildTheme\Components\SupportForm\SupportFormView;
use ChildTheme\Components\UserCard\UserCardView;
use ChildTheme\Components\VideoPosterCard\VideoPosterCardView;
use ChildTheme\Options\GlobalOptions;
use ChildTheme\Playlist\PlaylistRepository;
use ChildTheme\Service\UserMonetizationService;
use ChildTheme\Service\UserService;
use ChildTheme\Service\UserSubscribeService;
use ChildTheme\User\User;
use ChildTheme\Video\VideoRepository;

$Author = User::createFromAuthor();
$CurrentUser = User::createFromCurrentUser();

$personal_website_url = $Author->getPersonalWebsiteUrl();
$facebook_url = $Author->getFacebookUrl();
$instagram_url = $Author->getInstagramUrl();
$twitter_url = $Author->getTwitterUrl();

?>

<section class="content-section content-section--hero">
    <div class="content-section__container container">
        <div class="content-row row">
            <div class="content-column col-12">
                <div class="content-column__inner">
                    <div class="profile-cover-photo image-upload">
                        <div class="profile-cover-photo__cover-photo-container <?= empty($profile_cover_photo_url = $Author->getProfileCoverPhotoUrl()) ? 'profile-cover-photo__image--hide' : ''; ?>">
                            <div class="profile-cover-photo__cover-photo-inner">
                                <img src="<?= $Author->getProfileCoverPhotoUrl(); ?>" class="profile-cover-photo__image" <?= ($coordinate = $Author->getProfileCoverPhotoCoordinate()) ? 'style="transform: translate(0, ' . $coordinate . '%);"' : ''; ?> />
                            </div>
                        </div>
                        <div class="profile-cover-photo__placeholder <?= empty($profile_cover_photo_url) ? 'profile-cover-photo__placeholder--show' : ''; ?>">
                            <div class="profile-cover-photo__placeholder-inner">
                                <h2 class="profile-cover-photo__placeholder-title">Insert Cover Photo</h2>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
<section class="content-section">
    <div class="content-section__container container">
        <div class="content-row row">
            <div class="content-column col-12 col-sm-6">
                <div class="content-column__inner">
                    <div class="profile-nameplate">
                        <div class="profile-nameplate__user-card image-upload">
                            <?= new UserCardView($Author->getUserPhotoUrl()); ?>
                        </div>
                        <div class="profile-nameplate__content">
                            <div class="profile-nameplate__content-inner">
                                <h1 class="heading heading--medium">
                                    <?= $Author->getDisplayName(); ?>
                                </h1>
                                <?php if ($Author->isVerified() && ($verified_artists_image = GlobalOptions::verifiedArtistsImage())): ?>
                                    <div class="profile-nameplate__verified">
                                        <?= $verified_artists_image; ?>
                                    </div>
                                <?php endif; ?>
                            </div>
                            <p><?= ($UserSubscribeService = new UserSubscribeService())->getFollowersCount($Author); ?> Subscribers</p>
                        </div>
                    </div>
                </div>
            </div>
            <div class="content-column col-12 col-sm-6 align-self-center text--right">
                <div class="content-column__inner">
                    <button class="subscribe-to-user button button--secondary" data-user-id="<?= $Author->ID; ?>" <?= $UserSubscribeService->isUserFollowing($CurrentUser, $Author) ? 'disabled' : ''; ?>>Subscribe</button>
                </div>
            </div>
        </div>
    </div>
</section>
<section class="content-section content-section--mb-half content-section--width-narrow messaging__section">
    <div class="content-section__container container">
        <div class="content-row row">
            <div class="content-column col-12">
                <div class="content-column__inner">
                    <?= new MessagingView(); ?>
                </div>
            </div>
        </div>
    </div>
</section>
<section class="content-section content-section--mb-none">
    <div class="container content-section__container">
        <div class="content-row row">
            <div class="content-column col-md-12 content-column--last">
                <div class="content-column__inner">
                    <div class="profile-tabs">
                        <div class="profile-tabs sub-nav sub-nav--horizontal sub-nav--text">
                            <ul class="sub-nav__list text--left" role="tablist">
                                <li class="sub-nav__item" role="presentation">
                                    <a class="sub-nav__link" href="#videos">Videos</a></li>
                                <li class="sub-nav__item" role="presentation">
                                    <a class="sub-nav__link" href="#playlists">Playlists</a></li>
                                <?php if ($Author->hasAboutAttributes()): ?>
                                    <li class="sub-nav__item" role="presentation">
                                        <a class="sub-nav__link" href="#about">About</a></li>
                                <?php endif; ?>
                            </ul>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
<?php if (($UserMonetizationService = new UserMonetizationService())->isVerified($Author)): ?>
    <section class="content-section content-section--has-bg content-section--gray-light-tint content-section--tpad-double content-section--bpad-double content-section--mb-none support-section">
        <div class="content-section__container container">
            <div class="content-row row">
                <div class="content-column col-12 col-md-6">
                    <div class="content-column__inner">
                        <?php if (!empty($support_message = $UserMonetizationService->getSupportMessage($Author))): ?>
                            <?= $support_message; ?>
                        <?php endif; ?>
                    </div>
                </div>
                <div class="content-column col-12 col-md-6 text--center">
                    <div class="content-column__inner">
                        <div class="support-section__forms">
                            <div class="support-section__tabs">
                                <ul class="support-section__tab-list list list--inline">
                                    <li class="support-section__tab-list-item support-section__tab-list-item--active"><button class="button button--secondary" data-tab-id="support">Support</button></li>
                                    <li class="support-section__tab-list-item"><button class="button button--secondary" data-tab-id="membership">Membership</button></li>
                                </ul>
                            </div>
                            <div class="support-section__content">
                                <div class="support-section__tab support-section__tab--show" id="support">
                                    <h2 class="heading heading--default">Give a one-time tip to <span class="text--secondary"><?= $Author->getDisplayName(); ?></span></h2>
                                    <?= (new SupportFormView($Author))->elementAttributes(['class' => 'support-section__form']); ?>
                                </div>
                                <div class="support-section__tab support-section__tab-show" id="membership">
                                    <?php if (!(new UserService())->isValidUser($CurrentUser)): ?>
                                        <p>You must be signed into your 10glo account to become a member of <?= $Author->getDisplayName(); ?>'s channel.</p>
                                        <a href="<?= home_url('/sign-in/'); ?>" class="button button--secondary button--block">Please Sign In</a>
                                    <?php else: ?>
                                        <?= (new MembershipFormView($Author))->elementAttributes(['class' => 'support-section__form']); ?>
                                    <?php endif; ?>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
<?php endif; ?>
<section
    class="profile-tabs__content content-section content-section--has-bg content-section--gray-light-tint content-section--tpad-double content-section--bpad-double content-section--mb-none"
    role="tabpanel" aria-labelledby="home-sub-nav">
    <div class="container content-section__container">
        <div class="content-row row" data-toggle-target="videos" data-toggle-default="">
            <div class="content-column col-md-12 content-column--last">
                <div class="content-column__inner">
                    <div class="profile-tabs__tab-content profile-tabs__tab-content--videos">
                        <?php if (!empty($videos = (new VideoRepository())->findPublishedByAuthor($Author))): ?>
                            <div class="row">
                                <?php foreach ($videos as $Video): ?>
                                    <div class="col-12 col-sm-6 col-md-4 col-lg-3">
                                        <?= new VideoPosterCardView($Video); ?>
                                    </div>
                                <?php endforeach; ?>
                            </div>
                        <?php else: ?>
                            <p>This user currently does not have any videos.</p>
                        <?php endif; ?>
                    </div>
                </div>
            </div>
        </div>
        <div class="content-row row" data-toggle-target="playlists">
            <div class="content-column col-md-12 content-column--last">
                <div class="content-column__inner">
                    <div class="profile-tabs__tab-content profile-tabs__tab-content--playlists">
                        <div class="playlist__container">
                            <?php if (!empty($playlists = (new PlaylistRepository())->findAllFromUser($Author))): ?>
                                <?php foreach($playlists as $Playlist): ?>
                                    <div class="playlist playlist--<?= $Playlist->post_name; ?>">
                                        <?php
                                        $args = [
                                            'data-page' => 1,
                                            'data-posts-per-page' => get_option('posts_per_page'),
                                            'data-playlist-id' => $Playlist->ID
                                        ];
                                        if (isset($_GET['sort'])) {
                                            $args['data-criteria'] = ($_GET['sort'] == 'most-viewed') ? 'views' : 'likes_count';
                                        }
                                        $videos = (new VideoRepository())->findFromPlaylist($Playlist, isset($_GET['sort']) ? ['criteria' => $_GET['sort'] == 'most-viewed' ? 'views' : 'likes_count'] : []);
                                        $View = new PlaylistView($videos, Element::create('h2', $Playlist->post()->post_title)->class(Util::componentClasses('heading', ['medium'])));
                                        echo $View
                                            ->toggleLoadMore(count($videos) == get_option('posts_per_page'))
                                            ->showSort(true)
                                            ->elementAttributes($args);
                                        ?>
                                    </div>
                                <?php endforeach; ?>
                            <?php else: ?>
                                <p>This user doesn't currently have any playlists.</p>
                            <?php endif; ?>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <?php if ($Author->hasAboutAttributes()): ?>
            <div class="content-row row" data-toggle-target="about">
                <div class="content-column col-md-12 content-column--last">
                    <div class="content-column__inner">
                        <div class="profile-about-tab">
                            <div class="row">
                                <div class="col-12 col-sm-6">
                                    <?php if (!empty($bio = $Author->getBio())): ?>
                                        <div class="profile-about-tab__bio-container">
                                            <h2 class="profile-about-tab__heading heading heading--medium">Bio</h2>
                                            <div class="profile-about-tab__bio external-links">
                                                <?= $bio; ?>
                                            </div>
                                        </div>
                                    <?php endif; ?>
                                    <?php if (!empty($location = $Author->getLocation())): ?>
                                        <div class="profile-about-tab__location-container">
                                            <h2 class="profile-about-tab__heading heading--medium">Location</h2>
                                            <p class="profile-about-tab__location"><?= $location; ?></p>
                                        </div>
                                    <?php endif; ?>
                                    <?php if (!empty($school = $Author->getSchool())): ?>
                                        <div class="profile-about-tab__school-container">
                                            <h2 class="profile-about-tab__heading heading heading--medium">School with Major/Minor</h2>
                                            <p class="profile-about-tab__school"><?= $school; ?></p>
                                        </div>
                                    <?php endif; ?>
                                </div>
                                <?php if (!empty($personal_website_url) || !empty($facebook_url) || !empty($instagram_url) || !empty($twitter_url)): ?>
                                    <div class="col-12 col-sm-6">
                                        <h2 class="profile-about-tab__heading heading heading--medium">Social</h2>
                                        <ul class="profile-about-tab__links list--unstyled">
                                            <?php if (!empty($personal_website_url)): ?>
                                                <li>
                                                    <strong class="heading">Personal Website URL</strong>
                                                    <p class="profile-about-tab__personal-website"><a href="<?= $personal_website_url; ?>" target="_blank"><?= preg_replace('#^https?://#', '', $personal_website_url); ?></a></p>
                                                </li>
                                            <?php endif; ?>
                                            <?php if (!empty($facebook_url)): ?>
                                                <li>
                                                    <strong class="heading">Facebook URL</strong>
                                                    <p class="profile-about-tab__facebook"><a href="<?= $facebook_url; ?>" target="_blank"><?= preg_replace('#^https?://#', '', $facebook_url); ?></a></p>
                                                </li>
                                            <?php endif; ?>
                                            <?php if (!empty($instagram_url)): ?>
                                                <li>
                                                    <strong class="heading">Instagram URL</strong>
                                                    <p class="profile-about-tab__instagram"><a href="<?= $instagram_url; ?>" target="_blank"><?= preg_replace('#^https?://#', '', $instagram_url); ?></a></p>
                                                </li>
                                            <?php endif; ?>
                                            <?php if (!empty($twitter_url)): ?>
                                                <li>
                                                    <strong class="heading">Twitter URL</strong>
                                                    <p class="profile-about-tab__twitter"><a href="<?= $twitter_url; ?>" target="_blank"><?= preg_replace('#^https?://#', '', $twitter_url); ?></a></p>
                                                </li>
                                            <?php endif; ?>
                                        </ul>
                                        <?php if ($bpve_url = $Author->getBroadwayPlusVirtualExperience()): ?>
                                            <strong class="heading">Sign up for my BroadwayPlus Virtual Experience</strong>
                                            <p><a href="<?= $bpve_url; ?>" target="_blank"><?= preg_replace('#^https?://#', '', $bpve_url); ?></a></p>
                                        <?php endif; ?>
                                        <?php if ($purchase_url = $Author->getPurchaseMySheetMusical()): ?>
                                            <strong class="heading">Purchase sheet music</strong>
                                            <p><a href="<?= $purchase_url; ?>" target="_blank"><?= preg_replace('#^https?://#', '', $purchase_url); ?></a></p>
                                        <?php endif; ?>
                                    </div>
                                <?php endif; ?>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        <?php endif; ?>
    </div>
</section>



