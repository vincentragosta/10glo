<div class="profile-cover-photo-remove-modal">
    <h2 class="profile-cover-photo-remove-modal__heading heading heading--large">Warning</h2>
    <p>This process cannot be undone. Once a photo is removed from the website it is lost forever. Please select a removal option:</p>
    <button class="profile-cover-photo-remove-modal__button button">Remove Photo</button>
</div>
