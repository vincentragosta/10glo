<?php

namespace ChildTheme\Controller;

use ChildTheme\Options\GlobalOptions;

/**
 * Class UserAccessController
 * @package ChildTheme\Controller
 * @author Vincent Ragosta <vincentpasqualeragosta@gmail.com>
 * @version 1.0
 */
class UserAccessController
{
    public function __construct()
    {
        add_filter('login_url', [$this, 'loginUrl']);
        add_action('template_redirect', [$this, 'redirectIfNotLoggedIn']);
        add_action('init', [$this, 'blockUsersFromAccessingBackend']);
        add_action('init', [$this, 'setUserBaseUrl']);
    }

    public function loginUrl($url)
    {
        return GlobalOptions::loginPage()->permalink();
    }

    public function redirectIfNotLoggedIn()
    {
        if (is_page_template('templates/profile.php') && !is_user_logged_in()) {
            wp_redirect(home_url('/'));
            exit();
        }
    }

    public function setUserBaseUrl()
    {
        global $wp_rewrite;
        $wp_rewrite->author_base = 'user';
    }

    public function blockUsersFromAccessingBackend()
    {
        if (is_admin() && !(current_user_can('administrator') || current_user_can('editor')) &&
            !(defined('DOING_AJAX') && DOING_AJAX)) {
            wp_redirect(home_url());
            exit;
        }
    }
}
