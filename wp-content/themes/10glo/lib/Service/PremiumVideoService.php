<?php

namespace ChildTheme\Service;

use ChildTheme\User\User;
use ChildTheme\Video\Video;
use ChildTheme\Video\VideoRepository;
use Stripe\Checkout\Session;
use Stripe\Price;
use Stripe\Product;

/**
 * Class PremiumVideoService
 * @package ChildTheme\Service
 * @author Vincent Ragosta <vincentpasqualeragosta@gmail.com>
 * @version 1.0
 */
class PremiumVideoService
{
    const VIDEO_PRODUCT_ID_META = 'stripe_product_id';
    const VIDEO_PRICE_ID_META = 'stripe_price_id';
    const VIDEO_MONTHLY_MEMBERS_META = 'monthly_members';
    const USER_PREMIUM_VIDEO_SERVICE_META = 'user_premium_video_service';
    const USER_PREMIUM_VIDEO_SERVICE_VIDEOS_META = 'user_premium_video_service_videos';
    const USER_PAID_PREMIUM_VIDEOS_META = 'user_paid_premium_videos';
    const PAYMENT_SETTINGS_LINK = '/premium-video/%s';
    const VIDEO_APPLICATION_FEE = .3;
    const VIDEO_CHECKOUT_SUCCESS_URL = '/watch/%s?session_id={CHECKOUT_SESSION_ID}';
    const VIDEO_CHECKOUT_CANCEL_URL = '/watch/%s?payment=fail';
    const DEFAULT_CURRENCY = 'usd';

    public function enable(Video $Video)
    {
        if (!(new UserMonetizationService())->isVerified(($Author = User::createFromPost($Video)))) {
            return false;
        }
//        $this->setVideoAsPrivate($Video);
        return $this->addVideoToUserPremiumVideos($Video, $Author);
    }

    public function disable(Video $Video)
    {
        $this->markProductAsInactive($Video);
        $this->markPriceAsInactive($Video);

        $this->unsetProductOnVideo($Video);
        $this->unsetPriceOnVideo($Video);

        $this->setVideoAsPublish($Video);
        $this->removeVideoFromUserPremiumVideos($Video, User::createFromPost($Video));
    }

    // User Specific Methods

    public function isUserInValidCountry(User $User)
    {
        if (empty($country = $User->getCountry())) {
            return false;
        }
        return in_array($country, StripeService::VALID_COUNTRIES);
    }

    public function isEnabled(User $User)
    {
        return $User->getField(static::USER_PREMIUM_VIDEO_SERVICE_META, true);
    }

    public function getUserPremiumVideos(User $User)
    {
        return !empty($video_ids = $User->getField(static::USER_PREMIUM_VIDEO_SERVICE_VIDEOS_META)) ? json_decode($video_ids, true) : [];
    }

    public function addVideoToUserPremiumVideos(Video $Video, User $User)
    {
        if (!in_array($Video->ID, $premium_video_ids = $this->getUserPremiumVideos($User))) {
            $premium_video_ids[] = $Video->ID;
        }
        return $User->updateField(static::USER_PREMIUM_VIDEO_SERVICE_VIDEOS_META, json_encode($premium_video_ids));
    }

    public function getUserPaidVideos(User $User)
    {
        return !empty($video_ids = $User->getField(static::USER_PAID_PREMIUM_VIDEOS_META)) ? json_decode($video_ids, true) : [];
    }

    public function addVideoToUserPaidVideos(Video $Video, User $User)
    {
        if (!in_array($Video->ID, $paid_video_ids = $this->getUserPaidVideos($User))) {
            $paid_video_ids[] = $Video->ID;
        }
        return $User->updateField(static::USER_PAID_PREMIUM_VIDEOS_META, json_encode($paid_video_ids));
    }

    public function removeVideoFromUserPremiumVideos(Video $Video, User $User)
    {
        if (($position = array_search($Video->ID, $premium_video_ids = $this->getUserPremiumVideos($User))) !== false) {
            unset($premium_video_ids[$position]);
        }
        return $User->updateField(static::USER_PREMIUM_VIDEO_SERVICE_VIDEOS_META, json_encode($premium_video_ids));
    }

    public function canUserWatchVideo(Video $Video, $User)
    {
        if (is_null($User)) {
            return false;
        }
        $VideoAuthor = User::createFromPost($Video);
        if ((new UserService())->isSameUser($User, $VideoAuthor)) {
            return true;
        }
        return in_array($Video->ID, $this->getUserPaidVideos($User));
    }

    public function setPaidVideoOnUserFromSession(Session $Session, Video $Video)
    {
        if (!(new StripeService())->isCheckoutSessionStatusPaid($Session)) {
            return false;
        }
        return $this->addVideoToUserPaidVideos($Video, User::createFromCurrentUser());
    }

    // Video Specific Methods

    public function isPremiumVideo(Video $Video)
    {
        return in_array($Video->ID, $this->getUserPremiumVideos(User::createFromPost($Video)));
    }

    public function isVideoPrivate(Video $Video)
    {
        return $Video->post_status === 'private';
    }

    public function getProductIdFromVideo(Video $Video)
    {
        return $Video->field(static::VIDEO_PRODUCT_ID_META);
    }

    public function setProductOnVideo($Product, Video $Video)
    {
        if (!(new StripeService())->isProductValid($Product)) {
            return false;
        }
        return update_post_meta($Video->ID, static::VIDEO_PRODUCT_ID_META, $Product->id);
    }

    public function setPriceOnVideo($Price, Video $Video)
    {
        if (!(new StripeService())->isPriceValid($Price)) {
            return false;
        }
        return update_post_meta($Video->ID, static::VIDEO_PRICE_ID_META, $Price->id);
    }

    public function getPriceIdFromVideo(Video $Video)
    {
        return $Video->field(static::VIDEO_PRICE_ID_META);
    }

    public function unsetProductOnVideo(Video $Video)
    {
        return update_post_meta($Video->ID, static::VIDEO_PRODUCT_ID_META, '');
    }

    public function unsetPriceOnVideo(Video $Video)
    {
        return update_post_meta($Video->ID, static::VIDEO_PRICE_ID_META, '');
    }

    public function setVideoAsPrivate(Video $Video)
    {
        $Video->post_status = 'private';
        return (new VideoRepository())->add($Video);
    }

    public function setVideoAsPublish(Video $Video)
    {
        $Video->post_status = 'publish';
        return (new VideoRepository())->add($Video);
    }

    public function isVideoMarkedForMonthlyMembers(Video $Video)
    {
        return $Video->field(static::VIDEO_MONTHLY_MEMBERS_META) ?: false;
    }

    public function markVideoForMonthlyMembers(Video $Video)
    {
        return update_post_meta($Video->ID, static::VIDEO_MONTHLY_MEMBERS_META, true);
    }

    public function unmarkVideoForMonthlyMembers(Video $Video)
    {
        return update_post_meta($Video->ID, static::VIDEO_MONTHLY_MEMBERS_META, false);
    }

    public function getVideoPriceLabel(Price $Price)
    {
        if (!(new StripeService())->isPriceValid($Price)) {
            return '';
        }
        if (empty($unit_amount = $Price->unit_amount)) {
            return '';
        }
        return sprintf('$%s', $unit_amount/100);
    }

    public function getVideoPaymentSettingsLink(Video $Video)
    {
        if (!$Video instanceof Video) {
            return '';
        }
        return sprintf(static::PAYMENT_SETTINGS_LINK, $Video->post_name);
    }

    public function getProductFromVideo(Video $Video)
    {
        if (empty($product_id = $this->getProductIdFromVideo($Video))) {
            return false;
        }
        return (new StripeService())->getProduct($product_id);
    }

    public function updateProductTitle(Video $Video, Product $Product)
    {
        if (empty($name = $Video->title()) || !($StripeService = new StripeService())->isProductValid($Product)) {
            return false;
        }
        return (new StripeService())->updateProduct($Product, compact('name'));
    }

    public function getPriceFromVideo(Video $Video)
    {
        if (empty($price_id = $this->getPriceIdFromVideo($Video))) {
            return false;
        }
        return (new StripeService())->getPrice($price_id);
    }

    public function markPriceAsInactive(Video $Video)
    {
        $Price = (new UserMonetizationService())->getPriceFromVendor(User::createFromPost($Video));
        if (!($StripeService = new StripeService())->isPriceValid($Price)) {
            return false;
        }
        return $StripeService->updatePrice($Price, ['active' => false]);
    }

    public function markProductAsInactive(Video $Video)
    {
        $Product = (new UserMonetizationService())->getProductFromVendor(User::createFromPost($Video));
        if (!($StripeService = new StripeService())->isProductValid($Product)) {
            return false;
        }
        return $StripeService->updateProduct($Product, ['active' => false]);
    }

    public function calculateApplicationFeeForPremiumVideo(Video $Video)
    {
        if (empty($Price = $this->getPriceFromVideo($Video)) && empty($unit_amount = $Price->unit_amount)) {
            return 0;
        }
        return $unit_amount * static::VIDEO_APPLICATION_FEE;
    }

    public function createCheckoutSessionForPremiumVideo(Video $Video)
    {
        $Account = (new UserMonetizationService())->getAccountFromVendor(User::createFromPost($Video));
        if (!($StripeService = new StripeService())->isAccountValid($Account)) {
            return false;
        }
        return $StripeService->createSession([
            'mode' => 'payment',
            'payment_method_types' => ['card'],
            'line_items' => [[
                'name' => $Video->title(),
                'amount' => $this->getPriceFromVideo($Video)->unit_amount,
                'currency' => static::DEFAULT_CURRENCY,
                'quantity' => 1,
            ]],
            'payment_intent_data' => [
                'application_fee_amount' => (int) $this->calculateApplicationFeeForPremiumVideo($Video),
                'transfer_data' => [
                    'destination' => $Account->id,
                ],
            ],
            'success_url' => home_url(sprintf(static::VIDEO_CHECKOUT_SUCCESS_URL, $Video->post_name)),
            'cancel_url' => home_url(sprintf(static::VIDEO_CHECKOUT_CANCEL_URL, $Video->post_name)),
        ]);
    }
}
