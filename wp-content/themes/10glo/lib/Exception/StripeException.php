<?php

namespace ChildTheme\Exception;

use Exception;

/**
 * Class StripeException
 * @package ChildTheme\Exception
 * @author Vincent Ragosta <vincentpasqualeragosta@gmail.com>
 * @version 1.0
 */
class StripeException extends Exception {}
