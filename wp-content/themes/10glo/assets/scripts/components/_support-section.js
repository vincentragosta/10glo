addAction(INIT, function() {
    var $supportSection = $('.support-section');
    if (!$supportSection.length) {
        return;
    }

    var stripe = new Stripe(sit.publishable_key);
    var elements = stripe.elements();
    var card = elements.create('card', {
        style: {
            base: {
                iconColor: '#492485',
                color: '#492485',
                fontFamily: 'Roboto, Open Sans, Segoe UI, sans-serif',
                fontSize: '16px',
                ':-webkit-autofill': {
                    color: '#fff',
                },
                '::placeholder': {
                    color: '#492485',
                },
            },
            invalid: {
                iconColor: '#FFC7EE',
                color: '#FFC7EE',
            },
        },
    });
    card.mount('#support-credit-card');

    var $tabs = $supportSection.find('.support-section__tab-list > li > button');
    var ACTIVE_STATE = 'support-section__tab--show';
    var LIST_ITEM_ACTIVE_STATE = 'support-section__tab-list-item--active';
    $tabs.on('click', function() {
        var tab_id = $(this).attr('data-tab-id');
        var $parent = $(this).parent();
        var $otherParent = $parent.siblings();
        if (!$parent.hasClass(LIST_ITEM_ACTIVE_STATE)) {
            $parent.addClass(LIST_ITEM_ACTIVE_STATE);
        }

        if ($otherParent.hasClass(LIST_ITEM_ACTIVE_STATE)) {
            $otherParent.removeClass(LIST_ITEM_ACTIVE_STATE);
        }

        // $(this).parent('.support-section__tab-list-item:not(.support-section__tab-list-item--active)')
        $('.support-section__tab:not(#' + tab_id + ')').removeClass(ACTIVE_STATE);
        $('.support-section__tab#' + tab_id).addClass(ACTIVE_STATE);
        card.unmount();
        card.mount('#' + tab_id + '-credit-card');
    });

    var $messaging = $('.messaging');
    var $messagingSection = $messaging.parents('.messaging__section');

    var $membershipForm = $supportSection.find('#membership-form');
    var $memberOptions = $membershipForm.find('.membership-form__option');
    var $membershipSubmit = $membershipForm.find('input[type=submit]');
    var $membershipToggle = $('#membership-form-toggle');
    var $membershipFormSubmitContainer = $('#membership-form-submit-container');
    if ($memberOptions.length) {
        var ACTIVE_CLASS = 'membership-form__option--active';
        $memberOptions.each(function(index, value) {
            $(value).on('click', function(e) {
                e.preventDefault();
                var $this = $(this);
                $('.membership-form__option').removeClass(ACTIVE_CLASS);
                $this.addClass(ACTIVE_CLASS);
                $membershipForm.find('input[name="membership-form__option-value"]').val($this.attr('data-value'));
                $membershipSubmit.val('Join ' + $this.text().slice(0, -3));
                $membershipToggle.text('Join ' + $this.text().slice(0, -3));
            });
        });
    }
    $membershipToggle.on('click', function(e) {
        e.preventDefault();
        $(this).addClass('d-none');
        $membershipFormSubmitContainer.removeClass('d-none');
    });
    $membershipForm.submit(function(e) {
        e.preventDefault();
        var $this = $(this);
        var $name = $this.find('#membership-form__full-name');
        var $email = $this.find('#membership-form__email');

        $('html, body').css('cursor', 'wait');
        $messagingSection.addClass('messaging__section--show');
        $messaging.find('.messaging__content').html('Your payment is processing. Stand by!');
        $this.find('input[type="submit"]').prop('disabled', 'disabled').val('Processing...');

        stripe.createToken(card, {
            name: $name.val()
        }).then(function(result) {
            $('html, body').css('cursor', 'auto');
            if (result.error) {
                $messagingSection.addClass('messaging__section--show');
                $messaging.find('.messaging__content').html(result.error.message);
            } else {
                var formData = new FormData();
                var optionValue = $this.find('input[name="membership-form__option-value"]').val();
                if (optionValue) {
                    formData.append('amount', optionValue);
                }
                formData.append('customerName', $name.val());
                formData.append('customerEmail', $email.val());
                formData.append('customerToken', result.token.id);
                formData.append('currentUserId', sit.current_user_id);
                formData.append('userId', $this.find('input[name="membership-form__author-id"]').val());
                $.ajax({
                    method: 'POST',
                    url: sit.api_url + 'user/monetize/subscribe',
                    data: formData,
                    processData: false,
                    contentType: false,
                    beforeSend: function (xhr) {
                        xhr.setRequestHeader('X-WP-Nonce', sit.nonce);
                        xhr.setRequestHeader('Authorization', 'Basic ' + sit.basic_auth);
                    },
                    success: function (response) {
                        $messagingSection.addClass('messaging__section--show');
                        $messaging.find('.messaging__content').html('Congratulations! You are now a member of this vendor\'s channel for $' + (optionValue/100) + '/month.');
                        doAction('hideModal', $('#video-support-modal'));
                        $this.find('input[type="submit"]').removeAttr('disabled').val('Join $5');
                    },
                    error: function (response) {

                    },
                    fail: function (response) {

                    }
                });
            }
        });
    });

    var $supportForm = $supportSection.find('#support-form');
    var $amountFields = $supportForm.find('input[name=support-form__amount]');
    var $customAmount = $supportForm.find('input[name=support-form__custom-amount]');
    var $supportSubmit = $supportForm.find('input[type=submit]');
    var $supportToggle = $('#support-form-toggle');
    var $supportFormSubmitContainer = $('#support-form-submit-container');
    $amountFields.on('click', function() {
        $supportSubmit.val('Support ' + $(this).parent().text());
        $supportToggle.text('Support ' + $(this).parent().text());
    });
    $customAmount.on('change', function() {
        var $this = $(this);
        var value = $this.val();

        $this.val(Math.ceil($this.val()).toFixed(2));

        if (value.length !== 0) {
            $amountFields.each(function(index, value) {
                $(value).attr('disabled', 'disabled').removeAttr('checked');
            });
        } else {
            $amountFields.each(function(index, value) {
                $(value).removeAttr('disabled');
            });
        }
        $supportToggle.text('Support $' + value);
        $supportSubmit.val('Support $' + value);
    });
    $supportToggle.on('click', function(e) {
        e.preventDefault();
        $(this).addClass('d-none');
        $supportFormSubmitContainer.removeClass('d-none');
    });

    $supportForm.submit(function(e) {
        e.preventDefault();
        var $this = $(this);
        var $name = $this.find('#support-form__name');
        var $email = $this.find('#support-form__email');

        $('html, body').css('cursor', 'wait');
        $messagingSection.addClass('messaging__section--show');
        $messaging.find('.messaging__content').html('Your payment is processing. Stand by!');
        $this.find('input[type="submit"]').prop('disabled', 'disabled').val('Processing...');

        stripe.createToken(card, {
            name: $name.val()
        }).then(function(result) {
            $('html, body').css('cursor', 'auto');
            if (result.error) {
                $messagingSection.addClass('messaging__section--show');
                $messaging.find('.messaging__content').html(result.error.message);
            } else {
                var formData = new FormData();
                var amount = $this.find('input[name="support-form__amount"]:checked').val();
                var customAmount = $this.find('input[name="support-form__custom-amount"]').val();
                var name = $this.find('input[name="support-form__name"]').val();
                var message = $this.find('textarea[name="support-form__message"]').text();
                if (customAmount) {
                    if (customAmount.replace(/\./g, "") < 500) {
                        $messagingSection.addClass('messaging__section--show');
                        $messaging.find('.messaging__content').html('You cannot tip below $5.');
                        return;
                    }
                    formData.append('amount', customAmount.replace(/\./g, ""));
                } else {
                    formData.append('amount', amount);
                }
                if (name) {
                    formData.append('name', name);
                }
                if (message) {
                    formData.append('message', message);
                }
                formData.append('customerName', $name.val());
                formData.append('customerEmail', $email.val());
                formData.append('customerToken', result.token.id);
                formData.append('userId', $this.find('input[name="support-form__author-id"]').val());
                $.ajax({
                    method: 'POST',
                    url: sit.api_url + 'user/monetize/tip',
                    data: formData,
                    processData: false,
                    contentType: false,
                    beforeSend: function (xhr) {
                        xhr.setRequestHeader('X-WP-Nonce', sit.nonce);
                        xhr.setRequestHeader('Authorization', 'Basic ' + sit.basic_auth);
                    },
                    success: function (response) {
                        $messagingSection.addClass('messaging__section--show');
                        $messaging.find('.messaging__content').html('Congratulations! You are have successfully tipped this vendor!');
                        doAction('hideModal', $('#video-support-modal'));
                        $this.find('input[type="submit"]').removeAttr('disabled').val('Support $5');
                    },
                    error: function (response) {

                    },
                    fail: function (response) {

                    }
                });
            }
        });
    });
});
