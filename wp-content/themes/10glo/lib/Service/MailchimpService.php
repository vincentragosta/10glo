<?php

namespace ChildTheme\Service;

use ChildTheme\Exception\MailchimpException;
use ChildTheme\Mailchimp\MailchimpList;
use ChildTheme\Mailchimp\MailchimpListMember;

/**
 * Class MailchimpService
 * @package ChildTheme\Controller
 * @author Vincent Ragosta <vincentpasqualeragosta@gmail.com>
 * @version 1.0
 */
class MailchimpService
{
    const ADD_MEMBER_TO_LIST = 'https://us18.api.mailchimp.com/3.0/lists/%s/members/';

    public function addToList(MailchimpList $List, MailchimpListMember $Member, string $tag = 'User')
    {
        if (!defined('MAILCHIMP_API_KEY')) {
            throw new MailchimpException('The Mailchimp API key is not set.', 400);
        }

        $ch = curl_init(sprintf(static::ADD_MEMBER_TO_LIST, $List->getListId()));
        curl_setopt_array($ch, array(
            CURLOPT_POST => TRUE,
            CURLOPT_RETURNTRANSFER => TRUE,
            CURLOPT_HTTPHEADER => array(
                'Authorization: apikey ' . MAILCHIMP_API_KEY,
                'Content-Type: application/json'
            ),
            CURLOPT_POSTFIELDS => json_encode([
                'email_address' => $Member->getEmailAddress(),
                'name' => $Member->getDisplayName(),
                'status' => $Member->getStatus(),
                'tags' => [$tag]
            ])
        ));
        return curl_exec($ch);
    }

    protected function getMemberId(MailchimpListMember $Member)
    {
        if (empty($email_address = $Member->getEmailAddress())) {
            throw new MailchimpException('The Mailchimp member email address was not sent in the request.', 400);
        }
        return md5(strtolower($email_address));
    }

    protected function getDataCenter()
    {
        return substr(MAILCHIMP_API_KEY, strpos(MAILCHIMP_API_KEY, '-') + 1);
    }

    protected function getEndpoint(MailchimpList $List, MailchimpListMember $Member)
    {
        return sprintf(
            static::MAILCHIMP_ENDPOINT,
            $dataCenter = $this->getDataCenter(),
            $List->getListId(),
            $this->getMemberId($Member)
        );
    }

    protected function getUserPassword()
    {
        return sprintf('user: %s', MAILCHIMP_API_KEY);
    }
}
