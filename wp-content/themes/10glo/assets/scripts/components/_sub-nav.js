addAction(INIT, function() {
    var $subNavs = $('.sub-nav');
    if (!$subNavs.length) {
        return;
    }
    $.each($subNavs, function(index, value) {
        var $activeListItems = $(value).find('.sub-nav__item--active');
        if (!$activeListItems.length) {
            var $subNavLinks = $(value).find('.sub-nav__link');
            if ($subNavLinks.length) {
                $.each($subNavLinks, function(index, value) {
                    var href = $(value).attr('href');
                    if (href && href.indexOf(window.location.pathname) !== -1) {
                        $(value).parent().addClass('sub-nav__item--active');
                    }
                });
            }
        }
    });
});
