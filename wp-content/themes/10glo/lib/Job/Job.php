<?php

namespace ChildTheme\Job;

use Backstage\Models\PostBase;
use Backstage\SetDesign\SocialIcons\SocialIcon;

/**
 * Class Job
 * @package ChildTheme\Job
 * @author Mike Miranda <m.miranda426@gmail.com>
 * @version 1.0
 * @property array social_icons
 */
class Job extends PostBase
{
    const POST_TYPE = 'job';

    public function getSocialIcons()
    {
        return array_map(function($row) {
            $label = isset($row['link']['title']) ? $row['link']['title'] : ucfirst($row['icon']);
            return new SocialIcon($row['icon'], $row['link']['url'] ?: '', $label, []);
        }, $this->field('social_icons'));
    }
    }
