addAction(INIT, function () {
    var $profileCoverPhotoForm = $('#profile-cover-photo-form');
    if (!$profileCoverPhotoForm.length) {
        return;
    }
    $profileCoverPhotoForm.submit(function (e) {
        e.preventDefault();

        var $messaging = $('.messaging');
        var $messagingSection = $messaging.parents('.messaging__section');

        var photoFile = $('#profile-cover-photo')[0].files[0];

        if (!photoFile) {
            $messagingSection.addClass('messaging__section--show');
            $messaging.find('.messaging__content').html('No photos have been sent in the request');
            return;
        }

        var formData = new FormData();
        formData.append('file', photoFile);
        formData.append('userId', sit.current_user_id);

        $.ajax({
            method: 'POST',
            url: sit.api_url + 'user/profile-cover-photo/create',
            data: formData,
            processData: false,
            contentType: false,
            beforeSend: function (xhr) {
                xhr.setRequestHeader('X-WP-Nonce', sit.nonce);
                xhr.setRequestHeader('Authorization', 'Basic ' + sit.basic_auth);
            },
            success: function (response) {
                $messagingSection.addClass('messaging__section--show');
                $messaging.find('.messaging__content').html(response.message);

                setTimeout(function () {
                    location.href = sit.profile_url;
                }, 500);
            },
            error: function (response) {
                var message = response.responseJSON.message ? response.responseJSON.message : response.message;
                $messagingSection.addClass('messaging__section--show');
                $messaging.find('.messaging__content').html(message);
            },
            fail: function (response) {
                $messagingSection.addClass('messaging__section--show');
                $messaging.find('.messaging__content').html(response);
            }
        });
    });
});
