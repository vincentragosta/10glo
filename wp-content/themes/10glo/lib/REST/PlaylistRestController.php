<?php

namespace ChildTheme\REST;

use Backstage\Controller\RestController;
use ChildTheme\Exception\PlaylistException;
use ChildTheme\Playlist\Playlist;
use ChildTheme\Playlist\PlaylistRepository;
use ChildTheme\Support\CodeGenerator;
use ChildTheme\Traits\GetCurrentUserFromRequest;
use ChildTheme\Video\Video;
use ChildTheme\Video\VideoDTOCollection;
use ChildTheme\Video\VideoRepository;
use WP_REST_Request;

/**
 * Class PlaylistRestController
 * @package ChildTheme\Controller
 * @author Vincent Ragosta <vincentpasqualeragosta@gmail.com>
 * @version 1.0
 */
class PlaylistRestController extends RestController
{
    protected $namespace = 'user';

    use GetCurrentUserFromRequest;

    public function registerRoutes()
    {
        $this->addCreateRoute('playlist/create', [$this, 'createPlaylist']);
        $this->addCreateRoute('playlist/add', [$this, 'addVideoToPlaylist']);
        $this->addCreateRoute('playlist/update', [$this, 'updatePlaylist']);
        $this->addCreateRoute('playlist/remove', [$this, 'removePlaylist']);
        $this->addCreateRoute('/playlist/load', [$this, 'loadMoreVideos']);
    }

    public function createPlaylist(WP_REST_Request $request)
    {
        $CurrentUser = $this->getCurrentUserFromRequest($request, 'Please login or create an account to create a playlist.');
        $playlist_name = $this->getPlaylistNameFromRequest($request);

        $Playlist = new Playlist();
        $Playlist->post_title = $playlist_name;
        $Playlist->post_status = 'publish';
        $Playlist->post_author = $CurrentUser->ID;
        $Playlist->post_name = CodeGenerator::generate();

        if (!empty($video_ids = $request->get_param('videoIds'))) {
            $Playlist->video_ids = $video_ids;
        }

        if (!(new PlaylistRepository())->add($Playlist)) {
            throw new PlaylistException('There was an error creating the playlist.', 400);
        }

        if (!empty($video_ids)) {
            foreach ($video_ids as $video_id) {
                $Video = new Video($video_id);
                if (!$Video instanceof Video) {
                    continue;
                }
                $playlist_ids = $Video->playlist_ids ? (array)$Video->playlist_ids : [];
                $playlist_ids[] = $Playlist->ID;
                $Video->playlist_ids = array_unique($playlist_ids);
                if (!(new VideoRepository())->add($Video)) {
                    throw new PlaylistException('The playlist could not be added to the Video.', 400);
                }
            }
        }

        return ['message' => 'The playlist has successfully been created', 'status' => 400];
    }

    public function updatePlaylist(WP_REST_Request $request)
    {
        $this->getCurrentUserFromRequest($request, 'Please login or create an account to create a playlist.');
        $Playlist = $this->getPlaylistFromRequest($request);
        $playlist_name = $this->getPlaylistNameFromRequest($request);

        $potential_video_ids = $Playlist->video_ids ?: [];
        if (!empty($playlist_video_ids = $request->get_param('playlistVideoIds'))) {
            $VideoRepository = new VideoRepository();
            foreach ($playlist_video_ids as $video_id) {
                if (($position = array_search($video_id, (array) $potential_video_ids)) !== false) {
                    unset($potential_video_ids[$position]);
                }

                $Video = new Video($video_id);
                $playlist_ids = $Video->playlist_ids ?: [];
                if (($position = array_search($Playlist->ID, (array)$playlist_ids)) !== false) {
                    unset($playlist_ids[$position]);
                    $Video->playlist_ids = $playlist_ids;
                    $VideoRepository->add($Video);
                }
            }
        }

        if (!empty($video_ids = $request->get_param('videoIds'))) {
            $potential_video_ids = array_unique(array_merge($potential_video_ids, $video_ids));
            foreach ($video_ids as $video_id) {
                if (!($Video = new Video($video_id)) instanceof Video) {
                    continue;
                }
                $playlist_ids = $Video->playlist_ids ? (array)$Video->playlist_ids : [];
                $playlist_ids[] = $Playlist->ID;
                $Video->playlist_ids = array_unique($playlist_ids);
                if (!(new VideoRepository())->add($Video)) {
                    throw new PlaylistException('The playlist could not be added to the Video.', 400);
                }
            }
        }

        $Playlist->post_title = $playlist_name;
        $Playlist->video_ids = $potential_video_ids;
        if (!(new PlaylistRepository())->add($Playlist)) {
            throw new PlaylistException('There was an issue updating the playlist.', 400);
        }

        return ['message' => 'The playlist was successfully updated.', 'status' => 200];
    }

    public function removePlaylist(WP_REST_Request $request)
    {
        $Playlist = $this->getPlaylistFromRequest($request);

        $VideoRepository = new VideoRepository();
        if (!empty($videos = $VideoRepository->findFromPlaylist($Playlist))) {
            foreach($videos as $Video) {
                $playlist_ids = $Video->playlist_ids;
                if (!empty($playlist_ids) && false !== ($position = array_search($Playlist->ID, (array) $playlist_ids))) {
                    unset($playlist_ids[$position]);
                    $Video->playlist_ids = $playlist_ids;
                    (new VideoRepository())->add($Video);
                }
            }
        }

        if (!wp_delete_post($Playlist->ID)) {
            throw new PlaylistException('We could not delete the playlist.', 400);
        }
        return ['message' => 'The playlist has been removed from the website.', 'status' => 200];
    }

    public function addVideoToPlaylist(WP_REST_Request $request)
    {
        /**
         * @var Video $Video
         */
        $Playlist = $this->getPlaylistFromRequest($request);
        $Video = $this->getVideoFromRequest($request);
        $CurrentUser = $this->getCurrentUserFromRequest($request, 'Please login or create an account to add this video to a playlist.');

        if ($Video->author()->ID !== $CurrentUser->ID) {
            throw new PlaylistException('You cannot add other users videos to your playlists.', 400);
        }

        $video_ids = $Playlist->video_ids ? (array)$Playlist->video_ids : [];
        $video_ids[] = (string)$Video->ID;
        $Playlist->video_ids = $video_ids;

        if (!(new PlaylistRepository())->add($Playlist)) {
            throw new PlaylistException('There was an issue adding the video to the playlist.', 400);
        }

        $playlist_ids = $Video->playlist_ids ? (array)$Video->playlist_ids : [];
        $playlist_ids[] = (string)$Playlist->ID;
        $Video->playlist_ids = $playlist_ids;

        if (!(new VideoRepository())->add($Video)) {
            throw new PlaylistException('There was an issue adding the playlist to the video.', 400);
        }

        return ['message' => 'The video was successfully added to the playlist.', 'status' => 200];
    }

    protected function loadMoreVideos(WP_REST_Request $request)
    {
        $posts_per_page = $request->get_param('postsPerPage') ?: '';
        $cat_id = $request->get_param('catId') ?: '';
        $term_ids = $request->get_param('hashtagId') ?: [];
        $post__not_in = $request->get_param('postNotIn') ?: [];
        $criteria = $request->get_param('criteria') ?: '';

        /* patch for incorrect names */
        if ($criteria == 'most-viewed') {
            $criteria = 'views';
        } elseif ($criteria == 'top-rated') {
            $criteria = 'likes_count';
        }
        /* end patch fix */

        if (empty($paged = $request->get_param('page'))) {
            throw new PlaylistException('The page variable was not sent in the request', 400);
        }

        if (!empty($playlist_id = $request->get_param('playlistId')) && ($Playlist = new Playlist($playlist_id)) instanceof Playlist) {
            $videos = (new VideoRepository())->findFromPlaylist($Playlist, compact('posts_per_page', 'paged', 'criteria'));
        } else {
            $videos = (new VideoRepository())->findCurated(compact('criteria', 'cat_id', 'term_ids', 'posts_per_page', 'paged', 'post__not_in'));
        }
        if (empty($videos)) {
            throw new PlaylistException('No videos returned in the request.', 400);
        }
        return ['data' => new VideoDTOCollection($videos), 'message' => 'Videos loaded.', 'status' => 200];
    }

    protected function getVideoFromRequest(WP_REST_Request $request)
    {
        if (empty($video_id = $request->get_param('videoId'))) {
            throw new PlaylistException('The video id was not sent in the request.', 400);
        }
        if (empty($Video = (new VideoRepository())->findById($video_id))) {
            throw new PlaylistException('The video id was incorrect.', 400);
        }
        return $Video;
    }

    protected function getPlaylistFromRequest(WP_REST_Request $request)
    {
        if (empty($playlist_id = $request->get_param('playlistId'))) {
            throw new PlaylistException('The playlist id was not sent in the request.', 400);
        }
        return new Playlist($playlist_id);
    }

    protected function getPlaylistNameFromRequest(WP_REST_Request $request)
    {
        if (empty($playlist_name = $request->get_param('playlistName'))) {
            throw new PlaylistException('The playlist name was not sent in the request.', 400);
        }
        return $playlist_name;
    }
}
