<?php

namespace ChildTheme\User;

/**
 * Class UserRepository
 * @package ChildTheme\User
 * @author Vincent Ragosta <vincentpasqualeragosta@gmail.com>
 * @version 1.0
 */
class UserRepository
{
    protected $model_class = User::class;
//    protected $UserQuery = null;

//    public function __construct()
//    {
//        $this->UserQuery = new \WP_User_Query();
//    }

    public function findByName(string $name)
    {
        if (empty($name)) {
            return [];
        }
        $results =  (new \WP_User_Query([
            'search' => $name,
            'search_fields' => ['user_login', 'user_nicename', 'display_name']
        ]))->get_results();

        if (empty($results)) {
            return [];
        }
        // TODO: turn this into a UserCollection() class
        return array_map(function($User) {
            return User::createFromUserId($User->ID);
        }, $results);
    }
}
