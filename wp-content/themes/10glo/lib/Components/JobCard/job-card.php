<?php
/**
 * Expected:
 * @var string $title
 * @var User $User
 * @var string $content
 * @var string $relative_published_time
 * @var string $url
 * @var array $class_modifiers
 * @var array $element_attributes
 */

use Backstage\SetDesign\Icon\IconView;
use Backstage\Util;
use Backstage\View\Link;
use ChildTheme\User\User;

if (empty($title) || empty($url)) {
    return;
}
$element_attributes['class'] .= 'col-reset-exclude';
?>

<div <?= Util::componentAttributes('job-card', $class_modifiers, $element_attributes); ?>>
    <div class="job-card__container">
        <div class="job-card__content-container">
            <h2 class="job-card__heading heading heading--default heading--dark">
                <a href="<?= $url; ?>" class="job-card__heading-link">
                    <?= $title; ?>
                </a>
            </h2>
            <?php if (!empty($content)): ?>
                <p class="job-card__content">
                    <?= $content; ?>
                </p>
            <?php endif; ?>
            <?php if ($relative_published_time): ?>
                <div class="job-card__author-time-container">
                    <p class="job-card__relative-published-time"><?= $relative_published_time; ?></p>
                </div>
            <?php endif; ?>
            <?php if ($relative_published_time): ?>
                <div class="job-card__author-time-container">
                    <p class="job-card__relative-published-time"><?= $relative_published_time; ?></p>
                </div>
            <?php endif; ?>
            <ul class="list list--inline">
              <li><a href="<?= $facebook; ?>" target="_blank"><?= new IconView(['icon_name' => 'facebook']); ?></a></li>
              <li><a href="<?= $twitter; ?>" target="_blank"><?= new IconView(['icon_name' => 'twitter']); ?></a></li>
            </ul>
        </div>
    </div>
</div>
