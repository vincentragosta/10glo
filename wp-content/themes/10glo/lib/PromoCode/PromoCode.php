<?php

namespace ChildTheme\PromoCode;

use Backstage\Models\PostBase;

/**
 * Class PromoCode
 * @package ChildTheme\PromoCode
 * @author Vincent Ragosta <vincentpasqualeragosta@gmail.com>
 * @version 1.0
 *
 * @property string $code
 * @property array $assigned_user_ids
 */
class PromoCode extends PostBase
{
    const POST_TYPE = 'promo-code';
}
