<?php
/**
 * Expected:
 * @var User $Vendor
 */

use ChildTheme\Components\MembershipForm\MembershipFormView;
use ChildTheme\Components\SupportForm\SupportFormView;
use ChildTheme\Service\UserService;
use ChildTheme\User\User; ?>

<div class="video-support-modal support-section">
    <div class="video-support-modal__forms support-section__forms">
        <div class="support-section__tabs">
            <ul class="support-section__tab-list list list--inline">
                <li class="support-section__tab-list-item support-section__tab-list-item--active"><button class="button button--secondary" data-tab-id="support">Support</button></li>
                <li class="support-section__tab-list-item"><button class="button button--secondary" data-tab-id="membership">Membership</button></li>
            </ul>
        </div>
        <div class="support-section__content">
            <div class="support-section__tab support-section__tab--show" id="support">
                <h2 class="heading heading--default">Give a one-time tip to <span class="text--secondary"><?= $Vendor->getDisplayName(); ?></span></h2>
                <?= (new SupportFormView($Vendor))->elementAttributes(['class' => 'support-section__form']); ?>
            </div>
            <div class="support-section__tab support-section__tab-show" id="membership">
                <?php if (!(new UserService())->isValidUser(User::createFromCurrentUser())): ?>
                    <p>You must be signed into your 10glo account to become a member of <?= $Vendor->getDisplayName(); ?>'s channel.</p>
                    <a href="<?= home_url('/sign-in/'); ?>" class="button button--secondary button--block">Please Sign In</a>
                <?php else: ?>
                    <?= (new MembershipFormView($Vendor))->elementAttributes(['class' => 'support-section__form']); ?>
                <?php endif; ?>
            </div>
        </div>
    </div>
</div>
