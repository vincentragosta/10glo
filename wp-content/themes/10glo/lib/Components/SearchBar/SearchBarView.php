<?php

namespace ChildTheme\Components\SearchBar;

use Backstage\View\Component;

/**
 * Class SearchBarView
 * @package ChildTheme\Components\SearchBar
 * @author Vincent Ragosta <vincentpasqualeragosta@gmail.com>
 * @version 1.0
 */
class SearchBarView extends Component
{
    protected $name = 'search-bar';
}
