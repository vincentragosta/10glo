<?php

namespace ChildTheme\Components\PodcastList;

use Backstage\Util;
use Backstage\VcLibrary\Support\Component;
use Backstage\View\Element;
use Backstage\View\Link;
use ChildTheme\Podcast\PodcastRepository;

/**
 * Class PodcastList
 * @package ChildTheme\Components\PodcastList
 * @author Vincent Ragosta <vincentpasqualeragosta@gmail.com>
 * @version 1.0
 */
class PodcastList extends Component
{
    const NAME = 'Podcast List';
    const TAG = 'podcast_list';
    const VIEW = PodcastListView::class;

    protected $component_config = [
        'description' => 'Display a list of podcasts in a horizontal fashion.',
        'icon' => 'icon-wpb-toggle-small-expand',
        'wrapper_class' => 'clearfix',
        'is_container' => false,
        'category' => 'Content',
        'params' => [
            'posts_per_page' => [
                'type' => 'textfield',
                'heading' => 'Posts per page',
                'param_name' => 'posts_per_page',
                'description' => 'Enter how many posts to display. Default: 4.',
                'admin_label' => true,
                'group' => 'Query'
            ],
            [
                'type' => 'checkbox',
                'heading' => 'Podcast Card Options',
                'param_name' => 'options',
                'value' => [
                    'Increase Heading Size' => 'increase-heading-size',
                    'Decrease Heading Size' => 'decrease-heading-size',
                    'Hide Content' => 'hide-content'
                ],
                'description' => 'Check the relevant options'
            ]
        ]
    ];

    protected function createView(array $atts)
    {
        $PodcastRepository = new PodcastRepository();
        $ViewClass = self::VIEW;
        /** @var PodcastListView $View */
        return new $ViewClass($PodcastRepository->findCurated($atts));
    }
}
