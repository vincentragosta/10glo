<?php

namespace ChildTheme\Exception;

use Exception;

/**
 * Class UserMonetizationException
 * @package ChildTheme\Exception
 * @author Vincent Ragosta <vincentpasqualeragosta@gmail.com>
 * @version 1.0
 */
class UserMonetizationException extends Exception {}
