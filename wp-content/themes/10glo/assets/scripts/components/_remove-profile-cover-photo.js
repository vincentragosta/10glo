addAction(INIT, function () {
    var $profileCoverPhotoRemoveButton = $('.profile-cover-photo-remove-modal__button');
    if (!$profileCoverPhotoRemoveButton.length) {
        return;
    }
    $profileCoverPhotoRemoveButton.each(function (key, value) {
        $(value).click(function () {
            var viewport = $(this).data('viewport');
            var $messaging = $('.messaging');
            var $messagingSection = $messaging.parents('.messaging__section');
            var $modal = $('#profile-cover-photo-remove');

            $.ajax({
                method: 'POST',
                url: sit.api_url + 'user/profile-cover-photo/remove',
                data: {
                    viewport: viewport,
                    userId: sit.current_user_id
                },
                beforeSend: function (xhr) {
                    xhr.setRequestHeader('X-WP-Nonce', sit.nonce);
                    xhr.setRequestHeader('Authorization', 'Basic ' + sit.basic_auth);
                },
                success: function (response) {
                    doAction('hideModal', $modal);
                    $messagingSection.addClass('messaging__section--show');
                    $messaging.find('.messaging__content').html(response.message);

                    setTimeout(function () {
                        location.href = sit.profile_url;
                    }, 500);
                },
                fail: function (response) {
                    doAction('hideModal', $modal);
                    $messagingSection.addClass('messaging__section--show');
                    $messaging.find('.messaging__content').html(response.data);
                }
            });
        });
    });
});
