<?php

namespace ChildTheme\Controller;

/**
 * Class FathomAnalyticsController
 * @package ChildTheme\Controller
 * @author Vincent Ragosta <vincentpasqualeragosta@gmail.com>
 * @version 1.0
 */
class FathomAnalyticsController
{
    public function __construct()
    {
        add_action('wp_head', [$this, 'addEmbedCode']);
    }

    public function addEmbedCode()
    {
        ?>
        <!-- Fathom - beautiful, simple website analytics -->
        <script src="https://cdn.usefathom.com/script.js" site="GPVRVQYQ" defer></script>
        <!-- / Fathom -->
        <?php
    }
}
