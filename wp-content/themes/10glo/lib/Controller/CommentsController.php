<?php

namespace ChildTheme\Controller;


use ChildTheme\Service\EmailNotificationService;
use ChildTheme\Video\Video;

/**
 * Class CommentsController
 * @package ChildTheme\Controller
 * @author Vincent Ragosta <vincentpasqualeragosta@gmail.com>
 * @version 1.0
 */
class CommentsController
{
    public function __construct()
    {
        add_filter('comment_form_default_fields', [$this, 'removeWebsiteField']);
        add_filter('comment_post_redirect', [$this, 'commentsRedirect']);
        add_filter('comment_post', [$this, 'sendEmailOnComment'], 10, 1);
    }

    public function removeWebsiteField($fields)
    {
        if (isset($fields['url'])) {
            unset($fields['url']);
        }
        return $fields;
    }

    public function commentsRedirect($location)
    {
        return $_SERVER['HTTP_REFERER'];
    }

    public function sendEmailOnComment($comment_id)
    {
        $Comment = get_comment($comment_id);
        if (($Video = new Video($Comment->comment_post_ID)) instanceof Video) {
            (new EmailNotificationService($Video->author()->user_email, 'New Comment on Video', 'Cue the (virtual) applause. <a href="' . $Video->permalink() . '">' . $Video->title() . '</a> just received a new comment on 10glo.<br />Keep up the good work! <br />- 10glo'))->send();
        }
    }
}
