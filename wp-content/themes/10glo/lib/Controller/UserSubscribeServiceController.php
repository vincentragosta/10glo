<?php

namespace ChildTheme\Controller;

use Backstage\Rewrite\QueryRewrite;
use ChildTheme\User\User;

/**
 * Class UserSubscribeServiceController
 * @package ChildTheme\Controller
 * @author Vincent Ragosta <vincentpasqualeragosta@gmail.com>
 * @version 1.0
 */
class UserSubscribeServiceController
{
    public function __construct()
    {
        new QueryRewrite('user/(.+)/subscribers/?$', ['related_user' => '$1', 'subscribers' => true]);
        add_action('template_redirect', [$this, 'templateRedirect']);
        add_filter('template_include', [$this, 'templateInclude'], 99);
        add_filter('wp_dropdown_users_args', [$this, 'addSubscribersToDropDown'], 10, 2);
    }

    public function templateRedirect()
    {
        global $wp_query;
        if (($related_user = $wp_query->get('related_user')) && $wp_query->get('subscribers') && !User::createFromUserLogin($related_user) instanceof User) {
            wp_safe_redirect(home_url());
            exit();
        }
    }

    public function templateInclude($template)
    {
        global $wp_query;
        if ($wp_query->get('related_user') && $wp_query->get('subscribers')) {
            $new_template = locate_template(['templates/subscribers.php']);
            if ('' != $new_template) {
                return $new_template;
            }
        }
        return $template;
    }

    public function addSubscribersToDropdown($query_args, $r)
    {
        $query_args['who'] = '';
        return $query_args;
    }
}
