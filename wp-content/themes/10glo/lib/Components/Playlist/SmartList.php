<?php

namespace ChildTheme\Components\Playlist;

use Backstage\Util;
use Backstage\VcLibrary\Support\Component;
use Backstage\View\Element;
use ChildTheme\Video\VideoRepository;

/**
 * Class SmartList
 * @package ChildTheme\Components\Playlist
 * @author Vincent Ragosta <vincentpasqualeragosta@gmail.com>
 * @version 1.0
 */
class SmartList extends Component
{
    const NAME = 'SmartList';
    const TAG = 'smartlist';
    const VIEW = PlaylistView::class;

    protected $component_config = [
        'description' => 'Display a playlist from a category and/or criteria.',
        'icon' => 'icon-wpb-toggle-small-expand',
        'wrapper_class' => 'clearfix',
        'is_container' => false,
        'category' => 'Content',
        'params' => [
            'heading' => [
                'type' => 'textfield',
                'heading' => 'Heading Text',
                'param_name' => 'heading_text',
                'description' => 'Set optional heading text.',
                'admin_label' => true,
                'group' => 'Heading'
            ],
            [
                'type' => 'dropdown',
                'heading' => 'Tag',
                'param_name' => 'heading_tag',
                'value' => [
                    'H2' => 'h2',
                    'H3' => 'h3',
                    'H4' => 'h4',
                    'H1' => 'h1'
                ],
                'description' => 'Set the heading tag [H1 -> H4]. This is for accessibility purposes only, this will not affect appearance. H1 should only be used once at the top of the page (if page title is not automatically added).',
                'group' => 'Heading',
                'admin_label' => true
            ],
            [
                'type' => 'dropdown',
                'heading' => 'Size',
                'param_name' => 'heading_size',
                'description' => 'Set the heading size.',
                'value' => [
                    'Default' => 'default',
                    'Medium' => 'medium',
                    'Large' => 'large',
                    'Extra Large' => 'xlarge'
                ],
                'group' => 'Heading',
                'admin_label' => true
            ],
            [
                'type' => 'checkbox',
                'heading' => 'Show Sort?',
                'param_name' => 'show_sort',
                'description' => 'Display Most Recent, Most Viewed and Top Rated dropdown.',
                'group' => 'Settings'
            ],
            'categories' => [
                'type' => 'dropdown',
                'heading' => 'Category',
                'param_name' => 'category',
                'value' => [],
                'description' => 'Select a category',
                'admin_label' => true,
                'group' => 'Query'
            ],
            'hashtag_ids' => [
                'type' => 'textfield',
                'heading' => 'Hashtag IDs',
                'param_name' => 'hashtag_ids',
                'value' => [],
                'description' => 'Enter any number of hashtag IDs separated by commas.',
                'admin_label' => true,
                'group' => 'Query'
            ],
            'criteria' => [
                'type' => 'dropdown',
                'heading' => 'Criteria',
                'param_name' => 'criteria',
                'description' => 'Select the criteria of videos to display. Default Recently Uploaded.',
                'value' => [
                    'Most Recent' => '',
                    'Most Viewed' => 'views',
                    'Top Rated' => 'likes_count'
                ],
                'admin_label' => true,
                'group' => 'Query'
            ],
            'posts_per_page' => [
                'type' => 'textfield',
                'heading' => 'Posts per page',
                'param_name' => 'posts_per_page',
                'description' => 'Enter how many posts to display. Default: 8.',
                'admin_label' => true,
                'group' => 'Query'
            ],
            'excluded_category' => [
                'type' => 'textfield',
                'heading' => 'Categories to Exclude',
                'param_name' => 'excluded_categories',
                'description' => 'Enter the negative category ID to exclude them from the smart list query. Separate by comma to remove multiple.',
                'group' => 'Query'
            ],
            'excluded_video_ids' => [
                'type' => 'textfield',
                'heading' => 'Videos to Exclude',
                'param_name' => 'post__not_in',
                'description' => 'Enter any number of video IDs separated by comma to exclude them from the smart list query. Please use the WP ID and not the custom generated ID.',
                'group' => 'Query'
            ],
            'date_query' => [
                'type' => 'textfield',
                'heading' => 'Last X Days',
                'param_name' => 'date_after',
                'description' => 'Add a date parameter to the query. For example, enter "7" to change the query to the last 7 days.',
                'group' => 'Query'
            ]
        ]
    ];

    public function setupConfig()
    {
        parent::setupConfig();
        $this->setCategories();
    }

    protected function setCategories()
    {
        $options['-- Select Category --'] = '';
        foreach(get_categories(['hide_empty' => false, 'order' => 'DESC']) as $category) {
            $options[$category->name] = $category->term_id;
        }
        $this->component_config['params']['categories']['value'] = $options;
    }

    protected function createView(array $atts)
    {
        $VideoRepository = new VideoRepository();

        /* Patch fix for incorrect param_name */
        /* This is even funnier looking back, cat_id is still incorrect */
        if (isset($atts['category'])) {
            $atts['cat_id'] = $atts['category'];
            unset($atts['category']);
        }
        /* End patch fix */

        if (!empty($excluded_categories = $atts['excluded_categories'])) {
            $atts['cat_id'] .= $excluded_categories;
        }

        if (isset($_GET['sort']) && $atts['show_sort']) {
            $atts['criteria'] = $_GET['sort'];
        }

        if (!empty($hashtag_ids = $atts['hashtag_ids'])) {
            $atts['term_ids'] = explode(',', $hashtag_ids);
        }

        if (empty($videos = $VideoRepository->findCurated($atts))) {
            return 'The have been no videos posted with criteria.';
        }

        $ViewClass = self::VIEW;
        /** @var PlaylistView $View */
        $Heading = $atts['heading_text'] ? Element::create($atts['heading_tag'], $atts['heading_text'])->class(Util::componentClasses('heading', [$atts['heading_size']])) : null;
        $View = new $ViewClass($videos, $Heading);
        $View->elementAttributes($this->getDataFromComponentAttributes($atts));
        return $View
            ->toggleLoadMore(count($videos) == ($atts['posts_per_page'] ?: get_option('posts_per_page')))
            ->showSort($atts['show_sort'] ?: false);
    }

    protected function getDataFromComponentAttributes($attributes)
    {
        if (empty($attributes)) {
            return [];
        }
        $dataAttributes = [];
        foreach(array_filter($attributes) as $key => $value) {
            $key = str_replace('_', '-', $key);
            $dataAttributes["data-{$key}"] = $value;
        }
        $dataAttributes['data-page'] = 1;
        return $dataAttributes;
    }
}
