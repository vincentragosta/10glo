<?php
/**
 * Expected:
 * @var string $vendor_name
 * @var string $vendor_id
 * @var bool $is_submit_disabled
 * @var array $class_modifiers
 * @var array $element_attributes
 */

use Backstage\Util;

$element_attributes['id'] = 'membership-form';
?>

<form <?= Util::componentAttributes('membership-form', $class_modifiers, $element_attributes); ?>>
    <p>You can provide ongoing support to <?= $vendor_name; ?> by becoming a paid monthly member of their channel.</p>
    <fieldset class="membership-form__fieldset">
        <div class="membership-form__options">
            <button type="button" class="membership-form__option membership-form__option--active button button--blue" data-value="500">$5/mo</button>
            <button type="button" class="membership-form__option button button--blue" data-value="1000">$10/mo</button>
            <button type="button" class="membership-form__option button button--blue" data-value="1500">$15/mo</button>
            <button type="button" class="membership-form__option button button--blue" data-value="2500">$25/mo</button>
        </div>
    </fieldset>
    <fieldset class="membership-form__fieldset">
        <div>
            <label for="membership-form__full-name">Name</label>
            <input id="membership-form__full-name" name="membership-form__full-name" placeholder="Jane Doe" />
        </div>
        <div>
            <label for="membership-form__email">Email</label>
            <input id="membership-form__email" name="membership-form__email" placeholder="janedoe@gmail.com" />
        </div>
    </fieldset>
    <p>You'll automatically be notified when they post new content to 10glo and have access to any videos they make available exclusively to their members.</p>
    <button type="button" class="button button--block" id="membership-form-toggle">Join $5</button>
    <fieldset class="membership-form__fieldset d-none" id="membership-form-submit-container">
        <div id="membership-credit-card"></div>
        <input type="hidden" name="membership-form__author-id" value="<?= $vendor_id; ?>" />
        <input type="hidden" name="membership-form__option-value" value="500" />
        <input type="submit" value="Join $5" <?= $is_submit_disabled ? 'disabled' : ''; ?>/>
    </fieldset>
</form>
