addAction(INIT, function () {
    var $uploadVideoForm = $('#upload-video-form');
    if (!$uploadVideoForm.length) {
        return;
    }
    var $videoButton = $('.upload-video-form__video-button');
    var $videoFile = $('.upload-video-form__video-file');
    var $videoCategory = $('.upload-video-form__category');

    $videoCategory.on('change', function() {
        var $container = $('.upload-video-form__row--bwayworldrecord');
        var ACTIVE_CLASS = 'upload-video-form__row--show';
       if (this.value === sit.bwayworldrecord_category && !$container.hasClass(ACTIVE_CLASS)) {
            $('.upload-video-form__row--bwayworldrecord').addClass(ACTIVE_CLASS);
       } else {
           $('.upload-video-form__row--bwayworldrecord').removeClass(ACTIVE_CLASS);
       }
    });

    $videoButton.click(function () {
        $videoFile.trigger('click');
    });

    $videoFile.on('change', function () {
        var text = 'No file uploaded';
        var $file = $(this)[0].files[0];
        if ($file.name.length > 20) {
            text = $file.name.slice(0, 20);
            text += '...';
        } else if ($file.name.length <= 20) {
            text = $file.name;
        }
        $('.upload-video-form__filename').text(text);
    });

    // $('.upload-video-form__premium-video').on('click', function() {
    //     $(this).parents('.upload-video-form__row--checkbox').siblings('.upload-video-form__row--checkbox.d-none').removeClass('d-none');
    // });

    $('.upload-video-form__one-time-fee').on('click', function() {
        var $sibling = $(this).parents().siblings('.upload-video-form__one-time-fee-additional-options');
        if ($sibling.hasClass('d-none')) {
            $sibling.removeClass('d-none');
        } else {
            $sibling.addClass('d-none');
        }
    });

    var $videoPrice = $('.upload-video-form__video-price');
    if ($videoPrice.length) {
        $videoPrice.change(function () {
            var $this = $(this);
            $this.val(Math.ceil($this.val()) - 0.01);
        });
    }

    $uploadVideoForm.submit(function (e) {
        e.preventDefault();

        var $messaging = $('.messaging');
        var $messagingSection = $messaging.parents('.messaging__section');
        var videoFile = $('#video')[0].files[0];
        var videoTitle = $('.upload-video-form__title').val();
        var categoryValue = $('.upload-video-form__category option:selected').val();
        var bwayworldrecordReleaseForm = $('.upload-video-form__bwayworldrecord').prop('checked');
        var hashtags = $('.upload-video-form__hashtags').val();
        var $monetization = $('#upload-video-form__monetization');

        if ($messagingSection.hasClass('messaging__section--show')) {
            $messagingSection.removeClass('messaging__section--show');
        }

        if (!videoFile) {
            $("html, body").animate({ scrollTop: 0 }, "slow");
            $messagingSection.addClass('messaging__section--show');
            $messaging.find('.messaging__content').html('You did not submit a video file! Please click the "Select Video File" button below.');
            return;
        } else if (videoFile && videoFile.name.indexOf(' ') !== -1) {
            $("html, body").animate({ scrollTop: 0 }, "slow");
            $messagingSection.addClass('messaging__section--show');
            $messaging.find('.messaging__content').html('There was an empty space in your video file name! Please make sure there are no spaces in your filename and resubmit the video.');
            return;
        }

        if (!videoTitle) {
            $("html, body").animate({ scrollTop: 0 }, "slow");
            $messagingSection.addClass('messaging__section--show');
            $messaging.find('.messaging__content').html('Make sure your video doesn\'t get lost – please add a title before uploading.');
            return;
        }

        if (!categoryValue) {
            $("html, body").animate({ scrollTop: 0 }, "slow");
            $messagingSection.addClass('messaging__section--show');
            $messaging.find('.messaging__content').html('Make sure your video doesn\'t get lost – please select a category before uploading.');
            return;
        }

        if (categoryValue === sit.bwayworldrecord_category && !bwayworldrecordReleaseForm) {
            $("html, body").animate({ scrollTop: 0 }, "slow");
            $messagingSection.addClass('messaging__section--show');
            $messaging.find('.messaging__content').html('To submit a video to the #BWAYWORLDRECORD you must read and accept the release terms (bottom of the form).');
            return;
        }

        // if (videoFile && videoFile.size >= 1500000000 && ($premiumVideo.length && !$premiumVideo.prop('checked'))) {
        if (videoFile && videoFile.size >= 1500000000 && !$monetization) {
            $("html, body").animate({ scrollTop: 0 }, "slow");
            $messagingSection.addClass('messaging__section--show');
            $messaging.find('.messaging__content').html('The file size was larger than 1.5 GB. Please compress the video and try again.');
            return;
        } else if (videoFile && videoFile.size >= 2500000000 && $monetization) {
            $("html, body").animate({ scrollTop: 0 }, "slow");
            $messagingSection.addClass('messaging__section--show');
            $messaging.find('.messaging__content').html('The file size was larger than 2.5 GB. Please compress the video and try again.');
            return;
        }

        if (hashtags) {
            var computed_hashtags = hashtags.split(',');
            for (var i = 0; i < computed_hashtags.length; i++) {
                var totalHashtags = computed_hashtags[i].match(/#/g);
                if (totalHashtags && totalHashtags.length > 1) {
                    $("html, body").animate({ scrollTop: 0 }, "slow");
                    $messagingSection.addClass('messaging__section--show');
                    $messaging.find('.messaging__content').html('Oops! Please use a comma to separate hashtags.');
                    return;
                }
            }
        }

        var videoPrice = '';
        if ($monetization.length) {
            videoPrice = $('.upload-video-form__video-price').val();
            if ($('.upload-video-form__one-time-fee').prop('checked') && !videoPrice) {
                $("html, body").animate({ scrollTop: 0 }, "slow");
                $messagingSection.addClass('messaging__section--show');
                $messaging.find('.messaging__content').html('You have elected to sell this video on 10glo, please enter a price to proceed.');
                return;
            }
            if (videoPrice.replace(/\./g, "") >= 29999) {
                $("html, body").animate({ scrollTop: 0 }, "slow");
                $messagingSection.addClass('messaging__section--show');
                $messaging.find('.messaging__content').html('You cannot enter a price greater than $299.99. Please reach out to <a href="mailto:help@10glo.com">help@10glo.com</a> for more help.');
                return;
            }
        }

        // if ($premiumVideo.length && $premiumVideoConsent.length && $premiumVideo.prop('checked') && !$premiumVideoConsent.prop('checked')) {
        //     $("html, body").animate({ scrollTop: 0 }, "slow");
        //     $messagingSection.addClass('messaging__section--show');
        //     $messaging.find('.messaging__content').html('Looks like you opted to mark this video as Premium. Please consent to charge for this video by checking the consent checkbox.');
        //     return;
        // }

        var formData = new FormData();
        formData.append('videoFile', videoFile);
        formData.append('videoTitle', videoTitle);
        formData.append('description', tinymce.activeEditor.getContent());
        formData.append('categoryId', categoryValue);
        formData.append('hashtags', hashtags);
        formData.append('playlistId', $('.upload-video-form__playlist option:selected').val());
        if (bwayworldrecordReleaseForm) {
            formData.append('bwayworldrecord_release', bwayworldrecordReleaseForm);
        }
        if ($monetization) {
            videoPrice = $('.upload-video-form__video-price').val();
            if ($('.upload-video-form__one-time-fee').prop('checked')) {
                formData.append('oneTimeFee', true);
            }
            if ($('.upload-video-form__monthly-members').prop('checked')) {
                formData.append('monthlyMembers', true);
            }
            if (videoPrice) {
                formData.append('videoPrice', videoPrice.replace(/\./g, ""));
            }
        }
        formData.append('userId', sit.current_user_id);

        $('html, body').css('cursor', 'wait');

        $.ajax({
            method: 'POST',
            url: sit.api_url + 'video/create',
            data: formData,
            processData: false,
            contentType: false,
            xhr: function () {
                var xhr = $.ajaxSettings.xhr();
                xhr.upload.onprogress = function (e) {
                    var percentComplete = Math.floor(e.loaded / e.total * 100);
                    $('.upload-progress-bar__container').addClass('upload-progress-bar__container--show');
                    $('#upload-progress-bar').val(percentComplete);
                };
                return xhr;
            },
            beforeSend: function (xhr) {
                xhr.setRequestHeader('X-WP-Nonce', sit.nonce);
                xhr.setRequestHeader('Authorization', 'Basic ' + sit.basic_auth);
            },
            success: function (response) {
                $('html, body').css('cursor', 'auto');
                $messagingSection.addClass('messaging__section--show');
                $messaging.find('.messaging__content').html(response.message);

                setTimeout(function () {
                    location.href = sit.profile_url;
                }, 500);
            },
            error: function (response) {
                $('html, body').css('cursor', 'auto');
                $("html, body").animate({ scrollTop: 0 }, "slow");
                var message = response.responseJSON.message ? response.responseJSON.message : response.message;
                $messagingSection.addClass('messaging__section--show');
                $messaging.find('.messaging__content').html(message);
                $('.upload-progress-bar__container').removeClass('upload-progress-bar__container--show');
                $('#upload-progress-bar').val(0);
            },
            fail: function (response) {
                $('html, body').css('cursor', 'auto');
                $("html, body").animate({ scrollTop: 0 }, "slow");
                var message = response.responseJSON.message ? response.responseJSON.message : response.message;
                $messagingSection.addClass('messaging__section--show');
                $messaging.find('.messaging__content').html(message);
                $('.upload-progress-bar__container').removeClass('upload-progress-bar__container--show');
                $('#upload-progress-bar').val(0);
            }
        });
    });
});
