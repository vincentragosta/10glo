<?php

use Backstage\Util;
use Backstage\View\Element;
use ChildTheme\Components\Playlist\PlaylistView;
use ChildTheme\Controller\VideoController;
use ChildTheme\Video\VideoRepository;

global $wp_query, $post;
if (!($Term = get_queried_object()) instanceof WP_Term) {
    return '';
}
$videos = (new VideoRepository())->findCurated([
    'term_ids' => $Term->term_id,
    'posts_per_page' => VideoController::TAXONOMY_POSTS_PER_PAGE,
    'criteria' => isset($_GET['sort']) ? $_GET['sort'] : 'views'
]);
?>

<div class="content-section">
    <div class="content-section__container container">
        <div class="content-row row">
            <div class="content-column col-12">
                <div class="content-column__inner">
                    <?php if (!empty($videos)): ?>
                        <?= (new PlaylistView($videos, Element::create('h2', $Term->name)->class(Util::componentClasses('heading', ['default']))))
                            ->elementAttributes([
                                'data-hashtag-id' => $Term->term_id,
                                'data-page' => 1,
                                'data-criteria' => isset($_GET['sort']) ? $_GET['sort'] : 'views',
                                'data-posts-per-page' => VideoController::TAXONOMY_POSTS_PER_PAGE
                            ])
                            ->toggleLoadMore(count($videos) == VideoController::TAXONOMY_POSTS_PER_PAGE)
                            ->showSort(true)
                        ?>
                    <?php else: ?>
                        <p>'The have been no videos posted with criteria.'</p>
                    <?php endif; ?>
                </div>
            </div>
        </div>
    </div>
</div>
