<?php

namespace ChildTheme\Exception;

use Exception;

/**
 * Class AmazonS3ServiceException
 * @package ChildTheme\Exception
 * @author Vincent Ragosta <vincentpasqualeragosta@gmail.com>
 * @version 1.0
 */
class AmazonS3ServiceException extends Exception {}
