<?php
/**
 * Template Name: Register
 */

use ChildTheme\Components\Messaging\MessagingView;
use ChildTheme\Options\GlobalOptions;
?>

<div class="content-section content-section--width-narrow">
    <div class="content-section__container container">
        <div class="content-row row messaging__section">
            <div class="content-column col-12">
                <div class="content-column__inner">
                    <?= new MessagingView(); ?>
                </div>
            </div>
        </div>
        <?php if ($register_preface = GlobalOptions::registerPreface()): ?>
            <div class="content-row row content-row--mb-double">
                <div class="content-column col-12">
                    <div class="content-column__inner">
                        <?= $register_preface; ?>
                    </div>
                </div>
            </div>
        <?php endif; ?>
        <div class="content-row row">
            <div class="content-column col-12">
                <div class="content-column__inner">
                    <h2 class="heading heading--medium">Register</h2>
                    <form class="user-profile-form user-profile-form--register" name="user-profile-form" id="user-profile-form" method="post">
                        <p class="user-profile-form__email">
                            <label class="user-profile-form__label" for="user-email">Email Address</label>
                            <input type="email" name="user-email" id="user-email" class="input" size="20"
                                   required>
                        </p>
                        <p class="user-profile-form__name">
                            <label class="user-profile-form__label" for="user-displayname">Name</label>
                            <input type="text" name="user-displayname" id="user-displayname" class="input" size="30"
                                   required>
                            <small>Help people discover your account by using the name you're known by: either your full name, nickname or business name. </small>
                        </p>
                        <p class="user-profile-form__username">
                            <label class="user-profile-form__label" for="user-username">Username</label>
                            <input type="text" name="user-username" id="user-username" class="input" size="20"
                                   required>
                        </p>
                        <p class="user-profile-form__password">
                            <label class="user-profile-form__label" for="user-password">Password</label>
                            <input type="password" name="user-password" id="user-password" class="input" value=""
                                   size="20" required>
                        </p>
                        <p class="user-profile-form__promo-code">
                            <label class="user-profile-form__label" for="user-promo-code">Promo Code</label>
                            <input type="text" name="user-promo-code" id="user-promo-code" class="input" value=""
                                   size="20">
                        </p>
                        <p class="user-profile-form__checkboxes">
                            <label class="user-profile-form__label">
                                <input class="user-profile-form__checkbox user-profile-form__checkbox--age-check"
                                       name="age-check" type="checkbox" id="age-check" required>Please verify you
                                are at least 13 years of age.
                            </label>
                            <label class="user-profile-form__label">
                                <input class="user-profile-form__checkbox user-profile-form__checkbox--policies"
                                       name="policies" type="checkbox" id="policies" required>Please confirm you
                                have read our&nbsp;<a href="<?= home_url('/privacy-policy/'); ?>" target="_blank">Privacy Policy</a>&nbsp;and&nbsp;<a
                                    href="<?= home_url('/terms-and-conditions/'); ?>" target="_blank">Terms of Use</a>.
                            </label>
                        </p>
                        <p class="user-profile-form__submit">
                            <input type="submit" name="wp-submit" id="wp-submit" class="button button-primary"
                                   value="Register">
                        </p>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
