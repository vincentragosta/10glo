addAction(INIT, function () {
    var $createPlaylistForm = $('#create-playlist-form');
    if (!$createPlaylistForm.length) {
        return;
    }
    $createPlaylistForm.submit(function (e) {
        e.preventDefault();

        var $messaging = $('.messaging');
        var $messagingSection = $messaging.parents('.messaging__section');
        var $modal = $('#create-playlist-modal');
        var playlistName = $('#playlist-name').val();
        var videoIds = [];
        $('.create-playlist-form__video-id').each(function (key, value) {
            if ($(value).prop('checked')) {
                videoIds.push($(value).val());
            }
        });

        if (!playlistName) {
            $messagingSection.addClass('messaging__section--show');
            $messaging.find('.messaging__content').html('Please enter a playlist name.');
            return;
        }

        $.ajax({
            method: 'POST',
            url: sit.api_url + 'user/playlist/create',
            data: {
                playlistName: playlistName,
                userId: sit.current_user_id,
                videoIds: videoIds
            },
            beforeSend: function (xhr) {
                xhr.setRequestHeader('X-WP-Nonce', sit.nonce);
                xhr.setRequestHeader('Authorization', 'Basic ' + sit.basic_auth);
            },
            success: function (response) {
                doAction('hideModal', $modal);
                $messagingSection.addClass('messaging__section--show');
                $messaging.find('.messaging__content').html(response.message);

                setTimeout(function () {
                    location.href = sit.profile_url;
                }, 500);
            },
            error: function (response) {
                doAction('hideModal', $modal);
                var message = response.responseJSON.message ? response.responseJSON.message : response.message;
                $messagingSection.addClass('messaging__section--show');
                $messaging.find('.messaging__content').html(message);
            },
            fail: function (response) {
                doAction('hideModal', $modal);
                $messagingSection.addClass('messaging__section--show');
                $messaging.find('.messaging__content').html(response);
            }
        });
    });
});
