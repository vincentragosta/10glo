<?php
/**
 * Template Name: Video Categories
 */
use Backstage\Models\Page;

$Page = Page::createFromGlobal();
?>

<?= $Page->content(false); ?>
<?php if (!empty($categories = get_categories())): ?>
    <div class="content-section">
        <div class="content-section__container container">
            <div class="content-row row">
                <div class="content-column col-12">
                    <div class="content-column__inner">
                        <h1 class="heading heading--default">All Categories on 10glo</h1>
                        <div class="column-text">
                            <ul class="list list--unstyled list--categories">
                                <?php foreach($categories as $Category): ?>
                                    <li><a href="<?= get_term_link($Category); ?>"><?= $Category->name; ?></a></li>
                                <?php endforeach; ?>
                            </ul>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
<?php endif; ?>
