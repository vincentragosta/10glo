<?php
use Backstage\SetDesign\SubNav\SubNavView;
use ChildTheme\Components\PodcastCard\PodcastCardView;
use ChildTheme\Podcast\PodcastRepository;

$PodcastRepository = new PodcastRepository();
?>

<section class="content-section anchor content-section--has-bg content-section--dark content-section--tpad-double content-section--bpad-double content-section--mb-none">
    <span class="content-background-image">
        <span class="content-background-image__images">
            <span class="content-background-image__img content-background-image__img--desktop" style="background-image: url(&quot;https://10glo.vincentragosta.dev/wp-content/uploads/2020/09/10glo-Stories-Hero-V3.png&quot;);"></span>
        </span>
    </span>
    <div class="container content-section__container">
        <div class="content-row row">
            <div class="content-column stories-hero-row col-md-6 content-column--last">
                <div class="content-column__inner">
                    <div class="vc-single-image vc-single-image--center">
                        <figure class="vc-single-image__figure image-loaded">
                            <img class="lazy-image-loaded vc-single-image__image in-view" src="/wp-content/uploads/2020/09/logo-1080-edges.png" alt="logo-1080-edges" width="783" height="714">
                        </figure>
                    </div>
                    <h1 class="heading heading--xlarge">Stories</h1>
                </div>
            </div>
        </div>
    </div>
</section>
<?php if (has_nav_menu('stories_navigation')): ?>
    <div class="content-section content-section--has-bg content-section--white sub-nav--stories-blue">
        <div class="content-section__container container">
            <div class="content-row row">
                <div class="content-column col-12">
                    <div class="content-column__inner">
                        <?= SubNavView::createFromMenu('Stories Navigation', ['horizontal']); ?>
                    </div>
                </div>
            </div>
        </div>
    </div>
<?php endif; ?>
<div class="content-section">
    <div class="content-section__container container">
        <div class="content-row row">
            <div class="content-column col-md-12 content-column--last">
                <div class="content-column__inner">
                    <h2 class="heading heading--medium">The <span class="text--blue">10glo Show</span></h2>
                </div>
            </div>
        </div>
    </div>
</div>
<?php if (!empty($podcasts = $PodcastRepository->findAll())): ?>
    <div class="content-section">
        <div class="content-section__container container">
            <div class="content-row row">
                <?php foreach($podcasts as $Podcast): ?>
                    <div class="content-column col-12 col-sm-6 col-md-4">
                        <div class="content-column__inner">
                            <?= new PodcastCardView($Podcast, 35); ?>
                        </div>
                    </div>
                <?php endforeach; ?>
            </div>
        </div>
    </div>
<?php else: ?>
    <div class="content-section">
        <div class="content-section__container container">
            <div class="content-row row">
                <div class="content-column col-12">
                    <div class="content-column__inner">
                        <p>There are no podcasts tagged with that category</p>
                    </div>
                </div>
            </div>
        </div>
    </div>
<?php endif; ?>
