<?php
/**
 * Expected
 * @var Element $Heading
 * @var NewsPost[] $stories
 * @var Link $ReadMore
 * @var bool $is_compact
 * @var array $class_modifiers
 * @var array $element_attributes
 */

use Backstage\Util;
use Backstage\View\Element;
use Backstage\View\Link;
use ChildTheme\Components\StoryCard\StoryCardView;
use ChildTheme\News\NewsPost;
?>

<div <?= Util::componentAttributes('stories-list', $class_modifiers, $element_attributes); ?>>
    <?php if ($Heading instanceof Element): ?>
        <div class="stories-list__row row">
            <div class="stories-list__col col-12">
                <div class="stories-list__heading-dropdown">
                    <?php if ($Heading instanceof Element): ?>
                        <?= $Heading; ?>
                    <?php endif; ?>
                </div>
            </div>
        </div>
    <?php endif; ?>
    <?php if (!empty($stories)): ?>
        <div class="stories-list__row stories-list__row--stories row">
            <?php foreach($stories as $Story): ?>
                <div class="stories-list__column col-12 col-sm-6 col-md-4 <?= $is_compact ? 'col-lg-3' : ''; ?>">
                    <?= (new StoryCardView($Story))->classModifiers($is_compact ? 'compact' : ''); ?>
                </div>
            <?php endforeach; ?>
        </div>
        <?php if ($ReadMore instanceof Link && $is_compact): ?>
            <div class="stories-list__row row">
                <div class="stories-list__column col-12">
                    <?= $ReadMore; ?>
                </div>
            </div>
        <?php endif; ?>
    <?php else: ?>
        <p>There are no stories with these query settings.</p>
    <?php endif; ?>
</div>
