<?php

namespace ChildTheme\Support;

/**
 * Class NumberGenerator
 * @package ChildTheme\Support
 * @author Vincent Ragosta <vincentpasqualeragosta@gmail.com>
 * @version 1.0
 */
class NumberGenerator
{
    const THOUSAND = 1000;
    const MILLION = 1000000;
    const BILLION = 1000000000;

    public static function generate(int $number)
    {
        if ($number > static::MILLION && $number < static::BILLION) {
            $number = sprintf(
                '%sM',
                static::numberFormat($number / static::MILLION)
            );
        } elseif ($number > static::BILLION) {
            $number = sprintf(
                '%sB',
                static::numberFormat($number / static::BILLION)
            );
        }
        return $number;
    }

    public static function numberFormat(int $number)
    {
        return number_format((float)$number, 2, '.', '');
    }
}
