<?php

namespace ChildTheme\Controller;

/**
 * Class WPEditorController
 * @package ChildTheme\Controller
 */
class WPEditorController
{
    public function __construct()
    {
        add_filter('mce_buttons', [$this, 'removeButtonsFromDisplay']);
        add_filter('mce_buttons', function ($buttons) {
            array_pop($buttons);
            return $buttons;
        });
    }

    public function removeButtonsFromDisplay($buttons)
    {
        if ($position = array_search('wp_more', $buttons)) {
            unset($buttons[$position]);
        }
        if ($position = array_search('blockquote', $buttons)) {
            unset($buttons[$position]);
        }
        return $buttons;
    }
}
