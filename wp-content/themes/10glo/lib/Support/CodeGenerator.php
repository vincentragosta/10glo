<?php

namespace ChildTheme\Support;

/**
 * Class CodeGenerator
 * @package ChildTheme\Support
 * @author Vincent Ragosta <vincentpasqualeragosta@gmail.com>
 * @version 1.0
 */
class CodeGenerator
{
    const DATASET = '0123456789abcdefghijklmnopqrstuvwxyz';
    const LENGTH = 16;

    public static function generate(string $length = self::LENGTH)
    {
        return substr(str_shuffle(str_repeat(static::DATASET, $length)), 0, $length);
    }
}
