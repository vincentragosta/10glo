<?php

namespace ChildTheme\Components\MembershipForm;

use Backstage\View\Component;
use ChildTheme\Service\UserService;
use ChildTheme\User\User;

/**
 * Class MembershipFormView
 * @package ChildTheme\Components\MembershipForm
 * @author Vincent Ragosta <vincentpasqualeragosta@gmail.com>
 * @version 1.0
 *
 * @property string $vendor_name
 * @property string $vendor_id
 * @property bool $is_submit_disabled
 */
class MembershipFormView extends Component
{
    protected $name = 'membership-form';
    protected static $default_properties = [
        'vendor_name' => '',
        'vendor_id' => '',
        'is_submit_disabled' => false
    ];

    public function __construct(User $Vendor)
    {
        parent::__construct([
            'vendor_name' => $Vendor->getDisplayName(),
            'vendor_id' => $Vendor->ID,
            'is_submit_disabled' => (new UserService())->isSameUser(User::createFromCurrentUser(), $Vendor)
        ]);
    }
}
