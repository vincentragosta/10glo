<?php

namespace ChildTheme\Components\StoriesList;

use Backstage\View\Component;
use Backstage\View\Element;
use Backstage\View\Link;

/**
 * Class StoriesListView
 * @package ChildTheme\Components\StoriesList
 * @author Vincent Ragosta <vincentpasqualeragosta@gmail.com>
 * @version 1.0
 *
 * @property array $stories
 * @property Element $Heading
 * @property Link $ReadMore
 * @property bool $is_compact
 */
class StoriesListView extends Component
{
    protected $name = 'stories-list';
    protected static $default_properties = [
        'stories' => [],
        'Heading' => null,
        'ReadMore' => false,
        'is_compact' => false
    ];

    public function __construct(array $stories, Element $Heading = null)
    {
        parent::__construct(compact('stories', 'Heading'));
    }

    public function toggleCompact(bool $is_compact)
    {
        $this->is_compact = $is_compact;
        return $this;
    }

    public function toggleReadMore(Link $ReadMore)
    {
        $this->ReadMore = $ReadMore;
        return $this;
    }
}
