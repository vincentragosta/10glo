<?php
/**
 * Expected:
 * @var string $url
 * @var string $image_url
 * @var string $heading
 * @var bool $is_verified
 * @var array $class_modifiers
 * @var array $element_attributes
 */

use Backstage\Util;
use ChildTheme\Options\GlobalOptions;

?>

<div <?= Util::componentAttributes('user-card', $class_modifiers, $element_attributes); ?>>
    <div class="user-card__container">
        <?php if (!empty($url)): ?>
            <a href="<?= $url; ?>" class="user-card__permalink">
        <?php endif; ?>
            <?php if (!empty($image_url)): ?>
                <span class="user-card__image" style="background-image: url(<?= $image_url; ?>)" title="User Profile Photo"></span>
            <?php else: ?>
                <span class="user-card__placeholder"><span class="user-card__placeholder-inner"></span></span>
            <?php endif; ?>
        <?php if (!empty($url)): ?>
            </a>
        <?php endif; ?>
        <?php if (!empty($heading)): ?>
            <div class="user-card__heading-container">
                <?= $heading; ?>
                <?php if ($is_verified && ($verified_image = GlobalOptions::verifiedArtistsImage())): ?>
                    <?= $verified_image; ?>
                <?php endif; ?>
            </div>
        <?php endif; ?>
    </div>
</div>
