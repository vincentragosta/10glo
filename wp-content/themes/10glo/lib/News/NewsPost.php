<?php

namespace ChildTheme\News;

use Backstage\Models\PostBase;

/**
 * Class NewsPost
 * @package ChildTheme\News
 * @author Vincent Ragosta <vincentpasqualeragosta@gmail.com>
 * @version 1.0
 *
 * @property string $story_portrait_image
 * @property array $deep_links
 */
class NewsPost extends PostBase
{
    const POST_TYPE = 'story';
    const TAXONOMY = 'story-category';
}
