<?php

namespace ChildTheme\Support;

/**
 * Class PreferredDimensionsHandler
 * @package ChildTheme\Support
 * @author Vincent Ragosta <vragosta@situationinteractive.com>
 * @version 1.0
 */
class PreferredDimensionsHandler
{
    const DIMENSIONS_LABEL = '<p>Preferred Dimensions: %d x %d</p> %s';
    const DETAIL_WIDTH = 1440;
    const DETAIL_HEIGHT = 600;
}
