<?php

namespace ChildTheme\Controller;

use Backstage\Util;

/**
 * Class BetaRibbon
 * @package ChildTheme\Support
 * @author Vincent Ragosta <vincentpasqualeragosta@gmail.com>
 * @version 1.0
 */
class BetaRibbonController
{
    public function __construct()
    {
        add_action('before_main', [$this, 'addBetaRibbon']);
    }

    public function addBetaRibbon()
    {
        if (is_page_template('templates/sign-in.php') || is_page_template('templates/register.php')) {
            echo sprintf('<img src="%s" class="beta-ribbon" width="150px"/>', Util::getAssetPath('images/beta-ribbon.png'));
        }
    }
}
