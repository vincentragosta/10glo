<?php

namespace ChildTheme\Components\TestimonialCard;

use Backstage\View\Component;
use ChildTheme\Testimonial\Testimonial;

/**
 * Class TestimonialCardView
 * @package ChildTheme\Components\TestimonialCard
 * @author Vincent Ragosta <vincentpasqualeragosta@gmail.com>
 * @version 1.0
 *
 * @property \WP_Image|bool $image
 * @property string $quote
 * @property string $attribution_name
 * @property \WP_Image|bool $attribution_image
 * @property string $attribution_job_title
 */
class TestimonialCardView extends Component
{
    protected $name = 'testimonial-card';
    protected static $default_properties = [
        'image' => null,
        'quote' => '',
        'attribution_name' => '',
        'attribution_image' => null,
        'attribution_job_title' => ''
    ];

    public function __construct(Testimonial $Testimonial)
    {
        parent::__construct([
            'image' => $Testimonial->featuredImage(),
            'quote' => $Testimonial->content(false),
            'attribution_name' => $Testimonial->attributionName(),
            'attribution_image' => $Testimonial->attributionImage(),
            'attribution_job_title' => $Testimonial->attributionJobTitle()
        ]);
    }
}
