<?php

use ChildTheme\Components\SearchBar\SearchBarView;
use ChildTheme\Components\UserCard\UserCardView;
use ChildTheme\Components\VideoPosterCard\VideoPosterCardView;
use ChildTheme\User\UserRepository;
use ChildTheme\Video\Video;

global $wp_query;
$search_query = get_query_var('s');
?>

<div class="content-section content-section--has-bg content-section--dark content-section--tpad-double content-section--bpad-double content-section--mb-double">
    <div class="content-section__container container">
        <div class="content-row row">
            <div class="content-column col-12 text--center">
                <div class="content-column__inner">
                    <h1 class="heading heading--xlarge">Search 10glo</h1>
                    <?= new SearchBarView(); ?>
                </div>
            </div>
        </div>
    </div>
</div>
<?php if (!empty($users = (new UserRepository())->findByName($search_query))): ?>
    <div class="content-section content-section--mb-double">
        <div class="content-section__container container">
            <div class="content-row row">
                <div class="content-column col-12">
                    <div class="content-column__inner">
                        <h2 class="heading heading--large">Users</h2>
                    </div>
                </div>
                <?php foreach($users as $User): ?>
                    <div class="content-column col-12 col-md-3">
                        <div class="content-column__inner">
                            <?= UserCardView::createFromUser($User); ?>
                        </div>
                    </div>
                <?php endforeach; ?>
            </div>
        </div>
    </div>
<?php endif; ?>
<?php if ($wp_query->found_posts > 0): ?>
    <div class="content-section content-section--mb-double">
        <div class="content-section__container container">
            <div class="content-row row">
                <div class="content-column col-12">
                    <div class="content-column__inner">
                        <h2 class="heading heading--large">Videos</h2>
                    </div>
                </div>
                <?php foreach($wp_query->posts as $post): ?>
                    <div class="content-column col-12 col-md-3">
                        <div class="content-column__inner">
                            <?php
                                // TODO: Turn into VideoCollection
                                echo new VideoPosterCardView(Video::create($post));
                            ?>
                        </div>
                    </div>
                <?php endforeach; ?>
            </div>
        </div>
    </div>
<?php endif; ?>
