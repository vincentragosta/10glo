<?php

namespace ChildTheme\Components\SmartJobsList;

use Backstage\VcLibrary\Support\Component;
use ChildTheme\Job\Job;
use ChildTheme\Job\JobRepository;

/**
 * Class SmartJobsList
 * @package ChildTheme\Components\SmartJobsList
 * @author Vincent Ragosta <vincentpasqualeragosta@gmail.com>
 * @version 1.0
 */
class SmartJobsList extends Component
{
    const NAME = 'Smart Jobs List';
    const TAG = 'smart_jobs_list';
    const VIEW = SmartJobsListView::class;

    protected $component_config = [
        'description' => 'Display a list of jobs ',
        'icon' => 'icon-wpb-toggle-small-expand',
        'wrapper_class' => 'clearfix',
        'is_container' => false,
        'category' => 'Content',
        'params' => [
            'posts_per_page' => [
                'type' => 'textfield',
                'heading' => 'Posts per page',
                'param_name' => 'posts_per_page',
                'description' => 'Enter how many posts to display. Default: 8.',
                'admin_label' => true,
            ]
          ]
    ];

    protected function createView(array $atts)
    {
      $ViewClass = static::VIEW;
        $JobRepository = new JobRepository();
        // var_dump($jobs = $JobRepository->findCurated($atts));
        if (empty($jobs = $JobRepository->findCurated($atts))) {
            return 'The have been no jobs posted with this criteria.';
        }
        return new $ViewClass($jobs);
    }
}
