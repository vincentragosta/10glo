<?php

namespace ChildTheme\Video;

use Backstage\Models\DTO;
use Backstage\Models\DTOCollection;

/**
 * Class VideoDTOCollection
 * @package ChildTheme\Video
 * @author Vincent Ragosta <vragosta@situationinteractive.com>
 * @version 1.0
 */
class VideoDTOCollection extends DTOCollection
{
    protected static $object_class_name = VideoDTO::class;

    /**
     * @param Video $item
     * @return DTO
     */
    protected function createDTO($item): DTO
    {
        return new VideoDTO($item);
    }
}
