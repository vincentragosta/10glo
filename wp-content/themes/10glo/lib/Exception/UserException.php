<?php

namespace ChildTheme\Exception;

use Exception;

/**
 * Class UserException
 * @package ChildTheme\Exception
 * @author Vincent Ragosta <vincentpasqualeragosta@gmail.com>
 * @version 1.0
 */
class UserException extends Exception {}
