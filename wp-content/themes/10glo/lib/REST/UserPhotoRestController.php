<?php

namespace ChildTheme\REST;

use Backstage\Controller\RestController;
use ChildTheme\Exception\UserPhotoException;
use ChildTheme\Service\LinodeS3Service;
use ChildTheme\Traits\GetCurrentUserFromRequest;
use ChildTheme\Traits\GetPhotoFileFromRequestTrait;
use \WP_REST_Request;

/**
 * Class UserPhotoRestController
 * @package ChildTheme\Controller
 * @author Vincent Ragosta <vincentpasqualeragosta@gmail.com>
 * @version 1.0
 *
 * @property string $namespace
 * @property LinodeS3Service $LinodeS3Service
 */
class UserPhotoRestController extends RestController
{
    const MB = 1048576;

    protected $namespace = 'user';
    protected $LinodeS3Service;

    use GetPhotoFileFromRequestTrait, GetCurrentUserFromRequest;

    public function __construct()
    {
        parent::__construct();
        $this->LinodeS3Service = new LinodeS3Service();
    }

    public function registerRoutes()
    {
        $this->addCreateRoute('/user-photo/create', [$this, 'createUserPhoto']);
        $this->addCreateRoute('/user-photo/remove', [$this, 'removeUserPhoto']);
    }

    public function createUserPhoto(WP_REST_Request $request)
    {
        $file = $this->getPhotoFileFromRequest($request);
        $CurrentUser = $this->getCurrentUserFromRequest($request,'Please login or create an account to upload a user photo.');
        $filename = LinodeS3Service::getPhotoArchitecture($CurrentUser->ID, $file['name']);

        if (!empty($existing_file = $CurrentUser->getUserPhotoUrl())) {
            $this->LinodeS3Service->deleteFile($existing_file);
            $CurrentUser->removeUserPhotoUrl();
        }

        $this->LinodeS3Service->putFile($filename, $file['tmp_name']);
        if (!$object_url = $this->LinodeS3Service->getObjectUrl($filename)) {
            throw new UserPhotoException('We could not store your file at this time.', 400);
        }
        $CurrentUser->updateUserPhotoUrl($object_url);

        return ['message' => 'The user profile photo has been uploaded.', 'status' => 200];
    }

    public function removeUserPhoto(WP_REST_Request $request)
    {
        $CurrentUser = $this->getCurrentUserFromRequest($request, 'Please login or create an account to remove your user photo.');
        if (empty($user_photo_url = $CurrentUser->getUserPhotoUrl())) {
            throw new UserPhotoException('The profile user photo is not set for this user.', 400);
        }
        $this->LinodeS3Service->deleteFile($user_photo_url);
        $CurrentUser->removeUserPhotoUrl();
        return ['message' => 'The user photo has been removed.', 'status' => 200];
    }
}
