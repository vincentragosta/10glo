<?php

namespace ChildTheme\Components\SupportForm;

use Backstage\View\Component;
use ChildTheme\Service\UserService;
use ChildTheme\User\User;

/**
 * Class SupportFormView
 * @package ChildTheme\Components\SupportForm
 * @author Vincent Ragosta <vincentpasqualeragosta@gmail.com>
 * @version 1.0
 *
 * @property string $vendor_id
 * @property bool $is_submit_disabled
 */
class SupportFormView extends Component
{
    protected $name = 'support-form';
    protected static $default_properties = [
        'vendor_id' => '',
        'is_submit_disabled' => false
    ];

    public function __construct(User $Vendor)
    {
        parent::__construct([
            'vendor_id' => $Vendor->ID,
            'is_submit_disabled' => (new UserService())->isSameUser(User::createFromCurrentUser(), $Vendor)
        ]);
    }
}
