<?php
/**
 * Expected:
 * @var string $quote
 * @var WP_Image|bool $image
 * @var string $attribution_name
 * @var WP_Image|bool $attribution_image
 * @var string $attribution_job_title
 * @var array $class_modifiers
 * @var array $element_attributes
 */

use Backstage\Util;

if (empty($quote)) {
    return '';
}
?>

<div <?= Util::componentAttributes('testimonial-card', $class_modifiers, $element_attributes); ?>>
    <?php if ($image instanceof \WP_Image): ?>
        <div class="testimonial-card__image-container">
            <?= $image->css_class('testimonial-card__image'); ?>
        </div>
    <?php endif; ?>
    <?= $quote; ?>
    <?php if (!empty($attribution_name)): ?>
        <div class="testimonial-card__attribution-container">
            <?php if ($attribution_image): ?>
                <div class="testimonial-card__attribution-image-container">
                    <?= $attribution_image->css_class('testimonial-card__attribution-image'); ?>
                </div>
            <?php endif; ?>
            <div class="testimonial-card__content-container">
                <h3 class="testimonial-card__attribution-name heading heading--default text--primary"><?= $attribution_name; ?></h3>
                <?php if (!empty($attribution_job_title)): ?>
                    <p class="testimonial-card__attribution-job-title"><?= $attribution_job_title; ?></p>
                <?php endif; ?>
            </div>
        </div>
    <?php endif; ?>
</div>
