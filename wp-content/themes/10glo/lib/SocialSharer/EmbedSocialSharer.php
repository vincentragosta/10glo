<?php

namespace ChildTheme\SocialSharer;

use Backstage\Models\PostBase;
use ChildTheme\Video\Video;

/**
 * Class EmbedSocialSharer
 * @package ChildTheme\SocialSharer
 * @author Vincent Ragosta <vincentpasqualeragosta@gmail.com>
 * @version 1.0
 */
class EmbedSocialSharer extends SocialSharer
{
    const LABEL = 'Embed';
    const SHARER = "<iframe width='560' height='315' src='%s' frameborder='0' allow='accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture' allowfullscreen></iframe>";
    const ICON = 'embed-circle';

    /**
     * @param PostBase|Video $Video
     * @return string
     */
    protected function generateSharerUrl($Video)
    {
        if (empty($video_source = $Video->video_url)) {
            return '';
        }
        return sprintf(static::SHARER, $video_source);
    }
}
