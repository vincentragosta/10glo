<?php
/**
 * Expected:
 * @var NewsPost[] $news
 * @var array $options
 * @var array $class_modifiers
 * @var array $element_attributes
 */

use ChildTheme\Components\NewsPostCard\NewsPostCardView;
use ChildTheme\News\NewsPost;
use Backstage\Util;

if (empty($news)) {
    return '';
}
?>

<div <?= Util::componentAttributes('smart-news-list', $class_modifiers, $element_attributes); ?>>
    <?php foreach($news as $NewsPost): ?>
        <?= (new NewsPostCardView($NewsPost))->classModifiers($options); ?>
    <?php endforeach; ?>
</div>
