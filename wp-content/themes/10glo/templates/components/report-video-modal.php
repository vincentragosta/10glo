<?php
/**
 * Expected:
 * @var Video $Video
 **/

namespace ChildTheme\Video\Video;

?>

<div class="report-video-modal">
  <form class="report-video-modal__form" action="" id="report-video-modal__form" method="post">
    <h2 class="report-video-modal__heading heading heading--large">Report</h2>
    <textarea class="report-video-modal__description">Describe The Offense</textarea>
    <input class="report-video-modal__submit" type="submit"/>
  </form>
</div>
