<?php

use ChildTheme\Components\Messaging\MessagingView;
use ChildTheme\REST\VideoRestController;
use ChildTheme\Options\GlobalOptions;
use ChildTheme\Service\PremiumVideoService;
use ChildTheme\Service\StripeService;
use ChildTheme\Service\UserMonetizationService;
use ChildTheme\User\User;
use ChildTheme\Video\Video;
use ChildTheme\Video\VideoRepository;
use Stripe\Price;

$CurrentUser = User::createFromCurrentUser();
$Video = (new VideoRepository())->findOneBySlug(get_query_var('related_video'));
if (!$Video instanceof Video) {
    echo 'A video with this ID does not exist.';
    return;
}
$Category = $Video->getPrimaryCategory();
$category_args = ['hide_empty' => false, 'order' => 'ASC'];
if (($BWAYWORLDRECORDCategory = GlobalOptions::bwayworldrecordCategory()) && $Category->term_id !== $BWAYWORLDRECORDCategory->term_id) {
    $category_args['exclude'] = $BWAYWORLDRECORDCategory->term_id;
}
$PremiumVideoService = new PremiumVideoService();
?>

<section class="content-section content-section--width-narrow">
    <div class="content-section__container container">
        <div class="content-row row">
            <div class="content-column col-12">
                <div class="content-column__inner">
                    <?php if (($UserMonetizationService = new UserMonetizationService())->isVerified($CurrentUser)): ?>
                        <div class="monetization-banner">
                            <h2 class="heading heading--medium">Whoops!</h2>
                            <p>If you’re interested in monetizing some or all of your content on 10glo, please be sure to first enroll in <a href="<?= $UserMonetizationService->getVendorMonetizeLink($CurrentUser); ?>">10glo’s Monetization Program</a>. Once you’re fully enrolled, you’ll see additional options on this page for monetizing your video. </p>
                        </div>
                    <?php endif; ?>
                </div>
            </div>
        </div>
        <div class="content-row row messaging__section">
            <div class="content-column col-12">
                <div class="content-column__inner">
                    <?= new MessagingView(); ?>
                </div>
            </div>
        </div>
    </div>
</section>
<section class="content-section content-section--width-narrow">
    <div class="content-section__container container">
        <div class="content-row row">
            <div class="content-column col-12">
                <div class="content-column__inner">
                    <div class="video-edit">
                        <h2 class="video-edit__heading heading heading--large">Edit Video</h2>
                        <form id="edit-video-form" class="video-edit__form edit-video-form" name="edit-video-form" action="" method="post" enctype="multipart/form-data">
                            <div class="edit-video-form__row">
                                <div class="edit-video-form__column">
                                    <label for="video-title">Video Title</label>
                                    <input type="text" class="edit-video-form__title" name="video-title" id="video-title" value="<?= $Video->title(); ?>"/>
                                </div>
                                <div class="edit-video-form__column">
                                    <label for="edit-video-form__category">Categories</label>
                                    <select name="edit-video-form__category" class="edit-video-form__category" <?= ($BWAYWORLDRECORDCategory && $Category->term_id === $BWAYWORLDRECORDCategory->term_id) ? 'disabled=disabled' : ''; ?>>
                                        <?php foreach (get_categories($category_args) as $category): ?>
                                            <option value="<?= $category->term_id; ?>" <?= $Category instanceof WP_Term && $Category->term_id == $category->term_id ? 'selected' : ''; ?> >
                                                <?= $category->name; ?>
                                            </option>
                                        <?php endforeach; ?>
                                    </select>
                                </div>
                            </div>
                            <div class="edit-video-form__row">
                                <label for="edit-video-form__description">Video Description</label>
                                <?php wp_editor($Video->post()->post_content, 'video-description', ['textarea_name' => 'edit-video-form__description', 'media_buttons' => false, 'quicktags' => false]); ?>
                                <small>Max length: 1500.</small>
                            </div>
                            <div class="edit-video-form__row">
                                <label for="edit-video-form__hashtags">Hashtags</label>
                                <input type="text" class="edit-video-form__hashtags" name="video-hashtags" id="video-hashtags" value="<?= implode(VideoRestController::HASHTAG_DELIMITER, array_column($Video->terms('hashtags'), 'name')); ?>"/>
                                <p class="edit-video-form__small" style="margin-bottom: 5px;"><small>Separate hashtags by using a comma. Please make sure to add the # before your tag. Spaces will be removed. <strong>Max 5 hashtags</strong>. If you’d like your video to appear in our BIPOC Voices playlist be sure to add the hashtag <strong>#bipocvoices</strong>.</small></p>
                            </div>
                            <div class="edit-video-form__row">
                                <div class="edit-video-form__preview-image-container">
                                    <button type="button" class="edit-video-form__preview-image-button button button--secondary">Select Preview Image</button>
                                    <input type="file" class="edit-video-form__preview-image" name="preview-image" id="preview-image" />
                                    <p class="edit-video-form__filename">No file uploaded</p>
                                </div>
                                <p class="edit-video-form__preview-text"><small>10glo automatically generates a preview image for each video. But if you'd like to use a custom image go ahead and upload that here. Suggested dimensions are 720x405 (16:9).</small></p>
                            </div>
                            <?php if ($UserMonetizationService->isVerified($CurrentUser)): ?>
                                <div class="edit-video-form__row" id="edit-video-form__monetization" style="background-color: rgba(var(--global__colors__secondary), .2); padding: var(--global__sizing__layout-spacing--half); border-radius: 15px;">
                                    <div class="edit-video-form__row--checkbox" style="margin-bottom: 0; display: flex; flex-flow: column nowrap;">
                                        <h3 class="heading heading--default">Monetization Options</h3>
                                        <p class="edit-video-form__small"><small>Your video will be free to watch by all users unless <strong>you select one, or both, options below</strong>.</small></p>
                                        <p class="edit-video-form__small"><small>If both boxes are selected, users who are not paying monthly members of your channel will be able to pay the one-time fee to watch this video.</small></p>
                                        <label class="edit-video-form__label" style="margin: 0;"><input class="edit-video-form__one-time-fee" name="one-time-fee" type="checkbox" <?= (($StripeService = new StripeService())->isPriceValid($Price = $PremiumVideoService->getPriceFromVideo($Video))) ? 'checked' : ''; ?>>Charge users a one-time fee to watch this video</label>
                                        <div class="edit-video-form__one-time-fee-additional-options <?= !$StripeService->isPriceValid($Price) ? 'd-none' : ''; ?>" style="display: flex; flex-flow: column nowrap; width: 100%; padding: var(--global__sizing__layout-spacing--half) 0;">
                                            <label for="edit-video-form__video-price">Please enter the price you'd like to charge users to watch this video.</label>
                                            <div class="form-fields__prepend-icon">
                                                <span>$</span>
                                                <input type="text" value="<?= !empty(($unit_amount = $Price->unit_amount)) ? $unit_amount / 100 : ''; ?>" class="edit-video-form__video-price" name="edit-video-form__video-price" id="edit-video-form__video-price" <?= $Price instanceof Price ? 'disabled' : ''; ?>/>
                                            </div>
                                        </div>
                                        <label class="edit-video-form__label" style="margin: 0;"><input class="edit-video-form__monthly-members" name="monthly-members" type="checkbox" <?= $PremiumVideoService->isVideoMarkedForMonthlyMembers($Video) ? 'checked' : ''; ?>/>Make available to paying monthly members</label>
                                    </div>
                                </div>
                            <?php endif; ?>
                            <input type="hidden" class="edit-video-form__video-id" value="<?= $Video->ID; ?>" />
                            <input type="submit" class="edit-video-form__submit" value="Update Video"/>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
