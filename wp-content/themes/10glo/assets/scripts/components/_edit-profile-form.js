addAction(INIT, function () {
    var $editProfileForm = $('.edit-profile-form');
    if (!$editProfileForm.length) {
        return;
    }

    $editProfileForm.on('submit', function (e) {
        e.preventDefault();

        var errors = ['<li>There are errors with your submission:</li>'];
        var $messaging = $('.messaging');
        var $messagingSection = $messaging.parents('.messaging__section');
        var userId = sit.current_user_id;
        var username = $.trim($('#user-username').val());
        var password = $.trim($('#user-password').val());
        var data = {
            user_email: $.trim($('#user-email').val()),
            display_name: $.trim($('#user-display-name').val()),
            location: $.trim($('#user-location').val()),
            school: $.trim($('#user-school').val()),
            personalWebsiteUrl: $.trim($('#user-personal').val()),
            facebookUrl: $.trim($('#user-facebook').val()),
            instagramUrl: $.trim($('#user-instagram').val()),
            twitterUrl: $.trim($('#user-twitter').val()),
            country: $.trim($('#user-country option:selected').val()),
            bio: tinymce.get('user-bio').getContent()
        };

        if (username.indexOf(' ') >= 0) {
            errors.push('<li>Please do not add spaces to your username.</li>');
        } else {
            data.user_login = username;
        }

        if (password.length > 0) {
            data.user_pass = password;
        }

        if (!userId) {
            errors.push('<li>The user id is not correct.</li>');
        } else {
            data.ID = userId;
        }

        if (errors.length > 1) {
            $messagingSection.addClass('messaging__section--show');
            $messaging.find('.messaging__content').html("<ul class='list--unstyled'>" + errors.join('') + "</ul>");
            return;
        }

        var $modal = $('#user-update-form-modal');

        $.ajax({
            method: 'POST',
            url: sit.api_url + 'user/update',
            data: data,
            beforeSend: function (xhr) {
                xhr.setRequestHeader('X-WP-Nonce', sit.nonce);
                xhr.setRequestHeader('Authorization', 'Basic ' + sit.basic_auth);
            },
            success: function (response) {
                doAction('hideModal', $modal);
                $messagingSection.addClass('messaging__section--show');
                $messaging.find('.messaging__content').html("You have successfully updated your account.");

                setTimeout(function () {
                    location.href = sit.profile_url;
                }, 500);
            },
            error: function (response) {
                doAction('hideModal', $modal);
                var message = response.responseJSON.message ? response.responseJSON.message : response.message;
                $messagingSection.addClass('messaging__section--show');
                $messaging.find('.messaging__content').html(message);
            },
            fail: function (response) {
                doAction('hideModal', $modal);
                $messagingSection.addClass('messaging__section--show');
                $messaging.find('.messaging__content').html(response);
            }
        });
    });
});
