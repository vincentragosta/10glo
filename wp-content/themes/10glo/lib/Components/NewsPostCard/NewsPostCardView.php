<?php

namespace ChildTheme\Components\NewsPostCard;

use Backstage\View\Component;
use ChildTheme\News\NewsPost;
use ChildTheme\Support\RelativeFormatConverter;
use ChildTheme\User\User;

/**
 * Class NewsPostCardView
 * @package ChildTheme\Components\NewsPostCard
 * @author Vincent Ragosta <vincentpasqualeragosta@gmail.com>
 * @version 1.0
 *
 * @property \WP_Image|bool $Image
 * @property string $title
 * @property User $Author
 * @property string $content
 * @property string $relative_published_time
 * @property string $url
 */
class NewsPostCardView extends Component
{
    protected $name = 'news-post-card';
    protected static $default_properties = [
        'Image' => false,
        'title' => '',
        'Author' => '',
        'content' => '',
        'relative_published_time' => '',
        'url' => ''
    ];

    public function __construct(NewsPost $NewsPost, array $options = [])
    {
        $args = [
            'title' => $NewsPost->title(),
            'Author' => User::createFromPost($NewsPost),
            'content' => $NewsPost->excerpt(),
            'url' => $NewsPost->permalink(),
            'deep_links' => $NewsPost->deep_links
        ];
        if (($Image = $NewsPost->featuredImage()) instanceof \WP_Image) {
            $args['Image'] = $Image->width(768)->height(432);
        }
        if (($PublishedTime = new RelativeFormatConverter(strtotime($NewsPost->publishedDate())))->isLessThanAnHour()) {
            $args['relative_published_time'] = $PublishedTime->getRelativeTime();
        }
        parent::__construct($args);
    }
}
