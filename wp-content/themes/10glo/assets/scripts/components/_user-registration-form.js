addAction(INIT, function () {
    var $registerForm = $('.user-profile-form--register');
    if (!$registerForm.length) {
        return;
    }
    $registerForm.on('submit', function (e) {
        e.preventDefault();

        var $messaging = $('.messaging');
        var $messagingSection = $messaging.parents('.messaging__section');
        var errors = ['<li>There are errors with your submission:</li>'];
        var ageCheck = $('.user-profile-form__checkbox--age-check').prop('checked');
        var policies = $('.user-profile-form__checkbox--policies').prop('checked');
        var username = $.trim($('#user-username').val());
        var data = {
            user_pass: $.trim($('#user-password').val()),
            user_email: $.trim($('#user-email').val()),
            display_name: $.trim($('#user-displayname').val()),
            promo_code: $.trim($('#user-promo-code').val())
        };

        if (username.indexOf(' ') >= 0) {
            errors.push('<li>Please do not add spaces to your username.</li>');
        } else {
            data.user_login = username;
        }

        if (!ageCheck) {
            errors.push('<li>Please confirm you are older than 13 years of age.</li>');
        }

        if (!policies) {
            errors.push('<li>Please confirm you have read the <a href="/privacy-policy" target="_blank">Privacy Policy</a> and <a href="/terms-of-use" target="_blank">Terms of Use</a>.</li>');
        }

        if (errors.length > 1) {
            $messagingSection.addClass('messaging__section--show');
            $messaging.find('.messaging__content').html("<ul class='list--unstyled'>" + errors.join('') + "</ul>");
            return;
        }

        $.ajax({
            method: 'POST',
            url: sit.api_url + 'user/register',
            data: data,
            beforeSend: function (xhr) {
                xhr.setRequestHeader('X-WP-Nonce', sit.nonce);
                xhr.setRequestHeader('Authorization', 'Basic ' + sit.basic_auth);
            },
            success: function (response) {
                $messagingSection.addClass('messaging__section--show');
                $messaging.find('.messaging__content').html(response.message);
                setTimeout(function () {
                    location.href = sit.profile_url;
                }, 500);
            },
            error: function (response) {
                var message = response.responseJSON.message ? response.responseJSON.message : response.message;
                $messagingSection.addClass('messaging__section--show');
                $messaging.find('.messaging__content').html(message);
            },
            fail: function (response) {
                $messagingSection.addClass('messaging__section--show');
                $messaging.find('.messaging__content').html(response.message);
            }
        });
    });
});
