<?php
/**
 * Expected:
 * @var string $icon
 * @var string $label
 * @var array $class_modifiers
 * @var array $element_attributes
 */

use Backstage\Util;

if (empty($icon)) {
    return '';
}
?>

<div <?= Util::componentAttributes('social-sharer-card', $class_modifiers, $element_attributes); ?>>
    <?= $icon; ?>
    <?php if (!empty($label)): ?>
        <p class="social-sharer-card__label"><?= $label; ?></p>
    <?php endif; ?>
</div>
