<?php

use ChildTheme\Job\Job;
use Backstage\SetDesign\Icon\IconView;
use ChildTheme\SocialSharer\FacebookSocialSharer;
use ChildTheme\SocialSharer\TwitterSocialSharer;

$Job = Job::createFromGlobal();
// var_dump($Job);

?>



<div class="content-section">
  <div class="content-section__container container">
    <div class="content-row row content-row--mb-half">
      <div class="content-column col-12">
          <div class="content-column__inner">
              <h1 class="heading heading--large"><?php echo $Job->title(); ?></h1>
              <p><?php echo $Job->content(false); ?></p>
          </div>
      </div>
    </div>
    <div class="content-row row">
      <div class="content-column col-12">
          <div class="content-column__inner">
            <ul class="list list--inline">
              <li><a href="<?= (new FacebookSocialSharer($Job))->getUrl(); ?>" target="_blank"><?= new IconView(['icon_name' => 'facebook']); ?></a></li>
              <li><a href="<?= (new TwitterSocialSharer($Job))->getUrl(); ?>" target="_blank"><?= new IconView(['icon_name' => 'twitter']); ?></a></li>
            </ul>
          </div>
      </div>
    </div>
  </div>
</div>
