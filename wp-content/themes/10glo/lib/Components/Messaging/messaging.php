<?php
/**
 * Expected:
 * @var string $message
 * @var array $class_modifiers
 * @var array $element_attributes
 */

use Backstage\Util;

?>

<div <?= Util::componentAttributes('messaging', $class_modifiers, $element_attributes); ?>>
    <button class="messaging__close" type="button" aria-label="Close messaging">✕</button>
    <div class="messaging__content">
        <?php if ($message): ?>
            <?= $message; ?>
        <?php endif; ?>
    </div>
</div>
