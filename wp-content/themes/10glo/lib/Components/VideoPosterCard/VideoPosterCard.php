<?php

namespace ChildTheme\Components\VideoPosterCard;

use Backstage\VcLibrary\Support\Component;
use ChildTheme\Video\VideoRepository;
use ChildTheme\Video\Video;

/**
 * Class VideoPosterCard
 * @package ChildTheme\Components\VideoPosterCard
 * @author Vincent Ragosta <vincentpasqualeragosta@gmail.com>
 * @version 1.0
 */
class VideoPosterCard extends Component
{
    const NAME = 'Video Poster Card';
    const TAG = 'video_poster_card';
    const VIEW = VideoPosterCardView::class;

    protected $component_config = [
        'description' => 'Display a video from a video ID.',
        'icon' => 'icon-wpb-toggle-small-expand',
        'wrapper_class' => 'clearfix',
        'is_container' => false,
        'category' => 'Content',
        'params' => [
            [
                'type' => 'textfield',
                'heading' => 'Video ID',
                'param_name' => 'video_id',
                'description' => 'Enter the video ID.',
                'admin_label' => true
            ]
        ]
    ];

    protected function createView(array $atts)
    {
        if (!($Video = (new VideoRepository())->findById($atts['video_id'])) instanceof Video) {
            return '';
        }
        $ViewClass = self::VIEW;
        return new $ViewClass($Video);
    }
}
