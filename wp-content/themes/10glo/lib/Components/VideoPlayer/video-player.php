<?php
/**
 * Expected:
 * @var User $User
 * @var string $title
 * @var string $url
 * @var array $class_modifiers
 * @var array $element_attributes
 */

use Backstage\Util;
use ChildTheme\User\User;

if (empty($url) || empty($type) || empty($permalink)) {
    return '';
}
?>

<div class="video-js__container">
    <?php if ($title): ?>
        <div class="video-js__title-container">
            <?php if ($User instanceof User): ?>
                <div class="video-js__user-image-container">
                    <a href="<?= $User->permalink(); ?>" target="_blank">
                        <img src="<?= $User->getUserPhotoUrl(); ?>" />
                    </a>
                </div>
            <?php endif; ?>
            <h2 class="video-js__heading heading heading--default"><a href="<?= $permalink; ?>" target="_blank"><?= $title; ?></a></h2>
        </div>
    <?php endif; ?>
    <video <?= Util::componentAttributes('video-js', $class_modifiers, $element_attributes); ?>>
        <source src="<?= $url; ?>" type="<?= $type; ?>" />
        <p class="vjs-no-js">To view this video please enable JavaScript, and consider upgrading to a web browser that<a href="https://videojs.com/html5-video-support/" target="_blank">supports HTML5 video</a></p>
    </video>
    <div class="video-js__drawer"><a href="<?= home_url(); ?>" target="_blank" class="video-js__button button">Watch on 10glo</a></div>
</div>
