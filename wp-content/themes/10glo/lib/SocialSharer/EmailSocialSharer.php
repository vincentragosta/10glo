<?php

namespace ChildTheme\SocialSharer;

use Backstage\Models\PostBase;
use ChildTheme\Video\Video;

/**
 * Class EmailSocialSharer
 * @package ChildTheme\SocialSharer
 * @author Vincent Ragosta <vragosta@situationinteractive.com>
 * @version 1.0
 */
class EmailSocialSharer extends SocialSharer
{
    const LABEL = 'Email';
    const SHARER = 'mailto:?subject=%s&body=%s';
    const ICON = 'email-circle';

    /**
     * @param PostBase|Video $Video
     * @return string
     */
    protected function generateSharerUrl($Video)
    {
        if (empty($title = $Video->title()) || empty($Video->permalink())) {
            return '';
        }
        return sprintf(static::SHARER,
            'Check out this video on 10glo!',
            sprintf('Check out this video %s.', $title)
        );
    }
}
