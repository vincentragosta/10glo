addAction(INIT, function () {
    var $socialSharerIcon = $('.social-sharer-card__icon');
    if ($socialSharerIcon.length) {
        $socialSharerIcon.click(function () {
            if (window.isSecureContext) {
                navigator.clipboard.writeText($('.share-video-modal__embed-iframe').val());
            }
            $('.copy-to-clipboard').addClass('copy-to-clipboard--show');
            doAction('hideModal', $('#share-video-modal'));

            setTimeout(function () {
                $('.copy-to-clipboard').removeClass('copy-to-clipboard--show');
            }, 2500);
        });
    }

    var $shareVideoModalCopyButton = $('.share-video-modal__copy-button');
    if ($shareVideoModalCopyButton.length) {
        $shareVideoModalCopyButton.click(function () {
            $('.share-video-modal__copy').select();
            document.execCommand('copy');
            $('.copy-to-clipboard').addClass('copy-to-clipboard--show');
            doAction('hideModal', $('#share-video-modal'));

            setTimeout(function () {
                $('.copy-to-clipboard').removeClass('copy-to-clipboard--show');
            }, 2500);
        });
    }
});
