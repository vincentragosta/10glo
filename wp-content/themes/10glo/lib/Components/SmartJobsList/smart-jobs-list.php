<?php
/**
 * Expected:
 * @var Job[] $jobs
 * @var array $class_modifiers
 * @var array $element_attributes
 */

use ChildTheme\Components\JobCard\JobCardView;
use ChildTheme\Job\Job;
use Backstage\Util;

if (empty($jobs)) {
    return '';
}
?>

<div <?= Util::componentAttributes('smart-jobs-list', $class_modifiers, $element_attributes); ?>>
    <?php foreach($jobs as $Job): ?>
        <?= new JobCardView($Job); ?>
    <?php endforeach; ?>
</div>
