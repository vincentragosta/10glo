<?php

namespace ChildTheme\Controller;

use Backstage\Rewrite\QueryRewrite;

/**
 * Class PodcastController
 * @package ChildTheme\Controller
 * @author Vincent Ragosta <vincentpasqualeragosta@gmail.com>
 * @version 1.0
 */
class PodcastController
{
    public function __construct()
    {
        new QueryRewrite('podcast/the-10glo-show', ['podcast_listing' => true]);
        add_filter('body_class', [$this, 'addBodyClass']);
        add_filter('template_include', [$this, 'templateInclude'], 99);
    }

    public function addBodyClass($classes)
    {
        global $wp_query;
        if ($wp_query->get('podcast_listing')) {
            $classes = array_merge($classes, ['page-template-podcast-listing']);
        }
        return $classes;
    }

    public function templateInclude($template)
    {
        $wp_query = $GLOBALS['wp_query'];
        if ($wp_query->get('podcast_listing')) {
            $new_template = locate_template(['templates/podcast-listing.php']);
            if ('' != $new_template) {
                return $new_template;
            }
        }
        return $template;
    }
}
