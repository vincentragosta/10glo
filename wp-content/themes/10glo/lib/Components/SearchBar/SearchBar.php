<?php

namespace ChildTheme\Components\SearchBar;

use Backstage\VcLibrary\Support\Component;

/**
 * Class SearchBar
 * @package ChildTheme\Components\SearchBar
 * @author Vincent Ragosta <vincentpasqualeragosta@gmail.com>
 * @version 1.0
 */
class SearchBar extends Component
{
    const NAME = 'Search Bar';
    const TAG = 'search_bar';
    const VIEW = SearchBarView::class;

    protected $component_config = [
        'description' => 'Display a search bar.',
        'icon' => 'icon-wpb-toggle-small-expand',
        'show_settings_on_create' => false,
        'wrapper_class' => 'clearfix',
        'is_container' => false,
        'category' => 'Content',
        'params' => []
    ];

    protected function createView(array $atts)
    {
        $ViewClass = static::VIEW;
        return new $ViewClass($atts);
    }
}
