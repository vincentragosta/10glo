<?php

use ChildTheme\Components\NewsPostCard\NewsPostCardView;
use ChildTheme\Components\StoryCard\StoryCardView;
use ChildTheme\News\NewsPost;
use ChildTheme\News\NewsPostRepository;
use ChildTheme\User\User;

$Story = NewsPost::createFromGlobal();
if (!$Story instanceof NewsPost) {
    return;
}
$StoryAuthor = User::createFromPost($Story);
?>

<div class="content-section content-section--has-bg content-section--tpad-double content-section--bpad-double">
    <?php if (($image = $Story->featuredImage()) instanceof WP_Image): ?>
        <span class="content-background-image">
            <span class="content-background-image__images">
                <span class="content-background-image__img content-background-image__img--desktop" style="background-image: url('/wp-content/uploads/2020/09/10glo-Stories-Hero-V3.png');"></span>
            </span>
        </span>
    <?php endif; ?>
    <div class="content-section__container container">
        <div class="content-row row">
            <div class="content-column stories-hero-row col-md-6 content-column--last">
                <div class="content-column__inner">
                    <div class="vc-single-image vc-single-image--center">
                        <figure class="vc-single-image__figure image-loaded">
                            <img class="lazy-image-loaded vc-single-image__image in-view" src="/wp-content/uploads/2020/09/logo-1080-edges.png" alt="logo-1080-edges" width="783" height="714">
                        </figure>
                    </div>
                    <h2 class="heading heading--xlarge text--contrast">News</h2>
                </div>
            </div>
        </div>
    </div>
</div>
<div class="content-section">
    <div class="content-section__container container">
        <div class="content-row row">
            <div class="content-column col-12 col-sm-8">
                <div class="content-column__inner">
                    <?php if (($featured_image = $Story->featuredImage()) instanceof WP_Image): ?>
                        <?= $featured_image->width(StoryCardView::IMAGE_WIDTH)->height(StoryCardView::IMAGE_HEIGHT); ?>
                    <?php endif; ?>
                    <div class="single-story__title-tagline">
                        <h1 class="heading heading--xlarge"><?= $Story->title(); ?></h1>
                        <p>By <a href="<?= $StoryAuthor->permalink(); ?>"><?= $StoryAuthor->getDisplayName(); ?></a></p>
                    </div>
                    <?php if (!empty($story_categories = $Story->terms(NewsPost::TAXONOMY))): ?>
                        <div class="single-story__categories">
                            <?php foreach($story_categories as $Category): ?>
                                <a href="<?= home_url('/news/category/' . $Category->slug); ?>" class="button button--secondary"><?= $Category->name; ?></a>
                            <?php endforeach; ?>
                        </div>
                    <?php endif; ?>
                    <?php if ($content = $Story->content(false)): ?>
                        <?= $content; ?>
                    <?php endif; ?>
                    <?php if (!empty($tags = $Story->terms('post_tag'))): ?>
                        <div class="single-story__tags">
                            <?php foreach($tags as $Tag): ?>
                                <a href="<?= home_url('/news/tag/' . $Tag->slug); ?>" class="button"><?= $Tag->name; ?></a>
                            <?php endforeach; ?>
                        </div>
                    <?php endif; ?>
                </div>
            </div>
            <div class="content-column col-12 col-sm-4">
                <div class="content-column__inner">
                    <?php if (!empty($recent_news = (new NewsPostRepository())->findRecent())): ?>
                        <?php foreach($recent_news as $NewsPost): ?>
                            <?= new NewsPostCardView($NewsPost); ?>
                        <?php endforeach; ?>
                    <?php endif; ?>
                </div>
            </div>
        </div>
    </div>
</div>
