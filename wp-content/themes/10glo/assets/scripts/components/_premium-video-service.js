addAction(INIT, function () {
    var $stripeProductForm = $('#stripe-product-form');
    if (!$stripeProductForm.length) {
        return;
    }

    var $productPrice = $stripeProductForm.find('#product-price');
    if ($productPrice.length) {
        $productPrice.change(function () {
            var $this = $(this);
            $this.val(Math.ceil($this.val()) - 0.01);
        });
    }

    $stripeProductForm.submit(function (e) {
        e.preventDefault();

        var $messaging = $('.messaging');
        var $messagingSection = $messaging.parents('.messaging__section');
        var productName = $('#product-name').val();
        var productPrice = $('#product-price').val();
        var videoId = $('#video-id').val();
        if (
            !productName.length ||
            !productPrice.length ||
            !videoId.length
        ) {
            return;
        }

        if (productPrice.replace(/\./g, "") >= 29999) {
            $("html, body").animate({ scrollTop: 0 }, "slow");
            $messagingSection.addClass('messaging__section--show');
            $messaging.find('.messaging__content').html('You cannot enter a price greater than $299.99. Please reach out to <a href="mailto:help@10glo.com">help@10glo.com</a> for more help.');
            return;
        }

        var formData = new FormData();
        formData.append('productName', productName);
        formData.append('productPrice', productPrice.replace(/\./g, ""));
        formData.append('videoId', videoId);
        formData.append('userId', sit.current_user_id);

        $.ajax({
            method: 'POST',
            url: sit.api_url + 'premium-video-service/add-product',
            data: formData,
            processData: false,
            contentType: false,
            beforeSend: function (xhr) {
                xhr.setRequestHeader('X-WP-Nonce', sit.nonce);
                xhr.setRequestHeader('Authorization', 'Basic ' + sit.basic_auth);
            },
            success: function (response) {
                setTimeout(function () {
                    location.href = sit.home_url + '/premium-video-service';
                }, 500);
            },
            error: function (response) {

            },
            fail: function (response) {

            }
        });
    });
});
