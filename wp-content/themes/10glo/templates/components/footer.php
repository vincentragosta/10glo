<?php
use Backstage\SetDesign\Icon\IconView;
use Backstage\SetDesign\Modal\ModalView;
use Backstage\SetDesign\SocialIcons\SocialIcon;
use ChildTheme\Options\GlobalOptions;
?>
<footer class="footer-nav" data-gtm="footer">
    <div class="footer-nav__container container">
        <div class="footer-nav__row row">
            <div class="footer-nav__column col-6 col-md-3">
                <?php if (has_nav_menu('footer_navigation_1')): ?>
                    <ul class="footer-nav__list">
                        <?php foreach(wp_get_nav_menu_items('Footer Navigation 1') as $NavItem): ?>
                            <?php $title = ($NavItem->post_title ? $NavItem->post_title : ($NavItem->title ? $NavItem->title : '')); ?>
                            <li class="footer-nav__list-item <?= $NavItem->menu_item_parent == 0 ? 'footer-nav__list-item--anchor' : ''; ?>">
                                <a class="footer-nav__list-item-link" href="<?= $NavItem->url; ?>"><?= $title; ?></a>
                            </li>
                        <?php endforeach; ?>
                    </ul>
                <?php endif; ?>
            </div>
            <div class="footer-nav__column col-6 col-md-3">
                <?php if (has_nav_menu('footer_navigation_2')): ?>
                    <ul class="footer-nav__list">
                        <?php foreach(wp_get_nav_menu_items('Footer Navigation 2') as $NavItem): ?>
                            <?php $title = ($NavItem->post_title ? $NavItem->post_title : ($NavItem->title ? $NavItem->title : '')); ?>
                            <li class="footer-nav__list-item <?= $NavItem->menu_item_parent == 0 ? 'footer-nav__list-item--anchor' : ''; ?>">
                                <a class="footer-nav__list-item-link" href="<?= $NavItem->url; ?>"><?= $title; ?></a>
                            </li>
                        <?php endforeach; ?>
                        <?php if (!empty($social_icons = GlobalOptions::socialIcons())): ?>
                            <li class="footer-nav__list-item footer-nav__list-item--anchor">
                                <a class="footer-nav__list-item-link" href="#">Social</a>
                            </li>
                            <li class="footer-nav__list-item">
                                <ul class="footer-nav__sublist">
                                    <?php foreach($social_icons as $SocialIcon): /* @var SocialIcon $SocialIcon */ ?>
                                        <li class="footer-nav__sublist-item">
                                            <a href="<?= $SocialIcon->getUrl(); ?>" class="footer-nav__sublist-item-link link-icon--<?= $SocialIcon->getName(); ?>" target="<?= $SocialIcon->getTarget(); ?>">
                                                <?= new IconView(['icon_name' => $SocialIcon->getName()]); ?>
                                            </a>
                                        </li>
                                    <?php endforeach; ?>
                                </ul>
                            </li>
                        <?php endif; ?>
                    </ul>
                <?php endif; ?>
            </div>
            <div class="footer-nav__column col-6 col-md-3">
                <?php if (has_nav_menu('footer_navigation_3')): ?>
                    <ul class="footer-nav__list">
                        <?php foreach(wp_get_nav_menu_items('Footer Navigation 3') as $NavItem): ?>
                            <?php $title = ($NavItem->post_title ? $NavItem->post_title : ($NavItem->title ? $NavItem->title : '')); ?>
                            <li class="footer-nav__list-item <?= $NavItem->menu_item_parent == 0 ? 'footer-nav__list-item--anchor' : ''; ?>">
                                <a class="footer-nav__list-item-link" href="<?= $NavItem->url; ?>"><?= $title; ?></a>
                            </li>
                        <?php endforeach; ?>
                    </ul>
                <?php endif; ?>
            </div>
            <div class="footer-nav__column col-6 col-md-3">
                <?php if (has_nav_menu('footer_navigation_4')): ?>
                    <ul class="footer-nav__list">
                        <?php foreach(wp_get_nav_menu_items('Footer Navigation 4') as $NavItem): ?>
                            <?php $title = ($NavItem->post_title ? $NavItem->post_title : ($NavItem->title ? $NavItem->title : '')); ?>
                            <li class="footer-nav__list-item <?= $NavItem->menu_item_parent == 0 ? 'footer-nav__list-item--anchor' : ''; ?>">
                                <a class="footer-nav__list-item-link" href="<?= $NavItem->url; ?>"><?= $title; ?></a>
                            </li>
                        <?php endforeach; ?>
                    </ul>
                <?php endif; ?>
            </div>
        </div>
    </div>
</footer>

<div class="copy-to-clipboard">
    Copied to Clipboard!
</div>

<?= ModalView::unloadAll(); ?>
