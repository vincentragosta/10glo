<?php

namespace ChildTheme\Components\JobCard;

use Backstage\VcLibrary\Support\Component;
use ChildTheme\Job\JobRepository;
use ChildTheme\Job\Job;

/**
 * Class JobCard
 * @package ChildTheme\Components\JobCard
 * @author Michael Miranda <m.miranda426@gmail.com>
 * @version 1.0
 */
class JobCard extends Component
{
    const NAME = 'Job Card';
    const TAG = 'job_card';
    const VIEW = JobCardView::class;

    protected $component_config = [
        'description' => 'Display a job post.',
        'icon' => 'icon-wpb-toggle-small-expand',
        'wrapper_class' => 'clearfix',
        'is_container' => false,
        'category' => 'Content',
        'params' => [
            [
                'type' => 'textfield',
                'heading' => 'Job ID',
                'param_name' => 'job_id',
                'description' => 'Enter the Job ID.',
                'admin_label' => true
            ]
        ]
    ];

    protected function createView(array $atts)
    {
        if (!($Job = (new JobRepository())->findById($atts['job_id'])) instanceof Job) {
            return '';
        }
        $ViewClass = self::VIEW;
        return new $ViewClass($Job);
    }
}
