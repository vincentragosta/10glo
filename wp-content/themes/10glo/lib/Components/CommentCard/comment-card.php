<?php
/**
 * Expected:
 * @var User $Author
 * @var string $date
 * @var string $content
 * @var array $class_modifiers
 * @var array $element_attributes
 */

use Backstage\Util;
use ChildTheme\Components\UserCard\UserCardView;
use ChildTheme\Options\GlobalOptions;
use ChildTheme\User\User;

if (empty($content) || empty($display_name = $Author->getDisplayName()) || empty($date)) {
    return '';
}
?>

<div <?= Util::componentAttributes('comment-card', $class_modifiers, $element_attributes); ?>>
    <?= new UserCardView($Author->getUserPhotoUrl(), $Author->permalink()); ?>
    <div class="comment-card__content-container">
        <div class="comment-card__heading-container">
            <h2 class="comment-card__author heading"><a href="<?= $Author->permalink(); ?>"><?= $display_name; ?></a></h2>
            <?php if ($Author->isVerified() && ($verified_image = GlobalOptions::verifiedArtistsImage())): ?>
                <?= $verified_image; ?>
            <?php endif; ?>
        </div>
        <p class="comment-card__date"><?= $date; ?></p>
        <p class="comment-card__content"><?= $content; ?></p>
    </div>
</div>
