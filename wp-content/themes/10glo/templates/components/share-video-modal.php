<?php
/**
 * Expected:
 * @var Video $Video
 */

use ChildTheme\Components\SocialSharerCard\SocialSharerCardView;
use ChildTheme\SocialSharer\EmbedSocialSharer;
use ChildTheme\Video\Video;

if (empty($social_sharers = $Video->getSocialSharers())) {
    return '';
}
?>

<div class="share-video-modal">
    <h2 class="share-video-modal__heading heading heading--medium">Share</h2>
    <ul class="share-video-modal__list list--inline">
        <?php foreach($social_sharers as $SocialSharer): ?>
            <li><?= new SocialSharerCardView($SocialSharer); ?></li>
        <?php endforeach; ?>
    </ul>
    <div class="share-video-modal__copy-container">
        <input type="text" class="share-video-modal__copy" value="<?= $Video->permalink(); ?>" />
        <button type="button" class="share-video-modal__copy-button button button--unstyled">Copy</button>
    </div>
    <input type="text" class="share-video-modal__embed-iframe" value="<?= sprintf(EmbedSocialSharer::SHARER, home_url('/embed/' . $Video->post_name)); ?>">
</div>
