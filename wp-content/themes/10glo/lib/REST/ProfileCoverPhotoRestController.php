<?php

namespace ChildTheme\REST;

use Backstage\Controller\RestController;
use ChildTheme\Exception\ProfileCoverPhotoException;
use ChildTheme\Service\LinodeS3Service;
use ChildTheme\Traits\GetCurrentUserFromRequest;
use ChildTheme\Traits\GetPhotoFileFromRequestTrait;
use \WP_REST_Request;

/**
 * Class ProfileCoverPhotoRestController
 * @package ChildTheme\Controller
 * @author Vincent Ragosta <vincentpasqualeragosta@gmail.com>
 * @version 1.0
 *
 * @property string $namespace
 * @property LinodeS3Service $LinodeS3Service
 */
class ProfileCoverPhotoRestController extends RestController
{
    const MB = 1048576;

    protected $namespace = 'user';
    protected $LinodeS3Service;

    use GetPhotoFileFromRequestTrait, GetCurrentUserFromRequest;

    public function __construct()
    {
        parent::__construct();
        $this->LinodeS3Service = new LinodeS3Service();
    }

    public function registerRoutes()
    {
        $this->addCreateRoute('/profile-cover-photo/create', [$this, 'createUserProfileCoverPhoto']);
        $this->addCreateRoute('/profile-cover-photo/reposition', [$this, 'repositionProfileCoverPhoto']);
        $this->addCreateRoute('/profile-cover-photo/remove', [$this, 'removeUserProfileCoverPhoto']);
    }

    public function createUserProfileCoverPhoto(WP_REST_Request $request)
    {
        $file = $this->getPhotoFileFromRequest($request);
        $CurrentUser = $this->getCurrentUserFromRequest($request, 'Please login or create an account to upload a profile cover photo.');

        $filename = LinodeS3Service::getPhotoArchitecture($CurrentUser->ID, $file['name']);

        if (!empty($existing_file = $CurrentUser->getProfileCoverPhotoUrl())) {
            $this->LinodeS3Service->deleteFile($existing_file);
            $CurrentUser->removeProfileCoverPhotoUrl();
        }

        $this->LinodeS3Service->putFile($filename, $file['tmp_name']);
        if (!$object_url = $this->LinodeS3Service->getObjectUrl($filename)) {
            throw new ProfileCoverPhotoException('We could not store your file at this time.', 400);
        }
        $CurrentUser->updateProfileCoverPhotoUrl($object_url);
        $CurrentUser->removeProfileCoverPhotoCoordinate();
        $CurrentUser->removeProfileCoverPhotoMobileCoordinate();
        return ['message' => 'The profile cover photo has been uploaded.', 'status' => 200];
    }

    public function repositionProfileCoverPhoto(WP_REST_Request $request)
    {
        $CurrentUser = $this->getCurrentUserFromRequest($request, 'Please login or create an account to reposition your profile cover photo');
        $coordinate = $this->getCoordinateFromRequest($request);
        if (empty($profile_cover_photo_url = $CurrentUser->getProfileCoverPhotoUrl())) {
            throw new ProfileCoverPhotoException('There profile cover photo is not set for this user.', 400);
        }
        if (!empty($isMobileDisplay = $request->get_param('isMobileDisplay'))) {
            if (!$CurrentUser->updateProfileCoverPhotoMobileCoordinate($coordinate)) {
                throw new ProfileCoverPhotoException('The profile cover photo could not be repositioned at this time.', 400);
            }
        } else {
            if (!$CurrentUser->updateProfileCoverPhotoCoordinate($coordinate)) {
                throw new ProfileCoverPhotoException('The profile cover photo could not be repositioned at this time.', 400);
            }
        }
        return ['message' => 'The profile cover photo has been repositioned!', 'status' => 200];
    }

    public function removeUserProfileCoverPhoto(WP_REST_Request $request)
    {
        $CurrentUser = $this->getCurrentUserFromRequest($request, 'Please login or create an account to remove your profile cover photo.');
        if (empty($profile_cover_photo_url = $CurrentUser->getProfileCoverPhotoUrl())) {
            throw new ProfileCoverPhotoException('There profile cover photo is not set for this user.', 400);
        }
        $this->LinodeS3Service->deleteFile($profile_cover_photo_url);
        $CurrentUser->removeProfileCoverPhotoUrl();
        $CurrentUser->removeProfileCoverPhotoCoordinate();
        $CurrentUser->removeProfileCoverPhotoMobileCoordinate();
        return ['message' => 'The profile cover photo has been removed', 'status' => 200];
    }

    protected function getCoordinateFromRequest(WP_REST_Request $request)
    {
        if (empty($coordinate = $request->get_param('coordinate'))) {
            throw new ProfileCoverPhotoException('The coordinate was not sent in the request', 400);
        }
        return $coordinate;
    }
}
