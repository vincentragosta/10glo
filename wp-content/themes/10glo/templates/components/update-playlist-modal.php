<?php
/**
 * Expected:
 * @var Playlist $Playlist
 * @var User $User
 */

use ChildTheme\Options\GlobalOptions;
use ChildTheme\Playlist\Playlist;
use ChildTheme\User\User;
use ChildTheme\Video\Video;
use ChildTheme\Video\VideoRepository;

?>

<div class="update-playlist-modal">
    <h2 class="update-playlist-modal__heading heading heading--large">Update Your Playlist</h2>
    <p class="update-playlist-modal__playlist-text"><?= GlobalOptions::createPlaylistText(); ?></p>
    <form class="update-playlist-modal__form update-playlist-form" id="update-playlist-form" name="update-user-form"
          method="post">
        <p class="update-playlist-form__playlist-name">
            <label class="update-playlist-form__label" for="playlist-name">Playlist Name</label>
            <input type="text" name="playlist-name" class="update-playlist-form__playlist-name-input" size="20"
                   value="<?= $Playlist->title(); ?>" required>
        </p>
        <?php if (!empty($playlist_videos = (new VideoRepository())->findFromPlaylist($Playlist))): ?>
            <div class="update-playlist-form__videos-container">
                <h2 class="heading heading--default">Remove videos:</h2>
                <div class="update-playlist-form__videos">
                    <?php foreach ($playlist_videos as $PlaylistVideo): /** @var Video $PlaylistVideo */ ?>
                        <label class="update-playlist-form__label">
                            <input class="update-playlist-form__playlist-video-id" name="playlist-video-ids[]"
                                   type="checkbox" value="<?= $PlaylistVideo->ID; ?>"><?= $PlaylistVideo->title(); ?>
                        </label>
                    <?php endforeach; ?>
                </div>
            </div>
        <?php endif; ?>
        <?php if (!empty($videos = (new VideoRepository())->findAllByAuthor($User))): ?>
            <div class="update-playlist-form__videos-container">
                <h2 class="heading heading--default">Add videos:</h2>
                <div class="update-playlist-form__videos">
                    <?php foreach ($videos as $Video): /** @var Video $Video */ ?>
                        <?php if (in_array($Video->ID, (array) $Playlist->video_ids)): ?>
                            <?php continue; ?>
                        <?php endif; ?>
                        <label class="update-playlist-form__label">
                            <input class="update-playlist-form__video-id" name="video-ids[]" type="checkbox"
                                   value="<?= $Video->ID; ?>"><?= $Video->title(); ?>
                        </label>
                    <?php endforeach; ?>
                </div>
            </div>
        <?php endif; ?>
        <div class="update-playlist-form__submit">
            <input type="hidden" class="update-playlist-form__playlist-id" value="<?= $Playlist->ID; ?>" />
            <input type="submit" name="wp-submit" id="wp-submit"
                   class="update-playlist-form__submit-button button button-primary" value="Update Playlist"/>
        </div>
    </form>
</div>
