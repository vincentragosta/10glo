<?php

use Backstage\Util;
use ChildTheme\Components\Messaging\MessagingView;
use ChildTheme\Components\VideoPosterCard\VideoPosterCardView;
use ChildTheme\Options\GlobalOptions;
use ChildTheme\Podcast\Podcast;
use ChildTheme\User\User;
use ChildTheme\Video\VideoRepository;

$Podcast = Podcast::createFromGlobal();
$Author = User::createFromPost($Podcast);

if (!$Podcast instanceof Podcast || !$Author instanceof User) {
    return;
}
?>

<div class="content-section">
    <div class="content-section__container container">
        <div class="content-row row">
            <div class="content-column col-12">
                <div class="content-column__inner">
                    <?php if ($published_date = $Podcast->libsyn_published_date): ?>
                        <p><?= (new DateTime($published_date))->format('F j, Y'); ?></p>
                    <?php endif; ?>
                    <h1 class="heading heading--large"><?= $Podcast->title(); ?></h1>
                </div>
            </div>
        </div>
        <div class="content-row row">
            <div class="content-column col-12">
                <div class="content-column__inner">
                    <?php if (!empty($url = $Podcast->podcast_url)): ?>
                        <audio controls style="width: 100%;">
                            <source src="<?= $url; ?>" type="<?= $Podcast->podcast_type; ?>">
                            Your browser does not support the audio tag.
                        </audio>
                    <?php endif; ?>
                </div>
            </div>
        </div>
        <div class="content-row row">
            <div class="content-column col-12">
                <div class="podcast-links">
                    <?php if ($apple_podcasts_url = GlobalOptions::applePodcastsUrl()): ?>
                        <a href="<?= $apple_podcasts_url; ?>" target="_blank">
                            <img src="<?= Util::getAssetPath('images/apple-podcasts.png'); ?>" />
                        </a>
                    <?php endif; ?>
                    <?php if ($overcast_url = GlobalOptions::overcastUrl()): ?>
                        <a href="<?= $overcast_url; ?>" target="_blank">
                            <img src="<?= Util::getAssetPath('images/overcast.png'); ?>" />
                        </a>
                    <?php endif; ?>
                    <?php if ($google_podcasts_url = GlobalOptions::googlePodcastsUrl()): ?>
                        <a href="<?= $google_podcasts_url; ?>" target="_blank">
                            <img src="<?= Util::getAssetPath('images/google-podcasts.png'); ?>" />
                        </a>
                    <?php endif; ?>
                    <?php if ($libsyn_rss_url = GlobalOptions::libsynRssUrl()): ?>
                        <a href="<?= $libsyn_rss_url; ?>" target="_blank">
                            <img src="<?= Util::getAssetPath('images/rss.png'); ?>" />
                        </a>
                    <?php endif; ?>
                </div>
            </div>
        </div>
        <div class="content-row row">
            <div class="content-column col-12">
                <div class="content-column__inner">
                    <?php if (!empty($content = $Podcast->content(false))): ?>
                        <?= $content; ?>
                    <?php endif; ?>
                </div>
            </div>
        </div>
    </div>
</div>
