<?php

namespace ChildTheme\Service;

use Postmark\Models\PostmarkException;
use Postmark\PostmarkClient;

/**
 * Class EmailNotificationService
 * @package ChildTheme\Service
 * @author Vincent Ragosta <vincentpasqualeragosta@gmail.com>
 * @version 1.0
 *
 * @Property PostmarkClient $Client
 * @property string $recipient
 * @property string $subject
 * @property string $body
 * @property array $headers
 * @property array $attachments
 */
class EmailNotificationService
{
    const FROM_ADDRESS = 'hello@10glo.com';

    protected $Client;
    protected $recipient;
    protected $subject;
    protected $body;
    protected $headers;
    protected $attachments;

    public function __construct(string $recipient, string $subject, string $body, array $headers = [], array $attachments = [])
    {
        if (!defined('POSTMARK_API_KEY')) {
            throw new PostmarkException('The postmark API key is not set.', 400);
        }
        $this->Client = new PostmarkClient(POSTMARK_API_KEY);
        $this->recipient = $recipient;
        $this->subject = $subject;
        $this->body = $body;
        $this->headers = $headers;
        $this->attachments = $attachments;
    }

    public function send()
    {
        $this->Client->sendEmail(
            static::FROM_ADDRESS,
            $this->recipient,
            $this->subject,
            $this->body
        );
    }
}
