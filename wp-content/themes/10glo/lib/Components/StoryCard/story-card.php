<?php
/**
 * Expected:
 * @var WP_Image|bool $image
 * @var string $title
 * @var string $content
 * @var string $url
 * @var array $class_modifiers
 * @var array $element_attributes
 */

use Backstage\Util;

if (empty($title) || empty($url)) {
    return;
}
?>

<div <?= Util::componentAttributes('story-card', $class_modifiers, $element_attributes); ?>>
    <div class="story-card__image-container">
        <?php if ($image instanceof WP_Image): ?>
            <a href="<?= $url; ?>" class="story-card__image-link">
                <?= $image->css_class('story-card__image'); ?>
            </a>
        <?php endif; ?>
    </div>
    <div class="story-card__content-container">
        <a href="<?= $url; ?>" class="story-card__heading-link">
            <h2 class="story-card__heading heading heading--medium text--blue"><?= $title; ?></h2>
        </a>
        <?php if (!empty($content)): ?>
            <?= $content; ?>
        <?php endif; ?>
        <a href="<?= $url; ?>" class="story-card__button button button--secondary">Read More</a>
    </div>
</div>
