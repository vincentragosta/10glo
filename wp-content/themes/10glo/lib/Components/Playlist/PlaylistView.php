<?php

namespace ChildTheme\Components\Playlist;

use Backstage\View\Component;
use Backstage\View\Element;
use ChildTheme\Video\Video;
use ChildTheme\Video\VideoRepository;
use ChildTheme\Playlist\Playlist;

/**
 * Class PlaylistView
 * @package ChildTheme\Components\Playlist
 * @author Vincent Ragosta <vincentpasqualeragosta@gmail.com>
 * @version 1.0
 *
 * @property Video[] $videos
 * @property Element $Heading
 * @property string $playlist_name
 * @property bool $show_sort
 * @property bool $show_load_more
 */
class PlaylistView extends Component
{
    protected $name = 'playlist-row';
    protected static $default_properties = [
        'videos' => [],
        'Heading' => null,
        'playlist_name' => '',
        'show_sort' => false,
        'show_load_more' => false
    ];

    public function __construct(array $videos, Element $Heading = null)
    {
        parent::__construct(compact('videos', 'Heading'));
    }

    public function playlistName(string $playlist_name = '')
    {
        $this->playlist_name = $playlist_name;
        return $this;
    }

    public function toggleLoadMore($visible = false)
    {
        $this->show_load_more = $visible;
        return $this;
    }

    public function showSort($sort = false)
    {
        $this->show_sort = $sort;
        return $this;
    }
}
