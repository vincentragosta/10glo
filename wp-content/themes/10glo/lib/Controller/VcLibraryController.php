<?php

namespace ChildTheme\Controller;

/**
 * Class VcLibraryController
 * @package ChildTheme\Controller
 * @author Vincent Ragosta <vragosta@situationinteractive.com>
 * @version 1.0
 */
class VcLibraryController
{
    protected static $content_width_options = [
        'Default' => '',
        'Extended' => 'extended',
        'Full' => 'width-full',
        'Narrow' => 'width-narrow'
    ];

    protected static $additional_section_options = [
        'Bordered Content' => 'bordered-content'
    ];

    protected static $additional_heading_options = [
        'Styled' => 'styled',
        'Small' => 'small'
    ];

    protected static $additional_background_colors = [
        'White' => 'white',
        'Gray Light Tint' => 'gray-light-tint'
    ];

    protected static $component_blacklist = [
        'content_card',
        'content_block',
        'callout',
        'ticket_calendar',
        'talent_card',
        'media_carousel',
        'image_carousel',
        'sit_video'
    ];

    public function __construct()
    {
        add_filter('backstage/vc-library/background-colors/vc_section', [$this, 'addBackgroundColors']);
        add_filter('backstage/vc-library/blacklist', [$this, 'blacklist']);
        add_filter('backstage/vc-library/section/additional-options', [$this, 'additionalSectionOptions']);
        add_filter('backstage/vc-library/heading/additional-options', [$this, 'additionalHeadingOptions']);
        add_filter('backstage/vc-library/section/content-width', [$this, 'contentWidthOptions']);
    }

    public function addBackgroundColors($colors) {
        return array_merge($colors, static::$additional_background_colors);
    }

    public function blacklist($components)
    {
        return array_merge($components, static::$component_blacklist);
    }

    public function additionalSectionOptions(array $config)
    {
        return static::$additional_section_options;
    }

    public function additionalHeadingOptions(array $config)
    {
        return static::$additional_heading_options;
    }

    public function contentWidthOptions(array $config)
    {
        return static::$content_width_options;
    }
}
