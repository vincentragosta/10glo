<?php

namespace ChildTheme\Exception;

use Exception;

/**
 * Class UserPhotoException
 * @package ChildTheme\Exception
 * @author Vincent Ragosta <vincentpasqualeragosta@gmail.com>
 * @version 1.0
 */
class UserPhotoException extends Exception {}
