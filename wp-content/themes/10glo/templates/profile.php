<?php

use Backstage\SetDesign\Icon\IconView;
use Backstage\SetDesign\Modal\ModalView;
use Backstage\Util;
use Backstage\View\Element;
use ChildTheme\Components\Messaging\MessagingView;
use ChildTheme\Components\Playlist\PlaylistView;
use ChildTheme\Components\UserCard\UserCardView;
use ChildTheme\Components\VideoPosterCard\VideoPosterCardView;
use ChildTheme\Options\GlobalOptions;
use ChildTheme\Playlist\PlaylistRepository;
use ChildTheme\Service\UserMonetizationService;
use ChildTheme\Service\UserSubscribeService;
use ChildTheme\User\User;
use ChildTheme\Video\VideoRepository;

$CurrentUser = User::createFromCurrentUser();
ModalView::load('profile-cover-photo-remove-modal', 'box', Util::getTemplateScoped('templates/components/profile-cover-photo-remove-modal.php'));
ModalView::load('user-photo-remove-modal', 'box', Util::getTemplateScoped('templates/components/user-photo-remove-modal.php', ['User' => $CurrentUser]));
ModalView::load('create-playlist-modal', 'box', Util::getTemplateScoped('templates/components/create-playlist-modal.php', ['User' => $CurrentUser]));
//ModalView::load('upload-video-modal', 'box', Util::getTemplateScoped('templates/components/upload-video-modal.php', ['Author' => $CurrentUser]));

// TODO: Add Service class
$personal_website_url = $CurrentUser->getPersonalWebsiteUrl();
$facebook_url = $CurrentUser->getFacebookUrl();
$instagram_url = $CurrentUser->getInstagramUrl();
$twitter_url = $CurrentUser->getTwitterUrl();
?>

<section class="content-section content-section--hero">
    <div class="content-section__container container">
        <div class="content-row row">
            <div class="content-column col-12">
                <div class="content-column__inner">
                    <div class="profile-cover-photo image-upload">
                        <div class="profile-cover-photo__reposition-update-container">
                            <p class="text--contrast">Your cover photo is public.</p>
                            <button class="profile-cover-photo__reposition-update-button button button--primary-filled">Update</button>
                        </div>
                        <div class="profile-cover-photo__cover-photo-container <?= empty($profile_cover_photo_url = $CurrentUser->getProfileCoverPhotoUrl()) ? 'profile-cover-photo__image--hide' : ''; ?>">
                            <div class="profile-cover-photo__cover-photo-inner">
                                <img
                                    src="<?= $CurrentUser->getProfileCoverPhotoUrl(); ?>"
                                    class="profile-cover-photo__image"
                                    <?= ($coordinate = $CurrentUser->getProfileCoverPhotoCoordinate()) ? 'style="transform: translate(0, ' . $coordinate . '%);"' : ''; ?>
                                    <?= $coordinate ? 'data-desktop-coordinate="' . $coordinate . '"' : ''; ?>
                                    <?= ($mobile_coordinate = $CurrentUser->getProfileCoverPhotoMobileCoordinate()) ? 'data-mobile-coordinate="' . $mobile_coordinate . '"' : ''; ?>
                                />
                                <div class="profile-cover-photo__reposition-icon">
                                    <?= new IconView(['icon_name' => 'reposition', 'style' => 'primary']); ?> Drag to Reposition
                                </div>
                            </div>
                        </div>
                        <div class="profile-cover-photo__placeholder <?= empty($profile_cover_photo_url) ? 'profile-cover-photo__placeholder--show' : ''; ?>">
                            <div class="profile-cover-photo__placeholder-inner">
                                <h2 class="profile-cover-photo__placeholder-title">Insert Cover Photo</h2>
                            </div>
                        </div>
                        <div class="image-upload__edit-container">
                            <form class="image-upload__form" name="profile-cover-photo-form"
                                  id="profile-cover-photo-form" action="" method="post" enctype="multipart/form-data">
                                <input type="file" class="image-upload__file" name="profile-cover-photo"
                                       id="profile-cover-photo"/>
                                <input type="submit"/>
                            </form>
                            <button
                                class="image-upload__edit-button button button--with-icon"><?= new IconView(['icon_name' => 'camera']); ?>
                                Edit Cover Photo
                            </button>
                            <ul class="image-upload__edit-list dropdown-list"
                                data-dropdown-activator=".profile-cover-photo .image-upload__edit-button">
                                <li>
                                    <ul class="image-upload__sublist list--unstyled">
                                        <li>
                                            <strong>Recommended</strong>
                                            <ul class="list--inline list--dotted">
                                                <li>2000x450</li>
                                                <li>JPG</li>
                                            </ul>
                                        </li>
                                        <li><strong>Max File Size: </strong>2MB</li>
                                    </ul>
                                </li>
                                <li>
                                    <button class="image-upload__upload-button button--unstyled button--with-icon"
                                            data-trigger-target="input#profile-cover-photo"><?= new IconView(['icon_name' => 'upload']); ?>
                                        Upload Photo
                                    </button>
                                </li>
                                <?php if (!empty($CurrentUser->getProfileCoverPhotoUrl())): ?>
                                    <li><button class="image-upload__reposition-button button--unstyled button--with-icon"><?= new IconView(['icon_name' => 'reposition']); ?>Reposition</button></li>
                                    <li><a href="#profile-cover-photo-remove-modal"
                                           class="profile-cover-photo__remove-button image-upload__remove-button button--unstyled button--with-icon js-modal-trigger"><?= new IconView(['icon_name' => 'trash-can']); ?>
                                            Remove</a></li>
                                <?php endif; ?>
                            </ul>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
<section class="content-section content-section--mb-half content-section--user-profile">
    <div class="content-section__container container">
        <div class="content-row row">
            <div class="content-column col-12">
                <div class="content-column__inner">
                    <div class="profile-nameplate">
                        <div class="profile-nameplate__user-card image-upload">
                            <?= new UserCardView($CurrentUser->getUserPhotoUrl()); ?>
                            <div class="image-upload__edit-container">
                                <form class="image-upload__form" name="user-photo-form" id="user-photo-form" action=""
                                      method="post" enctype="multipart/form-data">
                                    <input type="file" class="image-upload__file" name="user-photo" id="user-photo"/>
                                    <input type="submit"/>
                                </form>
                                <button
                                    class="image-upload__edit-button button button--with-icon"><?= new IconView(['icon_name' => 'camera']); ?></button>
                                <ul class="image-upload__edit-list dropdown-list"
                                    data-dropdown-activator=".profile-nameplate__user-card .image-upload__edit-button">
                                    <li>
                                        <ul class="image-upload__sublist list--unstyled">
                                            <li><strong>Recommended:</strong></li>
                                            <li>
                                                <ul class="list--inline list--dotted">
                                                    <li>400 x 400</li>
                                                    <li>JPG/PNG</li>
                                                </ul>
                                            </li>
                                            <li><strong>Max File Size: </strong>2MB</li>
                                        </ul>
                                    </li>
                                    <li>
                                        <button class="image-upload__upload-button button--unstyled button--with-icon"
                                                data-trigger-target="input#user-photo.image-upload__file"><?= new IconView(['icon_name' => 'upload']); ?>
                                            Upload Photo
                                        </button>
                                    </li>
                                    <?php if (!empty($CurrentUser->getUserPhotoUrl())): ?>
                                        <li><a href="#user-photo-remove-modal"
                                               class="image-upload__remove-button button--unstyled button--with-icon js-modal-trigger"><?= new IconView(['icon_name' => 'trash-can']); ?>
                                                Remove</a></li>
                                    <?php endif; ?>
                                </ul>
                            </div>
                        </div>
                        <div class="profile-nameplate__content">
                            <div class="profile-nameplate__content-inner">
                                <h1 class="heading heading--medium"><?= $CurrentUser->getDisplayName(); ?></h1>
                                <?php if ($CurrentUser->isVerified() && ($verified_artists_image = GlobalOptions::verifiedArtistsImage())): ?>
                                    <div class="profile-nameplate__verified">
                                        <?= $verified_artists_image; ?>
                                    </div>
                                <?php endif; ?>
                                <a href="<?= home_url('edit-profile'); ?>"><?= new IconView(['icon_name' => 'cog']); ?></a>
                                <?php if (($UserMonetizationService = new UserMonetizationService())->isEnabled($CurrentUser)): ?>
                                    <a href="<?= $UserMonetizationService->getVendorMonetizeLink($CurrentUser); ?>" class="monetize-my-videos">💳</a>
                                <?php endif; ?>
                            </div>
                            <p><?= (new UserSubscribeService())->getFollowersCount($CurrentUser); ?> Subscribers</p>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
<section class="content-section content-section--mb-half content-section--width-narrow messaging__section">
    <div class="content-section__container container">
        <div class="content-row row">
            <div class="content-column col-12">
                <div class="content-column__inner">
                    <?= new MessagingView(); ?>
                </div>
            </div>
        </div>
    </div>
</section>
<section class="content-section content-section--mb-none">
    <div class="container content-section__container">
        <div class="content-row row">
            <div class="content-column col-md-12 content-column--last">
                <div class="content-column__inner">
                    <div class="profile-tabs__container col-reset-exclude">
                        <div class="profile-tabs sub-nav sub-nav--horizontal sub-nav--text">
                            <ul class="sub-nav__list text--left" role="tablist">
                                <li class="sub-nav__item" role="presentation">
                                    <a class="sub-nav__link" href="#videos">Videos</a></li>
                                <li class="sub-nav__item" role="presentation">
                                    <a class="sub-nav__link" href="#playlists">Playlists</a></li>
                                <?php if ($CurrentUser->hasAboutAttributes()): ?>
                                    <li class="sub-nav__item" role="presentation">
                                        <a class="sub-nav__link" href="#about">About</a></li>
                                <?php endif; ?>
                            </ul>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
<?php if (($UserMonetizationService = new UserMonetizationService())->isEnabled($CurrentUser) && !$UserMonetizationService->isVerified($CurrentUser)): ?>
    <section class="content-section content-section--has-bg content-section--gray-light-tint content-section--mb-none content-section--bpad-none">
        <div class="content-section__container container">
            <div class="content-row row">
                <div class="content-column col-12">
                    <div class="banner">
                        <h2 class="heading heading--default">Congratulations! You have been selected to join in on our Monetization Beta!</h2>
                        <p>We have outlined some of the features available to you on our <strong>Monetize Your Videos</strong> page. To get started you will need to agree to the terms of use and connect as a Vendor in our Stripe platform by clicking <a href="<?= $UserMonetizationService->getVendorMonetizeLink($CurrentUser); ?>">here</a>.</p>
                    </div>
                </div>
            </div>
        </div>
    </section>
<?php endif; ?>
<section
    class="profile-tabs__content content-section content-section--has-bg content-section--gray-light-tint content-section--mb-none"
    role="tabpanel" aria-labelledby="home-sub-nav">
    <div class="container content-section__container">
        <div class="content-row row" data-toggle-target="videos" data-toggle-default="">
            <div class="content-column col-md-12 content-column--last">
                <div class="content-column__inner">
                    <div class="profile-tabs__tab-content profile-tabs__tab-content--videos">
                        <div class="profile-tabs__tab-content-heading">
                            <a href="<?= home_url('/video/upload'); ?>"
                               class="button button--with-icon"><?= new IconView(['icon_name' => 'upload']); ?>Upload
                                Video</a>
                        </div>
                        <h2 class="heading heading--medium">Your Videos</h2>
                        <?php if (!empty($videos = (new VideoRepository())->findAllByAuthor($CurrentUser))): ?>
                            <div class="row">
                                <?php foreach ($videos as $Video): ?>
                                    <div class="col-12 col-sm-6 col-md-4 col-lg-3">
                                        <?= new VideoPosterCardView($Video); ?>
                                    </div>
                                <?php endforeach; ?>
                            </div>
                        <?php else: ?>
                            <p>You don't currently have any videos.</p>
                        <?php endif; ?>
                    </div>
                </div>
            </div>
        </div>
        <div class="content-row row" data-toggle-target="playlists">
            <div class="content-column col-md-12 content-column--last">
                <div class="content-column__inner">
                    <div class="profile-tabs__tab-content profile-tabs__tab-content--playlists">
                        <a href="#create-playlist-modal"
                           class="profile-tabs__create-playlist-button button button--with-icon js-modal-trigger"><?= new IconView(['icon_name' => 'plus']); ?>
                            Create Playlist</a>
                        <div class="playlist__container">
                            <?php if (!empty($playlists = (new PlaylistRepository())->findAllFromUser($CurrentUser))): ?>
                                <?php foreach ($playlists as $Playlist): ?>
                                    <?php ModalView::load('update-playlist-modal', 'box', Util::getTemplateScoped('templates/components/update-playlist-modal.php', ['Playlist' => $Playlist, 'User' => $CurrentUser])); ?>
                                    <?php ModalView::load('remove-playlist-modal', 'box', Util::getTemplateScoped('templates/components/remove-playlist-modal.php', ['Playlist' => $Playlist, 'User' => $CurrentUser])); ?>
                                    <div class="playlist playlist--<?= $Playlist->post_name; ?>">
                                        <?php
                                        $args = [
                                            'data-page' => 1,
                                            'data-posts-per-page' => get_option('posts_per_page'),
                                            'data-playlist-id' => $Playlist->ID
                                        ];
                                        if (isset($_GET['sort'])) {
                                            $args['data-criteria'] = ($_GET['sort'] == 'most-viewed') ? 'views' : 'likes_count';
                                        }
                                        $videos = (new VideoRepository())->findFromPlaylist($Playlist, isset($_GET['sort']) ? ['criteria' => $_GET['sort'] == 'most-viewed' ? 'views' : 'likes_count'] : []);
                                        $View = new PlaylistView($videos, Element::create('h2', $Playlist->post()->post_title)->class(Util::componentClasses('heading', ['medium'])));
                                        echo $View
                                            ->toggleLoadMore(count($videos) == get_option('posts_per_page'))
                                            ->showSort(true)
                                            ->playlistName($Playlist->post_name)
                                            ->elementAttributes($args);
                                        ?>
                                    </div>
                                <?php endforeach; ?>
                            <?php else: ?>
                                <p>You don't currently have any playlists.</p>
                            <?php endif; ?>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <?php if ($CurrentUser->hasAboutAttributes()): ?>
            <div class="content-row row" data-toggle-target="about">
                <div class="content-column col-md-12 content-column--last">
                    <div class="content-column__inner">
                        <div class="profile-about-tab">
                            <div class="row">
                                <div class="col-12 col-sm-6">
                                    <?php if (!empty($bio = $CurrentUser->getBio())): ?>
                                        <div class="profile-about-tab__bio-container">
                                            <h2 class="profile-about-tab__heading heading heading--medium">Bio</h2>
                                            <div class="profile-about-tab__bio external-links">
                                                <?= $bio; ?>
                                            </div>
                                        </div>
                                    <?php endif; ?>
                                    <?php if (!empty($location = $CurrentUser->getLocation())): ?>
                                        <div class="profile-about-tab__location-container">
                                            <h2 class="profile-about-tab__heading heading--medium">Location</h2>
                                            <p class="profile-about-tab__location"><?= $location; ?></p>
                                        </div>
                                    <?php endif; ?>
                                    <?php if (!empty($school = $CurrentUser->getSchool())): ?>
                                        <div class="profile-about-tab__school-container">
                                            <h2 class="profile-about-tab__heading heading heading--medium">School with
                                                Major/Minor</h2>
                                            <p class="profile-about-tab__school"><?= $school; ?></p>
                                        </div>
                                    <?php endif; ?>
                                </div>
                                <?php if (!empty($personal_website_url) && !empty($facebook_url) && !empty($instagram_url) && !empty($twitter_url)): ?>
                                    <div class="col-12 col-sm-6">
                                        <h2 class="profile-about-tab__heading heading heading--medium">Social</h2>
                                        <ul class="profile-about-tab__links list--unstyled">
                                            <?php if (!empty($personal_website_url)): ?>
                                                <li>
                                                    <strong class="heading">Personal Website URL</strong>
                                                    <p class="profile-about-tab__personal-website"><a
                                                            href="<?= $personal_website_url; ?>"
                                                            target="_blank"><?= preg_replace('#^https?://#', '', $personal_website_url); ?></a>
                                                    </p>
                                                </li>
                                            <?php endif; ?>
                                            <?php if (!empty($facebook_url)): ?>
                                                <li>
                                                    <strong class="heading">Facebook URL</strong>
                                                    <p class="profile-about-tab__facebook"><a
                                                            href="<?= $facebook_url; ?>"
                                                            target="_blank"><?= preg_replace('#^https?://#', '', $facebook_url); ?></a>
                                                    </p>
                                                </li>
                                            <?php endif; ?>
                                            <?php if (!empty($instagram_url)): ?>
                                                <li>
                                                    <strong class="heading">Instagram URL</strong>
                                                    <p class="profile-about-tab__instagram"><a
                                                            href="<?= $instagram_url; ?>"
                                                            target="_blank"><?= preg_replace('#^https?://#', '', $instagram_url); ?></a>
                                                    </p>
                                                </li>
                                            <?php endif; ?>
                                            <?php if (!empty($twitter_url)): ?>
                                                <li>
                                                    <strong class="heading">Twitter URL</strong>
                                                    <p class="profile-about-tab__twitter"><a href="<?= $twitter_url; ?>"
                                                                                             target="_blank"><?= preg_replace('#^https?://#', '', $twitter_url); ?></a>
                                                    </p>
                                                </li>
                                            <?php endif; ?>
                                        </ul>
                                        <?php if ($bpve_url = $CurrentUser->getBroadwayPlusVirtualExperience()): ?>
                                            <strong class="heading">Sign up for my BroadwayPlus Virtual Experience</strong>
                                            <p><a href="<?= $bpve_url; ?>" target="_blank"><?= preg_replace('#^https?://#', '', $bpve_url); ?></a></p>
                                        <?php endif; ?>
                                        <?php if ($purchase_url = $CurrentUser->getPurchaseMySheetMusical()): ?>
                                            <strong class="heading">Purchase sheet music</strong>
                                            <p><a href="<?= $purchase_url; ?>" target="_blank"><?= preg_replace('#^https?://#', '', $purchase_url); ?></a></p>
                                        <?php endif; ?>
                                    </div>
                                <?php endif; ?>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        <?php endif; ?>
    </div>
</section>

