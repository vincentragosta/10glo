<?php

namespace ChildTheme\Components\NewsPostCard;

use Backstage\VcLibrary\Support\Component;
use ChildTheme\News\NewsPostRepository;
use ChildTheme\News\NewsPost;

/**
 * Class NewsPostCard
 * @package ChildTheme\Components\NewsPostCard
 * @author Vincent Ragosta <vincentpasqualeragosta@gmail.com>
 * @version 1.0
 */
class NewsPostCard extends Component
{
    const NAME = 'News Post Card';
    const TAG = 'news_post_card';
    const VIEW = NewsPostCardView::class;

    protected $component_config = [
        'description' => 'Display a news post.',
        'icon' => 'icon-wpb-toggle-small-expand',
        'wrapper_class' => 'clearfix',
        'is_container' => false,
        'category' => 'Content',
        'params' => [
            [
                'type' => 'textfield',
                'heading' => 'NewsPost ID',
                'param_name' => 'news_post_id',
                'description' => 'Enter the NewsPost ID.',
                'admin_label' => true
            ],
            [
                'type' => 'checkbox',
                'heading' => 'Options',
                'param_name' => 'options',
                'value' => [
                    'Display Image (Top)' => 'display-image-top',
                    'Display Image (Left)' => 'display-image-left',
                    'Display Image (Right)' => 'display-image-right',
                    'Increase Heading Size' => 'increase-heading-size',
                    'Decrease Heading Size' => 'decrease-heading-size',
                    'Hide Content' => 'hide-content'
                ],
                'description' => 'Check the relevant options'
            ]
        ]
    ];

    protected function createView(array $atts)
    {
        if (!($NewsPost = (new NewsPostRepository())->findById($atts['news_post_id'])) instanceof NewsPost) {
            return '';
        }
        $ViewClass = self::VIEW;
        return (new $ViewClass($NewsPost))->classModifiers(explode(',', $atts['options']));
    }
}
