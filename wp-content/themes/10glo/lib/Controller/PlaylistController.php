<?php

namespace ChildTheme\Controller;

use Backstage\Vendor\AdvancedCustomFields\BidirectionalRelationship;
use ChildTheme\Playlist\Playlist;
use ChildTheme\Video\Video;
use ChildTheme\Video\VideoRepository;

/**
 * Class PlaylistController
 * @package ChildTheme\Controller
 * @author Vincent Ragosta <vincentpasqualeragosta@gmail.com>
 * @version 1.0
 */
class PlaylistController
{
    const VIDEOS_ACF_KEY = 'field_5f0934bf2de32';

    public function __construct()
    {
        new BidirectionalRelationship('video_ids');
        add_action('after_save_post_playlist', [$this, 'savePlaylistToVideo']);
        add_action('acf/save_post', [$this, 'my_acf_save_post'], 5);
    }

    public function my_acf_save_post($post_id)
    {
        if (isset($_POST['acf'][static::VIDEOS_ACF_KEY])) {
            $playlist_videos = array_column((new VideoRepository())->findFromPlaylist(new Playlist($post_id)), 'ID');
            $video_ids = $_POST['acf'][static::VIDEOS_ACF_KEY];
            if (!empty($diff = array_diff($playlist_videos, $video_ids))) {
                $VideoRepository = new VideoRepository();
                foreach($diff as $id) {
                    $Video = new Video($id);
                    if (in_array($id, $playlist_videos) && !in_array($id, $video_ids)) {
                        $playlist_ids = $Video->playlist_ids ?: [];
                        if (($position = array_search($post_id, (array) $playlist_ids)) !== false) {
                            unset($playlist_ids[$position]);
                            $Video->playlist_ids = $playlist_ids;
                            $VideoRepository->add($Video);
                        }
                    }
                }
            }
        }
    }

    public function savePlaylistToVideo($object_id)
    {
        if (get_post_type($object_id) == Playlist::POST_TYPE) {
            $Playlist = new Playlist($object_id);
            if (!empty($video_ids = $Playlist->video_ids)) {
                foreach ($video_ids as $video_id) {
                    $Video = new Video($video_id);
                    $playlist_ids = $Video->playlist_ids;
                    if (!in_array($Playlist->ID, (array)$playlist_ids)) {
                        $playlist_ids[] = $Playlist->ID;
                        $Video->playlist_ids = $playlist_ids;
                        (new VideoRepository())->add($Video);
                    }
                }
            }
        }
    }
}
