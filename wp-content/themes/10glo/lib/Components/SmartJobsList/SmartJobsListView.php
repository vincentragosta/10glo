<?php

namespace ChildTheme\Components\SmartJobsList;

use Backstage\View\Component;
use ChildTheme\Job\Job;

/**
 * Class SmartJobsListView
 * @package ChildTheme\Components\SmartJobsList
 * @author Michael Miranda <m.miranda426@gmail.com
 * @version 1.0
 *
 * @property Job[] $jobs
 */
class SmartJobsListView extends Component
{
    protected $name = 'smart-jobs-list';
    protected static $default_properties = [
        'jobs' => [],
    ];

    public function __construct(array $jobs)
    {
        parent::__construct(compact('jobs'));
    }
}
