<?php

namespace ChildTheme\Controller;

/**
 * Class SetDesignController
 * @package ChildTheme\Controller
 * @author Vincent Ragosta <vincentpasqualeragosta@gmail.com>
 * @version 1.0
 */
class SetDesignController
{
    const READ_MORE_DELIMITER = '[break]';

    public function __construct()
    {
        add_filter('set-design/read-more/delimiter', function() {
            return static::READ_MORE_DELIMITER;
        });
    }
}
