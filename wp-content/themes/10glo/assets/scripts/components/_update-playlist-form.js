addAction(INIT, function () {
    var $updatePlaylistForm = $('#update-playlist-form');
    if (!$updatePlaylistForm.length) {
        return;
    }
    $updatePlaylistForm.submit(function (e) {
        e.preventDefault();

        var $messaging = $('.messaging');
        var $messagingSection = $messaging.parents('.messaging__section');
        var $modal = $('#update-playlist-modal');
        var playlistName = $('.update-playlist-form__playlist-name-input').val();
        var playlistVideoIds = [];
        var videoIds = [];

        $('.update-playlist-form__playlist-video-id').each(function (key, value) {
            if ($(value).prop('checked')) {
                playlistVideoIds.push($(value).val());
            }
        });

        $('.update-playlist-form__video-id').each(function (key, value) {
            if ($(value).prop('checked')) {
                videoIds.push($(value).val());
            }
        });

        $.ajax({
            method: 'POST',
            url: sit.api_url + 'user/playlist/update',
            data: {
                playlistId: $('.update-playlist-form__playlist-id').val(),
                playlistName: playlistName,
                userId: sit.current_user_id,
                videoIds: videoIds,
                playlistVideoIds: playlistVideoIds
            },
            beforeSend: function (xhr) {
                xhr.setRequestHeader('X-WP-Nonce', sit.nonce);
                xhr.setRequestHeader('Authorization', 'Basic ' + sit.basic_auth);
            },
            success: function (response) {
                doAction('hideModal', $modal);
                $messagingSection.addClass('messaging__section--show');
                $messaging.find('.messaging__content').html(response.message);

                setTimeout(function () {
                    location.href = sit.profile_url;
                }, 500);
            },
            error: function (response) {
                doAction('hideModal', $modal);
                var message = response.responseJSON.message ? response.responseJSON.message : response.message;
                $messagingSection.addClass('messaging__section--show');
                $messaging.find('.messaging__content').html(message);
            },
            fail: function (response) {
                doAction('hideModal', $modal);
                $messagingSection.addClass('messaging__section--show');
                $messaging.find('.messaging__content').html(response);
            }
        });
    });
});
