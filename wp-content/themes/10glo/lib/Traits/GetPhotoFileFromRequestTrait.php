<?php

namespace ChildTheme\Traits;

use ChildTheme\Exception\PhotoException;
use \WP_REST_Request;

/**
 * Trait GetMediaFileFromRequestTrait
 * @package ChildTheme\Traits
 * @author Vincent Ragosta <vincentpasqualeragosta@gmail.com>
 * @version 1.0
 */
trait GetPhotoFileFromRequestTrait
{
    protected function getPhotoFileFromRequest(WP_REST_Request $request, string $key = 'file')
    {
        if (empty($files = $request->get_file_params())) {
            throw new PhotoException('No files were sent in the request.', 400);
        }
        if (empty($file = $files[$key])) {
            throw new PhotoException('No files were sent in the request.', 400);
        }
        if ($file['error'] > 0) {
            throw new PhotoException('There was an error with the submission.', 400);
        }
        if ($file['size'] > (static::MB)*5) {
            throw new PhotoException('The file size was above 5MB.', 400);
        }
        if (empty($file['tmp_name'])) {
            throw new PhotoException('The file has no source location on the server', 400);
        }
        if (!getimagesize($file['tmp_name'])) {
            throw new PhotoException('The file is not an image.', 400);
        }
        if (empty($file['name'])) {
            throw new PhotoException('The file has no filename.', 400);
        }
        return $file;
    }
}
