<?php

namespace ChildTheme\Testimonial;

use Backstage\Repositories\PostRepository;

/**
 * Class TestimonialRepository
 * @package ChildTheme\Testimonial
 * @author Vincent Ragosta <vincentpasqualeragosta@gmail.com>
 * @version 1.0
 */
class TestimonialRepository extends PostRepository
{
    protected $model_class = Testimonial::class;
}
