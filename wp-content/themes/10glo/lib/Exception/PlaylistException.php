<?php

namespace ChildTheme\Exception;

use Exception;

/**
 * Class PlaylistException
 * @package ChildTheme\Exception
 * @author Vincent Ragosta <vincentpasqualeragosta@gmail.com>
 * @version 1.0
 */
class PlaylistException extends Exception {}
