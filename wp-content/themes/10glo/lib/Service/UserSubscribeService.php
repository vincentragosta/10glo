<?php

namespace ChildTheme\Service;

use ChildTheme\User\User;

/**
 * Class UserSubscribeService
 * @package ChildTheme\Service
 * @author Vincent Ragosta <vincentpasqualeragosta@gmail.com>
 * @version 1.0
 */
class UserSubscribeService
{
    const USER_FOLLOWERS_META = 'user_followers';
    const USER_FOLLOWING_META = 'user_following';

    public function subscribe(User $SubscribingUser, User $User)
    {
        if ($this->isUserFollowing($SubscribingUser, $User)) {
            return false;
        }
        if (empty($followers = $this->getFollowers($User))) {
            $followers = [$SubscribingUser->ID];
        } else if (!empty($followers) && !in_array($SubscribingUser->ID, $followers)) {
            $followers[] = $SubscribingUser->ID;
        }
        if (!$User->updateField(static::USER_FOLLOWERS_META, json_encode($followers))) {
            return false;
        }
        if (empty($following = $this->getFollowing($SubscribingUser))) {
            $following = [$User->ID];
        } else if (!empty($following) && !in_array($User->ID, $following)) {
            $following[] = $User->ID;
        }
        return $SubscribingUser->updateField(static::USER_FOLLOWING_META, json_encode($following));
    }

    public function getFollowers(User $User)
    {
        return ($followers = $User->getField(static::USER_FOLLOWERS_META)) ? json_decode($followers) : [];
    }

    public function getFollowing(User $User)
    {
        return ($following = $User->getField(static::USER_FOLLOWING_META)) ? json_decode($following) : [];
    }

    public function getFollowersCount(User $User)
    {
        return count($this->getFollowers($User));
    }

    public function isUserFollowing($User, User $UserToCheckAgainst)
    {
        if ((new UserService())->isSameUser($User, $UserToCheckAgainst)) {
            return true;
        }
        return in_array($User->ID, $this->getFollowers($UserToCheckAgainst));
    }
}
