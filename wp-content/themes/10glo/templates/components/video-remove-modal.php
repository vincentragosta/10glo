<?php
/**
 * Expected
 * @var Video $Video
 */

use ChildTheme\Video\Video;
?>

<div class="video-remove-modal">
    <h2 class="video-remove-modal__heading heading heading--large">Warning</h2>
    <p>This process cannot be undone. Once a video is removed from the website it is lost forever.</p>
    <button class="video-remove-modal__button button" data-video-id="<?= $Video->ID; ?>">Remove Video</button>
</div>
