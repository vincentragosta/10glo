addAction(SetDesign.READY, function () {
    var $imageUploads = $('.image-upload');
    if (!$imageUploads.length) {
        return;
    }
    var $triggers = $('.image-upload__upload-button');

    $imageUploads.each(function (key, upload) {
        var $imageUpload = $(upload);
        var $form = $imageUpload.find('.image-upload__form');
        var $fileInputs = $imageUpload.find('.image-upload__file');

        $fileInputs.each(function (key, value) {
            $(value).change(function () {
                var $file = $(this);
                if ($file && $file[0].files.length === 0) {
                    return;
                }
                $form.trigger('submit');
            });
        });
    });

    $triggers.each(function (key, value) {
        $(value).click(function () {
            var triggerTarget = $(this).data('trigger-target');
            $(triggerTarget).click();
        });
    });
});
