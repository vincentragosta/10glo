<?php
/**
 * Expected:
 * @var array $playlists
 * @var string $video_id
 */
?>

<div class="add-to-playlist-modal">
    <h2 class="add-to-playlist-modal__heading heading heading--medium">Add Video to playlist</h2>
    <p class="add-to-playlist-modal__text">Donec ullamcorper nulla non metus auctor fringilla. Maecenas sed diam eget risus varius blandit sit amet non magna.</p>
    <form class="add-to-playlist-modal__form add-to-playlist-form" name="add-to-playlist-form" id="add-to-playlist-form" method="post">
        <select class="add-to-playlist-form__playlists" name="add-to-playlist-form__playlists">
            <option value="">-- Select Playlist --</option>
            <?php foreach($playlists as $Playlist): ?>
                <option value="<?= $Playlist->ID; ?>"><?= $Playlist->post()->post_title; ?></option>
            <?php endforeach; ?>
        </select>
        <input type="hidden" class="add-to-playlist-form__video-id" value="<?= $video_id; ?>" />
        <input type="submit" class="add-to-playlist-form__submit-button" value="Add Video" />
    </form>
</div>
