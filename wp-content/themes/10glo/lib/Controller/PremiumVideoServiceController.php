<?php

namespace ChildTheme\Controller;

use Backstage\Rewrite\QueryRewrite;
use ChildTheme\Service\PremiumVideoService;
use ChildTheme\Service\UserService;
use ChildTheme\User\User;
use ChildTheme\Video\Video;
use ChildTheme\Video\VideoRepository;

/**
 * Class PremiumVideoServiceController
 * @package ChildTheme\Controller
 * @author Vincent Ragosta <vincentpasqualeragosta@gmail.com>
 * @version 1.0
 */
class PremiumVideoServiceController
{
    public function __construct()
    {
        add_action('template_redirect', [$this, 'templateRedirect']);
        add_filter('the_title', [$this, 'removePrivatePrefix']);
        add_action('after_save_post_video', [$this, 'updateProductTitle']);
    }

    public function templateRedirect()
    {
        if (is_singular(Video::POST_TYPE)) {
            $PremiumVideoService = new PremiumVideoService();
            $Video = Video::createFromGlobal();
            $VideoAuthor = User::createFromPost($Video);
            if (
                !$Video instanceof Video ||
                is_null($CurrentUser = User::createFromCurrentUser()) && $PremiumVideoService->isVideoPrivate($Video) ||
                !(new UserService())->isSameUser($CurrentUser, $VideoAuthor) && $PremiumVideoService->isVideoPrivate($Video)
            ) {
                wp_safe_redirect(home_url());
                exit();
            }
        }
    }

    public function removePrivatePrefix($title)
    {
        $title = str_replace('Private: ', '', $title);
        return $title;
    }

    public function updateProductTitle($object_id)
    {
        if (get_post_type($object_id) == Video::POST_TYPE) {
            /** @var Video $Video */
            $Video = (new VideoRepository())->findById($object_id);
            if (($PremiumVideoService = new PremiumVideoService())->isEnabled(User::createFromPost($Video))) {
                $Product = $PremiumVideoService->getProductFromVideo($Video);
                if ($Video->title() !== $Product->name) {
                    $PremiumVideoService->updateProductTitle($Video, $Product);
                }
            }
        }
    }
}
