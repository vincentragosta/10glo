addAction(INIT, function() {
    // var $advertisementLink = $('.single-video .advertisement-card__link');
    // if ($advertisementLink.length) {
    //     var fathomCode = $advertisementLink.attr('data-fathom-code');
    //     if (fathomCode.length) {
    //         $advertisementLink.click(function () {
    //             window.fathom.trackGoal(fathomCode, 0);
    //         });
    //     }
    // }
    var $advertisementCards = $('.advertisement-card');
    if (!$advertisementCards.length) {
        return;
    }
    $.each($advertisementCards, function(index, value) {
        var $advertisementCard = $(value);
        var fathomCode = $advertisementCard.attr('data-fathom-code');
        if (fathomCode.length) {
            var $links = $advertisementCard.find('a');
            if ($links.length) {
                $.each($links, function (index, value) {
                    $(value).click(function () {
                        window.fathom.trackGoal(fathomCode, 0);
                    });
                });
            }
        }
    });
});
