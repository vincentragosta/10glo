<?php
/**
 * Template Name: View Playlist
 *
 * @var Playlist $Playlist
 * @var User $Author
 */

use ChildTheme\Components\Messaging\MessagingView;
use ChildTheme\Components\VideoPosterCard\VideoPosterCardView;
use ChildTheme\Playlist\Playlist;
use ChildTheme\User\User;
use ChildTheme\Video\VideoRepository;

$Playlist = Playlist::createFromGlobal();
$Author = User::createFromPost($Playlist);

if (!$Playlist instanceof Playlist || !$Author instanceof User) {
    return;
}
?>

<div class="content-section">
    <div class="content-section__container container">
        <div class="content-row row">
            <div class="content-column col-8">
                <div class="content-column__inner">
                    <h1 class="heading heading--large"><?= $Playlist->title(); ?></h1>
                </div>
            </div>
            <div class="content-column col-4">
                <div class="video-watch__messages messaging__section">
                    <?= new MessagingView(); ?>
                </div>
            </div>
        </div>
    </div>
</div>
<div class="content-section">
    <div class="content-section__container container">
        <div class="content-row row">
            <div class="content-column col-12">
                <div class="content-column__inner">
                    <div class="row">
                        <?php foreach((new VideoRepository())->findFromPlaylist($Playlist) as $Video): ?>
                            <div class="col-12 col-sm-4">
                                <?= new VideoPosterCardView($Video); ?>
                            </div>
                        <?php endforeach; ?>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
