<?php

namespace ChildTheme\REST;

use Backstage\Controller\RestController;
use Backstage\Util;
use Backstage\View\Link;
use ChildTheme\Exception\StripeException;
use ChildTheme\Exception\UserMonetizationException;
use ChildTheme\Exception\UserPhotoException;
use ChildTheme\Exception\VideoException;
use ChildTheme\Options\GlobalOptions;
use ChildTheme\Playlist\Playlist;
use ChildTheme\Playlist\PlaylistRepository;
use ChildTheme\Service\EmailNotificationService;
use ChildTheme\Service\LinodeS3Service;
use ChildTheme\Service\PremiumVideoService;
use ChildTheme\Service\StripeService;
use ChildTheme\Service\UserMonetizationService;
use ChildTheme\Support\CodeGenerator;
use ChildTheme\Traits\GetCurrentUserFromRequest;
use ChildTheme\Traits\GetPhotoFileFromRequestTrait;
use ChildTheme\Traits\GetVideoFileFromRequest;
use ChildTheme\User\User;
use ChildTheme\Video\Video;
use ChildTheme\Video\VideoRepository;
use FFMpeg\Coordinate\TimeCode;
use FFMpeg\FFMpeg;
use Treinetic\ImageArtist\lib\Image;
use \WP_REST_Request;

/**
 * Class VideoRestController
 * @package ChildTheme\Controller
 * @author Vincent Ragosta <vincentpasqualeragosta@gmail.com>
 * @version 1.0
 *
 * @property string $namespace
 * @property LinodeS3Service $LinodeS3Service
 */
class VideoRestController extends RestController
{
    const VIDEO_TIME_CAPTURE = 10;
    const PREVIEW_IMAGE_NAME = 'preview-image.jpg';
    const SHARER_PREVIEW_IMAGE_NAME = 'sharer-preview-image.png';
    const MB = 1048576;
    const HASHTAG_DELIMITER = ',';
    const HASHTAG_LIMIT = 5;
    const REPORT_MESSAGE = 'hello 10glo,

    %s';

    protected $namespace = 'video';
    protected $LinodeS3Service;

    use GetVideoFileFromRequest, GetCurrentUserFromRequest, GetPhotoFileFromRequestTrait;

    public function __construct()
    {
        parent::__construct();
        $this->LinodeS3Service = new LinodeS3Service();
    }

    public function registerRoutes()
    {
        $this->addCreateRoute('/create', [$this, 'createVideo']);
        $this->addCreateRoute('/update', [$this, 'updateVideo']);
        $this->addCreateRoute('/remove', [$this, 'removeVideo']);
        $this->addCreateRoute('/rate', [$this, 'videoRating']);
        $this->addCreateRoute('/views', [$this, 'videoViews']);
        $this->addCreateRoute('/report', [$this, 'videoReport']);
    }

    public function videoReport(WP_REST_Request $request)
    {
        $description = $request->get_param('description');
        if (!wp_mail('report@10glo.com', 'Video Reported', sprintf(static::REPORT_MESSAGE, $description))) {
            throw new VideoException('Something went wrong', 400);
        }
        return ['message' => 'Video reported.', 'status' => '200'];
    }

    public function createVideo(WP_Rest_Request $request)
    {
        $video_file = $this->getVideoFileFromRequest($request, 'videoFile');
        $video_title = $this->getVideoTitleFromRequest($request);
        $CurrentUser = $this->getCurrentUserFromRequest($request, 'Please login or create an account to create a video.');
        $category_id = $this->getCategoryIdFromRequest($request);

        if ($category_id == ($BWAYWORLDRECORD_category = GlobalOptions::bwayworldrecordCategory())->term_id && !$request->get_param('bwayworldrecord_release')) {
            throw new VideoException('To submit a video to the #BWAYWORLDRECORD you must read and accept the release terms.', 400);
        }

        // handling this discretely over ternary for clarity
        if ($category_id == $BWAYWORLDRECORD_category->term_id && $request->get_param('bwayworldrecord_release')) {
            $video_filename = LinodeS3Service::getBWAYWORLDRECORDVideoArchitecture($CurrentUser->ID, $video_file['name']);
            $preview_filename = LinodeS3Service::getBWAYWORLDRECORDVideoArchitecture($CurrentUser->ID, static::PREVIEW_IMAGE_NAME, $video_file['name']);
            $sharer_preview_filename = LinodeS3Service::getBWAYWORLDRECORDVideoArchitecture($CurrentUser->ID, static::SHARER_PREVIEW_IMAGE_NAME, $video_file['name']);
        } else {
            $video_filename = LinodeS3Service::getVideoArchitecture($CurrentUser->ID, $video_file['name']);
            $preview_filename = LinodeS3Service::getVideoArchitecture($CurrentUser->ID, static::PREVIEW_IMAGE_NAME, $video_file['name']);
            $sharer_preview_filename = LinodeS3Service::getVideoArchitecture($CurrentUser->ID, static::SHARER_PREVIEW_IMAGE_NAME, $video_file['name']);
        }

        $this->LinodeS3Service->putFile($video_filename, $video_file['tmp_name']);
        if (!$video_object_url = $this->LinodeS3Service->getObjectUrl($video_filename)) {
            throw new VideoException('There was a problem uploading your video.', 400);
        }

        // capture poster image at 10s of video mark, save to poster-image.jpg
        $FFMpegVideo = (FFMpeg::create())->open($video_file['tmp_name']);
        $Frame = $FFMpegVideo->frame(TimeCode::fromSeconds(static::VIDEO_TIME_CAPTURE));
        $Frame->save(static::PREVIEW_IMAGE_NAME);
        $Frame->save(static::SHARER_PREVIEW_IMAGE_NAME);

        $logo = new Image(Util::getAssetPath('images/logo.png'));
        $playButton = new Image(Util::getAssetPath('images/play-button.png'));
        $overlay = new Image(static::SHARER_PREVIEW_IMAGE_NAME);
        $overlay->resize(720, 405);
        $overlay->merge($logo, 12, 12);
        $overlay->merge($playButton, ($overlay->getWidth() / 2 - $playButton->getWidth() / 2), ($overlay->getHeight() / 2 - $playButton->getHeight() / 2));
        $overlay->save(static::SHARER_PREVIEW_IMAGE_NAME, IMAGETYPE_PNG);

        $this->LinodeS3Service->putFile($preview_filename, static::PREVIEW_IMAGE_NAME);
        if (!$preview_object_url = $this->LinodeS3Service->getObjectUrl($preview_filename)) {
            throw new UserPhotoException('We could not store your preview image at this time.', 400);
        }

        $this->LinodeS3Service->putFile($sharer_preview_filename, static::SHARER_PREVIEW_IMAGE_NAME);
        if (!$sharer_object_url = $this->LinodeS3Service->getObjectUrl($sharer_preview_filename)) {
            throw new UserPhotoException('We could not store your preview image at this time.', 400);
        }

        // unlink preview image and sharer preview image files
        unlink(static::PREVIEW_IMAGE_NAME);
        unlink(static::SHARER_PREVIEW_IMAGE_NAME);

        $Video = new Video();
        $Video->post_title = $video_title;
        $Video->post_name = CodeGenerator::generate();
        $Video->post_status = 'publish';

        if (!empty($description = $request->get_param('description'))) {
            $Video->post_content = $description;
        }

        // TODO: Break down into Service classes
        $Video->video_filename = $video_filename;
        $Video->video_url = $video_object_url;
        $Video->poster_filename = $preview_filename;
        $Video->poster_image_url = $preview_object_url;
        $Video->sharer_filename = $sharer_preview_filename;
        $Video->sharer_image_url = $sharer_object_url;
        $Video->category = $category_id;
        $Video->comment_status = 'open';
        $Video->views = 0;
        $Video->likes = json_encode([]);
        $Video->likes_count = 0;
        $Video->video_type = $video_file['type'] === 'video/quicktime' ? 'video/mp4' : $video_file['type'];

        if (!empty($playlist_id = $request->get_param('playlistId'))) {
            $Video->playlist_ids = [$playlist_id];
        }

        if (!(new VideoRepository())->add($Video)) {
            throw new VideoException('The video post could not be created.', 400);
        }

        if (!empty($one_time_fee = $request->get_param('oneTimeFee'))) {
            if (empty($videoPrice = $request->get_param('videoPrice'))) {
                throw new VideoException('You have selected one-time fee but did not supply a price.', 400);
            }
            ($PremiumVideoService = new PremiumVideoService())->setVideoAsPrivate($Video);
            $PremiumVideoService->enable($Video);
            $Product = ($StripeService = new StripeService())->createProduct($video_title, ['metadata' => [
                'video_id' => $Video->ID
            ]]);
            if (!$StripeService->isProductValid($Product)) {
                throw new StripeException('There was an issue creating the product in Stripe.', 400);
            }
            $Price = $StripeService->createPrice([
                'product' => $Product->id,
                'unit_amount' => $videoPrice,
                'currency' => 'usd',
                'metadata' => ['video_id' => $Video->ID]
            ]);
            if (!$StripeService->isPriceValid($Price)) {
                throw new StripeException('There was an issue creating the price in Stripe.', 400);
            }
            $PremiumVideoService->setProductOnVideo($Product, $Video);
            $PremiumVideoService->setPriceOnVideo($Price, $Video);
            $PremiumVideoService->setVideoAsPublish($Video);
        }

        if (!empty($monthly_members = $request->get_param('monthlyMembers')) && ($UserMonetizationService = new UserMonetizationService())->isVerified($CurrentUser)) {
            (new PremiumVideoService())->markVideoForMonthlyMembers($Video);
        }

        if (!empty($playlist_id) && ($Playlist = new Playlist($playlist_id)) instanceof Playlist) {
            $video_ids = $Playlist->video_ids ? (array)$Playlist->video_ids : [];
            $video_ids[] = $Video->ID;
            $Playlist->video_ids = $video_ids;
            if (!(new PlaylistRepository())->add($Playlist)) {
                throw new VideoException('The video could not be added to the playlist.', 400);
            }
        }

        if (!empty($hashtags = $this->getHashtagsFromRequest($request))) {
            if (!wp_set_object_terms($Video->ID, $hashtags, 'hashtags')) {
                throw new VideoException('The hashtags could not be applied to the video.', 400);
            }
        }

        try {
            (new EmailNotificationService($CurrentUser->user_email, 'Video Successfully Uploaded!', 'Congrats! Your video, <a href="' . $Video->permalink() . '">' . $Video->title() . '</a> is now live. The hard work is done and you\'re at center stage on 10glo for all to see. <br /><br /><strong>Now it\'s time to share the link on your social channels</strong> for your friends, family, and fans to watch. And don\'t forget, <strong>the more love your video gets the more likely it is to be featured on our 10glo Trending page</strong>, weekly email, and social channels. <br /><br />Thanks for sharing and don\'t forget to take a bow.<br /><br />- Team 10glo'))->send();
        } catch (\Exception $e) {
            error_log($e->getMessage());
        }
        if (!empty($membership_ids = (new UserMonetizationService())->getSubscribedMembershipsFromVendor($Author = User::createFromPost($Video)))) {
            foreach ($membership_ids as $member_id) {
                $Member = User::createFromUserId($member_id);
                try {
                    (new EmailNotificationService(
                        $Member->user_email,
                        sprintf('%s has released a new Video!', $Author->getDisplayName()),
                        sprintf('Hi there - we wanted you to know that %s has uploaded a new video to their channel. As a paying member of their channel you\'re now one of the first to know.<br />You can check it out here: %s', $Author->getDisplayName(), Link::createFromPostPermalink($Video))
                    ))->send();
                } catch (\Exception $e) {
                    error_log($e->getMessage());
                }
            }
        }

        return ['message' => 'The video has successfully been uploaded.', 'status' => 200];
    }

    public function updateVideo(WP_Rest_Request $request)
    {
        $Video = $this->getVideoFromRequest($request);
        $CurrentUser = $this->getCurrentUserFromRequest($request, 'Please login or create an account to update this video.');
        $video_title = $request->get_param('videoTitle');
        $category_id = $request->get_param('categoryId');
        $description = $request->get_param('description');

        if (!(new User($Video->author()))->isSameAs($CurrentUser)) {
            throw new VideoException('You cannot edit another author\'s video.', 400);
        }

        if (empty($video_title) && empty($category_id)) {
            throw new VideoException('No new data entered in the request.', 400);
        }

        if (!empty($video_title)) {
            $Video->post_title = $video_title;
            if (!empty($stripe_product_id = ($PremiumVideoService = new PremiumVideoService())->getProductIdFromVideo($Video))) {
                $Product = $PremiumVideoService->getProductFromVideo($Video);
                if ($Video->title() !== $Product->name) {
                    $PremiumVideoService->updateProductTitle($Video, $Product);
                }
            }
        }

        $Video->post_content = $description;

        if (!empty($category_id)) {
            $Video->category = (int)$category_id;
        }

        $files = $request->get_file_params();
        if (!empty($files) && !empty($preview_image = $files['previewImage'])) {
            if ($preview_image['error'] > 0) {
                throw new VideoException('There was an error with the submission.', 400);
            }
            if ($preview_image['size'] > (static::MB) * 5) {
                throw new VideoException('The preview image size was above 5MB.', 400);
            }
            if (empty($preview_image['tmp_name'])) {
                throw new VideoException('The preview image has no source location on the server', 400);
            }
            if (!getimagesize($preview_image['tmp_name'])) {
                throw new VideoException('The preview image is not an image.', 400);
            }
            if (empty($preview_image['name'])) {
                throw new VideoException('The preview image has no filename.', 400);
            }

            $this->LinodeS3Service->putFile($preview_filename = $Video->poster_filename, $preview_image['tmp_name']);
            if (!$preview_object_url = $this->LinodeS3Service->getObjectUrl($preview_filename)) {
                throw new VideoException('We could not store your preview image at this time.', 400);
            }

            $logo = new Image(Util::getAssetPath('images/logo.png'));
            $playButton = new Image(Util::getAssetPath('images/play-button.png'));
            $overlay = new Image($preview_image['tmp_name']);
            $overlay->merge($logo, 12, 12);
            $overlay->merge($playButton, ($overlay->getWidth() / 2 - $playButton->getWidth() / 2), ($overlay->getHeight() / 2 - $playButton->getHeight() / 2));
            $overlay->resize(720, 405);
            $overlay->save($preview_image['tmp_name'], IMAGETYPE_PNG);

            $this->LinodeS3Service->putFile($sharer_preview_filename = $Video->sharer_filename, $preview_image['tmp_name']);
            if (!$sharer_object_url = $this->LinodeS3Service->getObjectUrl($sharer_preview_filename)) {
                throw new VideoException('We could not store your preview image at this time.', 400);
            }
        }

        if (!(new VideoRepository())->add($Video)) {
            throw new VideoException('The video post could not be updated.', 400);
        }

        if (!empty($hashtags = $this->getHashtagsFromRequest($request)) && !wp_set_object_terms($Video->ID, $hashtags, 'hashtags')) {
            throw new VideoException('The hashtags could not be applied to the video.', 400);
        }

        if (($UserMonetizationService = new UserMonetizationService())->isVerified($CurrentUser) && (($one_time_fee = $request->get_param('oneTimeFee')) != "false") && empty($stripe_product_id = ($PremiumVideoService = new PremiumVideoService())->getProductIdFromVideo($Video))) {
            if (empty($videoPrice = $request->get_param('videoPrice'))) {
                throw new VideoException('You have selected one-time fee but did not supply a price.', 400);
            }
            $PremiumVideoService->setVideoAsPrivate($Video);
            $PremiumVideoService->enable($Video);
            $Product = ($StripeService = new StripeService())->createProduct($video_title, ['metadata' => [
                'video_id' => $Video->ID
            ]]);
            if (!$StripeService->isProductValid($Product)) {
                throw new StripeException('There was an issue creating the product in Stripe.', 400);
            }
            $Price = $StripeService->createPrice([
                'product' => $Product->id,
                'unit_amount' => $videoPrice,
                'currency' => 'usd',
                'metadata' => ['video_id' => $Video->ID]
            ]);
            if (!$StripeService->isPriceValid($Price)) {
                throw new StripeException('There was an issue creating the price in Stripe.', 400);
            }
            $PremiumVideoService->setProductOnVideo($Product, $Video);
            $PremiumVideoService->setPriceOnVideo($Price, $Video);
            $PremiumVideoService->setVideoAsPublish($Video);
        } elseif ($UserMonetizationService->isVerified($CurrentUser) && empty($one_time_fee)) {
            (new PremiumVideoService())->disable($Video);
        }

        if ($UserMonetizationService->isVerified($CurrentUser) && (($monthly_members = $request->get_param('monthlyMembers')) != "false") && !($PremiumVideoService = new PremiumVideoService())->isVideoMarkedForMonthlyMembers($Video)) {
            $PremiumVideoService->markVideoForMonthlyMembers($Video);
        } elseif ($UserMonetizationService->isVerified($CurrentUser) && $request->get_param('monthlyMembers') == "false") {
            (new PremiumVideoService())->unmarkVideoForMonthlyMembers($Video);
        }

        return ['message' => 'The video has successfully been updated.', 'status' => 200];
    }

    public function removeVideo(WP_REST_Request $request)
    {
        $Video = $this->getVideoFromRequest($request);
        $this->LinodeS3Service->deleteFile($Video->video_filename);
        $this->LinodeS3Service->deleteFile($Video->poster_filename);
        $this->LinodeS3Service->deleteFile($Video->sharer_filename);

        $PlaylistRepository = new PlaylistRepository();
        if (!empty($playlists = $PlaylistRepository->findFromVideo($Video))) {
            foreach ($playlists as $Playlist) {
                $video_ids = $Playlist->video_ids;
                if (!empty($video_ids) && false !== ($position = array_search($Video->ID, $video_ids))) {
                    unset($video_ids[$position]);
                    $Playlist->video_ids = $video_ids;
                    (new PlaylistRepository())->add($Playlist);
                }
            }
        }

        if (($PremiumVideoService = new PremiumVideoService())->isPremiumVideo($Video)) {
            $PremiumVideoService->disable($Video);
        }

        if (!wp_delete_post($Video->ID)) {
            throw new VideoException('We could not delete the video', 400);
        }
        return ['message' => 'The video has been removed from the website.', 'status' => 200];
    }

    public function videoRating(WP_REST_Request $request)
    {
        $Video = $this->getVideoFromRequest($request);
        $CurrentUser = $this->getCurrentUserFromRequest($request, 'Please login or create an account to rate a video.');
        $Author = new User($Video->author());

        if ($CurrentUser->isSameAs($Author)) {
            throw new VideoException('You cannot rate your own video.', 400);
        }

        $likes = $Video->likes;
        if (in_array($CurrentUser->ID, $likes)) {
            throw new VideoException('You have already rated this video', 400);
        }
        if (empty($likes)) {
            $likes = [$CurrentUser->ID];
        } else {
            $likes[] = $CurrentUser->ID;
        }
        $Video->likes = json_encode($likes);
        $Video->likes_count = $Video->likes_count + 1;

        if (!(new VideoRepository())->add($Video)) {
            throw new VideoException('Something went wrong when updating the video.', 400);
        }

        (new EmailNotificationService($Author->user_email, 'Someone liked your video!', 'Cue the (virtual) applause. <a href="' . $Video->permalink() . '">' . $Video->title() . '</a> just received a new like on 10glo. <br />Keep up the good work!<br />- 10glo'))->send();

        return ['message' => 'Video liked.', 'status' => 200];
    }

    public function videoViews(WP_REST_Request $request)
    {
        $Video = $this->getVideoFromRequest($request);
        $Video->views = $Video->views + 1;
        if (!(new VideoRepository())->add($Video)) {
            throw new VideoException('Something went wrong when updating the video.', 400);
        }
        return ['message' => 'Video viewed.', 'status' => 200];
    }

    protected function getVideoTitleFromRequest(WP_REST_Request $request)
    {
        if (empty($title = $request->get_param('videoTitle'))) {
            throw new VideoException('There was no title sent in the request.', 400);
        }
        return $title;
    }

    protected function getCategoryIdFromRequest(WP_REST_Request $request)
    {
        if (empty($category_id = $request->get_param('categoryId'))) {
            throw new VideoException('Make sure your video doesn\'t get lost – please select a category before uploading.', 400);
        }
        return (int)$category_id;
    }

    protected function getHashtagsFromRequest(WP_REST_Request $request)
    {
        if (empty($hashtags = $request->get_param('hashtags'))) {
            return [];
        }
        if (strpos($hashtags, static::HASHTAG_DELIMITER) !== false) {
            $hashtags = explode(static::HASHTAG_DELIMITER, $hashtags);
        }
        if (!is_array($hashtags)) {
            $hashtags = [$hashtags];
        } elseif (count($hashtags) > static::HASHTAG_LIMIT) {
            throw new VideoException('There were too many hashtags supplied in the request.', 400);
        }
        return array_map(function ($hashtag) {
            $hashtag = str_replace(' ', '', $hashtag);
            if ($hashtag[0] !== '#') {
                $hashtag = '#' . $hashtag;
            }
            return $hashtag;
        }, $hashtags);

    }

    protected function getVideoFromRequest(WP_REST_Request $request)
    {
        if (empty($video_id = $request->get_param('videoId'))) {
            throw new VideoException('There was no video id sent in the request', 400);
        }
        return new Video($video_id);
    }
}
