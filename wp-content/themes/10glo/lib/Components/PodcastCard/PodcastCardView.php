<?php

namespace ChildTheme\Components\PodcastCard;

use Backstage\View\Component;
use Backstage\View\Element;
use ChildTheme\Podcast\Podcast;

/**
 * Class PodcastCardView
 * @package ChildTheme\Components\PodcastCard
 * @author Vincent Ragosta <vincentpasqualeragosta@gmail.com>
 * @version 1.0
 *
 * @property string $title
 * @property string $content
 * @property string $url
 */
class PodcastCardView extends Component
{
    protected $name = 'podcast-card';
    protected static $default_properties = [
        'title' => '',
        'content' => '',
        'url' => ''
    ];

    public function __construct(Podcast $Podcast)
    {
        parent::__construct([
            'title' => $Podcast->title(),
            'content' => $Podcast->excerpt(),
            'url' => $Podcast->permalink()
        ]);
    }
}
