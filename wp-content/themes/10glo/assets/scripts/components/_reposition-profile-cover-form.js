// addAction(INIT, function () {
//     function dragMoveListener(event) {
//         var target = event.target;
//         // var x = (parseFloat(target.getAttribute('data-x')) || 0) + event.dx;
//         var y = (parseFloat(target.getAttribute('data-y')) || 0) + event.dy;
//
//         target.style.webkitTransform =
//             target.style.transform =
//                 'translate(0, ' + (y/10) + '%)';
//
//         // target.setAttribute('data-x', 0);
//         target.setAttribute('data-y', y);
//
//         if (SetDesign.viewport.width() >= 576) {
//             target.setAttribute('data-desktop-coordinate', y/10);
//         } else if (SetDesign.viewport.width() <= 575) {
//             target.setAttribute('data-mobile-coordinate', y/10);
//         }
//     }
//
//     interact('.draggable').draggable({
//         inertia: true,
//         modifiers: [
//             interact.modifiers.restrictRect({
//                 restriction: 'parent',
//                 endOnly: false
//             })
//         ],
//         autoScroll: true,
//         listeners: {
//             move: dragMoveListener
//         },
//         cursorChecker: function() {
//             return 'ns-resize';
//         }
//     });
//
//     var $repositionButton = $('.image-upload__reposition-button');
//     if (!$repositionButton.length) {
//         return;
//     }
//     var $repositionUpdateContainer = $('.profile-cover-photo__reposition-update-container');
//     var $repositionIcon = $('.profile-cover-photo__reposition-icon');
//     var $editButton = $('.image-upload__edit-button');
//     var $profileCoverPhoto = $('.profile-cover-photo__image');
//
//     $repositionButton.click(function () {
//         $profileCoverPhoto.addClass('draggable');
//         $repositionIcon.addClass('profile-cover-photo__reposition-icon--show');
//         $editButton.addClass('profile-cover-photo__edit-button--hide');
//         $repositionUpdateContainer.addClass('profile-cover-photo__reposition-update-container--show');
//         $(this).parents('.dropdown-list').removeClass('dropdown-list--show');
//         setTimeout(function () {
//             $repositionIcon.removeClass('profile-cover-photo__reposition-icon--show');
//         }, 2500);
//     });
//
//     var $repositionUpdateButton = $('.profile-cover-photo__reposition-update-button');
//     addAction(SetDesign.LAYOUT, function() {
//         var mobileCoordinate = $profileCoverPhoto.attr('data-mobile-coordinate');
//         var desktopCoordinate = $profileCoverPhoto.attr('data-desktop-coordinate');
//         var width = SetDesign.viewport.width();
//
//         $repositionUpdateButton.toggleClass('profile-cover-photo__reposition-update-button--mobile-display', width <= 575);
//         if (!$profileCoverPhoto.hasClass('draggable') && width <= 575) {
//             if (mobileCoordinate) {
//                 $profileCoverPhoto.attr('style', 'transform: translate(0,' + mobileCoordinate + '%)');
//             } else {
//                 $profileCoverPhoto.removeAttr('style');
//             }
//         } else if (!$profileCoverPhoto.hasClass('draggable') && desktopCoordinate && width >= 576) {
//             $profileCoverPhoto.attr('style', 'transform: translate(0,' + desktopCoordinate + '%)');
//         }
//     });
//
//     $repositionUpdateButton.click(function () {
//         var $messaging = $('.messaging');
//         var $messagingSection = $messaging.parents('.messaging__section');
//
//         $.ajax({
//             method: 'POST',
//             url: sit.api_url + 'user/profile-cover-photo/reposition',
//             data: {
//                 userId: sit.current_user_id,
//                 coordinate: ($profileCoverPhoto.attr('data-y')/10),
//                 isMobileDisplay: $(this).hasClass('profile-cover-photo__reposition-update-button--mobile-display') ? true : 0
//             },
//             beforeSend: function (xhr) {
//                 xhr.setRequestHeader('X-WP-Nonce', sit.nonce);
//                 xhr.setRequestHeader('Authorization', 'Basic ' + sit.basic_auth);
//             },
//             success: function (response) {
//                 $messagingSection.addClass('messaging__section--show');
//                 $messaging.find('.messaging__content').html(response.message);
//
//                 $profileCoverPhoto.removeClass('draggable');
//                 $repositionIcon.removeClass('profile-cover-photo__reposition-icon--show');
//                 $editButton.removeClass('profile-cover-photo__edit-button--hide');
//                 $repositionUpdateContainer.removeClass('profile-cover-photo__reposition-update-container--show');
//
//                 // setTimeout(function () {
//                 //     location.href = sit.profile_url;
//                 // }, 500);
//             },
//             error: function (response) {
//                 $messagingSection.addClass('messaging__section--show');
//                 $messaging.find('.messaging__content').html(response.data);
//             },
//             fail: function (response) {
//                 $messagingSection.addClass('messaging__section--show');
//                 $messaging.find('.messaging__content').html(response.data);
//             }
//         });
//     });
// });
