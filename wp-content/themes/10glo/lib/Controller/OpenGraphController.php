<?php

namespace ChildTheme\Controller;

use ChildTheme\News\NewsPost;
use ChildTheme\SocialSharer\EmbedSocialSharer;
use ChildTheme\User\User;
use ChildTheme\Video\Video;

/**
 * Class OpenGraphController
 * @package ChildTheme\Controller
 * @author Vincent Ragosta <vincentpasqualeragosta@gmail.com>
 * @version 1.0
 */
class OpenGraphController
{
    public function __construct()
    {
        add_action('wp_head', [$this, 'additionalMetaTags'], 1);
        add_filter('wpseo_opengraph_image', [$this, 'hideImageOnVideoSingle'], 10, 1);
    }

    public function hideImageOnVideoSingle($img)
    {
        /** @var \WP_Query $wp_query */
        global $wp_query;
        return !$wp_query->is_singular(Video::POST_TYPE) ? $img : '';
    }

    public function additionalMetaTags()
    {
        if (is_singular(Video::POST_TYPE)) {
            $Video = Video::createFromGlobal();
            if (!$Video instanceof Video) {
                return '';
            }
            echo '<meta property="og:image" content="' . $Video->sharer_image_url . '" />';
            echo '<meta property="og:image:secure_url" content="' . $Video->sharer_image_url . '" />';
            echo '<meta property="og:description" content="Check out this video on 10glo.com!" />';
            echo '<meta property="twitter:description" content="Check out this video on 10glo.com!" />';
            echo '<meta property="twitter:card" content="player" />';
            echo '<meta property="twitter:player" content="' . home_url("/embed/" . $Video->post_name) . '" />';
            echo '<meta property="twitter:player:width" content="560" />';
            echo '<meta property="twitter:player:height" content="315" />';
            echo '<meta property="twitter:title" content="' . $Video->title() . '" />';
        }
        if (is_singular(NewsPost::POST_TYPE)) {
            $NewsPost = NewsPost::createFromGlobal();
            if (!$NewsPost instanceof NewsPost) {
                return '';
            }
            echo '<meta property="twitter:card" content="summary_large_image" />';
            echo '<meta property="twitter:creator" content="' . User::createFromPost($NewsPost)->getDisplayName() . '" />';
        }
        echo '<meta property="twitter:site" content="@10gloTV" />';
        echo '<meta property="og:image:width" content="831" />';
        echo '<meta property="og:image:height" content="414" />';
        echo '<meta property="fb:app_id" content="' . FB_APP_ID . '" />';
        return '';
    }
}
