addAction(INIT, function () {
    var TOGGLE_CLASS = 'dropdown-list--show';
    var $dropdownLists = $('.dropdown-list');

    if (!$dropdownLists.length) {
        return;
    }

    $dropdownLists.each(function (key, list) {
        var $list = $(list);
        var ACTIVATOR_CLASS = $list.data('dropdown-activator');
        var $activator = $(ACTIVATOR_CLASS);
        $activator.on('click', function (e) {
            e.preventDefault();
            $list.toggleClass(TOGGLE_CLASS, !$list.hasClass(TOGGLE_CLASS));
        });

        $(document).click(function (event) {
            var $target = $(event.target);
            if (!$target.closest('.dropdown-list').length && !$target.closest(ACTIVATOR_CLASS).length && $list.hasClass(TOGGLE_CLASS)) {
                $list.removeClass(TOGGLE_CLASS);
            }
        });
    });
});
