<?php

namespace ChildTheme\Exception;

use Exception;

/**
 * Class ProfileCoverPhotoException
 * @package ChildTheme\Exception
 * @author Vincent Ragosta <vincentpasqualeragosta@gmail.com>
 * @version 1.0
 */
class ProfileCoverPhotoException extends Exception {}
