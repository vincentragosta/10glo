<?php

namespace ChildTheme\Service;

use ChildTheme\User\User;
use Stripe\Account;
use Stripe\Customer;
use Stripe\Price;
use Stripe\Product;

/**
 * Class UserMonetizationService
 * @package ChildTheme\Service
 * @author Vincent Ragosta <vincentpasqualeragosta@gmail.com>
 * @version 1.0
 */
class UserMonetizationService
{
    const USER_STRIPE_ACCOUNT_ID = 'user_stripe_account_id';
    const USER_PROUDUCT_ID_META = 'user_product_id';
    const USER_PRICE_ID_META = 'user_price_%s_id';
    const USER_MONETIZATION_META = 'user_full_monetization';
    const CONFIRM_MATERIAL_OWNERSHIP_META = 'user_monetization_confirm_material_ownership';
    const CONFIRM_MONETIZATION_POLICIES_META = 'user_monetization_confirm_policies';
    const USER_SUBSCRIBED_MEMBERSHIP_IDS_META = 'user_subscribed_membership_ids';
    const MONETIZE_LINK = 'user/%s/monetize';
    const PAYMENT_SETTINGS_LINK = 'user/%s/payment-settings';
    const USER_SUPPORT_MESSAGE_META = 'user_support_message';
    const DEFAULT_STRIPE_ACCOUNT_TYPE = 'standard';
    const DEFAULT_SUBSCRIPTION_AMOUNTS = [500, 1000, 1500, 2500];
    const USER_APPLICATION_FEE = .3;
    const DEFAULT_CURRENCY = 'usd';
    const DEFAULT_ACCOUNT_LINK_TYPE = 'account_onboarding';

    public function isEnabled(User $User)
    {
        return $User->getField(static::USER_MONETIZATION_META, true);
    }

    public function isVerified(User $User)
    {
        if (empty($this->isEnabled($User))) {
            return false;
        }
        return !empty($this->isMaterialOwnershipConfirmed($User)) && !empty($this->areMonetizationPoliciesConfirmed($User));
    }

    public function isMaterialOwnershipConfirmed(User $User)
    {
        return $User->getField(static::CONFIRM_MATERIAL_OWNERSHIP_META, true);
    }

    public function confirmUserMaterialOwnership(User $User)
    {
        return $User->updateField(static::CONFIRM_MATERIAL_OWNERSHIP_META, true);
    }

    public function areMonetizationPoliciesConfirmed(User $User)
    {
        return $User->getField(static::CONFIRM_MONETIZATION_POLICIES_META, true);
    }

    public function confirmMonetizationPolicies(User $User)
    {
        return $User->updateField(static::CONFIRM_MONETIZATION_POLICIES_META, true);
    }

    public function getSubscribedMembershipsFromVendor(User $Vendor)
    {
        return ($membership_ids = $Vendor->getField(static::USER_SUBSCRIBED_MEMBERSHIP_IDS_META, true)) ? json_decode($membership_ids) : [];
    }

    public function addCustomerToVendorMemberships(User $Customer, User $Vendor)
    {
        if ((new UserService())->isSameUser($Customer, $Vendor)) {
            return false;
        }
        if (in_array($Customer->ID, $membership_ids = $this->getSubscribedMembershipsFromVendor($Vendor))) {
            return false;
        }
        if (empty($membership_ids)) {
            $membership_ids = [$Customer->ID];
        } else {
            $membership_ids[] = $Customer->ID;
        }
        return $Vendor->updateField(static::USER_SUBSCRIBED_MEMBERSHIP_IDS_META, json_encode($membership_ids));
    }

    public function getSupportMessage(User $User)
    {
        return $User->getField(static::USER_SUPPORT_MESSAGE_META, true);
    }

    public function updateSupportMessage(string $message, User $User)
    {
        return $User->updateField(static::USER_SUPPORT_MESSAGE_META, $message);
    }

    public function getAccountLink(Account $Account, string $refresh_url, string $return_url = '')
    {
//        $Account = $this->getAccountFromVendor($Vendor);
        if (!($StripeService = new StripeService())->isAccountValid($Account)) {
            return false;
        }
        return $StripeService->getAccountLink([
            'account' => $Account->id,
            'refresh_url' => $refresh_url,
            'return_url' => $return_url ?: $refresh_url,
            'type' => static::DEFAULT_ACCOUNT_LINK_TYPE
        ]);
    }

    public function getVendorMonetizeLink(User $Vendor)
    {
        return home_url(sprintf(static::MONETIZE_LINK, $Vendor->user_login));
    }

    public function getVendorPaymentSettingsLink(User $Vendor)
    {
        return home_url(sprintf(static::PAYMENT_SETTINGS_LINK, $Vendor->user_login));
    }

    public function calculateUserApplicationFee(string $amount)
    {
        if (empty($amount)) {
            return 0;
        }
        return $amount * static::USER_APPLICATION_FEE;
    }

    public function getProductFromVendor(User $Vendor)
    {
        if (empty($product_id = $Vendor->getField(static::USER_PROUDUCT_ID_META, true))) {
            return false;
        }
        return (new StripeService())->getProduct($product_id);
    }

    public function setProductOnVendor(Product $Product, User $Vendor)
    {
        if (!(new StripeService())->isProductValid($Product)) {
            return false;
        }
        return $Vendor->updateField(static::USER_PROUDUCT_ID_META, $Product->id);
    }

    public function getPriceFromVendor(User $Vendor, string $variant = '500')
    {
        if (empty($price_id = $Vendor->getField(sprintf(static::USER_PRICE_ID_META, $variant), true))) {
            return false;
        }
        return (new StripeService())->getPrice($price_id);
    }

    public function setPriceOnVendor(Price $Price, string $variant, User $Vendor)
    {
        if (!(new StripeService())->isPriceValid($Price)) {
            return false;
        }
        return $Vendor->updateField(sprintf(static::USER_PRICE_ID_META, $variant), $Price->id);
    }

    public function setAccountOnVendor(Account $Account, User $Vendor)
    {
        if (!(new StripeService())->isAccountValid($Account)) {
            return false;
        }
        return $Vendor->updateField(static::USER_STRIPE_ACCOUNT_ID, $Account->id);
    }

    public function getStripeIdFromVendor(User $Vendor)
    {
        return $Vendor->getField(static::USER_STRIPE_ACCOUNT_ID) ?: '';
    }

    public function createAccount(User $Vendor)
    {
        $Account = (new StripeService())->createAccount([
            'country' => $Vendor->getCountry() ?: 'US',
            'type' => static::DEFAULT_STRIPE_ACCOUNT_TYPE,
            [
                'metadata' => [
                    'user_id' => $Vendor->ID
                ]
            ]
        ]);
        if (!$this->setAccountOnVendor($Account, $Vendor)) {
            return false;
        }
        return $Account;
    }

    public function getAccountFromVendor(User $Vendor)
    {
        if (empty($account_id = $this->getStripeIdFromVendor($Vendor))) {
            $Account = $this->createAccount($Vendor);
            return $Account;
        }
        return (new StripeService())->getAccount($account_id);
    }

    public function createVendorAsProduct(User $Vendor)
    {
        $Account = $this->getAccountFromVendor($Vendor);
        if (!($StripeService = new StripeService())->isAccountValid($Account)) {
            return false;
        }
        $Product = $StripeService->createProduct($Vendor->getDisplayName(), ['metadata' => [
            'account' => $Account->id,
            'user_id' => $Vendor->ID
        ]]);
        if (!$StripeService->isProductValid($Product)) {
            return false;
        }
        $this->setProductOnVendor($Product, $Vendor);
        foreach (static::DEFAULT_SUBSCRIPTION_AMOUNTS as $amount) {
            $Price = $StripeService->createPrice([
                'product' => $Product->id,
                'unit_amount' => $amount,
                'currency' => static::DEFAULT_CURRENCY,
                'recurring' => [
                    'interval' => 'month'
                ],
                'metadata' => [
                    'account' => $Account->id,
                    'user_id' => $Vendor->ID
                ]
            ]);
            $this->setPriceOnVendor($Price, $amount, $Vendor);
        }
        return $Product;
    }

    public function createCustomer(string $token, string $name, string $email)
    {
        if (empty($token)) {
            return false;
        }
        return (new StripeService())->createCustomer([
            'source' => $token,
            'name' => $name ?: '',
            'email' => $email ?: ''
        ]);
    }

    public function tipVendor(Customer $Customer, User $Vendor, string $amount)
    {
        $Account = $this->getAccountFromVendor($Vendor);
        if (!($StripeService = new StripeService())->isAccountValid($Account)) {
            return false;
        }
        return $StripeService->createPaymentIntent([
            'payment_method_types' => ['card'],
            'customer' => $Customer->id,
            'confirm' => true,
            'amount' => $amount,
            'currency' => static::DEFAULT_CURRENCY,
            'application_fee_amount' => $this->calculateUserApplicationFee($amount),
            'transfer_data' => [
                'destination' => $Account->id,
            ]
        ]);
    }

    public function subscribeToVendor(Customer $Customer, User $Vendor, string $amount)
    {
        $SelectedPrice = $this->getPriceFromVendor($Vendor, $amount);
        $Account = $this->getAccountFromVendor($Vendor);
        if (!($StripeService = new StripeService())->isPriceValid($SelectedPrice) || !$StripeService->isAccountValid($Account)) {
            return false;
        }
        return $StripeService->createSubscription([
            'customer' => $Customer->id,
            'items' => [
                ['price' => $SelectedPrice->id],
            ],
            'expand' => ['latest_invoice.payment_intent'],
            'application_fee_percent' => static::USER_APPLICATION_FEE * 100,
            'transfer_data' => [
                'destination' => $Account->id,
            ]
        ]);
    }
}
