<?php

namespace ChildTheme\Options;

use Backstage\Models\OptionsBase;
use Backstage\Models\Page;
use Backstage\SetDesign\SocialIcons\SocialIcon;
use ChildTheme\Advertisement\Advertisement;
use \WP_Image;

/**
 * Class GlobalOptions
 * @package ChildTheme\Options
 * @author Vincent Ragosta <vincentpasqualeragosta@gmail.com
 * @version 1.0
 *
 * @method static WP_Image headerBrandImage()
 * @method static array callToAction()
 * @method static array specialLink()
 * @method static string newsletterText()
 * @method static array socialIcons()
 * @method static string registerPreface()
 * @method static string uploadVideoText()
 * @method static string createPlaylistText()
 * @method static string newsletterPageText()
 * @method static Page registrationPage()
 * @method static Page loginPage()
 * @method static Page storiesPage()
 * @method static bipocTag()
 * @method static bwayworldrecordCategory()
 * @method static headlinesCategory()
 * @method static applePodcastsUrl()
 * @method static overcastUrl()
 * @method static googlePodcastsUrl()
 * @method static libsynRssUrl()
 * @method static verifiedArtistsImage()
 * @method static Advertisement|bool dvpUpcomingVideoAdvertisement()
 * @method static string stripeClientId()
 * @method static Page monetizeYourVideosPage()
 * @method static array monetizationSteps()
 * @method static Page paymentSettingsPage()
 */
class GlobalOptions extends OptionsBase
{
    protected $default_values = [
        'header__brand_image' => null,
        'call_to_action' => [],
        'special_link' => [],
        'newsletter_text' => '',
        'social_icons' => [],
        'register_preface' => '',
        'upload_video_text' => '',
        'create_playlist_text' => '',
        'registration_page' => null,
        'login_page' => null,
        'stories_page' => null,
        'bipocTag' => null,
        'bwayworldrecord_category' => null,
        'headlines_category' => null,
        'verified_artists_image' => null,
        'verified_artists' => [],
        'stripe_client_id' => '',
        'monetize_your_videos_page' => null,
        'monetization_steps' => [],
        'payment_settings_page' => null
    ];

    public static function isMobileDevice() {
        return preg_match("/(android|avantgo|blackberry|bolt|boost|cricket|docomo|fone|hiptop|mini|mobi|palm|phone|pie|tablet|up\.browser|up\.link|webos|wos)/i", $_SERVER["HTTP_USER_AGENT"]);
    }

    protected function getHeaderBrandImage()
    {
        $brand_image = $this->get('header__brand_image');
        return WP_Image::get_by_attachment_id($brand_image);
    }

    protected function getSocialIcons()
    {
        return array_map(function($row) {
            $label = isset($row['link']['title']) ? $row['link']['title'] : ucfirst($row['icon']);
            return new SocialIcon($row['icon'], $row['link']['url'] ?: '', $label, []);
        }, $this->get('social_icons'));
    }

    protected function getRegistrationPage()
    {
        $page_id = $this->get('registration_page');
        return !empty($page_id) ? new Page($page_id) : null;
    }

    protected function getLoginPage()
    {
        $page_id = $this->get('login_page');
        return !empty($page_id) ? new Page($page_id) : null;
    }

    protected function getStoriesPage()
    {
        $page_id = $this->get('stories_page');
        return !empty($page_id) ? new Page($page_id) : null;
    }

    protected function getVerifiedArtistsImage()
    {
        $image = $this->get('verified_artists_image');
        return WP_Image::get_by_attachment_id($image);
    }

    protected function getDvpUpcomingVideoAdvertisement()
    {
        $advertisement_id = $this->get('dvp_upcoming_video_advertisement');
        return new Advertisement($advertisement_id);
    }

    protected function getMonetizeYourVideosPage()
    {
        $page_id = $this->get('monetize_your_videos_page');
        return !empty($page_id) ? new Page($page_id) : null;
    }

    public static function monetizationStepOne()
    {
        if (empty($monetization_steps = static::monetizationSteps())) {
            return [];
        }
        return $monetization_steps['step_one'];
    }

    public static function monetizationStepTwo()
    {
        if (empty($monetization_steps = static::monetizationSteps())) {
            return [];
        }
        return $monetization_steps['step_two'];
    }

    public static function monetizationStepThree()
    {
        if (empty($monetization_steps = static::monetizationSteps())) {
            return [];
        }
        return $monetization_steps['step_three'];
    }

    public static function monetizationStepFour()
    {
        if (empty($monetization_steps = static::monetizationSteps())) {
            return [];
        }
        return $monetization_steps['step_four'];
    }

    public static function monetizationStepFive()
    {
        if (empty($monetization_steps = static::monetizationSteps())) {
            return [];
        }
        return $monetization_steps['step_five'];
    }

    protected function getPaymentSettingsPage()
    {
        $page_id = $this->get('payment_settings_page');
        return !empty($page_id) ? new Page($page_id) : null;
    }
}
