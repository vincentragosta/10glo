addAction(INIT, function () {
    var $subscribeButton = $('.subscribe-to-user');
    if (!$subscribeButton.length) {
        return;
    }
    $subscribeButton.click(function () {
        var $messaging = $('.messaging');
        var $messagingSection = $messaging.parents('.messaging__section');

        $.ajax({
            method: 'POST',
            url: sit.api_url + 'user/subscribe',
            data: {
                authorId: $(this).data('user-id'),
                userId: sit.current_user_id
            },
            beforeSend: function (xhr) {
                xhr.setRequestHeader('X-WP-Nonce', sit.nonce);
                xhr.setRequestHeader('Authorization', 'Basic ' + sit.basic_auth);
            },
            success: function (response) {
                $messagingSection.addClass('messaging__section--show');
                $messaging.find('.messaging__content').html(response.message);
            },
            error: function (response) {
                var message = response.responseJSON.message ? response.responseJSON.message : response.message;
                $messagingSection.addClass('messaging__section--show');
                $messaging.find('.messaging__content').html(message);
            },
            fail: function (response) {
                $messagingSection.addClass('messaging__section--show');
                $messaging.find('.messaging__content').html(response);
            }
        });
    });
});
