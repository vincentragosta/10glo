/**
 * Global variables within theme scope
 */

(function(addAction, addFilter, applyFilters) {
    var $footer = $(".footer-nav");
    addFilter("footer-height", function() {
        return $footer.outerHeight(true);
    }, 5);

    addFilter("css-vars/register", function(styles) {
        styles["footer-height"] = function() {
            return applyFilters("footer-height") + "px";
        };
        return styles;
    });
})(SetDesign.addAction, SetDesign.addFilter, SetDesign.applyFilters);
