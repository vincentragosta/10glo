<div class="reposition-profile-cover-modal">
    <h2 class="reposition-profile-cover-modal__heading heading heading--large">Reposition The Image</h2>
    <p>Below is a very rudimentary way to reposition your image. By default the image is centered with respects to its container. Please select choose from the following four options <strong>Top</strong>, <strong>Bottom</strong>, <strong>Left</strong> or <strong>Right</strong> to reposition your image.</p>
    <form id="reposition-profile-cover-form" class="video-upload__form reposition-profile-cover-form" name="reposition-profile-cover-form" action="" method="post" enctype="multipart/form-data">
        <div class="reposition-profile-cover-form__row">
            <label for="reposition-profile-cover-form__direction">Direction</label>
            <select name="reposition-profile-cover-form__direction" class="reposition-profile-cover-form__direction" required>
                <option value="">-- Select Direction --</option>
                <option value="top">Top</option>
                <option value="bottom">Bottom</option>
                <option value="left">Left</option>
                <option value="right">Right</option>
            </select>
        </div>
        <input type="submit" class="reposition-profile-cover-form__submit" value="Reposition" />
    </form>
</div>
