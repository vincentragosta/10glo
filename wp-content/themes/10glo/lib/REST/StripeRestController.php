<?php

namespace ChildTheme\REST;

use Backstage\Controller\RestController;
use ChildTheme\Service\EmailNotificationService;
use ChildTheme\User\User;

/**
 * Class StripeRestController
 * @package ChildTheme\REST
 * @author Vincent Ragosta <vincentpasqualeragosta@gmail.com>
 * @version 1.0
 */
class StripeRestController extends RestController
{
    protected $namespace = 'stripe';

    public function registerRoutes()
    {
        $this->addCreateRoute('/hooks/invoice-created', [$this, 'sendEmailToCustomer']);
    }

    public function sendEmailToCustomer(\WP_REST_Request $request)
    {
        $Event = null;
        $Invoice = null;

        try {
            // change to StripeService method?
            $Event = \Stripe\Event::constructFrom((array)json_decode($request->get_body()));
        } catch (\UnexpectedValueException $e) {
            http_response_code(400);
            exit();
        }

        if ($Event->type == 'invoice.created') {
            $Invoice = $Event->data->object;
            $Price = $Invoice->lines->data[0]->price;
            $Vendor = User::createFromUserId($Price->metadata->user_id ?: '');
            try {
                (new EmailNotificationService(
                    $Invoice->customer_email,
                    sprintf('Congratulations on subscribing to %s\'s channel.', $Vendor->getDisplayName()),
                    sprintf('Thank you for becoming a member of %1$s\'s channel. This is a great way to show your support for %1$s and to be alerted to know content they post to 10glo.', $Vendor->getDisplayName())
                ))->send();
            } catch(\Exception $e) {
                error_log($e->getMessage());
            }
        }
        return ['message' => 'Success', 'data' => $Invoice, 'status' => '200'];
//        http_response_code(200);
    }
}
