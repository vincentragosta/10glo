addAction(INIT, function () {
    var $paymentSettingsForm = $('.payment-settings-form');
    if (!$paymentSettingsForm.length) {
        return;
    }

    $paymentSettingsForm.on('submit', function (e) {
        e.preventDefault();
        var content = tinymce.get('user-support-message').getContent().trim();
        if (!content.length) {
            return;
        }
        $.ajax({
            method: 'POST',
            url: sit.api_url + 'user/monetize/payment-settings',
            data: {
                supportMessage: content,
                userId: sit.current_user_id
            },
            beforeSend: function (xhr) {
                xhr.setRequestHeader('X-WP-Nonce', sit.nonce);
                xhr.setRequestHeader('Authorization', 'Basic ' + sit.basic_auth);
            },
            success: function (response) {
                console.log(response);
                // doAction('hideModal', $modal);
                // $messagingSection.addClass('messaging__section--show');
                // $messaging.find('.messaging__content').html("You have successfully updated your account.");
            },
            error: function (response) {
                console.log(response);
                // doAction('hideModal', $modal);
                // var message = response.responseJSON.message ? response.responseJSON.message : response.message;
                // $messagingSection.addClass('messaging__section--show');
                // $messaging.find('.messaging__content').html(message);
            },
            fail: function (response) {
                console.log(response);
                // doAction('hideModal', $modal);
                // $messagingSection.addClass('messaging__section--show');
                // $messaging.find('.messaging__content').html(response);
            }
        });
    });
});
