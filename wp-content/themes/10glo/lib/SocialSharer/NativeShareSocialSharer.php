<?php

namespace ChildTheme\SocialSharer;

use Backstage\Models\PostBase;

/**
 * Class NativeShareSocialSharer
 * @package ChildTheme\SocialSharer
 * @author Vincent Ragosta <vincentpasqualeragosta@gmail.com>
 * @version 1.0
 */
class NativeShareSocialSharer extends SocialSharer
{
    const LABEL = 'More';
    const SHARER = "#share";
    const ICON = 'link-circle';

    /**
     * @param PostBase $PostBase
     * @return string
     */
    protected function generateSharerUrl($PostBase)
    {
        return static::SHARER;
    }
}
