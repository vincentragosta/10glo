<?php

namespace ChildTheme\Components\StripeConnect;

use Backstage\View\Component;
use ChildTheme\Service\StripeService;
use ChildTheme\Service\UserMonetizationService;
use ChildTheme\User\User;

/**
 * Class StripeConnectView
 * @package ChildTheme\Components\StripeConnect
 * @author Vincent Ragosta <vincentpasqualeragosta@gmail.com>
 * @version 1.0
 *
 * @property string $url
 * @property string $label
 * @property string $icon_text
 */
class StripeConnectView extends Component
{
    const CONNECT_LABEL = 'Connect';
    const FINISH_STRIPE_SETUP_LABEL = 'Finish Stripe Setup';
    const MY_STRIPE_ACCOUNT_LABEL = 'My Stripe Account';

    const GO_TO_DASHBOARD_ICON_TEXT = 'Go to Dashboard';
    const OPEN_STRIPE_SETTINGS_ICON_TEXT = 'Open Stripe Settings';

    const DASHBOARD_URL = 'https://dashboard.stripe.com/dashboard';

    protected $name = 'stripe-connect';
    protected static $default_properties = [
        'url' => '',
        'label' => '',
        'icon_text' => ''
    ];

    public function __construct(User $Vendor)
    {
        if (empty(($UserMonetizationService = new UserMonetizationService())->getStripeIdFromVendor($Vendor))) {
            $url = $UserMonetizationService->getAccountLink($UserMonetizationService->createAccount($Vendor), $UserMonetizationService->getVendorPaymentSettingsLink($Vendor))->url;
            $label = static::CONNECT_LABEL;
            $icon_text = static::OPEN_STRIPE_SETTINGS_ICON_TEXT;
        } elseif (!(new StripeService())->isAccountActive($UserMonetizationService->getAccountFromVendor($Vendor))) {
            $url = $UserMonetizationService->getAccountLink($UserMonetizationService->createAccount($Vendor), $UserMonetizationService->getVendorPaymentSettingsLink($Vendor))->url;
            $label = static::FINISH_STRIPE_SETUP_LABEL;
            $icon_text = static::OPEN_STRIPE_SETTINGS_ICON_TEXT;
        } else {
            $url = static::DASHBOARD_URL;
            $label = static::MY_STRIPE_ACCOUNT_LABEL;
            $icon_text = static::GO_TO_DASHBOARD_ICON_TEXT;
        }
        parent::__construct(compact('url', 'label', 'icon_text'));
    }
}
