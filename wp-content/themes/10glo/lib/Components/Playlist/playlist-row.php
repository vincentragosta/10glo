<?php
/**
 * Expected
 * @var Element $Heading
 * @var Video[] $videos
 * @var string $playlist_name
 * @var bool $show_sort
 * @var bool $show_load_more
 * @var array $class_modifiers
 * @var array $element_attributes
 */

use Backstage\SetDesign\Icon\IconView;
use Backstage\Util;
use Backstage\View\Element;
use ChildTheme\Components\VideoPosterCard\VideoPosterCardView;
use ChildTheme\Video\Video;

?>

<div <?= Util::componentAttributes('playlist-row', $class_modifiers, $element_attributes); ?>>
    <?php if ($Heading instanceof Element || $show_sort): ?>
        <div class="playlist-row__row row">
            <div class="playlist-row__col col-12">
                <div class="playlist-row__heading-dropdown">
                    <?php if ($Heading instanceof Element): ?>
                        <?= $Heading; ?>
                    <?php endif; ?>
                    <?php if ($playlist_name): ?>
                        <button class="playlist__settings button--unstyled button--with-icon">
                            <?= new IconView(['icon_name' => 'ellipsis']); ?>
                            <ul class="playlist__settings-list dropdown-list"
                                data-dropdown-activator=".playlist--<?= $playlist_name; ?> .playlist__settings">
                                <li>
                                    <a href="#update-playlist-modal"><?= new IconView(['icon_name' => 'cog']); ?>
                                        Edit Playlist</a></li>
                                <li>
                                    <a href="#remove-playlist-modal"><?= new IconView(['icon_name' => 'trash-can']); ?>
                                        Remove Playlist</a></li>
                            </ul>
                        </button>
                    <?php endif; ?>
                </div>
                <?php if ($show_sort): ?>
                    <div class="playlist-row__sort">
                        <button class="button button--secondary button--with-icon"><?= isset($_GET['sort']) ? str_replace('-', ' ', $_GET['sort']) : 'Most Viewed'; ?> <?= new IconView(['icon_name' => 'controls', 'style' => 'inverted']); ?></button>
                        <ul class="header-nav__user-settings dropdown-list" data-dropdown-activator=".playlist-row__sort > button">
                            <?php if (isset($_GET['sort'])): ?>
                                <li data-value="most-viewed">Most Viewed</li>
                            <?php endif; ?>
                            <?php if ($_GET['sort'] !== 'most-recent'): ?>
                                <li data-value="most-recent">Most Recent</li>
                            <?php endif; ?>
                            <?php if ($_GET['sort'] !== 'top-rated'): ?>
                                <li data-value="top-rated">Top Rated</li>
                            <?php endif; ?>
                        </ul>
                    </div>
                <?php endif; ?>
            </div>
        </div>
    <?php endif; ?>
    <?php if (!empty($videos)): ?>
        <div class="playlist-row__row playlist-row__row--videos row">
            <?php foreach($videos as $Video): ?>
                <div class="playlist-row__column col-12 col-sm-6 col-md-4 col-lg-3">
                    <?= new VideoPosterCardView($Video); ?>
                </div>
            <?php endforeach; ?>
        </div>
        <?php if ($show_load_more): ?>
            <div class="playlist-row__row row">
                <div class="playlist-row__column col-12">
                    <button type="button" class="playlist-row__load-more button button--full-width">Load More</button>
                </div>
            </div>
        <?php endif; ?>
    <?php else: ?>
        <p>There are no videos in this playlist.</p>
    <?php endif; ?>
</div>
