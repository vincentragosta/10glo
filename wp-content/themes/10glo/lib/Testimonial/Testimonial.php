<?php

namespace ChildTheme\Testimonial;

use Backstage\Models\PostBase;

/**
 * Class Testimonial
 * @package ChildTheme\Testimonial
 * @author Vincent Ragosta <vincentpasqualeragosta@gmail.com>
 * @version 1.0
 *
 * @property array $attribution
 */
class Testimonial extends PostBase
{
    const POST_TYPE = 'testimonial';

    public function attributionName()
    {
        $attribution = $this->field('attribution');
        return $attribution['name'];
    }

    public function attributionImage()
    {
        $attribution = $this->field('attribution');
        return \WP_Image::get_by_attachment_id($attribution['image_id']);
    }

    public function attributionJobTitle()
    {
        $attribution = $this->field('attribution');
        return $attribution['job_title'];
    }
}
