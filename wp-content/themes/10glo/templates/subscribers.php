<?php

use ChildTheme\Components\UserCard\UserCardView;
use ChildTheme\Service\UserService;
use ChildTheme\Service\UserSubscribeService;
use ChildTheme\User\User;

$Author = User::createFromUserLogin(get_query_var('related_user'));
$CurrentUser = User::createFromCurrentUser();

?>

<div class="content-section">
    <div class="content-section__container container">
        <?php if (empty($author_followers = ($UserSubscribeService = new UserSubscribeService())->getFollowing($Author))): ?>
            <div class="content-row row">
                <div class="content-column col-12">
                    <div class="content-column__inner">
                        <p>You do not subscribe to any channels. Check out the <a href="<?= home_url('/#trending'); ?>">Trending Page</a> to get started!</p>
                    </div>
                </div>
            </div>
        <?php else: ?>
            <div class="content-row row">
                <div class="content-column col-12">
                    <div class="content-column__inner">
                        <h1 class="heading heading--medium">My Subscriptions</h1>
                    </div>
                </div>
            </div>
            <div class="content-row row content-row--subscribers-grid">
                <?php foreach($author_followers as $user_id): /** @var User $Subscriber */?>
                    <div class="content-column col-12 col-sm-4 col-md-3 col-lg-2 text--center content-column--mb-double">
                        <div class="content-column__inner">
                            <?= UserCardView::createFromUser($Subscriber = User::createFromUserId($user_id)); ?>
                            <button class="subscribe-to-user button button--secondary-filled" <?= (new UserSubscribeService())->isUserFollowing($CurrentUser, $Subscriber) ? 'disabled' : ''; ?>>Subscribe</button>
                        </div>
                    </div>
                <?php endforeach; ?>
            </div>
        <?php endif; ?>
    </div>
</div>
