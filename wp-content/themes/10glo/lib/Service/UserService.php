<?php

namespace ChildTheme\Service;

use ChildTheme\User\User;

/**
 * Class UserService
 * @package ChildTheme\Service
 * @author Vincent Ragosta <vincentpasqualeragosta@gmail.com>
 * @version 1.0
 */
class UserService
{
    public function isValidUser($User)
    {
        return $User instanceof User;
    }

    public function isSameUser($User, $UserToCompare)
    {
        if (!($this->isValidUser($User) || $this->isValidUser($UserToCompare))) {
            return false;
        }
        return $User->ID === $UserToCompare->ID;
    }
}
