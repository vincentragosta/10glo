<?php

$payload = @file_get_contents('php://input');
$event = null;

try {
    $event = \Stripe\Event::constructFrom(
        json_decode($payload, true)
    );
} catch (\UnexpectedValueException $e) {
// Invalid payload
    http_response_code(400);
    exit();
}

// Handle the event
switch ($event->type) {
    case 'invoice.created':
        \Stripe\Stripe::setApiKey(STRIPE_SECRET);
        $invoice = Stripe\Invoice::update($event->id, [
            'transfer_data' => ['destination' => $event->account],
            'application_fee_percent' => 30,
        ]);
        break;
    default:
        echo 'Received unknown event type ' . $event->type;
}

http_response_code(200);
